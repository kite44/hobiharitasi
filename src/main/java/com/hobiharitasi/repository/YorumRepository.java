package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Yorum;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Yorum entity.
 */
@Repository
public interface YorumRepository extends JpaRepository<Yorum, Long>, JpaSpecificationExecutor<Yorum> {
    @Query("select yorum from Yorum yorum where yorum.user.login = ?#{principal.username}")
    List<Yorum> findByUserIsCurrentUser();

    default Optional<Yorum> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Yorum> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Yorum> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct yorum from Yorum yorum left join fetch yorum.user left join fetch yorum.kullanici left join fetch yorum.etkinlik left join fetch yorum.grupEtkinligi",
        countQuery = "select count(distinct yorum) from Yorum yorum"
    )
    Page<Yorum> findAllWithToOneRelationships(Pageable pageable);

    @Query(
        "select distinct yorum from Yorum yorum left join fetch yorum.user left join fetch yorum.kullanici left join fetch yorum.etkinlik left join fetch yorum.grupEtkinligi"
    )
    List<Yorum> findAllWithToOneRelationships();

    @Query(
        "select yorum from Yorum yorum left join fetch yorum.user left join fetch yorum.kullanici left join fetch yorum.etkinlik left join fetch yorum.grupEtkinligi where yorum.id =:id"
    )
    Optional<Yorum> findOneWithToOneRelationships(@Param("id") Long id);
}
