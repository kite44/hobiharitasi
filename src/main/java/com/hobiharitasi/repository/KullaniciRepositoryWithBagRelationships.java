package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Kullanici;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface KullaniciRepositoryWithBagRelationships {
    Optional<Kullanici> fetchBagRelationships(Optional<Kullanici> kullanici);

    List<Kullanici> fetchBagRelationships(List<Kullanici> kullanicis);

    Page<Kullanici> fetchBagRelationships(Page<Kullanici> kullanicis);
}
