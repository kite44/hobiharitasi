package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Grup;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Grup entity.
 */
@Repository
public interface GrupRepository extends JpaRepository<Grup, Long>, JpaSpecificationExecutor<Grup> {
    default Optional<Grup> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Grup> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Grup> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct grup from Grup grup left join fetch grup.kategori",
        countQuery = "select count(distinct grup) from Grup grup"
    )
    Page<Grup> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct grup from Grup grup left join fetch grup.kategori")
    List<Grup> findAllWithToOneRelationships();

    @Query("select grup from Grup grup left join fetch grup.kategori where grup.id =:id")
    Optional<Grup> findOneWithToOneRelationships(@Param("id") Long id);
}
