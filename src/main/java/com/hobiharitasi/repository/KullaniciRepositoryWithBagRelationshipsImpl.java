package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Kullanici;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class KullaniciRepositoryWithBagRelationshipsImpl implements KullaniciRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Kullanici> fetchBagRelationships(Optional<Kullanici> kullanici) {
        return kullanici.map(this::fetchGrups).map(this::fetchHobis);
    }

    @Override
    public Page<Kullanici> fetchBagRelationships(Page<Kullanici> kullanicis) {
        return new PageImpl<>(fetchBagRelationships(kullanicis.getContent()), kullanicis.getPageable(), kullanicis.getTotalElements());
    }

    @Override
    public List<Kullanici> fetchBagRelationships(List<Kullanici> kullanicis) {
        return Optional.of(kullanicis).map(this::fetchGrups).map(this::fetchHobis).orElse(Collections.emptyList());
    }

    Kullanici fetchGrups(Kullanici result) {
        return entityManager
            .createQuery(
                "select kullanici from Kullanici kullanici left join fetch kullanici.grups where kullanici is :kullanici",
                Kullanici.class
            )
            .setParameter("kullanici", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Kullanici> fetchGrups(List<Kullanici> kullanicis) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, kullanicis.size()).forEach(index -> order.put(kullanicis.get(index).getId(), index));
        List<Kullanici> result = entityManager
            .createQuery(
                "select distinct kullanici from Kullanici kullanici left join fetch kullanici.grups where kullanici in :kullanicis",
                Kullanici.class
            )
            .setParameter("kullanicis", kullanicis)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }

    Kullanici fetchHobis(Kullanici result) {
        return entityManager
            .createQuery(
                "select kullanici from Kullanici kullanici left join fetch kullanici.hobis where kullanici is :kullanici",
                Kullanici.class
            )
            .setParameter("kullanici", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Kullanici> fetchHobis(List<Kullanici> kullanicis) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, kullanicis.size()).forEach(index -> order.put(kullanicis.get(index).getId(), index));
        List<Kullanici> result = entityManager
            .createQuery(
                "select distinct kullanici from Kullanici kullanici left join fetch kullanici.hobis where kullanici in :kullanicis",
                Kullanici.class
            )
            .setParameter("kullanicis", kullanicis)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
