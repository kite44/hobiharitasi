package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Tavsiye;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Tavsiye entity.
 */
@Repository
public interface TavsiyeRepository extends JpaRepository<Tavsiye, Long>, JpaSpecificationExecutor<Tavsiye> {
    default Optional<Tavsiye> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Tavsiye> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Tavsiye> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct tavsiye from Tavsiye tavsiye left join fetch tavsiye.kullanici left join fetch tavsiye.kullanici",
        countQuery = "select count(distinct tavsiye) from Tavsiye tavsiye"
    )
    Page<Tavsiye> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct tavsiye from Tavsiye tavsiye left join fetch tavsiye.kullanici left join fetch tavsiye.kullanici")
    List<Tavsiye> findAllWithToOneRelationships();

    @Query("select tavsiye from Tavsiye tavsiye left join fetch tavsiye.kullanici left join fetch tavsiye.kullanici where tavsiye.id =:id")
    Optional<Tavsiye> findOneWithToOneRelationships(@Param("id") Long id);
}
