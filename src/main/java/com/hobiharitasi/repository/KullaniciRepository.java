package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Kullanici;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Kullanici entity.
 *
 * When extending this class, extend KullaniciRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface KullaniciRepository
    extends KullaniciRepositoryWithBagRelationships, JpaRepository<Kullanici, Long>, JpaSpecificationExecutor<Kullanici> {
    @Query("select kullanici from Kullanici kullanici where kullanici.user.login = ?#{principal.username}")
    List<Kullanici> findByUserIsCurrentUser();

    default Optional<Kullanici> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findOneWithToOneRelationships(id));
    }

    default List<Kullanici> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships());
    }

    default Page<Kullanici> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships(pageable));
    }

    @Query(
        value = "select distinct kullanici from Kullanici kullanici left join fetch kullanici.user left join fetch kullanici.etkinlik",
        countQuery = "select count(distinct kullanici) from Kullanici kullanici"
    )
    Page<Kullanici> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct kullanici from Kullanici kullanici left join fetch kullanici.user left join fetch kullanici.etkinlik")
    List<Kullanici> findAllWithToOneRelationships();

    @Query(
        "select kullanici from Kullanici kullanici left join fetch kullanici.user left join fetch kullanici.etkinlik where kullanici.id =:id"
    )
    Optional<Kullanici> findOneWithToOneRelationships(@Param("id") Long id);
}
