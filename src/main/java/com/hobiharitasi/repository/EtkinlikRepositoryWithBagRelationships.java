package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Etkinlik;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface EtkinlikRepositoryWithBagRelationships {
    Optional<Etkinlik> fetchBagRelationships(Optional<Etkinlik> etkinlik);

    List<Etkinlik> fetchBagRelationships(List<Etkinlik> etkinliks);

    Page<Etkinlik> fetchBagRelationships(Page<Etkinlik> etkinliks);
}
