package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Takvim;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Takvim entity.
 */
@Repository
public interface TakvimRepository extends JpaRepository<Takvim, Long>, JpaSpecificationExecutor<Takvim> {
    default Optional<Takvim> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Takvim> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Takvim> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct takvim from Takvim takvim left join fetch takvim.kullanici left join fetch takvim.kullanici",
        countQuery = "select count(distinct takvim) from Takvim takvim"
    )
    Page<Takvim> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct takvim from Takvim takvim left join fetch takvim.kullanici left join fetch takvim.kullanici")
    List<Takvim> findAllWithToOneRelationships();

    @Query("select takvim from Takvim takvim left join fetch takvim.kullanici left join fetch takvim.kullanici where takvim.id =:id")
    Optional<Takvim> findOneWithToOneRelationships(@Param("id") Long id);
}
