package com.hobiharitasi.repository;

import com.hobiharitasi.domain.GrupEtkinligi;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the GrupEtkinligi entity.
 */
@Repository
public interface GrupEtkinligiRepository extends JpaRepository<GrupEtkinligi, Long>, JpaSpecificationExecutor<GrupEtkinligi> {
    default Optional<GrupEtkinligi> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<GrupEtkinligi> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<GrupEtkinligi> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct grupEtkinligi from GrupEtkinligi grupEtkinligi left join fetch grupEtkinligi.grup",
        countQuery = "select count(distinct grupEtkinligi) from GrupEtkinligi grupEtkinligi"
    )
    Page<GrupEtkinligi> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct grupEtkinligi from GrupEtkinligi grupEtkinligi left join fetch grupEtkinligi.grup")
    List<GrupEtkinligi> findAllWithToOneRelationships();

    @Query("select grupEtkinligi from GrupEtkinligi grupEtkinligi left join fetch grupEtkinligi.grup where grupEtkinligi.id =:id")
    Optional<GrupEtkinligi> findOneWithToOneRelationships(@Param("id") Long id);
}
