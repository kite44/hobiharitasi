package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Etkinlik;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class EtkinlikRepositoryWithBagRelationshipsImpl implements EtkinlikRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Etkinlik> fetchBagRelationships(Optional<Etkinlik> etkinlik) {
        return etkinlik.map(this::fetchKategoris);
    }

    @Override
    public Page<Etkinlik> fetchBagRelationships(Page<Etkinlik> etkinliks) {
        return new PageImpl<>(fetchBagRelationships(etkinliks.getContent()), etkinliks.getPageable(), etkinliks.getTotalElements());
    }

    @Override
    public List<Etkinlik> fetchBagRelationships(List<Etkinlik> etkinliks) {
        return Optional.of(etkinliks).map(this::fetchKategoris).orElse(Collections.emptyList());
    }

    Etkinlik fetchKategoris(Etkinlik result) {
        return entityManager
            .createQuery(
                "select etkinlik from Etkinlik etkinlik left join fetch etkinlik.kategoris where etkinlik is :etkinlik",
                Etkinlik.class
            )
            .setParameter("etkinlik", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Etkinlik> fetchKategoris(List<Etkinlik> etkinliks) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, etkinliks.size()).forEach(index -> order.put(etkinliks.get(index).getId(), index));
        List<Etkinlik> result = entityManager
            .createQuery(
                "select distinct etkinlik from Etkinlik etkinlik left join fetch etkinlik.kategoris where etkinlik in :etkinliks",
                Etkinlik.class
            )
            .setParameter("etkinliks", etkinliks)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
