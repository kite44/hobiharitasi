package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Etkinlik;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Etkinlik entity.
 *
 * When extending this class, extend EtkinlikRepositoryWithBagRelationships too.
 * For more information refer to https://github.com/jhipster/generator-jhipster/issues/17990.
 */
@Repository
public interface EtkinlikRepository
    extends EtkinlikRepositoryWithBagRelationships, JpaRepository<Etkinlik, Long>, JpaSpecificationExecutor<Etkinlik> {
    default Optional<Etkinlik> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<Etkinlik> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Etkinlik> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }
}
