package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Kategori;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Kategori entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KategoriRepository extends JpaRepository<Kategori, Long>, JpaSpecificationExecutor<Kategori> {}
