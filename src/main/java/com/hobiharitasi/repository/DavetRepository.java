package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Davet;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Davet entity.
 */
@Repository
public interface DavetRepository extends JpaRepository<Davet, Long>, JpaSpecificationExecutor<Davet> {
    default Optional<Davet> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Davet> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Davet> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct davet from Davet davet left join fetch davet.gonderen",
        countQuery = "select count(distinct davet) from Davet davet"
    )
    Page<Davet> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct davet from Davet davet left join fetch davet.gonderen")
    List<Davet> findAllWithToOneRelationships();

    @Query("select davet from Davet davet left join fetch davet.gonderen where davet.id =:id")
    Optional<Davet> findOneWithToOneRelationships(@Param("id") Long id);
}
