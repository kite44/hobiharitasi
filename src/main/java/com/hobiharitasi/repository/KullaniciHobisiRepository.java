package com.hobiharitasi.repository;

import com.hobiharitasi.domain.KullaniciHobisi;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the KullaniciHobisi entity.
 */
@Repository
public interface KullaniciHobisiRepository extends JpaRepository<KullaniciHobisi, Long>, JpaSpecificationExecutor<KullaniciHobisi> {
    default Optional<KullaniciHobisi> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<KullaniciHobisi> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<KullaniciHobisi> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct kullaniciHobisi from KullaniciHobisi kullaniciHobisi left join fetch kullaniciHobisi.hobi left join fetch kullaniciHobisi.kullanici",
        countQuery = "select count(distinct kullaniciHobisi) from KullaniciHobisi kullaniciHobisi"
    )
    Page<KullaniciHobisi> findAllWithToOneRelationships(Pageable pageable);

    @Query(
        "select distinct kullaniciHobisi from KullaniciHobisi kullaniciHobisi left join fetch kullaniciHobisi.hobi left join fetch kullaniciHobisi.kullanici"
    )
    List<KullaniciHobisi> findAllWithToOneRelationships();

    @Query(
        "select kullaniciHobisi from KullaniciHobisi kullaniciHobisi left join fetch kullaniciHobisi.hobi left join fetch kullaniciHobisi.kullanici where kullaniciHobisi.id =:id"
    )
    Optional<KullaniciHobisi> findOneWithToOneRelationships(@Param("id") Long id);
}
