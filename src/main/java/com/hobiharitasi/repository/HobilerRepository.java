package com.hobiharitasi.repository;

import com.hobiharitasi.domain.Hobiler;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Hobiler entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HobilerRepository extends JpaRepository<Hobiler, Long>, JpaSpecificationExecutor<Hobiler> {}
