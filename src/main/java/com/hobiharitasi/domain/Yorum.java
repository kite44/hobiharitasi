package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Yorum.
 */
@Entity
@Table(name = "yorum")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Yorum implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "baslik", nullable = false)
    private String baslik;

    @NotNull
    @Column(name = "icerik", nullable = false)
    private String icerik;

    @ManyToOne
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    @ManyToOne
    @JsonIgnoreProperties(value = { "kategoris" }, allowSetters = true)
    private Etkinlik etkinlik;

    @ManyToOne
    @JsonIgnoreProperties(value = { "grup" }, allowSetters = true)
    private GrupEtkinligi grupEtkinligi;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Yorum id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaslik() {
        return this.baslik;
    }

    public Yorum baslik(String baslik) {
        this.setBaslik(baslik);
        return this;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getIcerik() {
        return this.icerik;
    }

    public Yorum icerik(String icerik) {
        this.setIcerik(icerik);
        return this;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Yorum user(User user) {
        this.setUser(user);
        return this;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public Yorum kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    public Etkinlik getEtkinlik() {
        return this.etkinlik;
    }

    public void setEtkinlik(Etkinlik etkinlik) {
        this.etkinlik = etkinlik;
    }

    public Yorum etkinlik(Etkinlik etkinlik) {
        this.setEtkinlik(etkinlik);
        return this;
    }

    public GrupEtkinligi getGrupEtkinligi() {
        return this.grupEtkinligi;
    }

    public void setGrupEtkinligi(GrupEtkinligi grupEtkinligi) {
        this.grupEtkinligi = grupEtkinligi;
    }

    public Yorum grupEtkinligi(GrupEtkinligi grupEtkinligi) {
        this.setGrupEtkinligi(grupEtkinligi);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Yorum)) {
            return false;
        }
        return id != null && id.equals(((Yorum) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Yorum{" +
            "id=" + getId() +
            ", baslik='" + getBaslik() + "'" +
            ", icerik='" + getIcerik() + "'" +
            "}";
    }
}
