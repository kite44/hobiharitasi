package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Takvim.
 */
@Entity
@Table(name = "takvim")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Takvim implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "etkinlik_tarihi", nullable = false)
    private Instant etkinlikTarihi;

    @NotNull
    @Column(name = "etkinlik_basligi", nullable = false)
    private String etkinlikBasligi;

    @Column(name = "aciklama")
    private String aciklama;

    @ManyToOne
    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    @ManyToOne
    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Takvim id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getEtkinlikTarihi() {
        return this.etkinlikTarihi;
    }

    public Takvim etkinlikTarihi(Instant etkinlikTarihi) {
        this.setEtkinlikTarihi(etkinlikTarihi);
        return this;
    }

    public void setEtkinlikTarihi(Instant etkinlikTarihi) {
        this.etkinlikTarihi = etkinlikTarihi;
    }

    public String getEtkinlikBasligi() {
        return this.etkinlikBasligi;
    }

    public Takvim etkinlikBasligi(String etkinlikBasligi) {
        this.setEtkinlikBasligi(etkinlikBasligi);
        return this;
    }

    public void setEtkinlikBasligi(String etkinlikBasligi) {
        this.etkinlikBasligi = etkinlikBasligi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Takvim aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public Takvim kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public Takvim kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Takvim)) {
            return false;
        }
        return id != null && id.equals(((Takvim) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Takvim{" +
            "id=" + getId() +
            ", etkinlikTarihi='" + getEtkinlikTarihi() + "'" +
            ", etkinlikBasligi='" + getEtkinlikBasligi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            "}";
    }
}
