package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A GrupEtkinligi.
 */
@Entity
@Table(name = "grup_etkinligi")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GrupEtkinligi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "etkinlik_adi", nullable = false)
    private String etkinlikAdi;

    @NotNull
    @Column(name = "aciklama", nullable = false)
    private String aciklama;

    @NotNull
    @Column(name = "tarih", nullable = false)
    private Instant tarih;

    @NotNull
    @Column(name = "konum", nullable = false)
    private String konum;

    @NotNull
    @Column(name = "group_id", nullable = false)
    private Long groupId;

    @ManyToOne
    @JsonIgnoreProperties(value = { "kategori", "kullanicis" }, allowSetters = true)
    private Grup grup;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public GrupEtkinligi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtkinlikAdi() {
        return this.etkinlikAdi;
    }

    public GrupEtkinligi etkinlikAdi(String etkinlikAdi) {
        this.setEtkinlikAdi(etkinlikAdi);
        return this;
    }

    public void setEtkinlikAdi(String etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public GrupEtkinligi aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Instant getTarih() {
        return this.tarih;
    }

    public GrupEtkinligi tarih(Instant tarih) {
        this.setTarih(tarih);
        return this;
    }

    public void setTarih(Instant tarih) {
        this.tarih = tarih;
    }

    public String getKonum() {
        return this.konum;
    }

    public GrupEtkinligi konum(String konum) {
        this.setKonum(konum);
        return this;
    }

    public void setKonum(String konum) {
        this.konum = konum;
    }

    public Long getGroupId() {
        return this.groupId;
    }

    public GrupEtkinligi groupId(Long groupId) {
        this.setGroupId(groupId);
        return this;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Grup getGrup() {
        return this.grup;
    }

    public void setGrup(Grup grup) {
        this.grup = grup;
    }

    public GrupEtkinligi grup(Grup grup) {
        this.setGrup(grup);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GrupEtkinligi)) {
            return false;
        }
        return id != null && id.equals(((GrupEtkinligi) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GrupEtkinligi{" +
            "id=" + getId() +
            ", etkinlikAdi='" + getEtkinlikAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", tarih='" + getTarih() + "'" +
            ", konum='" + getKonum() + "'" +
            ", groupId=" + getGroupId() +
            "}";
    }
}
