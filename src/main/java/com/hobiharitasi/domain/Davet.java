package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Davet.
 */
@Entity
@Table(name = "davet")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Davet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "davet_mesaji", nullable = false)
    private String davetMesaji;

    @NotNull
    @Column(name = "durum", nullable = false)
    private String durum;

    @NotNull
    @Column(name = "olusturulma_tarihi", nullable = false)
    private Instant olusturulmaTarihi;

    @NotNull
    @Column(name = "davet_tarihi", nullable = false)
    private Instant davetTarihi;

    @Column(name = "kabul_edildi_mi")
    private Boolean kabulEdildiMi;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici gonderen;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Davet id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDavetMesaji() {
        return this.davetMesaji;
    }

    public Davet davetMesaji(String davetMesaji) {
        this.setDavetMesaji(davetMesaji);
        return this;
    }

    public void setDavetMesaji(String davetMesaji) {
        this.davetMesaji = davetMesaji;
    }

    public String getDurum() {
        return this.durum;
    }

    public Davet durum(String durum) {
        this.setDurum(durum);
        return this;
    }

    public void setDurum(String durum) {
        this.durum = durum;
    }

    public Instant getOlusturulmaTarihi() {
        return this.olusturulmaTarihi;
    }

    public Davet olusturulmaTarihi(Instant olusturulmaTarihi) {
        this.setOlusturulmaTarihi(olusturulmaTarihi);
        return this;
    }

    public void setOlusturulmaTarihi(Instant olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public Instant getDavetTarihi() {
        return this.davetTarihi;
    }

    public Davet davetTarihi(Instant davetTarihi) {
        this.setDavetTarihi(davetTarihi);
        return this;
    }

    public void setDavetTarihi(Instant davetTarihi) {
        this.davetTarihi = davetTarihi;
    }

    public Boolean getKabulEdildiMi() {
        return this.kabulEdildiMi;
    }

    public Davet kabulEdildiMi(Boolean kabulEdildiMi) {
        this.setKabulEdildiMi(kabulEdildiMi);
        return this;
    }

    public void setKabulEdildiMi(Boolean kabulEdildiMi) {
        this.kabulEdildiMi = kabulEdildiMi;
    }

    public Kullanici getGonderen() {
        return this.gonderen;
    }

    public void setGonderen(Kullanici kullanici) {
        this.gonderen = kullanici;
    }

    public Davet gonderen(Kullanici kullanici) {
        this.setGonderen(kullanici);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Davet)) {
            return false;
        }
        return id != null && id.equals(((Davet) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Davet{" +
            "id=" + getId() +
            ", davetMesaji='" + getDavetMesaji() + "'" +
            ", durum='" + getDurum() + "'" +
            ", olusturulmaTarihi='" + getOlusturulmaTarihi() + "'" +
            ", davetTarihi='" + getDavetTarihi() + "'" +
            ", kabulEdildiMi='" + getKabulEdildiMi() + "'" +
            "}";
    }
}
