package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Kullanici.
 */
@Entity
@Table(name = "kullanici")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Kullanici implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "ad", nullable = false)
    private String ad;

    @NotNull
    @Column(name = "soyad", nullable = false)
    private String soyad;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "sifre", nullable = false)
    private String sifre;

    @NotNull
    @Column(name = "tel_no", nullable = false)
    private Integer telNo;

    @Size(max = 500)
    @Column(name = "biyografi", length = 500)
    private String biyografi;

    @Column(name = "adres")
    private String adres;

    @Column(name = "profil_resmi")
    private String profilResmi;

    @Column(name = "acces_code")
    private String accesCode;

    @Column(name = "jwt_code")
    private String jwtCode;

    @OneToMany(mappedBy = "kullanici")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "kullanici", "kullanici" }, allowSetters = true)
    private Set<Tavsiye> tavsiyes = new HashSet<>();

    @OneToMany(mappedBy = "kullanici")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "kullanici", "kullanici" }, allowSetters = true)
    private Set<Takvim> takvims = new HashSet<>();

    @ManyToOne
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(value = { "kategoris" }, allowSetters = true)
    private Etkinlik etkinlik;

    @ManyToMany
    @JoinTable(
        name = "rel_kullanici__grup",
        joinColumns = @JoinColumn(name = "kullanici_id"),
        inverseJoinColumns = @JoinColumn(name = "grup_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "kategori", "kullanicis" }, allowSetters = true)
    private Set<Grup> grups = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_kullanici__hobi",
        joinColumns = @JoinColumn(name = "kullanici_id"),
        inverseJoinColumns = @JoinColumn(name = "hobi_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "kullanicis" }, allowSetters = true)
    private Set<Hobiler> hobis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Kullanici id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAd() {
        return this.ad;
    }

    public Kullanici ad(String ad) {
        this.setAd(ad);
        return this;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return this.soyad;
    }

    public Kullanici soyad(String soyad) {
        this.setSoyad(soyad);
        return this;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getEmail() {
        return this.email;
    }

    public Kullanici email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSifre() {
        return this.sifre;
    }

    public Kullanici sifre(String sifre) {
        this.setSifre(sifre);
        return this;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public Integer getTelNo() {
        return this.telNo;
    }

    public Kullanici telNo(Integer telNo) {
        this.setTelNo(telNo);
        return this;
    }

    public void setTelNo(Integer telNo) {
        this.telNo = telNo;
    }

    public String getBiyografi() {
        return this.biyografi;
    }

    public Kullanici biyografi(String biyografi) {
        this.setBiyografi(biyografi);
        return this;
    }

    public void setBiyografi(String biyografi) {
        this.biyografi = biyografi;
    }

    public String getAdres() {
        return this.adres;
    }

    public Kullanici adres(String adres) {
        this.setAdres(adres);
        return this;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getProfilResmi() {
        return this.profilResmi;
    }

    public Kullanici profilResmi(String profilResmi) {
        this.setProfilResmi(profilResmi);
        return this;
    }

    public void setProfilResmi(String profilResmi) {
        this.profilResmi = profilResmi;
    }

    public String getAccesCode() {
        return this.accesCode;
    }

    public Kullanici accesCode(String accesCode) {
        this.setAccesCode(accesCode);
        return this;
    }

    public void setAccesCode(String accesCode) {
        this.accesCode = accesCode;
    }

    public String getJwtCode() {
        return this.jwtCode;
    }

    public Kullanici jwtCode(String jwtCode) {
        this.setJwtCode(jwtCode);
        return this;
    }

    public void setJwtCode(String jwtCode) {
        this.jwtCode = jwtCode;
    }

    public Set<Tavsiye> getTavsiyes() {
        return this.tavsiyes;
    }

    public void setTavsiyes(Set<Tavsiye> tavsiyes) {
        if (this.tavsiyes != null) {
            this.tavsiyes.forEach(i -> i.setKullanici(null));
        }
        if (tavsiyes != null) {
            tavsiyes.forEach(i -> i.setKullanici(this));
        }
        this.tavsiyes = tavsiyes;
    }

    public Kullanici tavsiyes(Set<Tavsiye> tavsiyes) {
        this.setTavsiyes(tavsiyes);
        return this;
    }

    public Kullanici addTavsiye(Tavsiye tavsiye) {
        this.tavsiyes.add(tavsiye);
        tavsiye.setKullanici(this);
        return this;
    }

    public Kullanici removeTavsiye(Tavsiye tavsiye) {
        this.tavsiyes.remove(tavsiye);
        tavsiye.setKullanici(null);
        return this;
    }

    public Set<Takvim> getTakvims() {
        return this.takvims;
    }

    public void setTakvims(Set<Takvim> takvims) {
        if (this.takvims != null) {
            this.takvims.forEach(i -> i.setKullanici(null));
        }
        if (takvims != null) {
            takvims.forEach(i -> i.setKullanici(this));
        }
        this.takvims = takvims;
    }

    public Kullanici takvims(Set<Takvim> takvims) {
        this.setTakvims(takvims);
        return this;
    }

    public Kullanici addTakvim(Takvim takvim) {
        this.takvims.add(takvim);
        takvim.setKullanici(this);
        return this;
    }

    public Kullanici removeTakvim(Takvim takvim) {
        this.takvims.remove(takvim);
        takvim.setKullanici(null);
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Kullanici user(User user) {
        this.setUser(user);
        return this;
    }

    public Etkinlik getEtkinlik() {
        return this.etkinlik;
    }

    public void setEtkinlik(Etkinlik etkinlik) {
        this.etkinlik = etkinlik;
    }

    public Kullanici etkinlik(Etkinlik etkinlik) {
        this.setEtkinlik(etkinlik);
        return this;
    }

    public Set<Grup> getGrups() {
        return this.grups;
    }

    public void setGrups(Set<Grup> grups) {
        this.grups = grups;
    }

    public Kullanici grups(Set<Grup> grups) {
        this.setGrups(grups);
        return this;
    }

    public Kullanici addGrup(Grup grup) {
        this.grups.add(grup);
        grup.getKullanicis().add(this);
        return this;
    }

    public Kullanici removeGrup(Grup grup) {
        this.grups.remove(grup);
        grup.getKullanicis().remove(this);
        return this;
    }

    public Set<Hobiler> getHobis() {
        return this.hobis;
    }

    public void setHobis(Set<Hobiler> hobilers) {
        this.hobis = hobilers;
    }

    public Kullanici hobis(Set<Hobiler> hobilers) {
        this.setHobis(hobilers);
        return this;
    }

    public Kullanici addHobi(Hobiler hobiler) {
        this.hobis.add(hobiler);
        hobiler.getKullanicis().add(this);
        return this;
    }

    public Kullanici removeHobi(Hobiler hobiler) {
        this.hobis.remove(hobiler);
        hobiler.getKullanicis().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kullanici)) {
            return false;
        }
        return id != null && id.equals(((Kullanici) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Kullanici{" +
            "id=" + getId() +
            ", ad='" + getAd() + "'" +
            ", soyad='" + getSoyad() + "'" +
            ", email='" + getEmail() + "'" +
            ", sifre='" + getSifre() + "'" +
            ", telNo=" + getTelNo() +
            ", biyografi='" + getBiyografi() + "'" +
            ", adres='" + getAdres() + "'" +
            ", profilResmi='" + getProfilResmi() + "'" +
            ", accesCode='" + getAccesCode() + "'" +
            ", jwtCode='" + getJwtCode() + "'" +
            "}";
    }
}
