package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Tavsiye.
 */
@Entity
@Table(name = "tavsiye")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Tavsiye implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "baslik", nullable = false)
    private String baslik;

    @NotNull
    @Column(name = "icerik", nullable = false)
    private String icerik;

    @NotNull
    @Column(name = "olusturulma_tarihi", nullable = false)
    private Instant olusturulmaTarihi;

    @ManyToOne
    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    @ManyToOne
    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Tavsiye id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaslik() {
        return this.baslik;
    }

    public Tavsiye baslik(String baslik) {
        this.setBaslik(baslik);
        return this;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getIcerik() {
        return this.icerik;
    }

    public Tavsiye icerik(String icerik) {
        this.setIcerik(icerik);
        return this;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public Instant getOlusturulmaTarihi() {
        return this.olusturulmaTarihi;
    }

    public Tavsiye olusturulmaTarihi(Instant olusturulmaTarihi) {
        this.setOlusturulmaTarihi(olusturulmaTarihi);
        return this;
    }

    public void setOlusturulmaTarihi(Instant olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public Tavsiye kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public Tavsiye kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tavsiye)) {
            return false;
        }
        return id != null && id.equals(((Tavsiye) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Tavsiye{" +
            "id=" + getId() +
            ", baslik='" + getBaslik() + "'" +
            ", icerik='" + getIcerik() + "'" +
            ", olusturulmaTarihi='" + getOlusturulmaTarihi() + "'" +
            "}";
    }
}
