package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A KullaniciHobisi.
 */
@Entity
@Table(name = "kullanici_hobisi")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KullaniciHobisi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "seviye", nullable = false)
    private Integer seviye;

    @ManyToOne
    @JsonIgnoreProperties(value = { "kullanicis" }, allowSetters = true)
    private Hobiler hobi;

    @ManyToOne
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Kullanici kullanici;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public KullaniciHobisi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSeviye() {
        return this.seviye;
    }

    public KullaniciHobisi seviye(Integer seviye) {
        this.setSeviye(seviye);
        return this;
    }

    public void setSeviye(Integer seviye) {
        this.seviye = seviye;
    }

    public Hobiler getHobi() {
        return this.hobi;
    }

    public void setHobi(Hobiler hobiler) {
        this.hobi = hobiler;
    }

    public KullaniciHobisi hobi(Hobiler hobiler) {
        this.setHobi(hobiler);
        return this;
    }

    public Kullanici getKullanici() {
        return this.kullanici;
    }

    public void setKullanici(Kullanici kullanici) {
        this.kullanici = kullanici;
    }

    public KullaniciHobisi kullanici(Kullanici kullanici) {
        this.setKullanici(kullanici);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KullaniciHobisi)) {
            return false;
        }
        return id != null && id.equals(((KullaniciHobisi) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KullaniciHobisi{" +
            "id=" + getId() +
            ", seviye=" + getSeviye() +
            "}";
    }
}
