package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Grup.
 */
@Entity
@Table(name = "grup")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Grup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "grup_adi", nullable = false)
    private String grupAdi;

    @NotNull
    @Column(name = "aciklama", nullable = false)
    private String aciklama;

    @NotNull
    @Column(name = "grup_kategorisi", nullable = false)
    private String grupKategorisi;

    @ManyToOne
    @JsonIgnoreProperties(value = { "etkinliks" }, allowSetters = true)
    private Kategori kategori;

    @ManyToMany(mappedBy = "grups")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Set<Kullanici> kullanicis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Grup id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGrupAdi() {
        return this.grupAdi;
    }

    public Grup grupAdi(String grupAdi) {
        this.setGrupAdi(grupAdi);
        return this;
    }

    public void setGrupAdi(String grupAdi) {
        this.grupAdi = grupAdi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Grup aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getGrupKategorisi() {
        return this.grupKategorisi;
    }

    public Grup grupKategorisi(String grupKategorisi) {
        this.setGrupKategorisi(grupKategorisi);
        return this;
    }

    public void setGrupKategorisi(String grupKategorisi) {
        this.grupKategorisi = grupKategorisi;
    }

    public Kategori getKategori() {
        return this.kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public Grup kategori(Kategori kategori) {
        this.setKategori(kategori);
        return this;
    }

    public Set<Kullanici> getKullanicis() {
        return this.kullanicis;
    }

    public void setKullanicis(Set<Kullanici> kullanicis) {
        if (this.kullanicis != null) {
            this.kullanicis.forEach(i -> i.removeGrup(this));
        }
        if (kullanicis != null) {
            kullanicis.forEach(i -> i.addGrup(this));
        }
        this.kullanicis = kullanicis;
    }

    public Grup kullanicis(Set<Kullanici> kullanicis) {
        this.setKullanicis(kullanicis);
        return this;
    }

    public Grup addKullanici(Kullanici kullanici) {
        this.kullanicis.add(kullanici);
        kullanici.getGrups().add(this);
        return this;
    }

    public Grup removeKullanici(Kullanici kullanici) {
        this.kullanicis.remove(kullanici);
        kullanici.getGrups().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Grup)) {
            return false;
        }
        return id != null && id.equals(((Grup) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Grup{" +
            "id=" + getId() +
            ", grupAdi='" + getGrupAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", grupKategorisi='" + getGrupKategorisi() + "'" +
            "}";
    }
}
