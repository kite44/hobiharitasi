package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Kategori.
 */
@Entity
@Table(name = "kategori")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Kategori implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "kategori_adi", nullable = false)
    private String kategoriAdi;

    @NotNull
    @Column(name = "aciklama", nullable = false)
    private String aciklama;

    @ManyToMany(mappedBy = "kategoris")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "kategoris" }, allowSetters = true)
    private Set<Etkinlik> etkinliks = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Kategori id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategoriAdi() {
        return this.kategoriAdi;
    }

    public Kategori kategoriAdi(String kategoriAdi) {
        this.setKategoriAdi(kategoriAdi);
        return this;
    }

    public void setKategoriAdi(String kategoriAdi) {
        this.kategoriAdi = kategoriAdi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Kategori aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Set<Etkinlik> getEtkinliks() {
        return this.etkinliks;
    }

    public void setEtkinliks(Set<Etkinlik> etkinliks) {
        if (this.etkinliks != null) {
            this.etkinliks.forEach(i -> i.removeKategori(this));
        }
        if (etkinliks != null) {
            etkinliks.forEach(i -> i.addKategori(this));
        }
        this.etkinliks = etkinliks;
    }

    public Kategori etkinliks(Set<Etkinlik> etkinliks) {
        this.setEtkinliks(etkinliks);
        return this;
    }

    public Kategori addEtkinlik(Etkinlik etkinlik) {
        this.etkinliks.add(etkinlik);
        etkinlik.getKategoris().add(this);
        return this;
    }

    public Kategori removeEtkinlik(Etkinlik etkinlik) {
        this.etkinliks.remove(etkinlik);
        etkinlik.getKategoris().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kategori)) {
            return false;
        }
        return id != null && id.equals(((Kategori) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Kategori{" +
            "id=" + getId() +
            ", kategoriAdi='" + getKategoriAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            "}";
    }
}
