package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Hobiler.
 */
@Entity
@Table(name = "hobiler")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Hobiler implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "hobi_adi", nullable = false)
    private String hobiAdi;

    @Column(name = "aciklama")
    private String aciklama;

    @Column(name = "lokasyon")
    private String lokasyon;

    @Column(name = "cesidi")
    private String cesidi;

    @ManyToMany(mappedBy = "hobis")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tavsiyes", "takvims", "user", "etkinlik", "grups", "hobis" }, allowSetters = true)
    private Set<Kullanici> kullanicis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Hobiler id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHobiAdi() {
        return this.hobiAdi;
    }

    public Hobiler hobiAdi(String hobiAdi) {
        this.setHobiAdi(hobiAdi);
        return this;
    }

    public void setHobiAdi(String hobiAdi) {
        this.hobiAdi = hobiAdi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Hobiler aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getLokasyon() {
        return this.lokasyon;
    }

    public Hobiler lokasyon(String lokasyon) {
        this.setLokasyon(lokasyon);
        return this;
    }

    public void setLokasyon(String lokasyon) {
        this.lokasyon = lokasyon;
    }

    public String getCesidi() {
        return this.cesidi;
    }

    public Hobiler cesidi(String cesidi) {
        this.setCesidi(cesidi);
        return this;
    }

    public void setCesidi(String cesidi) {
        this.cesidi = cesidi;
    }

    public Set<Kullanici> getKullanicis() {
        return this.kullanicis;
    }

    public void setKullanicis(Set<Kullanici> kullanicis) {
        if (this.kullanicis != null) {
            this.kullanicis.forEach(i -> i.removeHobi(this));
        }
        if (kullanicis != null) {
            kullanicis.forEach(i -> i.addHobi(this));
        }
        this.kullanicis = kullanicis;
    }

    public Hobiler kullanicis(Set<Kullanici> kullanicis) {
        this.setKullanicis(kullanicis);
        return this;
    }

    public Hobiler addKullanici(Kullanici kullanici) {
        this.kullanicis.add(kullanici);
        kullanici.getHobis().add(this);
        return this;
    }

    public Hobiler removeKullanici(Kullanici kullanici) {
        this.kullanicis.remove(kullanici);
        kullanici.getHobis().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hobiler)) {
            return false;
        }
        return id != null && id.equals(((Hobiler) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Hobiler{" +
            "id=" + getId() +
            ", hobiAdi='" + getHobiAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", lokasyon='" + getLokasyon() + "'" +
            ", cesidi='" + getCesidi() + "'" +
            "}";
    }
}
