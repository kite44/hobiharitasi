package com.hobiharitasi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Etkinlik.
 */
@Entity
@Table(name = "etkinlik")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Etkinlik implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "etkinlik_adi", nullable = false)
    private String etkinlikAdi;

    @NotNull
    @Column(name = "aciklama", nullable = false)
    private String aciklama;

    @NotNull
    @Column(name = "tarih", nullable = false)
    private Instant tarih;

    @NotNull
    @Column(name = "konum", nullable = false)
    private String konum;

    @NotNull
    @Column(name = "maksimum_katilimci", nullable = false)
    private Integer maksimumKatilimci;

    @Column(name = "minimum_katilimci")
    private Integer minimumKatilimci;

    @NotNull
    @Column(name = "etkinlik_kategorisi", nullable = false)
    private String etkinlikKategorisi;

    @ManyToMany
    @JoinTable(
        name = "rel_etkinlik__kategori",
        joinColumns = @JoinColumn(name = "etkinlik_id"),
        inverseJoinColumns = @JoinColumn(name = "kategori_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "etkinliks" }, allowSetters = true)
    private Set<Kategori> kategoris = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Etkinlik id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtkinlikAdi() {
        return this.etkinlikAdi;
    }

    public Etkinlik etkinlikAdi(String etkinlikAdi) {
        this.setEtkinlikAdi(etkinlikAdi);
        return this;
    }

    public void setEtkinlikAdi(String etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Etkinlik aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Instant getTarih() {
        return this.tarih;
    }

    public Etkinlik tarih(Instant tarih) {
        this.setTarih(tarih);
        return this;
    }

    public void setTarih(Instant tarih) {
        this.tarih = tarih;
    }

    public String getKonum() {
        return this.konum;
    }

    public Etkinlik konum(String konum) {
        this.setKonum(konum);
        return this;
    }

    public void setKonum(String konum) {
        this.konum = konum;
    }

    public Integer getMaksimumKatilimci() {
        return this.maksimumKatilimci;
    }

    public Etkinlik maksimumKatilimci(Integer maksimumKatilimci) {
        this.setMaksimumKatilimci(maksimumKatilimci);
        return this;
    }

    public void setMaksimumKatilimci(Integer maksimumKatilimci) {
        this.maksimumKatilimci = maksimumKatilimci;
    }

    public Integer getMinimumKatilimci() {
        return this.minimumKatilimci;
    }

    public Etkinlik minimumKatilimci(Integer minimumKatilimci) {
        this.setMinimumKatilimci(minimumKatilimci);
        return this;
    }

    public void setMinimumKatilimci(Integer minimumKatilimci) {
        this.minimumKatilimci = minimumKatilimci;
    }

    public String getEtkinlikKategorisi() {
        return this.etkinlikKategorisi;
    }

    public Etkinlik etkinlikKategorisi(String etkinlikKategorisi) {
        this.setEtkinlikKategorisi(etkinlikKategorisi);
        return this;
    }

    public void setEtkinlikKategorisi(String etkinlikKategorisi) {
        this.etkinlikKategorisi = etkinlikKategorisi;
    }

    public Set<Kategori> getKategoris() {
        return this.kategoris;
    }

    public void setKategoris(Set<Kategori> kategoris) {
        this.kategoris = kategoris;
    }

    public Etkinlik kategoris(Set<Kategori> kategoris) {
        this.setKategoris(kategoris);
        return this;
    }

    public Etkinlik addKategori(Kategori kategori) {
        this.kategoris.add(kategori);
        kategori.getEtkinliks().add(this);
        return this;
    }

    public Etkinlik removeKategori(Kategori kategori) {
        this.kategoris.remove(kategori);
        kategori.getEtkinliks().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Etkinlik)) {
            return false;
        }
        return id != null && id.equals(((Etkinlik) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Etkinlik{" +
            "id=" + getId() +
            ", etkinlikAdi='" + getEtkinlikAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", tarih='" + getTarih() + "'" +
            ", konum='" + getKonum() + "'" +
            ", maksimumKatilimci=" + getMaksimumKatilimci() +
            ", minimumKatilimci=" + getMinimumKatilimci() +
            ", etkinlikKategorisi='" + getEtkinlikKategorisi() + "'" +
            "}";
    }
}
