package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.GrupEtkinligiRepository;
import com.hobiharitasi.service.GrupEtkinligiQueryService;
import com.hobiharitasi.service.GrupEtkinligiService;
import com.hobiharitasi.service.criteria.GrupEtkinligiCriteria;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.GrupEtkinligi}.
 */
@RestController
@RequestMapping("/api")
public class GrupEtkinligiResource {

    private final Logger log = LoggerFactory.getLogger(GrupEtkinligiResource.class);

    private static final String ENTITY_NAME = "grupEtkinligi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GrupEtkinligiService grupEtkinligiService;

    private final GrupEtkinligiRepository grupEtkinligiRepository;

    private final GrupEtkinligiQueryService grupEtkinligiQueryService;

    public GrupEtkinligiResource(
        GrupEtkinligiService grupEtkinligiService,
        GrupEtkinligiRepository grupEtkinligiRepository,
        GrupEtkinligiQueryService grupEtkinligiQueryService
    ) {
        this.grupEtkinligiService = grupEtkinligiService;
        this.grupEtkinligiRepository = grupEtkinligiRepository;
        this.grupEtkinligiQueryService = grupEtkinligiQueryService;
    }

    /**
     * {@code POST  /grup-etkinligis} : Create a new grupEtkinligi.
     *
     * @param grupEtkinligiDTO the grupEtkinligiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new grupEtkinligiDTO, or with status {@code 400 (Bad Request)} if the grupEtkinligi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/grup-etkinligis")
    public ResponseEntity<GrupEtkinligiDTO> createGrupEtkinligi(@Valid @RequestBody GrupEtkinligiDTO grupEtkinligiDTO)
        throws URISyntaxException {
        log.debug("REST request to save GrupEtkinligi : {}", grupEtkinligiDTO);
        if (grupEtkinligiDTO.getId() != null) {
            throw new BadRequestAlertException("A new grupEtkinligi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GrupEtkinligiDTO result = grupEtkinligiService.save(grupEtkinligiDTO);
        return ResponseEntity
            .created(new URI("/api/grup-etkinligis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /grup-etkinligis/:id} : Updates an existing grupEtkinligi.
     *
     * @param id the id of the grupEtkinligiDTO to save.
     * @param grupEtkinligiDTO the grupEtkinligiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated grupEtkinligiDTO,
     * or with status {@code 400 (Bad Request)} if the grupEtkinligiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the grupEtkinligiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/grup-etkinligis/{id}")
    public ResponseEntity<GrupEtkinligiDTO> updateGrupEtkinligi(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GrupEtkinligiDTO grupEtkinligiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update GrupEtkinligi : {}, {}", id, grupEtkinligiDTO);
        if (grupEtkinligiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, grupEtkinligiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!grupEtkinligiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GrupEtkinligiDTO result = grupEtkinligiService.update(grupEtkinligiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, grupEtkinligiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /grup-etkinligis/:id} : Partial updates given fields of an existing grupEtkinligi, field will ignore if it is null
     *
     * @param id the id of the grupEtkinligiDTO to save.
     * @param grupEtkinligiDTO the grupEtkinligiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated grupEtkinligiDTO,
     * or with status {@code 400 (Bad Request)} if the grupEtkinligiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the grupEtkinligiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the grupEtkinligiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/grup-etkinligis/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GrupEtkinligiDTO> partialUpdateGrupEtkinligi(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GrupEtkinligiDTO grupEtkinligiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update GrupEtkinligi partially : {}, {}", id, grupEtkinligiDTO);
        if (grupEtkinligiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, grupEtkinligiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!grupEtkinligiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GrupEtkinligiDTO> result = grupEtkinligiService.partialUpdate(grupEtkinligiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, grupEtkinligiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /grup-etkinligis} : get all the grupEtkinligis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of grupEtkinligis in body.
     */
    @GetMapping("/grup-etkinligis")
    public ResponseEntity<List<GrupEtkinligiDTO>> getAllGrupEtkinligis(
        GrupEtkinligiCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get GrupEtkinligis by criteria: {}", criteria);
        Page<GrupEtkinligiDTO> page = grupEtkinligiQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /grup-etkinligis/count} : count all the grupEtkinligis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/grup-etkinligis/count")
    public ResponseEntity<Long> countGrupEtkinligis(GrupEtkinligiCriteria criteria) {
        log.debug("REST request to count GrupEtkinligis by criteria: {}", criteria);
        return ResponseEntity.ok().body(grupEtkinligiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /grup-etkinligis/:id} : get the "id" grupEtkinligi.
     *
     * @param id the id of the grupEtkinligiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the grupEtkinligiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/grup-etkinligis/{id}")
    public ResponseEntity<GrupEtkinligiDTO> getGrupEtkinligi(@PathVariable Long id) {
        log.debug("REST request to get GrupEtkinligi : {}", id);
        Optional<GrupEtkinligiDTO> grupEtkinligiDTO = grupEtkinligiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(grupEtkinligiDTO);
    }

    /**
     * {@code DELETE  /grup-etkinligis/:id} : delete the "id" grupEtkinligi.
     *
     * @param id the id of the grupEtkinligiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/grup-etkinligis/{id}")
    public ResponseEntity<Void> deleteGrupEtkinligi(@PathVariable Long id) {
        log.debug("REST request to delete GrupEtkinligi : {}", id);
        grupEtkinligiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
