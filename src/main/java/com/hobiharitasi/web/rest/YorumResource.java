package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.YorumRepository;
import com.hobiharitasi.service.YorumQueryService;
import com.hobiharitasi.service.YorumService;
import com.hobiharitasi.service.criteria.YorumCriteria;
import com.hobiharitasi.service.dto.YorumDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Yorum}.
 */
@RestController
@RequestMapping("/api")
public class YorumResource {

    private final Logger log = LoggerFactory.getLogger(YorumResource.class);

    private static final String ENTITY_NAME = "yorum";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final YorumService yorumService;

    private final YorumRepository yorumRepository;

    private final YorumQueryService yorumQueryService;

    public YorumResource(YorumService yorumService, YorumRepository yorumRepository, YorumQueryService yorumQueryService) {
        this.yorumService = yorumService;
        this.yorumRepository = yorumRepository;
        this.yorumQueryService = yorumQueryService;
    }

    /**
     * {@code POST  /yorums} : Create a new yorum.
     *
     * @param yorumDTO the yorumDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new yorumDTO, or with status {@code 400 (Bad Request)} if the yorum has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/yorums")
    public ResponseEntity<YorumDTO> createYorum(@Valid @RequestBody YorumDTO yorumDTO) throws URISyntaxException {
        log.debug("REST request to save Yorum : {}", yorumDTO);
        if (yorumDTO.getId() != null) {
            throw new BadRequestAlertException("A new yorum cannot already have an ID", ENTITY_NAME, "idexists");
        }
        YorumDTO result = yorumService.save(yorumDTO);
        return ResponseEntity
            .created(new URI("/api/yorums/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /yorums/:id} : Updates an existing yorum.
     *
     * @param id the id of the yorumDTO to save.
     * @param yorumDTO the yorumDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated yorumDTO,
     * or with status {@code 400 (Bad Request)} if the yorumDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the yorumDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/yorums/{id}")
    public ResponseEntity<YorumDTO> updateYorum(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody YorumDTO yorumDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Yorum : {}, {}", id, yorumDTO);
        if (yorumDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, yorumDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!yorumRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        YorumDTO result = yorumService.update(yorumDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, yorumDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /yorums/:id} : Partial updates given fields of an existing yorum, field will ignore if it is null
     *
     * @param id the id of the yorumDTO to save.
     * @param yorumDTO the yorumDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated yorumDTO,
     * or with status {@code 400 (Bad Request)} if the yorumDTO is not valid,
     * or with status {@code 404 (Not Found)} if the yorumDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the yorumDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/yorums/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<YorumDTO> partialUpdateYorum(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody YorumDTO yorumDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Yorum partially : {}, {}", id, yorumDTO);
        if (yorumDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, yorumDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!yorumRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<YorumDTO> result = yorumService.partialUpdate(yorumDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, yorumDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /yorums} : get all the yorums.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of yorums in body.
     */
    @GetMapping("/yorums")
    public ResponseEntity<List<YorumDTO>> getAllYorums(
        YorumCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Yorums by criteria: {}", criteria);
        Page<YorumDTO> page = yorumQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /yorums/count} : count all the yorums.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/yorums/count")
    public ResponseEntity<Long> countYorums(YorumCriteria criteria) {
        log.debug("REST request to count Yorums by criteria: {}", criteria);
        return ResponseEntity.ok().body(yorumQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /yorums/:id} : get the "id" yorum.
     *
     * @param id the id of the yorumDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the yorumDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/yorums/{id}")
    public ResponseEntity<YorumDTO> getYorum(@PathVariable Long id) {
        log.debug("REST request to get Yorum : {}", id);
        Optional<YorumDTO> yorumDTO = yorumService.findOne(id);
        return ResponseUtil.wrapOrNotFound(yorumDTO);
    }

    /**
     * {@code DELETE  /yorums/:id} : delete the "id" yorum.
     *
     * @param id the id of the yorumDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/yorums/{id}")
    public ResponseEntity<Void> deleteYorum(@PathVariable Long id) {
        log.debug("REST request to delete Yorum : {}", id);
        yorumService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
