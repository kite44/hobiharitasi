package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.KategoriRepository;
import com.hobiharitasi.service.KategoriQueryService;
import com.hobiharitasi.service.KategoriService;
import com.hobiharitasi.service.criteria.KategoriCriteria;
import com.hobiharitasi.service.dto.KategoriDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Kategori}.
 */
@RestController
@RequestMapping("/api")
public class KategoriResource {

    private final Logger log = LoggerFactory.getLogger(KategoriResource.class);

    private static final String ENTITY_NAME = "kategori";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KategoriService kategoriService;

    private final KategoriRepository kategoriRepository;

    private final KategoriQueryService kategoriQueryService;

    public KategoriResource(
        KategoriService kategoriService,
        KategoriRepository kategoriRepository,
        KategoriQueryService kategoriQueryService
    ) {
        this.kategoriService = kategoriService;
        this.kategoriRepository = kategoriRepository;
        this.kategoriQueryService = kategoriQueryService;
    }

    /**
     * {@code POST  /kategoris} : Create a new kategori.
     *
     * @param kategoriDTO the kategoriDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kategoriDTO, or with status {@code 400 (Bad Request)} if the kategori has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kategoris")
    public ResponseEntity<KategoriDTO> createKategori(@Valid @RequestBody KategoriDTO kategoriDTO) throws URISyntaxException {
        log.debug("REST request to save Kategori : {}", kategoriDTO);
        if (kategoriDTO.getId() != null) {
            throw new BadRequestAlertException("A new kategori cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KategoriDTO result = kategoriService.save(kategoriDTO);
        return ResponseEntity
            .created(new URI("/api/kategoris/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kategoris/:id} : Updates an existing kategori.
     *
     * @param id the id of the kategoriDTO to save.
     * @param kategoriDTO the kategoriDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kategoriDTO,
     * or with status {@code 400 (Bad Request)} if the kategoriDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kategoriDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kategoris/{id}")
    public ResponseEntity<KategoriDTO> updateKategori(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KategoriDTO kategoriDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Kategori : {}, {}", id, kategoriDTO);
        if (kategoriDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kategoriDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kategoriRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KategoriDTO result = kategoriService.update(kategoriDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kategoriDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /kategoris/:id} : Partial updates given fields of an existing kategori, field will ignore if it is null
     *
     * @param id the id of the kategoriDTO to save.
     * @param kategoriDTO the kategoriDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kategoriDTO,
     * or with status {@code 400 (Bad Request)} if the kategoriDTO is not valid,
     * or with status {@code 404 (Not Found)} if the kategoriDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the kategoriDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/kategoris/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<KategoriDTO> partialUpdateKategori(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KategoriDTO kategoriDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Kategori partially : {}, {}", id, kategoriDTO);
        if (kategoriDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kategoriDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kategoriRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KategoriDTO> result = kategoriService.partialUpdate(kategoriDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kategoriDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /kategoris} : get all the kategoris.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kategoris in body.
     */
    @GetMapping("/kategoris")
    public ResponseEntity<List<KategoriDTO>> getAllKategoris(
        KategoriCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Kategoris by criteria: {}", criteria);
        Page<KategoriDTO> page = kategoriQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kategoris/count} : count all the kategoris.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/kategoris/count")
    public ResponseEntity<Long> countKategoris(KategoriCriteria criteria) {
        log.debug("REST request to count Kategoris by criteria: {}", criteria);
        return ResponseEntity.ok().body(kategoriQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /kategoris/:id} : get the "id" kategori.
     *
     * @param id the id of the kategoriDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kategoriDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kategoris/{id}")
    public ResponseEntity<KategoriDTO> getKategori(@PathVariable Long id) {
        log.debug("REST request to get Kategori : {}", id);
        Optional<KategoriDTO> kategoriDTO = kategoriService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kategoriDTO);
    }

    /**
     * {@code DELETE  /kategoris/:id} : delete the "id" kategori.
     *
     * @param id the id of the kategoriDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kategoris/{id}")
    public ResponseEntity<Void> deleteKategori(@PathVariable Long id) {
        log.debug("REST request to delete Kategori : {}", id);
        kategoriService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
