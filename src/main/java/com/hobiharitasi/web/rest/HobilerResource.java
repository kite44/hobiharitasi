package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.HobilerRepository;
import com.hobiharitasi.service.HobilerQueryService;
import com.hobiharitasi.service.HobilerService;
import com.hobiharitasi.service.criteria.HobilerCriteria;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Hobiler}.
 */
@RestController
@RequestMapping("/api")
public class HobilerResource {

    private final Logger log = LoggerFactory.getLogger(HobilerResource.class);

    private static final String ENTITY_NAME = "hobiler";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HobilerService hobilerService;

    private final HobilerRepository hobilerRepository;

    private final HobilerQueryService hobilerQueryService;

    public HobilerResource(HobilerService hobilerService, HobilerRepository hobilerRepository, HobilerQueryService hobilerQueryService) {
        this.hobilerService = hobilerService;
        this.hobilerRepository = hobilerRepository;
        this.hobilerQueryService = hobilerQueryService;
    }

    /**
     * {@code POST  /hobilers} : Create a new hobiler.
     *
     * @param hobilerDTO the hobilerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hobilerDTO, or with status {@code 400 (Bad Request)} if the hobiler has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hobilers")
    public ResponseEntity<HobilerDTO> createHobiler(@Valid @RequestBody HobilerDTO hobilerDTO) throws URISyntaxException {
        log.debug("REST request to save Hobiler : {}", hobilerDTO);
        if (hobilerDTO.getId() != null) {
            throw new BadRequestAlertException("A new hobiler cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HobilerDTO result = hobilerService.save(hobilerDTO);
        return ResponseEntity
            .created(new URI("/api/hobilers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hobilers/:id} : Updates an existing hobiler.
     *
     * @param id the id of the hobilerDTO to save.
     * @param hobilerDTO the hobilerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hobilerDTO,
     * or with status {@code 400 (Bad Request)} if the hobilerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hobilerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hobilers/{id}")
    public ResponseEntity<HobilerDTO> updateHobiler(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody HobilerDTO hobilerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Hobiler : {}, {}", id, hobilerDTO);
        if (hobilerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hobilerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hobilerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        HobilerDTO result = hobilerService.update(hobilerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hobilerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /hobilers/:id} : Partial updates given fields of an existing hobiler, field will ignore if it is null
     *
     * @param id the id of the hobilerDTO to save.
     * @param hobilerDTO the hobilerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hobilerDTO,
     * or with status {@code 400 (Bad Request)} if the hobilerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the hobilerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the hobilerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/hobilers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<HobilerDTO> partialUpdateHobiler(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody HobilerDTO hobilerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Hobiler partially : {}, {}", id, hobilerDTO);
        if (hobilerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hobilerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hobilerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<HobilerDTO> result = hobilerService.partialUpdate(hobilerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hobilerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /hobilers} : get all the hobilers.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hobilers in body.
     */
    @GetMapping("/hobilers")
    public ResponseEntity<List<HobilerDTO>> getAllHobilers(
        HobilerCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Hobilers by criteria: {}", criteria);
        Page<HobilerDTO> page = hobilerQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /hobilers/count} : count all the hobilers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/hobilers/count")
    public ResponseEntity<Long> countHobilers(HobilerCriteria criteria) {
        log.debug("REST request to count Hobilers by criteria: {}", criteria);
        return ResponseEntity.ok().body(hobilerQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /hobilers/:id} : get the "id" hobiler.
     *
     * @param id the id of the hobilerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hobilerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hobilers/{id}")
    public ResponseEntity<HobilerDTO> getHobiler(@PathVariable Long id) {
        log.debug("REST request to get Hobiler : {}", id);
        Optional<HobilerDTO> hobilerDTO = hobilerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(hobilerDTO);
    }

    /**
     * {@code DELETE  /hobilers/:id} : delete the "id" hobiler.
     *
     * @param id the id of the hobilerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hobilers/{id}")
    public ResponseEntity<Void> deleteHobiler(@PathVariable Long id) {
        log.debug("REST request to delete Hobiler : {}", id);
        hobilerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
