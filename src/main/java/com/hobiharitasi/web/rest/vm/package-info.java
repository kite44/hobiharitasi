/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hobiharitasi.web.rest.vm;
