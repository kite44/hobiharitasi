package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.KullaniciRepository;
import com.hobiharitasi.service.KullaniciQueryService;
import com.hobiharitasi.service.KullaniciService;
import com.hobiharitasi.service.criteria.KullaniciCriteria;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Kullanici}.
 */
@RestController
@RequestMapping("/api")
public class KullaniciResource {

    private final Logger log = LoggerFactory.getLogger(KullaniciResource.class);

    private static final String ENTITY_NAME = "kullanici";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KullaniciService kullaniciService;

    private final KullaniciRepository kullaniciRepository;

    private final KullaniciQueryService kullaniciQueryService;

    public KullaniciResource(
        KullaniciService kullaniciService,
        KullaniciRepository kullaniciRepository,
        KullaniciQueryService kullaniciQueryService
    ) {
        this.kullaniciService = kullaniciService;
        this.kullaniciRepository = kullaniciRepository;
        this.kullaniciQueryService = kullaniciQueryService;
    }

    /**
     * {@code POST  /kullanicis} : Create a new kullanici.
     *
     * @param kullaniciDTO the kullaniciDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kullaniciDTO, or with status {@code 400 (Bad Request)} if the kullanici has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kullanicis")
    public ResponseEntity<KullaniciDTO> createKullanici(@Valid @RequestBody KullaniciDTO kullaniciDTO) throws URISyntaxException {
        log.debug("REST request to save Kullanici : {}", kullaniciDTO);
        if (kullaniciDTO.getId() != null) {
            throw new BadRequestAlertException("A new kullanici cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KullaniciDTO result = kullaniciService.save(kullaniciDTO);
        return ResponseEntity
            .created(new URI("/api/kullanicis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kullanicis/:id} : Updates an existing kullanici.
     *
     * @param id the id of the kullaniciDTO to save.
     * @param kullaniciDTO the kullaniciDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kullaniciDTO,
     * or with status {@code 400 (Bad Request)} if the kullaniciDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kullaniciDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kullanicis/{id}")
    public ResponseEntity<KullaniciDTO> updateKullanici(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KullaniciDTO kullaniciDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Kullanici : {}, {}", id, kullaniciDTO);
        if (kullaniciDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kullaniciDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kullaniciRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KullaniciDTO result = kullaniciService.update(kullaniciDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kullaniciDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /kullanicis/:id} : Partial updates given fields of an existing kullanici, field will ignore if it is null
     *
     * @param id the id of the kullaniciDTO to save.
     * @param kullaniciDTO the kullaniciDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kullaniciDTO,
     * or with status {@code 400 (Bad Request)} if the kullaniciDTO is not valid,
     * or with status {@code 404 (Not Found)} if the kullaniciDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the kullaniciDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/kullanicis/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<KullaniciDTO> partialUpdateKullanici(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KullaniciDTO kullaniciDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Kullanici partially : {}, {}", id, kullaniciDTO);
        if (kullaniciDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kullaniciDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kullaniciRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KullaniciDTO> result = kullaniciService.partialUpdate(kullaniciDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kullaniciDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /kullanicis} : get all the kullanicis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kullanicis in body.
     */
    @GetMapping("/kullanicis")
    public ResponseEntity<List<KullaniciDTO>> getAllKullanicis(
        KullaniciCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Kullanicis by criteria: {}", criteria);
        Page<KullaniciDTO> page = kullaniciQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kullanicis/count} : count all the kullanicis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/kullanicis/count")
    public ResponseEntity<Long> countKullanicis(KullaniciCriteria criteria) {
        log.debug("REST request to count Kullanicis by criteria: {}", criteria);
        return ResponseEntity.ok().body(kullaniciQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /kullanicis/:id} : get the "id" kullanici.
     *
     * @param id the id of the kullaniciDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kullaniciDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kullanicis/{id}")
    public ResponseEntity<KullaniciDTO> getKullanici(@PathVariable Long id) {
        log.debug("REST request to get Kullanici : {}", id);
        Optional<KullaniciDTO> kullaniciDTO = kullaniciService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kullaniciDTO);
    }

    /**
     * {@code DELETE  /kullanicis/:id} : delete the "id" kullanici.
     *
     * @param id the id of the kullaniciDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kullanicis/{id}")
    public ResponseEntity<Void> deleteKullanici(@PathVariable Long id) {
        log.debug("REST request to delete Kullanici : {}", id);
        kullaniciService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
