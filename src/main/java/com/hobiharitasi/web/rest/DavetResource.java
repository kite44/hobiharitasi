package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.DavetRepository;
import com.hobiharitasi.service.DavetQueryService;
import com.hobiharitasi.service.DavetService;
import com.hobiharitasi.service.criteria.DavetCriteria;
import com.hobiharitasi.service.dto.DavetDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Davet}.
 */
@RestController
@RequestMapping("/api")
public class DavetResource {

    private final Logger log = LoggerFactory.getLogger(DavetResource.class);

    private static final String ENTITY_NAME = "davet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DavetService davetService;

    private final DavetRepository davetRepository;

    private final DavetQueryService davetQueryService;

    public DavetResource(DavetService davetService, DavetRepository davetRepository, DavetQueryService davetQueryService) {
        this.davetService = davetService;
        this.davetRepository = davetRepository;
        this.davetQueryService = davetQueryService;
    }

    /**
     * {@code POST  /davets} : Create a new davet.
     *
     * @param davetDTO the davetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new davetDTO, or with status {@code 400 (Bad Request)} if the davet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/davets")
    public ResponseEntity<DavetDTO> createDavet(@Valid @RequestBody DavetDTO davetDTO) throws URISyntaxException {
        log.debug("REST request to save Davet : {}", davetDTO);
        if (davetDTO.getId() != null) {
            throw new BadRequestAlertException("A new davet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DavetDTO result = davetService.save(davetDTO);
        return ResponseEntity
            .created(new URI("/api/davets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /davets/:id} : Updates an existing davet.
     *
     * @param id the id of the davetDTO to save.
     * @param davetDTO the davetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated davetDTO,
     * or with status {@code 400 (Bad Request)} if the davetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the davetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/davets/{id}")
    public ResponseEntity<DavetDTO> updateDavet(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DavetDTO davetDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Davet : {}, {}", id, davetDTO);
        if (davetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, davetDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!davetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DavetDTO result = davetService.update(davetDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, davetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /davets/:id} : Partial updates given fields of an existing davet, field will ignore if it is null
     *
     * @param id the id of the davetDTO to save.
     * @param davetDTO the davetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated davetDTO,
     * or with status {@code 400 (Bad Request)} if the davetDTO is not valid,
     * or with status {@code 404 (Not Found)} if the davetDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the davetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/davets/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DavetDTO> partialUpdateDavet(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DavetDTO davetDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Davet partially : {}, {}", id, davetDTO);
        if (davetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, davetDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!davetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DavetDTO> result = davetService.partialUpdate(davetDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, davetDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /davets} : get all the davets.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of davets in body.
     */
    @GetMapping("/davets")
    public ResponseEntity<List<DavetDTO>> getAllDavets(
        DavetCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Davets by criteria: {}", criteria);
        Page<DavetDTO> page = davetQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /davets/count} : count all the davets.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/davets/count")
    public ResponseEntity<Long> countDavets(DavetCriteria criteria) {
        log.debug("REST request to count Davets by criteria: {}", criteria);
        return ResponseEntity.ok().body(davetQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /davets/:id} : get the "id" davet.
     *
     * @param id the id of the davetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the davetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/davets/{id}")
    public ResponseEntity<DavetDTO> getDavet(@PathVariable Long id) {
        log.debug("REST request to get Davet : {}", id);
        Optional<DavetDTO> davetDTO = davetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(davetDTO);
    }

    /**
     * {@code DELETE  /davets/:id} : delete the "id" davet.
     *
     * @param id the id of the davetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/davets/{id}")
    public ResponseEntity<Void> deleteDavet(@PathVariable Long id) {
        log.debug("REST request to delete Davet : {}", id);
        davetService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
