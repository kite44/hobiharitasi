package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.GrupRepository;
import com.hobiharitasi.service.GrupQueryService;
import com.hobiharitasi.service.GrupService;
import com.hobiharitasi.service.criteria.GrupCriteria;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Grup}.
 */
@RestController
@RequestMapping("/api")
public class GrupResource {

    private final Logger log = LoggerFactory.getLogger(GrupResource.class);

    private static final String ENTITY_NAME = "grup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GrupService grupService;

    private final GrupRepository grupRepository;

    private final GrupQueryService grupQueryService;

    public GrupResource(GrupService grupService, GrupRepository grupRepository, GrupQueryService grupQueryService) {
        this.grupService = grupService;
        this.grupRepository = grupRepository;
        this.grupQueryService = grupQueryService;
    }

    /**
     * {@code POST  /grups} : Create a new grup.
     *
     * @param grupDTO the grupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new grupDTO, or with status {@code 400 (Bad Request)} if the grup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/grups")
    public ResponseEntity<GrupDTO> createGrup(@Valid @RequestBody GrupDTO grupDTO) throws URISyntaxException {
        log.debug("REST request to save Grup : {}", grupDTO);
        if (grupDTO.getId() != null) {
            throw new BadRequestAlertException("A new grup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GrupDTO result = grupService.save(grupDTO);
        return ResponseEntity
            .created(new URI("/api/grups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /grups/:id} : Updates an existing grup.
     *
     * @param id the id of the grupDTO to save.
     * @param grupDTO the grupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated grupDTO,
     * or with status {@code 400 (Bad Request)} if the grupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the grupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/grups/{id}")
    public ResponseEntity<GrupDTO> updateGrup(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody GrupDTO grupDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Grup : {}, {}", id, grupDTO);
        if (grupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, grupDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!grupRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        GrupDTO result = grupService.update(grupDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, grupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /grups/:id} : Partial updates given fields of an existing grup, field will ignore if it is null
     *
     * @param id the id of the grupDTO to save.
     * @param grupDTO the grupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated grupDTO,
     * or with status {@code 400 (Bad Request)} if the grupDTO is not valid,
     * or with status {@code 404 (Not Found)} if the grupDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the grupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/grups/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<GrupDTO> partialUpdateGrup(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody GrupDTO grupDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Grup partially : {}, {}", id, grupDTO);
        if (grupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, grupDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!grupRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<GrupDTO> result = grupService.partialUpdate(grupDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, grupDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /grups} : get all the grups.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of grups in body.
     */
    @GetMapping("/grups")
    public ResponseEntity<List<GrupDTO>> getAllGrups(
        GrupCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Grups by criteria: {}", criteria);
        Page<GrupDTO> page = grupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /grups/count} : count all the grups.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/grups/count")
    public ResponseEntity<Long> countGrups(GrupCriteria criteria) {
        log.debug("REST request to count Grups by criteria: {}", criteria);
        return ResponseEntity.ok().body(grupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /grups/:id} : get the "id" grup.
     *
     * @param id the id of the grupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the grupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/grups/{id}")
    public ResponseEntity<GrupDTO> getGrup(@PathVariable Long id) {
        log.debug("REST request to get Grup : {}", id);
        Optional<GrupDTO> grupDTO = grupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(grupDTO);
    }

    /**
     * {@code DELETE  /grups/:id} : delete the "id" grup.
     *
     * @param id the id of the grupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/grups/{id}")
    public ResponseEntity<Void> deleteGrup(@PathVariable Long id) {
        log.debug("REST request to delete Grup : {}", id);
        grupService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
