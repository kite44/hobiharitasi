package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.EtkinlikRepository;
import com.hobiharitasi.service.EtkinlikQueryService;
import com.hobiharitasi.service.EtkinlikService;
import com.hobiharitasi.service.criteria.EtkinlikCriteria;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Etkinlik}.
 */
@RestController
@RequestMapping("/api")
public class EtkinlikResource {

    private final Logger log = LoggerFactory.getLogger(EtkinlikResource.class);

    private static final String ENTITY_NAME = "etkinlik";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EtkinlikService etkinlikService;

    private final EtkinlikRepository etkinlikRepository;

    private final EtkinlikQueryService etkinlikQueryService;

    public EtkinlikResource(
        EtkinlikService etkinlikService,
        EtkinlikRepository etkinlikRepository,
        EtkinlikQueryService etkinlikQueryService
    ) {
        this.etkinlikService = etkinlikService;
        this.etkinlikRepository = etkinlikRepository;
        this.etkinlikQueryService = etkinlikQueryService;
    }

    /**
     * {@code POST  /etkinliks} : Create a new etkinlik.
     *
     * @param etkinlikDTO the etkinlikDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new etkinlikDTO, or with status {@code 400 (Bad Request)} if the etkinlik has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etkinliks")
    public ResponseEntity<EtkinlikDTO> createEtkinlik(@Valid @RequestBody EtkinlikDTO etkinlikDTO) throws URISyntaxException {
        log.debug("REST request to save Etkinlik : {}", etkinlikDTO);
        if (etkinlikDTO.getId() != null) {
            throw new BadRequestAlertException("A new etkinlik cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EtkinlikDTO result = etkinlikService.save(etkinlikDTO);
        return ResponseEntity
            .created(new URI("/api/etkinliks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etkinliks/:id} : Updates an existing etkinlik.
     *
     * @param id the id of the etkinlikDTO to save.
     * @param etkinlikDTO the etkinlikDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etkinlikDTO,
     * or with status {@code 400 (Bad Request)} if the etkinlikDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the etkinlikDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etkinliks/{id}")
    public ResponseEntity<EtkinlikDTO> updateEtkinlik(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody EtkinlikDTO etkinlikDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Etkinlik : {}, {}", id, etkinlikDTO);
        if (etkinlikDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, etkinlikDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!etkinlikRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EtkinlikDTO result = etkinlikService.update(etkinlikDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etkinlikDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /etkinliks/:id} : Partial updates given fields of an existing etkinlik, field will ignore if it is null
     *
     * @param id the id of the etkinlikDTO to save.
     * @param etkinlikDTO the etkinlikDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated etkinlikDTO,
     * or with status {@code 400 (Bad Request)} if the etkinlikDTO is not valid,
     * or with status {@code 404 (Not Found)} if the etkinlikDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the etkinlikDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/etkinliks/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EtkinlikDTO> partialUpdateEtkinlik(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody EtkinlikDTO etkinlikDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Etkinlik partially : {}, {}", id, etkinlikDTO);
        if (etkinlikDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, etkinlikDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!etkinlikRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EtkinlikDTO> result = etkinlikService.partialUpdate(etkinlikDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, etkinlikDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /etkinliks} : get all the etkinliks.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of etkinliks in body.
     */
    @GetMapping("/etkinliks")
    public ResponseEntity<List<EtkinlikDTO>> getAllEtkinliks(
        EtkinlikCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Etkinliks by criteria: {}", criteria);
        Page<EtkinlikDTO> page = etkinlikQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /etkinliks/count} : count all the etkinliks.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/etkinliks/count")
    public ResponseEntity<Long> countEtkinliks(EtkinlikCriteria criteria) {
        log.debug("REST request to count Etkinliks by criteria: {}", criteria);
        return ResponseEntity.ok().body(etkinlikQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etkinliks/:id} : get the "id" etkinlik.
     *
     * @param id the id of the etkinlikDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the etkinlikDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etkinliks/{id}")
    public ResponseEntity<EtkinlikDTO> getEtkinlik(@PathVariable Long id) {
        log.debug("REST request to get Etkinlik : {}", id);
        Optional<EtkinlikDTO> etkinlikDTO = etkinlikService.findOne(id);
        return ResponseUtil.wrapOrNotFound(etkinlikDTO);
    }

    /**
     * {@code DELETE  /etkinliks/:id} : delete the "id" etkinlik.
     *
     * @param id the id of the etkinlikDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etkinliks/{id}")
    public ResponseEntity<Void> deleteEtkinlik(@PathVariable Long id) {
        log.debug("REST request to delete Etkinlik : {}", id);
        etkinlikService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
