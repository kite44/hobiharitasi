package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.TakvimRepository;
import com.hobiharitasi.service.TakvimQueryService;
import com.hobiharitasi.service.TakvimService;
import com.hobiharitasi.service.criteria.TakvimCriteria;
import com.hobiharitasi.service.dto.TakvimDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Takvim}.
 */
@RestController
@RequestMapping("/api")
public class TakvimResource {

    private final Logger log = LoggerFactory.getLogger(TakvimResource.class);

    private static final String ENTITY_NAME = "takvim";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TakvimService takvimService;

    private final TakvimRepository takvimRepository;

    private final TakvimQueryService takvimQueryService;

    public TakvimResource(TakvimService takvimService, TakvimRepository takvimRepository, TakvimQueryService takvimQueryService) {
        this.takvimService = takvimService;
        this.takvimRepository = takvimRepository;
        this.takvimQueryService = takvimQueryService;
    }

    /**
     * {@code POST  /takvims} : Create a new takvim.
     *
     * @param takvimDTO the takvimDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new takvimDTO, or with status {@code 400 (Bad Request)} if the takvim has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/takvims")
    public ResponseEntity<TakvimDTO> createTakvim(@Valid @RequestBody TakvimDTO takvimDTO) throws URISyntaxException {
        log.debug("REST request to save Takvim : {}", takvimDTO);
        if (takvimDTO.getId() != null) {
            throw new BadRequestAlertException("A new takvim cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TakvimDTO result = takvimService.save(takvimDTO);
        return ResponseEntity
            .created(new URI("/api/takvims/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /takvims/:id} : Updates an existing takvim.
     *
     * @param id the id of the takvimDTO to save.
     * @param takvimDTO the takvimDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated takvimDTO,
     * or with status {@code 400 (Bad Request)} if the takvimDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the takvimDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/takvims/{id}")
    public ResponseEntity<TakvimDTO> updateTakvim(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TakvimDTO takvimDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Takvim : {}, {}", id, takvimDTO);
        if (takvimDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, takvimDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!takvimRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TakvimDTO result = takvimService.update(takvimDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, takvimDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /takvims/:id} : Partial updates given fields of an existing takvim, field will ignore if it is null
     *
     * @param id the id of the takvimDTO to save.
     * @param takvimDTO the takvimDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated takvimDTO,
     * or with status {@code 400 (Bad Request)} if the takvimDTO is not valid,
     * or with status {@code 404 (Not Found)} if the takvimDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the takvimDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/takvims/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TakvimDTO> partialUpdateTakvim(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TakvimDTO takvimDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Takvim partially : {}, {}", id, takvimDTO);
        if (takvimDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, takvimDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!takvimRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TakvimDTO> result = takvimService.partialUpdate(takvimDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, takvimDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /takvims} : get all the takvims.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of takvims in body.
     */
    @GetMapping("/takvims")
    public ResponseEntity<List<TakvimDTO>> getAllTakvims(
        TakvimCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Takvims by criteria: {}", criteria);
        Page<TakvimDTO> page = takvimQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /takvims/count} : count all the takvims.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/takvims/count")
    public ResponseEntity<Long> countTakvims(TakvimCriteria criteria) {
        log.debug("REST request to count Takvims by criteria: {}", criteria);
        return ResponseEntity.ok().body(takvimQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /takvims/:id} : get the "id" takvim.
     *
     * @param id the id of the takvimDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the takvimDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/takvims/{id}")
    public ResponseEntity<TakvimDTO> getTakvim(@PathVariable Long id) {
        log.debug("REST request to get Takvim : {}", id);
        Optional<TakvimDTO> takvimDTO = takvimService.findOne(id);
        return ResponseUtil.wrapOrNotFound(takvimDTO);
    }

    /**
     * {@code DELETE  /takvims/:id} : delete the "id" takvim.
     *
     * @param id the id of the takvimDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/takvims/{id}")
    public ResponseEntity<Void> deleteTakvim(@PathVariable Long id) {
        log.debug("REST request to delete Takvim : {}", id);
        takvimService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
