package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.TavsiyeRepository;
import com.hobiharitasi.service.TavsiyeQueryService;
import com.hobiharitasi.service.TavsiyeService;
import com.hobiharitasi.service.criteria.TavsiyeCriteria;
import com.hobiharitasi.service.dto.TavsiyeDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.Tavsiye}.
 */
@RestController
@RequestMapping("/api")
public class TavsiyeResource {

    private final Logger log = LoggerFactory.getLogger(TavsiyeResource.class);

    private static final String ENTITY_NAME = "tavsiye";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TavsiyeService tavsiyeService;

    private final TavsiyeRepository tavsiyeRepository;

    private final TavsiyeQueryService tavsiyeQueryService;

    public TavsiyeResource(TavsiyeService tavsiyeService, TavsiyeRepository tavsiyeRepository, TavsiyeQueryService tavsiyeQueryService) {
        this.tavsiyeService = tavsiyeService;
        this.tavsiyeRepository = tavsiyeRepository;
        this.tavsiyeQueryService = tavsiyeQueryService;
    }

    /**
     * {@code POST  /tavsiyes} : Create a new tavsiye.
     *
     * @param tavsiyeDTO the tavsiyeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tavsiyeDTO, or with status {@code 400 (Bad Request)} if the tavsiye has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tavsiyes")
    public ResponseEntity<TavsiyeDTO> createTavsiye(@Valid @RequestBody TavsiyeDTO tavsiyeDTO) throws URISyntaxException {
        log.debug("REST request to save Tavsiye : {}", tavsiyeDTO);
        if (tavsiyeDTO.getId() != null) {
            throw new BadRequestAlertException("A new tavsiye cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TavsiyeDTO result = tavsiyeService.save(tavsiyeDTO);
        return ResponseEntity
            .created(new URI("/api/tavsiyes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tavsiyes/:id} : Updates an existing tavsiye.
     *
     * @param id the id of the tavsiyeDTO to save.
     * @param tavsiyeDTO the tavsiyeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tavsiyeDTO,
     * or with status {@code 400 (Bad Request)} if the tavsiyeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tavsiyeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tavsiyes/{id}")
    public ResponseEntity<TavsiyeDTO> updateTavsiye(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TavsiyeDTO tavsiyeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Tavsiye : {}, {}", id, tavsiyeDTO);
        if (tavsiyeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tavsiyeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tavsiyeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TavsiyeDTO result = tavsiyeService.update(tavsiyeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tavsiyeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /tavsiyes/:id} : Partial updates given fields of an existing tavsiye, field will ignore if it is null
     *
     * @param id the id of the tavsiyeDTO to save.
     * @param tavsiyeDTO the tavsiyeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tavsiyeDTO,
     * or with status {@code 400 (Bad Request)} if the tavsiyeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the tavsiyeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the tavsiyeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tavsiyes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TavsiyeDTO> partialUpdateTavsiye(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TavsiyeDTO tavsiyeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Tavsiye partially : {}, {}", id, tavsiyeDTO);
        if (tavsiyeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tavsiyeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tavsiyeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TavsiyeDTO> result = tavsiyeService.partialUpdate(tavsiyeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tavsiyeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /tavsiyes} : get all the tavsiyes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tavsiyes in body.
     */
    @GetMapping("/tavsiyes")
    public ResponseEntity<List<TavsiyeDTO>> getAllTavsiyes(
        TavsiyeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Tavsiyes by criteria: {}", criteria);
        Page<TavsiyeDTO> page = tavsiyeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /tavsiyes/count} : count all the tavsiyes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/tavsiyes/count")
    public ResponseEntity<Long> countTavsiyes(TavsiyeCriteria criteria) {
        log.debug("REST request to count Tavsiyes by criteria: {}", criteria);
        return ResponseEntity.ok().body(tavsiyeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tavsiyes/:id} : get the "id" tavsiye.
     *
     * @param id the id of the tavsiyeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tavsiyeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tavsiyes/{id}")
    public ResponseEntity<TavsiyeDTO> getTavsiye(@PathVariable Long id) {
        log.debug("REST request to get Tavsiye : {}", id);
        Optional<TavsiyeDTO> tavsiyeDTO = tavsiyeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tavsiyeDTO);
    }

    /**
     * {@code DELETE  /tavsiyes/:id} : delete the "id" tavsiye.
     *
     * @param id the id of the tavsiyeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tavsiyes/{id}")
    public ResponseEntity<Void> deleteTavsiye(@PathVariable Long id) {
        log.debug("REST request to delete Tavsiye : {}", id);
        tavsiyeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
