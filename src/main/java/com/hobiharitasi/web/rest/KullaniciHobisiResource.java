package com.hobiharitasi.web.rest;

import com.hobiharitasi.repository.KullaniciHobisiRepository;
import com.hobiharitasi.service.KullaniciHobisiQueryService;
import com.hobiharitasi.service.KullaniciHobisiService;
import com.hobiharitasi.service.criteria.KullaniciHobisiCriteria;
import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import com.hobiharitasi.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.hobiharitasi.domain.KullaniciHobisi}.
 */
@RestController
@RequestMapping("/api")
public class KullaniciHobisiResource {

    private final Logger log = LoggerFactory.getLogger(KullaniciHobisiResource.class);

    private static final String ENTITY_NAME = "kullaniciHobisi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KullaniciHobisiService kullaniciHobisiService;

    private final KullaniciHobisiRepository kullaniciHobisiRepository;

    private final KullaniciHobisiQueryService kullaniciHobisiQueryService;

    public KullaniciHobisiResource(
        KullaniciHobisiService kullaniciHobisiService,
        KullaniciHobisiRepository kullaniciHobisiRepository,
        KullaniciHobisiQueryService kullaniciHobisiQueryService
    ) {
        this.kullaniciHobisiService = kullaniciHobisiService;
        this.kullaniciHobisiRepository = kullaniciHobisiRepository;
        this.kullaniciHobisiQueryService = kullaniciHobisiQueryService;
    }

    /**
     * {@code POST  /kullanici-hobisis} : Create a new kullaniciHobisi.
     *
     * @param kullaniciHobisiDTO the kullaniciHobisiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kullaniciHobisiDTO, or with status {@code 400 (Bad Request)} if the kullaniciHobisi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/kullanici-hobisis")
    public ResponseEntity<KullaniciHobisiDTO> createKullaniciHobisi(@Valid @RequestBody KullaniciHobisiDTO kullaniciHobisiDTO)
        throws URISyntaxException {
        log.debug("REST request to save KullaniciHobisi : {}", kullaniciHobisiDTO);
        if (kullaniciHobisiDTO.getId() != null) {
            throw new BadRequestAlertException("A new kullaniciHobisi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KullaniciHobisiDTO result = kullaniciHobisiService.save(kullaniciHobisiDTO);
        return ResponseEntity
            .created(new URI("/api/kullanici-hobisis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kullanici-hobisis/:id} : Updates an existing kullaniciHobisi.
     *
     * @param id the id of the kullaniciHobisiDTO to save.
     * @param kullaniciHobisiDTO the kullaniciHobisiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kullaniciHobisiDTO,
     * or with status {@code 400 (Bad Request)} if the kullaniciHobisiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kullaniciHobisiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/kullanici-hobisis/{id}")
    public ResponseEntity<KullaniciHobisiDTO> updateKullaniciHobisi(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody KullaniciHobisiDTO kullaniciHobisiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update KullaniciHobisi : {}, {}", id, kullaniciHobisiDTO);
        if (kullaniciHobisiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kullaniciHobisiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kullaniciHobisiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        KullaniciHobisiDTO result = kullaniciHobisiService.update(kullaniciHobisiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kullaniciHobisiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /kullanici-hobisis/:id} : Partial updates given fields of an existing kullaniciHobisi, field will ignore if it is null
     *
     * @param id the id of the kullaniciHobisiDTO to save.
     * @param kullaniciHobisiDTO the kullaniciHobisiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kullaniciHobisiDTO,
     * or with status {@code 400 (Bad Request)} if the kullaniciHobisiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the kullaniciHobisiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the kullaniciHobisiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/kullanici-hobisis/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<KullaniciHobisiDTO> partialUpdateKullaniciHobisi(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody KullaniciHobisiDTO kullaniciHobisiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update KullaniciHobisi partially : {}, {}", id, kullaniciHobisiDTO);
        if (kullaniciHobisiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kullaniciHobisiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kullaniciHobisiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<KullaniciHobisiDTO> result = kullaniciHobisiService.partialUpdate(kullaniciHobisiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, kullaniciHobisiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /kullanici-hobisis} : get all the kullaniciHobisis.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kullaniciHobisis in body.
     */
    @GetMapping("/kullanici-hobisis")
    public ResponseEntity<List<KullaniciHobisiDTO>> getAllKullaniciHobisis(
        KullaniciHobisiCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get KullaniciHobisis by criteria: {}", criteria);
        Page<KullaniciHobisiDTO> page = kullaniciHobisiQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kullanici-hobisis/count} : count all the kullaniciHobisis.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/kullanici-hobisis/count")
    public ResponseEntity<Long> countKullaniciHobisis(KullaniciHobisiCriteria criteria) {
        log.debug("REST request to count KullaniciHobisis by criteria: {}", criteria);
        return ResponseEntity.ok().body(kullaniciHobisiQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /kullanici-hobisis/:id} : get the "id" kullaniciHobisi.
     *
     * @param id the id of the kullaniciHobisiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kullaniciHobisiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/kullanici-hobisis/{id}")
    public ResponseEntity<KullaniciHobisiDTO> getKullaniciHobisi(@PathVariable Long id) {
        log.debug("REST request to get KullaniciHobisi : {}", id);
        Optional<KullaniciHobisiDTO> kullaniciHobisiDTO = kullaniciHobisiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(kullaniciHobisiDTO);
    }

    /**
     * {@code DELETE  /kullanici-hobisis/:id} : delete the "id" kullaniciHobisi.
     *
     * @param id the id of the kullaniciHobisiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/kullanici-hobisis/{id}")
    public ResponseEntity<Void> deleteKullaniciHobisi(@PathVariable Long id) {
        log.debug("REST request to delete KullaniciHobisi : {}", id);
        kullaniciHobisiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
