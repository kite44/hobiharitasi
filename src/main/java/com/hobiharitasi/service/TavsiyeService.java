package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.TavsiyeDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Tavsiye}.
 */
public interface TavsiyeService {
    /**
     * Save a tavsiye.
     *
     * @param tavsiyeDTO the entity to save.
     * @return the persisted entity.
     */
    TavsiyeDTO save(TavsiyeDTO tavsiyeDTO);

    /**
     * Updates a tavsiye.
     *
     * @param tavsiyeDTO the entity to update.
     * @return the persisted entity.
     */
    TavsiyeDTO update(TavsiyeDTO tavsiyeDTO);

    /**
     * Partially updates a tavsiye.
     *
     * @param tavsiyeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TavsiyeDTO> partialUpdate(TavsiyeDTO tavsiyeDTO);

    /**
     * Get all the tavsiyes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TavsiyeDTO> findAll(Pageable pageable);

    /**
     * Get all the tavsiyes with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TavsiyeDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" tavsiye.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TavsiyeDTO> findOne(Long id);

    /**
     * Delete the "id" tavsiye.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
