package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.KullaniciDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Kullanici}.
 */
public interface KullaniciService {
    /**
     * Save a kullanici.
     *
     * @param kullaniciDTO the entity to save.
     * @return the persisted entity.
     */
    KullaniciDTO save(KullaniciDTO kullaniciDTO);

    /**
     * Updates a kullanici.
     *
     * @param kullaniciDTO the entity to update.
     * @return the persisted entity.
     */
    KullaniciDTO update(KullaniciDTO kullaniciDTO);

    /**
     * Partially updates a kullanici.
     *
     * @param kullaniciDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KullaniciDTO> partialUpdate(KullaniciDTO kullaniciDTO);

    /**
     * Get all the kullanicis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KullaniciDTO> findAll(Pageable pageable);

    /**
     * Get all the kullanicis with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KullaniciDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" kullanici.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KullaniciDTO> findOne(Long id);

    /**
     * Delete the "id" kullanici.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
