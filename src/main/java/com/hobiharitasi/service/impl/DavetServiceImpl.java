package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Davet;
import com.hobiharitasi.repository.DavetRepository;
import com.hobiharitasi.service.DavetService;
import com.hobiharitasi.service.dto.DavetDTO;
import com.hobiharitasi.service.mapper.DavetMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Davet}.
 */
@Service
@Transactional
public class DavetServiceImpl implements DavetService {

    private final Logger log = LoggerFactory.getLogger(DavetServiceImpl.class);

    private final DavetRepository davetRepository;

    private final DavetMapper davetMapper;

    public DavetServiceImpl(DavetRepository davetRepository, DavetMapper davetMapper) {
        this.davetRepository = davetRepository;
        this.davetMapper = davetMapper;
    }

    @Override
    public DavetDTO save(DavetDTO davetDTO) {
        log.debug("Request to save Davet : {}", davetDTO);
        Davet davet = davetMapper.toEntity(davetDTO);
        davet = davetRepository.save(davet);
        return davetMapper.toDto(davet);
    }

    @Override
    public DavetDTO update(DavetDTO davetDTO) {
        log.debug("Request to update Davet : {}", davetDTO);
        Davet davet = davetMapper.toEntity(davetDTO);
        davet = davetRepository.save(davet);
        return davetMapper.toDto(davet);
    }

    @Override
    public Optional<DavetDTO> partialUpdate(DavetDTO davetDTO) {
        log.debug("Request to partially update Davet : {}", davetDTO);

        return davetRepository
            .findById(davetDTO.getId())
            .map(existingDavet -> {
                davetMapper.partialUpdate(existingDavet, davetDTO);

                return existingDavet;
            })
            .map(davetRepository::save)
            .map(davetMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DavetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Davets");
        return davetRepository.findAll(pageable).map(davetMapper::toDto);
    }

    public Page<DavetDTO> findAllWithEagerRelationships(Pageable pageable) {
        return davetRepository.findAllWithEagerRelationships(pageable).map(davetMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DavetDTO> findOne(Long id) {
        log.debug("Request to get Davet : {}", id);
        return davetRepository.findOneWithEagerRelationships(id).map(davetMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Davet : {}", id);
        davetRepository.deleteById(id);
    }
}
