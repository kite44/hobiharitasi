package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Takvim;
import com.hobiharitasi.repository.TakvimRepository;
import com.hobiharitasi.service.TakvimService;
import com.hobiharitasi.service.dto.TakvimDTO;
import com.hobiharitasi.service.mapper.TakvimMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Takvim}.
 */
@Service
@Transactional
public class TakvimServiceImpl implements TakvimService {

    private final Logger log = LoggerFactory.getLogger(TakvimServiceImpl.class);

    private final TakvimRepository takvimRepository;

    private final TakvimMapper takvimMapper;

    public TakvimServiceImpl(TakvimRepository takvimRepository, TakvimMapper takvimMapper) {
        this.takvimRepository = takvimRepository;
        this.takvimMapper = takvimMapper;
    }

    @Override
    public TakvimDTO save(TakvimDTO takvimDTO) {
        log.debug("Request to save Takvim : {}", takvimDTO);
        Takvim takvim = takvimMapper.toEntity(takvimDTO);
        takvim = takvimRepository.save(takvim);
        return takvimMapper.toDto(takvim);
    }

    @Override
    public TakvimDTO update(TakvimDTO takvimDTO) {
        log.debug("Request to update Takvim : {}", takvimDTO);
        Takvim takvim = takvimMapper.toEntity(takvimDTO);
        takvim = takvimRepository.save(takvim);
        return takvimMapper.toDto(takvim);
    }

    @Override
    public Optional<TakvimDTO> partialUpdate(TakvimDTO takvimDTO) {
        log.debug("Request to partially update Takvim : {}", takvimDTO);

        return takvimRepository
            .findById(takvimDTO.getId())
            .map(existingTakvim -> {
                takvimMapper.partialUpdate(existingTakvim, takvimDTO);

                return existingTakvim;
            })
            .map(takvimRepository::save)
            .map(takvimMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TakvimDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Takvims");
        return takvimRepository.findAll(pageable).map(takvimMapper::toDto);
    }

    public Page<TakvimDTO> findAllWithEagerRelationships(Pageable pageable) {
        return takvimRepository.findAllWithEagerRelationships(pageable).map(takvimMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TakvimDTO> findOne(Long id) {
        log.debug("Request to get Takvim : {}", id);
        return takvimRepository.findOneWithEagerRelationships(id).map(takvimMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Takvim : {}", id);
        takvimRepository.deleteById(id);
    }
}
