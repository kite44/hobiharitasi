package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.repository.GrupRepository;
import com.hobiharitasi.service.GrupService;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.mapper.GrupMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Grup}.
 */
@Service
@Transactional
public class GrupServiceImpl implements GrupService {

    private final Logger log = LoggerFactory.getLogger(GrupServiceImpl.class);

    private final GrupRepository grupRepository;

    private final GrupMapper grupMapper;

    public GrupServiceImpl(GrupRepository grupRepository, GrupMapper grupMapper) {
        this.grupRepository = grupRepository;
        this.grupMapper = grupMapper;
    }

    @Override
    public GrupDTO save(GrupDTO grupDTO) {
        log.debug("Request to save Grup : {}", grupDTO);
        Grup grup = grupMapper.toEntity(grupDTO);
        grup = grupRepository.save(grup);
        return grupMapper.toDto(grup);
    }

    @Override
    public GrupDTO update(GrupDTO grupDTO) {
        log.debug("Request to update Grup : {}", grupDTO);
        Grup grup = grupMapper.toEntity(grupDTO);
        grup = grupRepository.save(grup);
        return grupMapper.toDto(grup);
    }

    @Override
    public Optional<GrupDTO> partialUpdate(GrupDTO grupDTO) {
        log.debug("Request to partially update Grup : {}", grupDTO);

        return grupRepository
            .findById(grupDTO.getId())
            .map(existingGrup -> {
                grupMapper.partialUpdate(existingGrup, grupDTO);

                return existingGrup;
            })
            .map(grupRepository::save)
            .map(grupMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GrupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Grups");
        return grupRepository.findAll(pageable).map(grupMapper::toDto);
    }

    public Page<GrupDTO> findAllWithEagerRelationships(Pageable pageable) {
        return grupRepository.findAllWithEagerRelationships(pageable).map(grupMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<GrupDTO> findOne(Long id) {
        log.debug("Request to get Grup : {}", id);
        return grupRepository.findOneWithEagerRelationships(id).map(grupMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Grup : {}", id);
        grupRepository.deleteById(id);
    }
}
