package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Tavsiye;
import com.hobiharitasi.repository.TavsiyeRepository;
import com.hobiharitasi.service.TavsiyeService;
import com.hobiharitasi.service.dto.TavsiyeDTO;
import com.hobiharitasi.service.mapper.TavsiyeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Tavsiye}.
 */
@Service
@Transactional
public class TavsiyeServiceImpl implements TavsiyeService {

    private final Logger log = LoggerFactory.getLogger(TavsiyeServiceImpl.class);

    private final TavsiyeRepository tavsiyeRepository;

    private final TavsiyeMapper tavsiyeMapper;

    public TavsiyeServiceImpl(TavsiyeRepository tavsiyeRepository, TavsiyeMapper tavsiyeMapper) {
        this.tavsiyeRepository = tavsiyeRepository;
        this.tavsiyeMapper = tavsiyeMapper;
    }

    @Override
    public TavsiyeDTO save(TavsiyeDTO tavsiyeDTO) {
        log.debug("Request to save Tavsiye : {}", tavsiyeDTO);
        Tavsiye tavsiye = tavsiyeMapper.toEntity(tavsiyeDTO);
        tavsiye = tavsiyeRepository.save(tavsiye);
        return tavsiyeMapper.toDto(tavsiye);
    }

    @Override
    public TavsiyeDTO update(TavsiyeDTO tavsiyeDTO) {
        log.debug("Request to update Tavsiye : {}", tavsiyeDTO);
        Tavsiye tavsiye = tavsiyeMapper.toEntity(tavsiyeDTO);
        tavsiye = tavsiyeRepository.save(tavsiye);
        return tavsiyeMapper.toDto(tavsiye);
    }

    @Override
    public Optional<TavsiyeDTO> partialUpdate(TavsiyeDTO tavsiyeDTO) {
        log.debug("Request to partially update Tavsiye : {}", tavsiyeDTO);

        return tavsiyeRepository
            .findById(tavsiyeDTO.getId())
            .map(existingTavsiye -> {
                tavsiyeMapper.partialUpdate(existingTavsiye, tavsiyeDTO);

                return existingTavsiye;
            })
            .map(tavsiyeRepository::save)
            .map(tavsiyeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TavsiyeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Tavsiyes");
        return tavsiyeRepository.findAll(pageable).map(tavsiyeMapper::toDto);
    }

    public Page<TavsiyeDTO> findAllWithEagerRelationships(Pageable pageable) {
        return tavsiyeRepository.findAllWithEagerRelationships(pageable).map(tavsiyeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TavsiyeDTO> findOne(Long id) {
        log.debug("Request to get Tavsiye : {}", id);
        return tavsiyeRepository.findOneWithEagerRelationships(id).map(tavsiyeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tavsiye : {}", id);
        tavsiyeRepository.deleteById(id);
    }
}
