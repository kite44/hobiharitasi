package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.KullaniciHobisi;
import com.hobiharitasi.repository.KullaniciHobisiRepository;
import com.hobiharitasi.service.KullaniciHobisiService;
import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import com.hobiharitasi.service.mapper.KullaniciHobisiMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link KullaniciHobisi}.
 */
@Service
@Transactional
public class KullaniciHobisiServiceImpl implements KullaniciHobisiService {

    private final Logger log = LoggerFactory.getLogger(KullaniciHobisiServiceImpl.class);

    private final KullaniciHobisiRepository kullaniciHobisiRepository;

    private final KullaniciHobisiMapper kullaniciHobisiMapper;

    public KullaniciHobisiServiceImpl(KullaniciHobisiRepository kullaniciHobisiRepository, KullaniciHobisiMapper kullaniciHobisiMapper) {
        this.kullaniciHobisiRepository = kullaniciHobisiRepository;
        this.kullaniciHobisiMapper = kullaniciHobisiMapper;
    }

    @Override
    public KullaniciHobisiDTO save(KullaniciHobisiDTO kullaniciHobisiDTO) {
        log.debug("Request to save KullaniciHobisi : {}", kullaniciHobisiDTO);
        KullaniciHobisi kullaniciHobisi = kullaniciHobisiMapper.toEntity(kullaniciHobisiDTO);
        kullaniciHobisi = kullaniciHobisiRepository.save(kullaniciHobisi);
        return kullaniciHobisiMapper.toDto(kullaniciHobisi);
    }

    @Override
    public KullaniciHobisiDTO update(KullaniciHobisiDTO kullaniciHobisiDTO) {
        log.debug("Request to update KullaniciHobisi : {}", kullaniciHobisiDTO);
        KullaniciHobisi kullaniciHobisi = kullaniciHobisiMapper.toEntity(kullaniciHobisiDTO);
        kullaniciHobisi = kullaniciHobisiRepository.save(kullaniciHobisi);
        return kullaniciHobisiMapper.toDto(kullaniciHobisi);
    }

    @Override
    public Optional<KullaniciHobisiDTO> partialUpdate(KullaniciHobisiDTO kullaniciHobisiDTO) {
        log.debug("Request to partially update KullaniciHobisi : {}", kullaniciHobisiDTO);

        return kullaniciHobisiRepository
            .findById(kullaniciHobisiDTO.getId())
            .map(existingKullaniciHobisi -> {
                kullaniciHobisiMapper.partialUpdate(existingKullaniciHobisi, kullaniciHobisiDTO);

                return existingKullaniciHobisi;
            })
            .map(kullaniciHobisiRepository::save)
            .map(kullaniciHobisiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KullaniciHobisiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all KullaniciHobisis");
        return kullaniciHobisiRepository.findAll(pageable).map(kullaniciHobisiMapper::toDto);
    }

    public Page<KullaniciHobisiDTO> findAllWithEagerRelationships(Pageable pageable) {
        return kullaniciHobisiRepository.findAllWithEagerRelationships(pageable).map(kullaniciHobisiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KullaniciHobisiDTO> findOne(Long id) {
        log.debug("Request to get KullaniciHobisi : {}", id);
        return kullaniciHobisiRepository.findOneWithEagerRelationships(id).map(kullaniciHobisiMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete KullaniciHobisi : {}", id);
        kullaniciHobisiRepository.deleteById(id);
    }
}
