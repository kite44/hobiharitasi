package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.repository.HobilerRepository;
import com.hobiharitasi.service.HobilerService;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.service.mapper.HobilerMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Hobiler}.
 */
@Service
@Transactional
public class HobilerServiceImpl implements HobilerService {

    private final Logger log = LoggerFactory.getLogger(HobilerServiceImpl.class);

    private final HobilerRepository hobilerRepository;

    private final HobilerMapper hobilerMapper;

    public HobilerServiceImpl(HobilerRepository hobilerRepository, HobilerMapper hobilerMapper) {
        this.hobilerRepository = hobilerRepository;
        this.hobilerMapper = hobilerMapper;
    }

    @Override
    public HobilerDTO save(HobilerDTO hobilerDTO) {
        log.debug("Request to save Hobiler : {}", hobilerDTO);
        Hobiler hobiler = hobilerMapper.toEntity(hobilerDTO);
        hobiler = hobilerRepository.save(hobiler);
        return hobilerMapper.toDto(hobiler);
    }

    @Override
    public HobilerDTO update(HobilerDTO hobilerDTO) {
        log.debug("Request to update Hobiler : {}", hobilerDTO);
        Hobiler hobiler = hobilerMapper.toEntity(hobilerDTO);
        hobiler = hobilerRepository.save(hobiler);
        return hobilerMapper.toDto(hobiler);
    }

    @Override
    public Optional<HobilerDTO> partialUpdate(HobilerDTO hobilerDTO) {
        log.debug("Request to partially update Hobiler : {}", hobilerDTO);

        return hobilerRepository
            .findById(hobilerDTO.getId())
            .map(existingHobiler -> {
                hobilerMapper.partialUpdate(existingHobiler, hobilerDTO);

                return existingHobiler;
            })
            .map(hobilerRepository::save)
            .map(hobilerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HobilerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Hobilers");
        return hobilerRepository.findAll(pageable).map(hobilerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HobilerDTO> findOne(Long id) {
        log.debug("Request to get Hobiler : {}", id);
        return hobilerRepository.findById(id).map(hobilerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Hobiler : {}", id);
        hobilerRepository.deleteById(id);
    }
}
