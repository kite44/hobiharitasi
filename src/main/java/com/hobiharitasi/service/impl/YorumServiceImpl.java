package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Yorum;
import com.hobiharitasi.repository.YorumRepository;
import com.hobiharitasi.service.YorumService;
import com.hobiharitasi.service.dto.YorumDTO;
import com.hobiharitasi.service.mapper.YorumMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Yorum}.
 */
@Service
@Transactional
public class YorumServiceImpl implements YorumService {

    private final Logger log = LoggerFactory.getLogger(YorumServiceImpl.class);

    private final YorumRepository yorumRepository;

    private final YorumMapper yorumMapper;

    public YorumServiceImpl(YorumRepository yorumRepository, YorumMapper yorumMapper) {
        this.yorumRepository = yorumRepository;
        this.yorumMapper = yorumMapper;
    }

    @Override
    public YorumDTO save(YorumDTO yorumDTO) {
        log.debug("Request to save Yorum : {}", yorumDTO);
        Yorum yorum = yorumMapper.toEntity(yorumDTO);
        yorum = yorumRepository.save(yorum);
        return yorumMapper.toDto(yorum);
    }

    @Override
    public YorumDTO update(YorumDTO yorumDTO) {
        log.debug("Request to update Yorum : {}", yorumDTO);
        Yorum yorum = yorumMapper.toEntity(yorumDTO);
        yorum = yorumRepository.save(yorum);
        return yorumMapper.toDto(yorum);
    }

    @Override
    public Optional<YorumDTO> partialUpdate(YorumDTO yorumDTO) {
        log.debug("Request to partially update Yorum : {}", yorumDTO);

        return yorumRepository
            .findById(yorumDTO.getId())
            .map(existingYorum -> {
                yorumMapper.partialUpdate(existingYorum, yorumDTO);

                return existingYorum;
            })
            .map(yorumRepository::save)
            .map(yorumMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<YorumDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Yorums");
        return yorumRepository.findAll(pageable).map(yorumMapper::toDto);
    }

    public Page<YorumDTO> findAllWithEagerRelationships(Pageable pageable) {
        return yorumRepository.findAllWithEagerRelationships(pageable).map(yorumMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<YorumDTO> findOne(Long id) {
        log.debug("Request to get Yorum : {}", id);
        return yorumRepository.findOneWithEagerRelationships(id).map(yorumMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Yorum : {}", id);
        yorumRepository.deleteById(id);
    }
}
