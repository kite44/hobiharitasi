package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.repository.KategoriRepository;
import com.hobiharitasi.service.KategoriService;
import com.hobiharitasi.service.dto.KategoriDTO;
import com.hobiharitasi.service.mapper.KategoriMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Kategori}.
 */
@Service
@Transactional
public class KategoriServiceImpl implements KategoriService {

    private final Logger log = LoggerFactory.getLogger(KategoriServiceImpl.class);

    private final KategoriRepository kategoriRepository;

    private final KategoriMapper kategoriMapper;

    public KategoriServiceImpl(KategoriRepository kategoriRepository, KategoriMapper kategoriMapper) {
        this.kategoriRepository = kategoriRepository;
        this.kategoriMapper = kategoriMapper;
    }

    @Override
    public KategoriDTO save(KategoriDTO kategoriDTO) {
        log.debug("Request to save Kategori : {}", kategoriDTO);
        Kategori kategori = kategoriMapper.toEntity(kategoriDTO);
        kategori = kategoriRepository.save(kategori);
        return kategoriMapper.toDto(kategori);
    }

    @Override
    public KategoriDTO update(KategoriDTO kategoriDTO) {
        log.debug("Request to update Kategori : {}", kategoriDTO);
        Kategori kategori = kategoriMapper.toEntity(kategoriDTO);
        kategori = kategoriRepository.save(kategori);
        return kategoriMapper.toDto(kategori);
    }

    @Override
    public Optional<KategoriDTO> partialUpdate(KategoriDTO kategoriDTO) {
        log.debug("Request to partially update Kategori : {}", kategoriDTO);

        return kategoriRepository
            .findById(kategoriDTO.getId())
            .map(existingKategori -> {
                kategoriMapper.partialUpdate(existingKategori, kategoriDTO);

                return existingKategori;
            })
            .map(kategoriRepository::save)
            .map(kategoriMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KategoriDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Kategoris");
        return kategoriRepository.findAll(pageable).map(kategoriMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KategoriDTO> findOne(Long id) {
        log.debug("Request to get Kategori : {}", id);
        return kategoriRepository.findById(id).map(kategoriMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kategori : {}", id);
        kategoriRepository.deleteById(id);
    }
}
