package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.repository.EtkinlikRepository;
import com.hobiharitasi.service.EtkinlikService;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.mapper.EtkinlikMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Etkinlik}.
 */
@Service
@Transactional
public class EtkinlikServiceImpl implements EtkinlikService {

    private final Logger log = LoggerFactory.getLogger(EtkinlikServiceImpl.class);

    private final EtkinlikRepository etkinlikRepository;

    private final EtkinlikMapper etkinlikMapper;

    public EtkinlikServiceImpl(EtkinlikRepository etkinlikRepository, EtkinlikMapper etkinlikMapper) {
        this.etkinlikRepository = etkinlikRepository;
        this.etkinlikMapper = etkinlikMapper;
    }

    @Override
    public EtkinlikDTO save(EtkinlikDTO etkinlikDTO) {
        log.debug("Request to save Etkinlik : {}", etkinlikDTO);
        Etkinlik etkinlik = etkinlikMapper.toEntity(etkinlikDTO);
        etkinlik = etkinlikRepository.save(etkinlik);
        return etkinlikMapper.toDto(etkinlik);
    }

    @Override
    public EtkinlikDTO update(EtkinlikDTO etkinlikDTO) {
        log.debug("Request to update Etkinlik : {}", etkinlikDTO);
        Etkinlik etkinlik = etkinlikMapper.toEntity(etkinlikDTO);
        etkinlik = etkinlikRepository.save(etkinlik);
        return etkinlikMapper.toDto(etkinlik);
    }

    @Override
    public Optional<EtkinlikDTO> partialUpdate(EtkinlikDTO etkinlikDTO) {
        log.debug("Request to partially update Etkinlik : {}", etkinlikDTO);

        return etkinlikRepository
            .findById(etkinlikDTO.getId())
            .map(existingEtkinlik -> {
                etkinlikMapper.partialUpdate(existingEtkinlik, etkinlikDTO);

                return existingEtkinlik;
            })
            .map(etkinlikRepository::save)
            .map(etkinlikMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EtkinlikDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Etkinliks");
        return etkinlikRepository.findAll(pageable).map(etkinlikMapper::toDto);
    }

    public Page<EtkinlikDTO> findAllWithEagerRelationships(Pageable pageable) {
        return etkinlikRepository.findAllWithEagerRelationships(pageable).map(etkinlikMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EtkinlikDTO> findOne(Long id) {
        log.debug("Request to get Etkinlik : {}", id);
        return etkinlikRepository.findOneWithEagerRelationships(id).map(etkinlikMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Etkinlik : {}", id);
        etkinlikRepository.deleteById(id);
    }
}
