package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.repository.KullaniciRepository;
import com.hobiharitasi.service.KullaniciService;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.mapper.KullaniciMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Kullanici}.
 */
@Service
@Transactional
public class KullaniciServiceImpl implements KullaniciService {

    private final Logger log = LoggerFactory.getLogger(KullaniciServiceImpl.class);

    private final KullaniciRepository kullaniciRepository;

    private final KullaniciMapper kullaniciMapper;

    public KullaniciServiceImpl(KullaniciRepository kullaniciRepository, KullaniciMapper kullaniciMapper) {
        this.kullaniciRepository = kullaniciRepository;
        this.kullaniciMapper = kullaniciMapper;
    }

    @Override
    public KullaniciDTO save(KullaniciDTO kullaniciDTO) {
        log.debug("Request to save Kullanici : {}", kullaniciDTO);
        Kullanici kullanici = kullaniciMapper.toEntity(kullaniciDTO);
        kullanici = kullaniciRepository.save(kullanici);
        return kullaniciMapper.toDto(kullanici);
    }

    @Override
    public KullaniciDTO update(KullaniciDTO kullaniciDTO) {
        log.debug("Request to update Kullanici : {}", kullaniciDTO);
        Kullanici kullanici = kullaniciMapper.toEntity(kullaniciDTO);
        kullanici = kullaniciRepository.save(kullanici);
        return kullaniciMapper.toDto(kullanici);
    }

    @Override
    public Optional<KullaniciDTO> partialUpdate(KullaniciDTO kullaniciDTO) {
        log.debug("Request to partially update Kullanici : {}", kullaniciDTO);

        return kullaniciRepository
            .findById(kullaniciDTO.getId())
            .map(existingKullanici -> {
                kullaniciMapper.partialUpdate(existingKullanici, kullaniciDTO);

                return existingKullanici;
            })
            .map(kullaniciRepository::save)
            .map(kullaniciMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<KullaniciDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Kullanicis");
        return kullaniciRepository.findAll(pageable).map(kullaniciMapper::toDto);
    }

    public Page<KullaniciDTO> findAllWithEagerRelationships(Pageable pageable) {
        return kullaniciRepository.findAllWithEagerRelationships(pageable).map(kullaniciMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<KullaniciDTO> findOne(Long id) {
        log.debug("Request to get Kullanici : {}", id);
        return kullaniciRepository.findOneWithEagerRelationships(id).map(kullaniciMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Kullanici : {}", id);
        kullaniciRepository.deleteById(id);
    }
}
