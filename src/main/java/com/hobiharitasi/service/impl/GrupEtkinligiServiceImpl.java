package com.hobiharitasi.service.impl;

import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.repository.GrupEtkinligiRepository;
import com.hobiharitasi.service.GrupEtkinligiService;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import com.hobiharitasi.service.mapper.GrupEtkinligiMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link GrupEtkinligi}.
 */
@Service
@Transactional
public class GrupEtkinligiServiceImpl implements GrupEtkinligiService {

    private final Logger log = LoggerFactory.getLogger(GrupEtkinligiServiceImpl.class);

    private final GrupEtkinligiRepository grupEtkinligiRepository;

    private final GrupEtkinligiMapper grupEtkinligiMapper;

    public GrupEtkinligiServiceImpl(GrupEtkinligiRepository grupEtkinligiRepository, GrupEtkinligiMapper grupEtkinligiMapper) {
        this.grupEtkinligiRepository = grupEtkinligiRepository;
        this.grupEtkinligiMapper = grupEtkinligiMapper;
    }

    @Override
    public GrupEtkinligiDTO save(GrupEtkinligiDTO grupEtkinligiDTO) {
        log.debug("Request to save GrupEtkinligi : {}", grupEtkinligiDTO);
        GrupEtkinligi grupEtkinligi = grupEtkinligiMapper.toEntity(grupEtkinligiDTO);
        grupEtkinligi = grupEtkinligiRepository.save(grupEtkinligi);
        return grupEtkinligiMapper.toDto(grupEtkinligi);
    }

    @Override
    public GrupEtkinligiDTO update(GrupEtkinligiDTO grupEtkinligiDTO) {
        log.debug("Request to update GrupEtkinligi : {}", grupEtkinligiDTO);
        GrupEtkinligi grupEtkinligi = grupEtkinligiMapper.toEntity(grupEtkinligiDTO);
        grupEtkinligi = grupEtkinligiRepository.save(grupEtkinligi);
        return grupEtkinligiMapper.toDto(grupEtkinligi);
    }

    @Override
    public Optional<GrupEtkinligiDTO> partialUpdate(GrupEtkinligiDTO grupEtkinligiDTO) {
        log.debug("Request to partially update GrupEtkinligi : {}", grupEtkinligiDTO);

        return grupEtkinligiRepository
            .findById(grupEtkinligiDTO.getId())
            .map(existingGrupEtkinligi -> {
                grupEtkinligiMapper.partialUpdate(existingGrupEtkinligi, grupEtkinligiDTO);

                return existingGrupEtkinligi;
            })
            .map(grupEtkinligiRepository::save)
            .map(grupEtkinligiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GrupEtkinligiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GrupEtkinligis");
        return grupEtkinligiRepository.findAll(pageable).map(grupEtkinligiMapper::toDto);
    }

    public Page<GrupEtkinligiDTO> findAllWithEagerRelationships(Pageable pageable) {
        return grupEtkinligiRepository.findAllWithEagerRelationships(pageable).map(grupEtkinligiMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<GrupEtkinligiDTO> findOne(Long id) {
        log.debug("Request to get GrupEtkinligi : {}", id);
        return grupEtkinligiRepository.findOneWithEagerRelationships(id).map(grupEtkinligiMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete GrupEtkinligi : {}", id);
        grupEtkinligiRepository.deleteById(id);
    }
}
