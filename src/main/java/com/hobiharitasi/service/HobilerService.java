package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.HobilerDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Hobiler}.
 */
public interface HobilerService {
    /**
     * Save a hobiler.
     *
     * @param hobilerDTO the entity to save.
     * @return the persisted entity.
     */
    HobilerDTO save(HobilerDTO hobilerDTO);

    /**
     * Updates a hobiler.
     *
     * @param hobilerDTO the entity to update.
     * @return the persisted entity.
     */
    HobilerDTO update(HobilerDTO hobilerDTO);

    /**
     * Partially updates a hobiler.
     *
     * @param hobilerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<HobilerDTO> partialUpdate(HobilerDTO hobilerDTO);

    /**
     * Get all the hobilers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HobilerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" hobiler.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HobilerDTO> findOne(Long id);

    /**
     * Delete the "id" hobiler.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
