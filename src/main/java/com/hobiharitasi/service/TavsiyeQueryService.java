package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Tavsiye;
import com.hobiharitasi.repository.TavsiyeRepository;
import com.hobiharitasi.service.criteria.TavsiyeCriteria;
import com.hobiharitasi.service.dto.TavsiyeDTO;
import com.hobiharitasi.service.mapper.TavsiyeMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Tavsiye} entities in the database.
 * The main input is a {@link TavsiyeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TavsiyeDTO} or a {@link Page} of {@link TavsiyeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TavsiyeQueryService extends QueryService<Tavsiye> {

    private final Logger log = LoggerFactory.getLogger(TavsiyeQueryService.class);

    private final TavsiyeRepository tavsiyeRepository;

    private final TavsiyeMapper tavsiyeMapper;

    public TavsiyeQueryService(TavsiyeRepository tavsiyeRepository, TavsiyeMapper tavsiyeMapper) {
        this.tavsiyeRepository = tavsiyeRepository;
        this.tavsiyeMapper = tavsiyeMapper;
    }

    /**
     * Return a {@link List} of {@link TavsiyeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TavsiyeDTO> findByCriteria(TavsiyeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Tavsiye> specification = createSpecification(criteria);
        return tavsiyeMapper.toDto(tavsiyeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TavsiyeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TavsiyeDTO> findByCriteria(TavsiyeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Tavsiye> specification = createSpecification(criteria);
        return tavsiyeRepository.findAll(specification, page).map(tavsiyeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TavsiyeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Tavsiye> specification = createSpecification(criteria);
        return tavsiyeRepository.count(specification);
    }

    /**
     * Function to convert {@link TavsiyeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Tavsiye> createSpecification(TavsiyeCriteria criteria) {
        Specification<Tavsiye> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Tavsiye_.id));
            }
            if (criteria.getBaslik() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBaslik(), Tavsiye_.baslik));
            }
            if (criteria.getIcerik() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIcerik(), Tavsiye_.icerik));
            }
            if (criteria.getOlusturulmaTarihi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOlusturulmaTarihi(), Tavsiye_.olusturulmaTarihi));
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(Tavsiye_.kullanici, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(Tavsiye_.kullanici, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
