package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.GrupEtkinligi} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.GrupEtkinligiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /grup-etkinligis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GrupEtkinligiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter etkinlikAdi;

    private StringFilter aciklama;

    private InstantFilter tarih;

    private StringFilter konum;

    private LongFilter groupId;

    private LongFilter grupId;

    private Boolean distinct;

    public GrupEtkinligiCriteria() {}

    public GrupEtkinligiCriteria(GrupEtkinligiCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.etkinlikAdi = other.etkinlikAdi == null ? null : other.etkinlikAdi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.tarih = other.tarih == null ? null : other.tarih.copy();
        this.konum = other.konum == null ? null : other.konum.copy();
        this.groupId = other.groupId == null ? null : other.groupId.copy();
        this.grupId = other.grupId == null ? null : other.grupId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public GrupEtkinligiCriteria copy() {
        return new GrupEtkinligiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEtkinlikAdi() {
        return etkinlikAdi;
    }

    public StringFilter etkinlikAdi() {
        if (etkinlikAdi == null) {
            etkinlikAdi = new StringFilter();
        }
        return etkinlikAdi;
    }

    public void setEtkinlikAdi(StringFilter etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public InstantFilter getTarih() {
        return tarih;
    }

    public InstantFilter tarih() {
        if (tarih == null) {
            tarih = new InstantFilter();
        }
        return tarih;
    }

    public void setTarih(InstantFilter tarih) {
        this.tarih = tarih;
    }

    public StringFilter getKonum() {
        return konum;
    }

    public StringFilter konum() {
        if (konum == null) {
            konum = new StringFilter();
        }
        return konum;
    }

    public void setKonum(StringFilter konum) {
        this.konum = konum;
    }

    public LongFilter getGroupId() {
        return groupId;
    }

    public LongFilter groupId() {
        if (groupId == null) {
            groupId = new LongFilter();
        }
        return groupId;
    }

    public void setGroupId(LongFilter groupId) {
        this.groupId = groupId;
    }

    public LongFilter getGrupId() {
        return grupId;
    }

    public LongFilter grupId() {
        if (grupId == null) {
            grupId = new LongFilter();
        }
        return grupId;
    }

    public void setGrupId(LongFilter grupId) {
        this.grupId = grupId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final GrupEtkinligiCriteria that = (GrupEtkinligiCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(etkinlikAdi, that.etkinlikAdi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(tarih, that.tarih) &&
            Objects.equals(konum, that.konum) &&
            Objects.equals(groupId, that.groupId) &&
            Objects.equals(grupId, that.grupId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, etkinlikAdi, aciklama, tarih, konum, groupId, grupId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GrupEtkinligiCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (etkinlikAdi != null ? "etkinlikAdi=" + etkinlikAdi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (tarih != null ? "tarih=" + tarih + ", " : "") +
            (konum != null ? "konum=" + konum + ", " : "") +
            (groupId != null ? "groupId=" + groupId + ", " : "") +
            (grupId != null ? "grupId=" + grupId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
