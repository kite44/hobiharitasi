package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Takvim} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.TakvimResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /takvims?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TakvimCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private InstantFilter etkinlikTarihi;

    private StringFilter etkinlikBasligi;

    private StringFilter aciklama;

    private LongFilter kullaniciId;

    private LongFilter kullaniciId;

    private Boolean distinct;

    public TakvimCriteria() {}

    public TakvimCriteria(TakvimCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.etkinlikTarihi = other.etkinlikTarihi == null ? null : other.etkinlikTarihi.copy();
        this.etkinlikBasligi = other.etkinlikBasligi == null ? null : other.etkinlikBasligi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public TakvimCriteria copy() {
        return new TakvimCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public InstantFilter getEtkinlikTarihi() {
        return etkinlikTarihi;
    }

    public InstantFilter etkinlikTarihi() {
        if (etkinlikTarihi == null) {
            etkinlikTarihi = new InstantFilter();
        }
        return etkinlikTarihi;
    }

    public void setEtkinlikTarihi(InstantFilter etkinlikTarihi) {
        this.etkinlikTarihi = etkinlikTarihi;
    }

    public StringFilter getEtkinlikBasligi() {
        return etkinlikBasligi;
    }

    public StringFilter etkinlikBasligi() {
        if (etkinlikBasligi == null) {
            etkinlikBasligi = new StringFilter();
        }
        return etkinlikBasligi;
    }

    public void setEtkinlikBasligi(StringFilter etkinlikBasligi) {
        this.etkinlikBasligi = etkinlikBasligi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TakvimCriteria that = (TakvimCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(etkinlikTarihi, that.etkinlikTarihi) &&
            Objects.equals(etkinlikBasligi, that.etkinlikBasligi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, etkinlikTarihi, etkinlikBasligi, aciklama, kullaniciId, kullaniciId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TakvimCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (etkinlikTarihi != null ? "etkinlikTarihi=" + etkinlikTarihi + ", " : "") +
            (etkinlikBasligi != null ? "etkinlikBasligi=" + etkinlikBasligi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
