package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Kullanici} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.KullaniciResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /kullanicis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KullaniciCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter ad;

    private StringFilter soyad;

    private StringFilter email;

    private StringFilter sifre;

    private IntegerFilter telNo;

    private StringFilter biyografi;

    private StringFilter adres;

    private StringFilter profilResmi;

    private StringFilter accesCode;

    private StringFilter jwtCode;

    private LongFilter tavsiyeId;

    private LongFilter takvimId;

    private LongFilter userId;

    private LongFilter etkinlikId;

    private LongFilter grupId;

    private LongFilter hobiId;

    private Boolean distinct;

    public KullaniciCriteria() {}

    public KullaniciCriteria(KullaniciCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ad = other.ad == null ? null : other.ad.copy();
        this.soyad = other.soyad == null ? null : other.soyad.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.sifre = other.sifre == null ? null : other.sifre.copy();
        this.telNo = other.telNo == null ? null : other.telNo.copy();
        this.biyografi = other.biyografi == null ? null : other.biyografi.copy();
        this.adres = other.adres == null ? null : other.adres.copy();
        this.profilResmi = other.profilResmi == null ? null : other.profilResmi.copy();
        this.accesCode = other.accesCode == null ? null : other.accesCode.copy();
        this.jwtCode = other.jwtCode == null ? null : other.jwtCode.copy();
        this.tavsiyeId = other.tavsiyeId == null ? null : other.tavsiyeId.copy();
        this.takvimId = other.takvimId == null ? null : other.takvimId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.etkinlikId = other.etkinlikId == null ? null : other.etkinlikId.copy();
        this.grupId = other.grupId == null ? null : other.grupId.copy();
        this.hobiId = other.hobiId == null ? null : other.hobiId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public KullaniciCriteria copy() {
        return new KullaniciCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAd() {
        return ad;
    }

    public StringFilter ad() {
        if (ad == null) {
            ad = new StringFilter();
        }
        return ad;
    }

    public void setAd(StringFilter ad) {
        this.ad = ad;
    }

    public StringFilter getSoyad() {
        return soyad;
    }

    public StringFilter soyad() {
        if (soyad == null) {
            soyad = new StringFilter();
        }
        return soyad;
    }

    public void setSoyad(StringFilter soyad) {
        this.soyad = soyad;
    }

    public StringFilter getEmail() {
        return email;
    }

    public StringFilter email() {
        if (email == null) {
            email = new StringFilter();
        }
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getSifre() {
        return sifre;
    }

    public StringFilter sifre() {
        if (sifre == null) {
            sifre = new StringFilter();
        }
        return sifre;
    }

    public void setSifre(StringFilter sifre) {
        this.sifre = sifre;
    }

    public IntegerFilter getTelNo() {
        return telNo;
    }

    public IntegerFilter telNo() {
        if (telNo == null) {
            telNo = new IntegerFilter();
        }
        return telNo;
    }

    public void setTelNo(IntegerFilter telNo) {
        this.telNo = telNo;
    }

    public StringFilter getBiyografi() {
        return biyografi;
    }

    public StringFilter biyografi() {
        if (biyografi == null) {
            biyografi = new StringFilter();
        }
        return biyografi;
    }

    public void setBiyografi(StringFilter biyografi) {
        this.biyografi = biyografi;
    }

    public StringFilter getAdres() {
        return adres;
    }

    public StringFilter adres() {
        if (adres == null) {
            adres = new StringFilter();
        }
        return adres;
    }

    public void setAdres(StringFilter adres) {
        this.adres = adres;
    }

    public StringFilter getProfilResmi() {
        return profilResmi;
    }

    public StringFilter profilResmi() {
        if (profilResmi == null) {
            profilResmi = new StringFilter();
        }
        return profilResmi;
    }

    public void setProfilResmi(StringFilter profilResmi) {
        this.profilResmi = profilResmi;
    }

    public StringFilter getAccesCode() {
        return accesCode;
    }

    public StringFilter accesCode() {
        if (accesCode == null) {
            accesCode = new StringFilter();
        }
        return accesCode;
    }

    public void setAccesCode(StringFilter accesCode) {
        this.accesCode = accesCode;
    }

    public StringFilter getJwtCode() {
        return jwtCode;
    }

    public StringFilter jwtCode() {
        if (jwtCode == null) {
            jwtCode = new StringFilter();
        }
        return jwtCode;
    }

    public void setJwtCode(StringFilter jwtCode) {
        this.jwtCode = jwtCode;
    }

    public LongFilter getTavsiyeId() {
        return tavsiyeId;
    }

    public LongFilter tavsiyeId() {
        if (tavsiyeId == null) {
            tavsiyeId = new LongFilter();
        }
        return tavsiyeId;
    }

    public void setTavsiyeId(LongFilter tavsiyeId) {
        this.tavsiyeId = tavsiyeId;
    }

    public LongFilter getTakvimId() {
        return takvimId;
    }

    public LongFilter takvimId() {
        if (takvimId == null) {
            takvimId = new LongFilter();
        }
        return takvimId;
    }

    public void setTakvimId(LongFilter takvimId) {
        this.takvimId = takvimId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getEtkinlikId() {
        return etkinlikId;
    }

    public LongFilter etkinlikId() {
        if (etkinlikId == null) {
            etkinlikId = new LongFilter();
        }
        return etkinlikId;
    }

    public void setEtkinlikId(LongFilter etkinlikId) {
        this.etkinlikId = etkinlikId;
    }

    public LongFilter getGrupId() {
        return grupId;
    }

    public LongFilter grupId() {
        if (grupId == null) {
            grupId = new LongFilter();
        }
        return grupId;
    }

    public void setGrupId(LongFilter grupId) {
        this.grupId = grupId;
    }

    public LongFilter getHobiId() {
        return hobiId;
    }

    public LongFilter hobiId() {
        if (hobiId == null) {
            hobiId = new LongFilter();
        }
        return hobiId;
    }

    public void setHobiId(LongFilter hobiId) {
        this.hobiId = hobiId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KullaniciCriteria that = (KullaniciCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(ad, that.ad) &&
            Objects.equals(soyad, that.soyad) &&
            Objects.equals(email, that.email) &&
            Objects.equals(sifre, that.sifre) &&
            Objects.equals(telNo, that.telNo) &&
            Objects.equals(biyografi, that.biyografi) &&
            Objects.equals(adres, that.adres) &&
            Objects.equals(profilResmi, that.profilResmi) &&
            Objects.equals(accesCode, that.accesCode) &&
            Objects.equals(jwtCode, that.jwtCode) &&
            Objects.equals(tavsiyeId, that.tavsiyeId) &&
            Objects.equals(takvimId, that.takvimId) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(etkinlikId, that.etkinlikId) &&
            Objects.equals(grupId, that.grupId) &&
            Objects.equals(hobiId, that.hobiId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            ad,
            soyad,
            email,
            sifre,
            telNo,
            biyografi,
            adres,
            profilResmi,
            accesCode,
            jwtCode,
            tavsiyeId,
            takvimId,
            userId,
            etkinlikId,
            grupId,
            hobiId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KullaniciCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (ad != null ? "ad=" + ad + ", " : "") +
            (soyad != null ? "soyad=" + soyad + ", " : "") +
            (email != null ? "email=" + email + ", " : "") +
            (sifre != null ? "sifre=" + sifre + ", " : "") +
            (telNo != null ? "telNo=" + telNo + ", " : "") +
            (biyografi != null ? "biyografi=" + biyografi + ", " : "") +
            (adres != null ? "adres=" + adres + ", " : "") +
            (profilResmi != null ? "profilResmi=" + profilResmi + ", " : "") +
            (accesCode != null ? "accesCode=" + accesCode + ", " : "") +
            (jwtCode != null ? "jwtCode=" + jwtCode + ", " : "") +
            (tavsiyeId != null ? "tavsiyeId=" + tavsiyeId + ", " : "") +
            (takvimId != null ? "takvimId=" + takvimId + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (etkinlikId != null ? "etkinlikId=" + etkinlikId + ", " : "") +
            (grupId != null ? "grupId=" + grupId + ", " : "") +
            (hobiId != null ? "hobiId=" + hobiId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
