package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Etkinlik} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.EtkinlikResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /etkinliks?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EtkinlikCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter etkinlikAdi;

    private StringFilter aciklama;

    private InstantFilter tarih;

    private StringFilter konum;

    private IntegerFilter maksimumKatilimci;

    private IntegerFilter minimumKatilimci;

    private StringFilter etkinlikKategorisi;

    private LongFilter kategoriId;

    private Boolean distinct;

    public EtkinlikCriteria() {}

    public EtkinlikCriteria(EtkinlikCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.etkinlikAdi = other.etkinlikAdi == null ? null : other.etkinlikAdi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.tarih = other.tarih == null ? null : other.tarih.copy();
        this.konum = other.konum == null ? null : other.konum.copy();
        this.maksimumKatilimci = other.maksimumKatilimci == null ? null : other.maksimumKatilimci.copy();
        this.minimumKatilimci = other.minimumKatilimci == null ? null : other.minimumKatilimci.copy();
        this.etkinlikKategorisi = other.etkinlikKategorisi == null ? null : other.etkinlikKategorisi.copy();
        this.kategoriId = other.kategoriId == null ? null : other.kategoriId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public EtkinlikCriteria copy() {
        return new EtkinlikCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEtkinlikAdi() {
        return etkinlikAdi;
    }

    public StringFilter etkinlikAdi() {
        if (etkinlikAdi == null) {
            etkinlikAdi = new StringFilter();
        }
        return etkinlikAdi;
    }

    public void setEtkinlikAdi(StringFilter etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public InstantFilter getTarih() {
        return tarih;
    }

    public InstantFilter tarih() {
        if (tarih == null) {
            tarih = new InstantFilter();
        }
        return tarih;
    }

    public void setTarih(InstantFilter tarih) {
        this.tarih = tarih;
    }

    public StringFilter getKonum() {
        return konum;
    }

    public StringFilter konum() {
        if (konum == null) {
            konum = new StringFilter();
        }
        return konum;
    }

    public void setKonum(StringFilter konum) {
        this.konum = konum;
    }

    public IntegerFilter getMaksimumKatilimci() {
        return maksimumKatilimci;
    }

    public IntegerFilter maksimumKatilimci() {
        if (maksimumKatilimci == null) {
            maksimumKatilimci = new IntegerFilter();
        }
        return maksimumKatilimci;
    }

    public void setMaksimumKatilimci(IntegerFilter maksimumKatilimci) {
        this.maksimumKatilimci = maksimumKatilimci;
    }

    public IntegerFilter getMinimumKatilimci() {
        return minimumKatilimci;
    }

    public IntegerFilter minimumKatilimci() {
        if (minimumKatilimci == null) {
            minimumKatilimci = new IntegerFilter();
        }
        return minimumKatilimci;
    }

    public void setMinimumKatilimci(IntegerFilter minimumKatilimci) {
        this.minimumKatilimci = minimumKatilimci;
    }

    public StringFilter getEtkinlikKategorisi() {
        return etkinlikKategorisi;
    }

    public StringFilter etkinlikKategorisi() {
        if (etkinlikKategorisi == null) {
            etkinlikKategorisi = new StringFilter();
        }
        return etkinlikKategorisi;
    }

    public void setEtkinlikKategorisi(StringFilter etkinlikKategorisi) {
        this.etkinlikKategorisi = etkinlikKategorisi;
    }

    public LongFilter getKategoriId() {
        return kategoriId;
    }

    public LongFilter kategoriId() {
        if (kategoriId == null) {
            kategoriId = new LongFilter();
        }
        return kategoriId;
    }

    public void setKategoriId(LongFilter kategoriId) {
        this.kategoriId = kategoriId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EtkinlikCriteria that = (EtkinlikCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(etkinlikAdi, that.etkinlikAdi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(tarih, that.tarih) &&
            Objects.equals(konum, that.konum) &&
            Objects.equals(maksimumKatilimci, that.maksimumKatilimci) &&
            Objects.equals(minimumKatilimci, that.minimumKatilimci) &&
            Objects.equals(etkinlikKategorisi, that.etkinlikKategorisi) &&
            Objects.equals(kategoriId, that.kategoriId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            etkinlikAdi,
            aciklama,
            tarih,
            konum,
            maksimumKatilimci,
            minimumKatilimci,
            etkinlikKategorisi,
            kategoriId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtkinlikCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (etkinlikAdi != null ? "etkinlikAdi=" + etkinlikAdi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (tarih != null ? "tarih=" + tarih + ", " : "") +
            (konum != null ? "konum=" + konum + ", " : "") +
            (maksimumKatilimci != null ? "maksimumKatilimci=" + maksimumKatilimci + ", " : "") +
            (minimumKatilimci != null ? "minimumKatilimci=" + minimumKatilimci + ", " : "") +
            (etkinlikKategorisi != null ? "etkinlikKategorisi=" + etkinlikKategorisi + ", " : "") +
            (kategoriId != null ? "kategoriId=" + kategoriId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
