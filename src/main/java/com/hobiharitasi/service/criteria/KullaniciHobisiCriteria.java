package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.KullaniciHobisi} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.KullaniciHobisiResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /kullanici-hobisis?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KullaniciHobisiCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter seviye;

    private LongFilter hobiId;

    private LongFilter kullaniciId;

    private Boolean distinct;

    public KullaniciHobisiCriteria() {}

    public KullaniciHobisiCriteria(KullaniciHobisiCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.seviye = other.seviye == null ? null : other.seviye.copy();
        this.hobiId = other.hobiId == null ? null : other.hobiId.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public KullaniciHobisiCriteria copy() {
        return new KullaniciHobisiCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getSeviye() {
        return seviye;
    }

    public IntegerFilter seviye() {
        if (seviye == null) {
            seviye = new IntegerFilter();
        }
        return seviye;
    }

    public void setSeviye(IntegerFilter seviye) {
        this.seviye = seviye;
    }

    public LongFilter getHobiId() {
        return hobiId;
    }

    public LongFilter hobiId() {
        if (hobiId == null) {
            hobiId = new LongFilter();
        }
        return hobiId;
    }

    public void setHobiId(LongFilter hobiId) {
        this.hobiId = hobiId;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KullaniciHobisiCriteria that = (KullaniciHobisiCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(seviye, that.seviye) &&
            Objects.equals(hobiId, that.hobiId) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, seviye, hobiId, kullaniciId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KullaniciHobisiCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (seviye != null ? "seviye=" + seviye + ", " : "") +
            (hobiId != null ? "hobiId=" + hobiId + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
