package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Tavsiye} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.TavsiyeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tavsiyes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TavsiyeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter baslik;

    private StringFilter icerik;

    private InstantFilter olusturulmaTarihi;

    private LongFilter kullaniciId;

    private LongFilter kullaniciId;

    private Boolean distinct;

    public TavsiyeCriteria() {}

    public TavsiyeCriteria(TavsiyeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.baslik = other.baslik == null ? null : other.baslik.copy();
        this.icerik = other.icerik == null ? null : other.icerik.copy();
        this.olusturulmaTarihi = other.olusturulmaTarihi == null ? null : other.olusturulmaTarihi.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public TavsiyeCriteria copy() {
        return new TavsiyeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getBaslik() {
        return baslik;
    }

    public StringFilter baslik() {
        if (baslik == null) {
            baslik = new StringFilter();
        }
        return baslik;
    }

    public void setBaslik(StringFilter baslik) {
        this.baslik = baslik;
    }

    public StringFilter getIcerik() {
        return icerik;
    }

    public StringFilter icerik() {
        if (icerik == null) {
            icerik = new StringFilter();
        }
        return icerik;
    }

    public void setIcerik(StringFilter icerik) {
        this.icerik = icerik;
    }

    public InstantFilter getOlusturulmaTarihi() {
        return olusturulmaTarihi;
    }

    public InstantFilter olusturulmaTarihi() {
        if (olusturulmaTarihi == null) {
            olusturulmaTarihi = new InstantFilter();
        }
        return olusturulmaTarihi;
    }

    public void setOlusturulmaTarihi(InstantFilter olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TavsiyeCriteria that = (TavsiyeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(baslik, that.baslik) &&
            Objects.equals(icerik, that.icerik) &&
            Objects.equals(olusturulmaTarihi, that.olusturulmaTarihi) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, baslik, icerik, olusturulmaTarihi, kullaniciId, kullaniciId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TavsiyeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (baslik != null ? "baslik=" + baslik + ", " : "") +
            (icerik != null ? "icerik=" + icerik + ", " : "") +
            (olusturulmaTarihi != null ? "olusturulmaTarihi=" + olusturulmaTarihi + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
