package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Hobiler} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.HobilerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /hobilers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class HobilerCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter hobiAdi;

    private StringFilter aciklama;

    private StringFilter lokasyon;

    private StringFilter cesidi;

    private LongFilter kullaniciId;

    private Boolean distinct;

    public HobilerCriteria() {}

    public HobilerCriteria(HobilerCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.hobiAdi = other.hobiAdi == null ? null : other.hobiAdi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.lokasyon = other.lokasyon == null ? null : other.lokasyon.copy();
        this.cesidi = other.cesidi == null ? null : other.cesidi.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public HobilerCriteria copy() {
        return new HobilerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getHobiAdi() {
        return hobiAdi;
    }

    public StringFilter hobiAdi() {
        if (hobiAdi == null) {
            hobiAdi = new StringFilter();
        }
        return hobiAdi;
    }

    public void setHobiAdi(StringFilter hobiAdi) {
        this.hobiAdi = hobiAdi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public StringFilter getLokasyon() {
        return lokasyon;
    }

    public StringFilter lokasyon() {
        if (lokasyon == null) {
            lokasyon = new StringFilter();
        }
        return lokasyon;
    }

    public void setLokasyon(StringFilter lokasyon) {
        this.lokasyon = lokasyon;
    }

    public StringFilter getCesidi() {
        return cesidi;
    }

    public StringFilter cesidi() {
        if (cesidi == null) {
            cesidi = new StringFilter();
        }
        return cesidi;
    }

    public void setCesidi(StringFilter cesidi) {
        this.cesidi = cesidi;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HobilerCriteria that = (HobilerCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(hobiAdi, that.hobiAdi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(lokasyon, that.lokasyon) &&
            Objects.equals(cesidi, that.cesidi) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, hobiAdi, aciklama, lokasyon, cesidi, kullaniciId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HobilerCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (hobiAdi != null ? "hobiAdi=" + hobiAdi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (lokasyon != null ? "lokasyon=" + lokasyon + ", " : "") +
            (cesidi != null ? "cesidi=" + cesidi + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
