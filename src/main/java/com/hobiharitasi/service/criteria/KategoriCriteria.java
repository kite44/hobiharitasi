package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Kategori} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.KategoriResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /kategoris?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KategoriCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter kategoriAdi;

    private StringFilter aciklama;

    private LongFilter etkinlikId;

    private Boolean distinct;

    public KategoriCriteria() {}

    public KategoriCriteria(KategoriCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.kategoriAdi = other.kategoriAdi == null ? null : other.kategoriAdi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.etkinlikId = other.etkinlikId == null ? null : other.etkinlikId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public KategoriCriteria copy() {
        return new KategoriCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getKategoriAdi() {
        return kategoriAdi;
    }

    public StringFilter kategoriAdi() {
        if (kategoriAdi == null) {
            kategoriAdi = new StringFilter();
        }
        return kategoriAdi;
    }

    public void setKategoriAdi(StringFilter kategoriAdi) {
        this.kategoriAdi = kategoriAdi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public LongFilter getEtkinlikId() {
        return etkinlikId;
    }

    public LongFilter etkinlikId() {
        if (etkinlikId == null) {
            etkinlikId = new LongFilter();
        }
        return etkinlikId;
    }

    public void setEtkinlikId(LongFilter etkinlikId) {
        this.etkinlikId = etkinlikId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KategoriCriteria that = (KategoriCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(kategoriAdi, that.kategoriAdi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(etkinlikId, that.etkinlikId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, kategoriAdi, aciklama, etkinlikId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KategoriCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (kategoriAdi != null ? "kategoriAdi=" + kategoriAdi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (etkinlikId != null ? "etkinlikId=" + etkinlikId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
