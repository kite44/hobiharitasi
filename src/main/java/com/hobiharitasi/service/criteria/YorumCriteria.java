package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Yorum} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.YorumResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /yorums?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class YorumCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter baslik;

    private StringFilter icerik;

    private LongFilter userId;

    private LongFilter kullaniciId;

    private LongFilter etkinlikId;

    private LongFilter grupEtkinligiId;

    private Boolean distinct;

    public YorumCriteria() {}

    public YorumCriteria(YorumCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.baslik = other.baslik == null ? null : other.baslik.copy();
        this.icerik = other.icerik == null ? null : other.icerik.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.etkinlikId = other.etkinlikId == null ? null : other.etkinlikId.copy();
        this.grupEtkinligiId = other.grupEtkinligiId == null ? null : other.grupEtkinligiId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public YorumCriteria copy() {
        return new YorumCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getBaslik() {
        return baslik;
    }

    public StringFilter baslik() {
        if (baslik == null) {
            baslik = new StringFilter();
        }
        return baslik;
    }

    public void setBaslik(StringFilter baslik) {
        this.baslik = baslik;
    }

    public StringFilter getIcerik() {
        return icerik;
    }

    public StringFilter icerik() {
        if (icerik == null) {
            icerik = new StringFilter();
        }
        return icerik;
    }

    public void setIcerik(StringFilter icerik) {
        this.icerik = icerik;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public LongFilter userId() {
        if (userId == null) {
            userId = new LongFilter();
        }
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public LongFilter getEtkinlikId() {
        return etkinlikId;
    }

    public LongFilter etkinlikId() {
        if (etkinlikId == null) {
            etkinlikId = new LongFilter();
        }
        return etkinlikId;
    }

    public void setEtkinlikId(LongFilter etkinlikId) {
        this.etkinlikId = etkinlikId;
    }

    public LongFilter getGrupEtkinligiId() {
        return grupEtkinligiId;
    }

    public LongFilter grupEtkinligiId() {
        if (grupEtkinligiId == null) {
            grupEtkinligiId = new LongFilter();
        }
        return grupEtkinligiId;
    }

    public void setGrupEtkinligiId(LongFilter grupEtkinligiId) {
        this.grupEtkinligiId = grupEtkinligiId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final YorumCriteria that = (YorumCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(baslik, that.baslik) &&
            Objects.equals(icerik, that.icerik) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(etkinlikId, that.etkinlikId) &&
            Objects.equals(grupEtkinligiId, that.grupEtkinligiId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, baslik, icerik, userId, kullaniciId, etkinlikId, grupEtkinligiId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "YorumCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (baslik != null ? "baslik=" + baslik + ", " : "") +
            (icerik != null ? "icerik=" + icerik + ", " : "") +
            (userId != null ? "userId=" + userId + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (etkinlikId != null ? "etkinlikId=" + etkinlikId + ", " : "") +
            (grupEtkinligiId != null ? "grupEtkinligiId=" + grupEtkinligiId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
