package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Davet} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.DavetResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /davets?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DavetCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter davetMesaji;

    private StringFilter durum;

    private InstantFilter olusturulmaTarihi;

    private InstantFilter davetTarihi;

    private BooleanFilter kabulEdildiMi;

    private LongFilter gonderenId;

    private Boolean distinct;

    public DavetCriteria() {}

    public DavetCriteria(DavetCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.davetMesaji = other.davetMesaji == null ? null : other.davetMesaji.copy();
        this.durum = other.durum == null ? null : other.durum.copy();
        this.olusturulmaTarihi = other.olusturulmaTarihi == null ? null : other.olusturulmaTarihi.copy();
        this.davetTarihi = other.davetTarihi == null ? null : other.davetTarihi.copy();
        this.kabulEdildiMi = other.kabulEdildiMi == null ? null : other.kabulEdildiMi.copy();
        this.gonderenId = other.gonderenId == null ? null : other.gonderenId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DavetCriteria copy() {
        return new DavetCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDavetMesaji() {
        return davetMesaji;
    }

    public StringFilter davetMesaji() {
        if (davetMesaji == null) {
            davetMesaji = new StringFilter();
        }
        return davetMesaji;
    }

    public void setDavetMesaji(StringFilter davetMesaji) {
        this.davetMesaji = davetMesaji;
    }

    public StringFilter getDurum() {
        return durum;
    }

    public StringFilter durum() {
        if (durum == null) {
            durum = new StringFilter();
        }
        return durum;
    }

    public void setDurum(StringFilter durum) {
        this.durum = durum;
    }

    public InstantFilter getOlusturulmaTarihi() {
        return olusturulmaTarihi;
    }

    public InstantFilter olusturulmaTarihi() {
        if (olusturulmaTarihi == null) {
            olusturulmaTarihi = new InstantFilter();
        }
        return olusturulmaTarihi;
    }

    public void setOlusturulmaTarihi(InstantFilter olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public InstantFilter getDavetTarihi() {
        return davetTarihi;
    }

    public InstantFilter davetTarihi() {
        if (davetTarihi == null) {
            davetTarihi = new InstantFilter();
        }
        return davetTarihi;
    }

    public void setDavetTarihi(InstantFilter davetTarihi) {
        this.davetTarihi = davetTarihi;
    }

    public BooleanFilter getKabulEdildiMi() {
        return kabulEdildiMi;
    }

    public BooleanFilter kabulEdildiMi() {
        if (kabulEdildiMi == null) {
            kabulEdildiMi = new BooleanFilter();
        }
        return kabulEdildiMi;
    }

    public void setKabulEdildiMi(BooleanFilter kabulEdildiMi) {
        this.kabulEdildiMi = kabulEdildiMi;
    }

    public LongFilter getGonderenId() {
        return gonderenId;
    }

    public LongFilter gonderenId() {
        if (gonderenId == null) {
            gonderenId = new LongFilter();
        }
        return gonderenId;
    }

    public void setGonderenId(LongFilter gonderenId) {
        this.gonderenId = gonderenId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DavetCriteria that = (DavetCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(davetMesaji, that.davetMesaji) &&
            Objects.equals(durum, that.durum) &&
            Objects.equals(olusturulmaTarihi, that.olusturulmaTarihi) &&
            Objects.equals(davetTarihi, that.davetTarihi) &&
            Objects.equals(kabulEdildiMi, that.kabulEdildiMi) &&
            Objects.equals(gonderenId, that.gonderenId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, davetMesaji, durum, olusturulmaTarihi, davetTarihi, kabulEdildiMi, gonderenId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DavetCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (davetMesaji != null ? "davetMesaji=" + davetMesaji + ", " : "") +
            (durum != null ? "durum=" + durum + ", " : "") +
            (olusturulmaTarihi != null ? "olusturulmaTarihi=" + olusturulmaTarihi + ", " : "") +
            (davetTarihi != null ? "davetTarihi=" + davetTarihi + ", " : "") +
            (kabulEdildiMi != null ? "kabulEdildiMi=" + kabulEdildiMi + ", " : "") +
            (gonderenId != null ? "gonderenId=" + gonderenId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
