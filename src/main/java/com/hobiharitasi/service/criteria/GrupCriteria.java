package com.hobiharitasi.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.hobiharitasi.domain.Grup} entity. This class is used
 * in {@link com.hobiharitasi.web.rest.GrupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /grups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GrupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter grupAdi;

    private StringFilter aciklama;

    private StringFilter grupKategorisi;

    private LongFilter kategoriId;

    private LongFilter kullaniciId;

    private Boolean distinct;

    public GrupCriteria() {}

    public GrupCriteria(GrupCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.grupAdi = other.grupAdi == null ? null : other.grupAdi.copy();
        this.aciklama = other.aciklama == null ? null : other.aciklama.copy();
        this.grupKategorisi = other.grupKategorisi == null ? null : other.grupKategorisi.copy();
        this.kategoriId = other.kategoriId == null ? null : other.kategoriId.copy();
        this.kullaniciId = other.kullaniciId == null ? null : other.kullaniciId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public GrupCriteria copy() {
        return new GrupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getGrupAdi() {
        return grupAdi;
    }

    public StringFilter grupAdi() {
        if (grupAdi == null) {
            grupAdi = new StringFilter();
        }
        return grupAdi;
    }

    public void setGrupAdi(StringFilter grupAdi) {
        this.grupAdi = grupAdi;
    }

    public StringFilter getAciklama() {
        return aciklama;
    }

    public StringFilter aciklama() {
        if (aciklama == null) {
            aciklama = new StringFilter();
        }
        return aciklama;
    }

    public void setAciklama(StringFilter aciklama) {
        this.aciklama = aciklama;
    }

    public StringFilter getGrupKategorisi() {
        return grupKategorisi;
    }

    public StringFilter grupKategorisi() {
        if (grupKategorisi == null) {
            grupKategorisi = new StringFilter();
        }
        return grupKategorisi;
    }

    public void setGrupKategorisi(StringFilter grupKategorisi) {
        this.grupKategorisi = grupKategorisi;
    }

    public LongFilter getKategoriId() {
        return kategoriId;
    }

    public LongFilter kategoriId() {
        if (kategoriId == null) {
            kategoriId = new LongFilter();
        }
        return kategoriId;
    }

    public void setKategoriId(LongFilter kategoriId) {
        this.kategoriId = kategoriId;
    }

    public LongFilter getKullaniciId() {
        return kullaniciId;
    }

    public LongFilter kullaniciId() {
        if (kullaniciId == null) {
            kullaniciId = new LongFilter();
        }
        return kullaniciId;
    }

    public void setKullaniciId(LongFilter kullaniciId) {
        this.kullaniciId = kullaniciId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final GrupCriteria that = (GrupCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(grupAdi, that.grupAdi) &&
            Objects.equals(aciklama, that.aciklama) &&
            Objects.equals(grupKategorisi, that.grupKategorisi) &&
            Objects.equals(kategoriId, that.kategoriId) &&
            Objects.equals(kullaniciId, that.kullaniciId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, grupAdi, aciklama, grupKategorisi, kategoriId, kullaniciId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GrupCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (grupAdi != null ? "grupAdi=" + grupAdi + ", " : "") +
            (aciklama != null ? "aciklama=" + aciklama + ", " : "") +
            (grupKategorisi != null ? "grupKategorisi=" + grupKategorisi + ", " : "") +
            (kategoriId != null ? "kategoriId=" + kategoriId + ", " : "") +
            (kullaniciId != null ? "kullaniciId=" + kullaniciId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
