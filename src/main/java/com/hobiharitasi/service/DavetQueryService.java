package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Davet;
import com.hobiharitasi.repository.DavetRepository;
import com.hobiharitasi.service.criteria.DavetCriteria;
import com.hobiharitasi.service.dto.DavetDTO;
import com.hobiharitasi.service.mapper.DavetMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Davet} entities in the database.
 * The main input is a {@link DavetCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DavetDTO} or a {@link Page} of {@link DavetDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DavetQueryService extends QueryService<Davet> {

    private final Logger log = LoggerFactory.getLogger(DavetQueryService.class);

    private final DavetRepository davetRepository;

    private final DavetMapper davetMapper;

    public DavetQueryService(DavetRepository davetRepository, DavetMapper davetMapper) {
        this.davetRepository = davetRepository;
        this.davetMapper = davetMapper;
    }

    /**
     * Return a {@link List} of {@link DavetDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DavetDTO> findByCriteria(DavetCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Davet> specification = createSpecification(criteria);
        return davetMapper.toDto(davetRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DavetDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DavetDTO> findByCriteria(DavetCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Davet> specification = createSpecification(criteria);
        return davetRepository.findAll(specification, page).map(davetMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DavetCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Davet> specification = createSpecification(criteria);
        return davetRepository.count(specification);
    }

    /**
     * Function to convert {@link DavetCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Davet> createSpecification(DavetCriteria criteria) {
        Specification<Davet> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Davet_.id));
            }
            if (criteria.getDavetMesaji() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDavetMesaji(), Davet_.davetMesaji));
            }
            if (criteria.getDurum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDurum(), Davet_.durum));
            }
            if (criteria.getOlusturulmaTarihi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOlusturulmaTarihi(), Davet_.olusturulmaTarihi));
            }
            if (criteria.getDavetTarihi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDavetTarihi(), Davet_.davetTarihi));
            }
            if (criteria.getKabulEdildiMi() != null) {
                specification = specification.and(buildSpecification(criteria.getKabulEdildiMi(), Davet_.kabulEdildiMi));
            }
            if (criteria.getGonderenId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getGonderenId(), root -> root.join(Davet_.gonderen, JoinType.LEFT).get(Kullanici_.id))
                    );
            }
        }
        return specification;
    }
}
