package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.DavetDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Davet}.
 */
public interface DavetService {
    /**
     * Save a davet.
     *
     * @param davetDTO the entity to save.
     * @return the persisted entity.
     */
    DavetDTO save(DavetDTO davetDTO);

    /**
     * Updates a davet.
     *
     * @param davetDTO the entity to update.
     * @return the persisted entity.
     */
    DavetDTO update(DavetDTO davetDTO);

    /**
     * Partially updates a davet.
     *
     * @param davetDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DavetDTO> partialUpdate(DavetDTO davetDTO);

    /**
     * Get all the davets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DavetDTO> findAll(Pageable pageable);

    /**
     * Get all the davets with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DavetDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" davet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DavetDTO> findOne(Long id);

    /**
     * Delete the "id" davet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
