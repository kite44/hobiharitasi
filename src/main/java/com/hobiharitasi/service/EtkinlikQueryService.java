package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.repository.EtkinlikRepository;
import com.hobiharitasi.service.criteria.EtkinlikCriteria;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.mapper.EtkinlikMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Etkinlik} entities in the database.
 * The main input is a {@link EtkinlikCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EtkinlikDTO} or a {@link Page} of {@link EtkinlikDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EtkinlikQueryService extends QueryService<Etkinlik> {

    private final Logger log = LoggerFactory.getLogger(EtkinlikQueryService.class);

    private final EtkinlikRepository etkinlikRepository;

    private final EtkinlikMapper etkinlikMapper;

    public EtkinlikQueryService(EtkinlikRepository etkinlikRepository, EtkinlikMapper etkinlikMapper) {
        this.etkinlikRepository = etkinlikRepository;
        this.etkinlikMapper = etkinlikMapper;
    }

    /**
     * Return a {@link List} of {@link EtkinlikDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EtkinlikDTO> findByCriteria(EtkinlikCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Etkinlik> specification = createSpecification(criteria);
        return etkinlikMapper.toDto(etkinlikRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EtkinlikDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EtkinlikDTO> findByCriteria(EtkinlikCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Etkinlik> specification = createSpecification(criteria);
        return etkinlikRepository.findAll(specification, page).map(etkinlikMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EtkinlikCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Etkinlik> specification = createSpecification(criteria);
        return etkinlikRepository.count(specification);
    }

    /**
     * Function to convert {@link EtkinlikCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Etkinlik> createSpecification(EtkinlikCriteria criteria) {
        Specification<Etkinlik> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Etkinlik_.id));
            }
            if (criteria.getEtkinlikAdi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEtkinlikAdi(), Etkinlik_.etkinlikAdi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), Etkinlik_.aciklama));
            }
            if (criteria.getTarih() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTarih(), Etkinlik_.tarih));
            }
            if (criteria.getKonum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKonum(), Etkinlik_.konum));
            }
            if (criteria.getMaksimumKatilimci() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaksimumKatilimci(), Etkinlik_.maksimumKatilimci));
            }
            if (criteria.getMinimumKatilimci() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMinimumKatilimci(), Etkinlik_.minimumKatilimci));
            }
            if (criteria.getEtkinlikKategorisi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEtkinlikKategorisi(), Etkinlik_.etkinlikKategorisi));
            }
            if (criteria.getKategoriId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKategoriId(),
                            root -> root.join(Etkinlik_.kategoris, JoinType.LEFT).get(Kategori_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
