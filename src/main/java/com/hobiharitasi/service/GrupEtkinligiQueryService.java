package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.repository.GrupEtkinligiRepository;
import com.hobiharitasi.service.criteria.GrupEtkinligiCriteria;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import com.hobiharitasi.service.mapper.GrupEtkinligiMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link GrupEtkinligi} entities in the database.
 * The main input is a {@link GrupEtkinligiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GrupEtkinligiDTO} or a {@link Page} of {@link GrupEtkinligiDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GrupEtkinligiQueryService extends QueryService<GrupEtkinligi> {

    private final Logger log = LoggerFactory.getLogger(GrupEtkinligiQueryService.class);

    private final GrupEtkinligiRepository grupEtkinligiRepository;

    private final GrupEtkinligiMapper grupEtkinligiMapper;

    public GrupEtkinligiQueryService(GrupEtkinligiRepository grupEtkinligiRepository, GrupEtkinligiMapper grupEtkinligiMapper) {
        this.grupEtkinligiRepository = grupEtkinligiRepository;
        this.grupEtkinligiMapper = grupEtkinligiMapper;
    }

    /**
     * Return a {@link List} of {@link GrupEtkinligiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GrupEtkinligiDTO> findByCriteria(GrupEtkinligiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<GrupEtkinligi> specification = createSpecification(criteria);
        return grupEtkinligiMapper.toDto(grupEtkinligiRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link GrupEtkinligiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GrupEtkinligiDTO> findByCriteria(GrupEtkinligiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<GrupEtkinligi> specification = createSpecification(criteria);
        return grupEtkinligiRepository.findAll(specification, page).map(grupEtkinligiMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GrupEtkinligiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<GrupEtkinligi> specification = createSpecification(criteria);
        return grupEtkinligiRepository.count(specification);
    }

    /**
     * Function to convert {@link GrupEtkinligiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<GrupEtkinligi> createSpecification(GrupEtkinligiCriteria criteria) {
        Specification<GrupEtkinligi> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), GrupEtkinligi_.id));
            }
            if (criteria.getEtkinlikAdi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEtkinlikAdi(), GrupEtkinligi_.etkinlikAdi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), GrupEtkinligi_.aciklama));
            }
            if (criteria.getTarih() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTarih(), GrupEtkinligi_.tarih));
            }
            if (criteria.getKonum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKonum(), GrupEtkinligi_.konum));
            }
            if (criteria.getGroupId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGroupId(), GrupEtkinligi_.groupId));
            }
            if (criteria.getGrupId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getGrupId(), root -> root.join(GrupEtkinligi_.grup, JoinType.LEFT).get(Grup_.id))
                    );
            }
        }
        return specification;
    }
}
