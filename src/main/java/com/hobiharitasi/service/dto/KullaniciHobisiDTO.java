package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.KullaniciHobisi} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KullaniciHobisiDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer seviye;

    private HobilerDTO hobi;

    private KullaniciDTO kullanici;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSeviye() {
        return seviye;
    }

    public void setSeviye(Integer seviye) {
        this.seviye = seviye;
    }

    public HobilerDTO getHobi() {
        return hobi;
    }

    public void setHobi(HobilerDTO hobi) {
        this.hobi = hobi;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KullaniciHobisiDTO)) {
            return false;
        }

        KullaniciHobisiDTO kullaniciHobisiDTO = (KullaniciHobisiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, kullaniciHobisiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KullaniciHobisiDTO{" +
            "id=" + getId() +
            ", seviye=" + getSeviye() +
            ", hobi=" + getHobi() +
            ", kullanici=" + getKullanici() +
            "}";
    }
}
