package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Tavsiye} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TavsiyeDTO implements Serializable {

    private Long id;

    @NotNull
    private String baslik;

    @NotNull
    private String icerik;

    @NotNull
    private Instant olusturulmaTarihi;

    private KullaniciDTO kullanici;

    private KullaniciDTO kullanici;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaslik() {
        return baslik;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getIcerik() {
        return icerik;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public Instant getOlusturulmaTarihi() {
        return olusturulmaTarihi;
    }

    public void setOlusturulmaTarihi(Instant olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TavsiyeDTO)) {
            return false;
        }

        TavsiyeDTO tavsiyeDTO = (TavsiyeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, tavsiyeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TavsiyeDTO{" +
            "id=" + getId() +
            ", baslik='" + getBaslik() + "'" +
            ", icerik='" + getIcerik() + "'" +
            ", olusturulmaTarihi='" + getOlusturulmaTarihi() + "'" +
            ", kullanici=" + getKullanici() +
            ", kullanici=" + getKullanici() +
            "}";
    }
}
