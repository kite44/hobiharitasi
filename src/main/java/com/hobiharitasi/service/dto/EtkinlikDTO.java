package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Etkinlik} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EtkinlikDTO implements Serializable {

    private Long id;

    @NotNull
    private String etkinlikAdi;

    @NotNull
    private String aciklama;

    @NotNull
    private Instant tarih;

    @NotNull
    private String konum;

    @NotNull
    private Integer maksimumKatilimci;

    private Integer minimumKatilimci;

    @NotNull
    private String etkinlikKategorisi;

    private Set<KategoriDTO> kategoris = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtkinlikAdi() {
        return etkinlikAdi;
    }

    public void setEtkinlikAdi(String etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Instant getTarih() {
        return tarih;
    }

    public void setTarih(Instant tarih) {
        this.tarih = tarih;
    }

    public String getKonum() {
        return konum;
    }

    public void setKonum(String konum) {
        this.konum = konum;
    }

    public Integer getMaksimumKatilimci() {
        return maksimumKatilimci;
    }

    public void setMaksimumKatilimci(Integer maksimumKatilimci) {
        this.maksimumKatilimci = maksimumKatilimci;
    }

    public Integer getMinimumKatilimci() {
        return minimumKatilimci;
    }

    public void setMinimumKatilimci(Integer minimumKatilimci) {
        this.minimumKatilimci = minimumKatilimci;
    }

    public String getEtkinlikKategorisi() {
        return etkinlikKategorisi;
    }

    public void setEtkinlikKategorisi(String etkinlikKategorisi) {
        this.etkinlikKategorisi = etkinlikKategorisi;
    }

    public Set<KategoriDTO> getKategoris() {
        return kategoris;
    }

    public void setKategoris(Set<KategoriDTO> kategoris) {
        this.kategoris = kategoris;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EtkinlikDTO)) {
            return false;
        }

        EtkinlikDTO etkinlikDTO = (EtkinlikDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, etkinlikDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EtkinlikDTO{" +
            "id=" + getId() +
            ", etkinlikAdi='" + getEtkinlikAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", tarih='" + getTarih() + "'" +
            ", konum='" + getKonum() + "'" +
            ", maksimumKatilimci=" + getMaksimumKatilimci() +
            ", minimumKatilimci=" + getMinimumKatilimci() +
            ", etkinlikKategorisi='" + getEtkinlikKategorisi() + "'" +
            ", kategoris=" + getKategoris() +
            "}";
    }
}
