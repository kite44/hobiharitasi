package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Grup} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GrupDTO implements Serializable {

    private Long id;

    @NotNull
    private String grupAdi;

    @NotNull
    private String aciklama;

    @NotNull
    private String grupKategorisi;

    private KategoriDTO kategori;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGrupAdi() {
        return grupAdi;
    }

    public void setGrupAdi(String grupAdi) {
        this.grupAdi = grupAdi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getGrupKategorisi() {
        return grupKategorisi;
    }

    public void setGrupKategorisi(String grupKategorisi) {
        this.grupKategorisi = grupKategorisi;
    }

    public KategoriDTO getKategori() {
        return kategori;
    }

    public void setKategori(KategoriDTO kategori) {
        this.kategori = kategori;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GrupDTO)) {
            return false;
        }

        GrupDTO grupDTO = (GrupDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, grupDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GrupDTO{" +
            "id=" + getId() +
            ", grupAdi='" + getGrupAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", grupKategorisi='" + getGrupKategorisi() + "'" +
            ", kategori=" + getKategori() +
            "}";
    }
}
