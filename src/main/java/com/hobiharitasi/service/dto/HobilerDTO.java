package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Hobiler} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class HobilerDTO implements Serializable {

    private Long id;

    @NotNull
    private String hobiAdi;

    private String aciklama;

    private String lokasyon;

    private String cesidi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHobiAdi() {
        return hobiAdi;
    }

    public void setHobiAdi(String hobiAdi) {
        this.hobiAdi = hobiAdi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public String getLokasyon() {
        return lokasyon;
    }

    public void setLokasyon(String lokasyon) {
        this.lokasyon = lokasyon;
    }

    public String getCesidi() {
        return cesidi;
    }

    public void setCesidi(String cesidi) {
        this.cesidi = cesidi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HobilerDTO)) {
            return false;
        }

        HobilerDTO hobilerDTO = (HobilerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hobilerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HobilerDTO{" +
            "id=" + getId() +
            ", hobiAdi='" + getHobiAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", lokasyon='" + getLokasyon() + "'" +
            ", cesidi='" + getCesidi() + "'" +
            "}";
    }
}
