package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Kategori} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KategoriDTO implements Serializable {

    private Long id;

    @NotNull
    private String kategoriAdi;

    @NotNull
    private String aciklama;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategoriAdi() {
        return kategoriAdi;
    }

    public void setKategoriAdi(String kategoriAdi) {
        this.kategoriAdi = kategoriAdi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KategoriDTO)) {
            return false;
        }

        KategoriDTO kategoriDTO = (KategoriDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, kategoriDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KategoriDTO{" +
            "id=" + getId() +
            ", kategoriAdi='" + getKategoriAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            "}";
    }
}
