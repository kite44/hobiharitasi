package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Davet} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DavetDTO implements Serializable {

    private Long id;

    @NotNull
    private String davetMesaji;

    @NotNull
    private String durum;

    @NotNull
    private Instant olusturulmaTarihi;

    @NotNull
    private Instant davetTarihi;

    private Boolean kabulEdildiMi;

    private KullaniciDTO gonderen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDavetMesaji() {
        return davetMesaji;
    }

    public void setDavetMesaji(String davetMesaji) {
        this.davetMesaji = davetMesaji;
    }

    public String getDurum() {
        return durum;
    }

    public void setDurum(String durum) {
        this.durum = durum;
    }

    public Instant getOlusturulmaTarihi() {
        return olusturulmaTarihi;
    }

    public void setOlusturulmaTarihi(Instant olusturulmaTarihi) {
        this.olusturulmaTarihi = olusturulmaTarihi;
    }

    public Instant getDavetTarihi() {
        return davetTarihi;
    }

    public void setDavetTarihi(Instant davetTarihi) {
        this.davetTarihi = davetTarihi;
    }

    public Boolean getKabulEdildiMi() {
        return kabulEdildiMi;
    }

    public void setKabulEdildiMi(Boolean kabulEdildiMi) {
        this.kabulEdildiMi = kabulEdildiMi;
    }

    public KullaniciDTO getGonderen() {
        return gonderen;
    }

    public void setGonderen(KullaniciDTO gonderen) {
        this.gonderen = gonderen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DavetDTO)) {
            return false;
        }

        DavetDTO davetDTO = (DavetDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, davetDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DavetDTO{" +
            "id=" + getId() +
            ", davetMesaji='" + getDavetMesaji() + "'" +
            ", durum='" + getDurum() + "'" +
            ", olusturulmaTarihi='" + getOlusturulmaTarihi() + "'" +
            ", davetTarihi='" + getDavetTarihi() + "'" +
            ", kabulEdildiMi='" + getKabulEdildiMi() + "'" +
            ", gonderen=" + getGonderen() +
            "}";
    }
}
