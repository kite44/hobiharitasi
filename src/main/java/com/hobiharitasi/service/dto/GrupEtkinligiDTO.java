package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.GrupEtkinligi} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class GrupEtkinligiDTO implements Serializable {

    private Long id;

    @NotNull
    private String etkinlikAdi;

    @NotNull
    private String aciklama;

    @NotNull
    private Instant tarih;

    @NotNull
    private String konum;

    @NotNull
    private Long groupId;

    private GrupDTO grup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtkinlikAdi() {
        return etkinlikAdi;
    }

    public void setEtkinlikAdi(String etkinlikAdi) {
        this.etkinlikAdi = etkinlikAdi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Instant getTarih() {
        return tarih;
    }

    public void setTarih(Instant tarih) {
        this.tarih = tarih;
    }

    public String getKonum() {
        return konum;
    }

    public void setKonum(String konum) {
        this.konum = konum;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public GrupDTO getGrup() {
        return grup;
    }

    public void setGrup(GrupDTO grup) {
        this.grup = grup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GrupEtkinligiDTO)) {
            return false;
        }

        GrupEtkinligiDTO grupEtkinligiDTO = (GrupEtkinligiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, grupEtkinligiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GrupEtkinligiDTO{" +
            "id=" + getId() +
            ", etkinlikAdi='" + getEtkinlikAdi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", tarih='" + getTarih() + "'" +
            ", konum='" + getKonum() + "'" +
            ", groupId=" + getGroupId() +
            ", grup=" + getGrup() +
            "}";
    }
}
