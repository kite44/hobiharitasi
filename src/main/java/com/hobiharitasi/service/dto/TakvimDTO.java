package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Takvim} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TakvimDTO implements Serializable {

    private Long id;

    @NotNull
    private Instant etkinlikTarihi;

    @NotNull
    private String etkinlikBasligi;

    private String aciklama;

    private KullaniciDTO kullanici;

    private KullaniciDTO kullanici;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getEtkinlikTarihi() {
        return etkinlikTarihi;
    }

    public void setEtkinlikTarihi(Instant etkinlikTarihi) {
        this.etkinlikTarihi = etkinlikTarihi;
    }

    public String getEtkinlikBasligi() {
        return etkinlikBasligi;
    }

    public void setEtkinlikBasligi(String etkinlikBasligi) {
        this.etkinlikBasligi = etkinlikBasligi;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TakvimDTO)) {
            return false;
        }

        TakvimDTO takvimDTO = (TakvimDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, takvimDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TakvimDTO{" +
            "id=" + getId() +
            ", etkinlikTarihi='" + getEtkinlikTarihi() + "'" +
            ", etkinlikBasligi='" + getEtkinlikBasligi() + "'" +
            ", aciklama='" + getAciklama() + "'" +
            ", kullanici=" + getKullanici() +
            ", kullanici=" + getKullanici() +
            "}";
    }
}
