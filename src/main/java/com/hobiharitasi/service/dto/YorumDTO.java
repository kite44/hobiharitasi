package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Yorum} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class YorumDTO implements Serializable {

    private Long id;

    @NotNull
    private String baslik;

    @NotNull
    private String icerik;

    private UserDTO user;

    private KullaniciDTO kullanici;

    private EtkinlikDTO etkinlik;

    private GrupEtkinligiDTO grupEtkinligi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaslik() {
        return baslik;
    }

    public void setBaslik(String baslik) {
        this.baslik = baslik;
    }

    public String getIcerik() {
        return icerik;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public KullaniciDTO getKullanici() {
        return kullanici;
    }

    public void setKullanici(KullaniciDTO kullanici) {
        this.kullanici = kullanici;
    }

    public EtkinlikDTO getEtkinlik() {
        return etkinlik;
    }

    public void setEtkinlik(EtkinlikDTO etkinlik) {
        this.etkinlik = etkinlik;
    }

    public GrupEtkinligiDTO getGrupEtkinligi() {
        return grupEtkinligi;
    }

    public void setGrupEtkinligi(GrupEtkinligiDTO grupEtkinligi) {
        this.grupEtkinligi = grupEtkinligi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof YorumDTO)) {
            return false;
        }

        YorumDTO yorumDTO = (YorumDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, yorumDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "YorumDTO{" +
            "id=" + getId() +
            ", baslik='" + getBaslik() + "'" +
            ", icerik='" + getIcerik() + "'" +
            ", user=" + getUser() +
            ", kullanici=" + getKullanici() +
            ", etkinlik=" + getEtkinlik() +
            ", grupEtkinligi=" + getGrupEtkinligi() +
            "}";
    }
}
