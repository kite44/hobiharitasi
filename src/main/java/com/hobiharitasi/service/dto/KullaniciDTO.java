package com.hobiharitasi.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.hobiharitasi.domain.Kullanici} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class KullaniciDTO implements Serializable {

    private Long id;

    @NotNull
    private String ad;

    @NotNull
    private String soyad;

    @NotNull
    private String email;

    @NotNull
    private String sifre;

    @NotNull
    private Integer telNo;

    @Size(max = 500)
    private String biyografi;

    private String adres;

    private String profilResmi;

    private String accesCode;

    private String jwtCode;

    private UserDTO user;

    private EtkinlikDTO etkinlik;

    private Set<GrupDTO> grups = new HashSet<>();

    private Set<HobilerDTO> hobis = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public Integer getTelNo() {
        return telNo;
    }

    public void setTelNo(Integer telNo) {
        this.telNo = telNo;
    }

    public String getBiyografi() {
        return biyografi;
    }

    public void setBiyografi(String biyografi) {
        this.biyografi = biyografi;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getProfilResmi() {
        return profilResmi;
    }

    public void setProfilResmi(String profilResmi) {
        this.profilResmi = profilResmi;
    }

    public String getAccesCode() {
        return accesCode;
    }

    public void setAccesCode(String accesCode) {
        this.accesCode = accesCode;
    }

    public String getJwtCode() {
        return jwtCode;
    }

    public void setJwtCode(String jwtCode) {
        this.jwtCode = jwtCode;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public EtkinlikDTO getEtkinlik() {
        return etkinlik;
    }

    public void setEtkinlik(EtkinlikDTO etkinlik) {
        this.etkinlik = etkinlik;
    }

    public Set<GrupDTO> getGrups() {
        return grups;
    }

    public void setGrups(Set<GrupDTO> grups) {
        this.grups = grups;
    }

    public Set<HobilerDTO> getHobis() {
        return hobis;
    }

    public void setHobis(Set<HobilerDTO> hobis) {
        this.hobis = hobis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KullaniciDTO)) {
            return false;
        }

        KullaniciDTO kullaniciDTO = (KullaniciDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, kullaniciDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KullaniciDTO{" +
            "id=" + getId() +
            ", ad='" + getAd() + "'" +
            ", soyad='" + getSoyad() + "'" +
            ", email='" + getEmail() + "'" +
            ", sifre='" + getSifre() + "'" +
            ", telNo=" + getTelNo() +
            ", biyografi='" + getBiyografi() + "'" +
            ", adres='" + getAdres() + "'" +
            ", profilResmi='" + getProfilResmi() + "'" +
            ", accesCode='" + getAccesCode() + "'" +
            ", jwtCode='" + getJwtCode() + "'" +
            ", user=" + getUser() +
            ", etkinlik=" + getEtkinlik() +
            ", grups=" + getGrups() +
            ", hobis=" + getHobis() +
            "}";
    }
}
