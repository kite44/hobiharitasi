package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.KullaniciHobisi}.
 */
public interface KullaniciHobisiService {
    /**
     * Save a kullaniciHobisi.
     *
     * @param kullaniciHobisiDTO the entity to save.
     * @return the persisted entity.
     */
    KullaniciHobisiDTO save(KullaniciHobisiDTO kullaniciHobisiDTO);

    /**
     * Updates a kullaniciHobisi.
     *
     * @param kullaniciHobisiDTO the entity to update.
     * @return the persisted entity.
     */
    KullaniciHobisiDTO update(KullaniciHobisiDTO kullaniciHobisiDTO);

    /**
     * Partially updates a kullaniciHobisi.
     *
     * @param kullaniciHobisiDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KullaniciHobisiDTO> partialUpdate(KullaniciHobisiDTO kullaniciHobisiDTO);

    /**
     * Get all the kullaniciHobisis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KullaniciHobisiDTO> findAll(Pageable pageable);

    /**
     * Get all the kullaniciHobisis with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KullaniciHobisiDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" kullaniciHobisi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KullaniciHobisiDTO> findOne(Long id);

    /**
     * Delete the "id" kullaniciHobisi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
