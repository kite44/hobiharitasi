package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.KategoriDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Kategori}.
 */
public interface KategoriService {
    /**
     * Save a kategori.
     *
     * @param kategoriDTO the entity to save.
     * @return the persisted entity.
     */
    KategoriDTO save(KategoriDTO kategoriDTO);

    /**
     * Updates a kategori.
     *
     * @param kategoriDTO the entity to update.
     * @return the persisted entity.
     */
    KategoriDTO update(KategoriDTO kategoriDTO);

    /**
     * Partially updates a kategori.
     *
     * @param kategoriDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<KategoriDTO> partialUpdate(KategoriDTO kategoriDTO);

    /**
     * Get all the kategoris.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<KategoriDTO> findAll(Pageable pageable);

    /**
     * Get the "id" kategori.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<KategoriDTO> findOne(Long id);

    /**
     * Delete the "id" kategori.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
