package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.GrupEtkinligi}.
 */
public interface GrupEtkinligiService {
    /**
     * Save a grupEtkinligi.
     *
     * @param grupEtkinligiDTO the entity to save.
     * @return the persisted entity.
     */
    GrupEtkinligiDTO save(GrupEtkinligiDTO grupEtkinligiDTO);

    /**
     * Updates a grupEtkinligi.
     *
     * @param grupEtkinligiDTO the entity to update.
     * @return the persisted entity.
     */
    GrupEtkinligiDTO update(GrupEtkinligiDTO grupEtkinligiDTO);

    /**
     * Partially updates a grupEtkinligi.
     *
     * @param grupEtkinligiDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<GrupEtkinligiDTO> partialUpdate(GrupEtkinligiDTO grupEtkinligiDTO);

    /**
     * Get all the grupEtkinligis.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GrupEtkinligiDTO> findAll(Pageable pageable);

    /**
     * Get all the grupEtkinligis with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GrupEtkinligiDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" grupEtkinligi.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GrupEtkinligiDTO> findOne(Long id);

    /**
     * Delete the "id" grupEtkinligi.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
