package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.repository.KategoriRepository;
import com.hobiharitasi.service.criteria.KategoriCriteria;
import com.hobiharitasi.service.dto.KategoriDTO;
import com.hobiharitasi.service.mapper.KategoriMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Kategori} entities in the database.
 * The main input is a {@link KategoriCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KategoriDTO} or a {@link Page} of {@link KategoriDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KategoriQueryService extends QueryService<Kategori> {

    private final Logger log = LoggerFactory.getLogger(KategoriQueryService.class);

    private final KategoriRepository kategoriRepository;

    private final KategoriMapper kategoriMapper;

    public KategoriQueryService(KategoriRepository kategoriRepository, KategoriMapper kategoriMapper) {
        this.kategoriRepository = kategoriRepository;
        this.kategoriMapper = kategoriMapper;
    }

    /**
     * Return a {@link List} of {@link KategoriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KategoriDTO> findByCriteria(KategoriCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Kategori> specification = createSpecification(criteria);
        return kategoriMapper.toDto(kategoriRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KategoriDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KategoriDTO> findByCriteria(KategoriCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Kategori> specification = createSpecification(criteria);
        return kategoriRepository.findAll(specification, page).map(kategoriMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KategoriCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Kategori> specification = createSpecification(criteria);
        return kategoriRepository.count(specification);
    }

    /**
     * Function to convert {@link KategoriCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Kategori> createSpecification(KategoriCriteria criteria) {
        Specification<Kategori> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Kategori_.id));
            }
            if (criteria.getKategoriAdi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKategoriAdi(), Kategori_.kategoriAdi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), Kategori_.aciklama));
            }
            if (criteria.getEtkinlikId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getEtkinlikId(),
                            root -> root.join(Kategori_.etkinliks, JoinType.LEFT).get(Etkinlik_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
