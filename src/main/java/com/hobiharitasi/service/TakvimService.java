package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.TakvimDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Takvim}.
 */
public interface TakvimService {
    /**
     * Save a takvim.
     *
     * @param takvimDTO the entity to save.
     * @return the persisted entity.
     */
    TakvimDTO save(TakvimDTO takvimDTO);

    /**
     * Updates a takvim.
     *
     * @param takvimDTO the entity to update.
     * @return the persisted entity.
     */
    TakvimDTO update(TakvimDTO takvimDTO);

    /**
     * Partially updates a takvim.
     *
     * @param takvimDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TakvimDTO> partialUpdate(TakvimDTO takvimDTO);

    /**
     * Get all the takvims.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TakvimDTO> findAll(Pageable pageable);

    /**
     * Get all the takvims with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TakvimDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" takvim.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TakvimDTO> findOne(Long id);

    /**
     * Delete the "id" takvim.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
