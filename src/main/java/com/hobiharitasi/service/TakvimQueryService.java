package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Takvim;
import com.hobiharitasi.repository.TakvimRepository;
import com.hobiharitasi.service.criteria.TakvimCriteria;
import com.hobiharitasi.service.dto.TakvimDTO;
import com.hobiharitasi.service.mapper.TakvimMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Takvim} entities in the database.
 * The main input is a {@link TakvimCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TakvimDTO} or a {@link Page} of {@link TakvimDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TakvimQueryService extends QueryService<Takvim> {

    private final Logger log = LoggerFactory.getLogger(TakvimQueryService.class);

    private final TakvimRepository takvimRepository;

    private final TakvimMapper takvimMapper;

    public TakvimQueryService(TakvimRepository takvimRepository, TakvimMapper takvimMapper) {
        this.takvimRepository = takvimRepository;
        this.takvimMapper = takvimMapper;
    }

    /**
     * Return a {@link List} of {@link TakvimDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TakvimDTO> findByCriteria(TakvimCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Takvim> specification = createSpecification(criteria);
        return takvimMapper.toDto(takvimRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TakvimDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TakvimDTO> findByCriteria(TakvimCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Takvim> specification = createSpecification(criteria);
        return takvimRepository.findAll(specification, page).map(takvimMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TakvimCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Takvim> specification = createSpecification(criteria);
        return takvimRepository.count(specification);
    }

    /**
     * Function to convert {@link TakvimCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Takvim> createSpecification(TakvimCriteria criteria) {
        Specification<Takvim> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Takvim_.id));
            }
            if (criteria.getEtkinlikTarihi() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEtkinlikTarihi(), Takvim_.etkinlikTarihi));
            }
            if (criteria.getEtkinlikBasligi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEtkinlikBasligi(), Takvim_.etkinlikBasligi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), Takvim_.aciklama));
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(Takvim_.kullanici, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(Takvim_.kullanici, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
