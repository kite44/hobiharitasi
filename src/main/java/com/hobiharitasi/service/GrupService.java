package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.GrupDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Grup}.
 */
public interface GrupService {
    /**
     * Save a grup.
     *
     * @param grupDTO the entity to save.
     * @return the persisted entity.
     */
    GrupDTO save(GrupDTO grupDTO);

    /**
     * Updates a grup.
     *
     * @param grupDTO the entity to update.
     * @return the persisted entity.
     */
    GrupDTO update(GrupDTO grupDTO);

    /**
     * Partially updates a grup.
     *
     * @param grupDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<GrupDTO> partialUpdate(GrupDTO grupDTO);

    /**
     * Get all the grups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GrupDTO> findAll(Pageable pageable);

    /**
     * Get all the grups with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GrupDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" grup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GrupDTO> findOne(Long id);

    /**
     * Delete the "id" grup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
