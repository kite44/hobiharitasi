package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.YorumDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Yorum}.
 */
public interface YorumService {
    /**
     * Save a yorum.
     *
     * @param yorumDTO the entity to save.
     * @return the persisted entity.
     */
    YorumDTO save(YorumDTO yorumDTO);

    /**
     * Updates a yorum.
     *
     * @param yorumDTO the entity to update.
     * @return the persisted entity.
     */
    YorumDTO update(YorumDTO yorumDTO);

    /**
     * Partially updates a yorum.
     *
     * @param yorumDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<YorumDTO> partialUpdate(YorumDTO yorumDTO);

    /**
     * Get all the yorums.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<YorumDTO> findAll(Pageable pageable);

    /**
     * Get all the yorums with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<YorumDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" yorum.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<YorumDTO> findOne(Long id);

    /**
     * Delete the "id" yorum.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
