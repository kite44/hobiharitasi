package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.Tavsiye;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.dto.TavsiyeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Tavsiye} and its DTO {@link TavsiyeDTO}.
 */
@Mapper(componentModel = "spring")
public interface TavsiyeMapper extends EntityMapper<TavsiyeDTO, Tavsiye> {
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    TavsiyeDTO toDto(Tavsiye s);

    @Named("kullaniciAd")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ad", source = "ad")
    KullaniciDTO toDtoKullaniciAd(Kullanici kullanici);
}
