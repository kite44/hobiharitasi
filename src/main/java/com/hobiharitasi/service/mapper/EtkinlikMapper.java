package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.dto.KategoriDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Etkinlik} and its DTO {@link EtkinlikDTO}.
 */
@Mapper(componentModel = "spring")
public interface EtkinlikMapper extends EntityMapper<EtkinlikDTO, Etkinlik> {
    @Mapping(target = "kategoris", source = "kategoris", qualifiedByName = "kategoriKategoriAdiSet")
    EtkinlikDTO toDto(Etkinlik s);

    @Mapping(target = "removeKategori", ignore = true)
    Etkinlik toEntity(EtkinlikDTO etkinlikDTO);

    @Named("kategoriKategoriAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "kategoriAdi", source = "kategoriAdi")
    KategoriDTO toDtoKategoriKategoriAdi(Kategori kategori);

    @Named("kategoriKategoriAdiSet")
    default Set<KategoriDTO> toDtoKategoriKategoriAdiSet(Set<Kategori> kategori) {
        return kategori.stream().map(this::toDtoKategoriKategoriAdi).collect(Collectors.toSet());
    }
}
