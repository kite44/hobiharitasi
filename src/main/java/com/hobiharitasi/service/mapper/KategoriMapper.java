package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.service.dto.KategoriDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Kategori} and its DTO {@link KategoriDTO}.
 */
@Mapper(componentModel = "spring")
public interface KategoriMapper extends EntityMapper<KategoriDTO, Kategori> {}
