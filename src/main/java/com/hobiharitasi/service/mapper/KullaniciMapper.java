package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.User;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.dto.UserDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Kullanici} and its DTO {@link KullaniciDTO}.
 */
@Mapper(componentModel = "spring")
public interface KullaniciMapper extends EntityMapper<KullaniciDTO, Kullanici> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userLogin")
    @Mapping(target = "etkinlik", source = "etkinlik", qualifiedByName = "etkinlikEtkinlikAdi")
    @Mapping(target = "grups", source = "grups", qualifiedByName = "grupGrupAdiSet")
    @Mapping(target = "hobis", source = "hobis", qualifiedByName = "hobilerHobiAdiSet")
    KullaniciDTO toDto(Kullanici s);

    @Mapping(target = "removeGrup", ignore = true)
    @Mapping(target = "removeHobi", ignore = true)
    Kullanici toEntity(KullaniciDTO kullaniciDTO);

    @Named("userLogin")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "login", source = "login")
    UserDTO toDtoUserLogin(User user);

    @Named("etkinlikEtkinlikAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "etkinlikAdi", source = "etkinlikAdi")
    EtkinlikDTO toDtoEtkinlikEtkinlikAdi(Etkinlik etkinlik);

    @Named("grupGrupAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "grupAdi", source = "grupAdi")
    GrupDTO toDtoGrupGrupAdi(Grup grup);

    @Named("grupGrupAdiSet")
    default Set<GrupDTO> toDtoGrupGrupAdiSet(Set<Grup> grup) {
        return grup.stream().map(this::toDtoGrupGrupAdi).collect(Collectors.toSet());
    }

    @Named("hobilerHobiAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "hobiAdi", source = "hobiAdi")
    HobilerDTO toDtoHobilerHobiAdi(Hobiler hobiler);

    @Named("hobilerHobiAdiSet")
    default Set<HobilerDTO> toDtoHobilerHobiAdiSet(Set<Hobiler> hobiler) {
        return hobiler.stream().map(this::toDtoHobilerHobiAdi).collect(Collectors.toSet());
    }
}
