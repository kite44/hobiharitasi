package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.User;
import com.hobiharitasi.domain.Yorum;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.dto.UserDTO;
import com.hobiharitasi.service.dto.YorumDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Yorum} and its DTO {@link YorumDTO}.
 */
@Mapper(componentModel = "spring")
public interface YorumMapper extends EntityMapper<YorumDTO, Yorum> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userLogin")
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    @Mapping(target = "etkinlik", source = "etkinlik", qualifiedByName = "etkinlikEtkinlikAdi")
    @Mapping(target = "grupEtkinligi", source = "grupEtkinligi", qualifiedByName = "grupEtkinligiEtkinlikAdi")
    YorumDTO toDto(Yorum s);

    @Named("userLogin")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "login", source = "login")
    UserDTO toDtoUserLogin(User user);

    @Named("kullaniciAd")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ad", source = "ad")
    KullaniciDTO toDtoKullaniciAd(Kullanici kullanici);

    @Named("etkinlikEtkinlikAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "etkinlikAdi", source = "etkinlikAdi")
    EtkinlikDTO toDtoEtkinlikEtkinlikAdi(Etkinlik etkinlik);

    @Named("grupEtkinligiEtkinlikAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "etkinlikAdi", source = "etkinlikAdi")
    GrupEtkinligiDTO toDtoGrupEtkinligiEtkinlikAdi(GrupEtkinligi grupEtkinligi);
}
