package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.Takvim;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.dto.TakvimDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Takvim} and its DTO {@link TakvimDTO}.
 */
@Mapper(componentModel = "spring")
public interface TakvimMapper extends EntityMapper<TakvimDTO, Takvim> {
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    TakvimDTO toDto(Takvim s);

    @Named("kullaniciAd")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ad", source = "ad")
    KullaniciDTO toDtoKullaniciAd(Kullanici kullanici);
}
