package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link GrupEtkinligi} and its DTO {@link GrupEtkinligiDTO}.
 */
@Mapper(componentModel = "spring")
public interface GrupEtkinligiMapper extends EntityMapper<GrupEtkinligiDTO, GrupEtkinligi> {
    @Mapping(target = "grup", source = "grup", qualifiedByName = "grupGrupAdi")
    GrupEtkinligiDTO toDto(GrupEtkinligi s);

    @Named("grupGrupAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "grupAdi", source = "grupAdi")
    GrupDTO toDtoGrupGrupAdi(Grup grup);
}
