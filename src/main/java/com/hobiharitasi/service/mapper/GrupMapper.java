package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.dto.KategoriDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Grup} and its DTO {@link GrupDTO}.
 */
@Mapper(componentModel = "spring")
public interface GrupMapper extends EntityMapper<GrupDTO, Grup> {
    @Mapping(target = "kategori", source = "kategori", qualifiedByName = "kategoriKategoriAdi")
    GrupDTO toDto(Grup s);

    @Named("kategoriKategoriAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "kategoriAdi", source = "kategoriAdi")
    KategoriDTO toDtoKategoriKategoriAdi(Kategori kategori);
}
