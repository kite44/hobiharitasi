package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.KullaniciHobisi;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link KullaniciHobisi} and its DTO {@link KullaniciHobisiDTO}.
 */
@Mapper(componentModel = "spring")
public interface KullaniciHobisiMapper extends EntityMapper<KullaniciHobisiDTO, KullaniciHobisi> {
    @Mapping(target = "hobi", source = "hobi", qualifiedByName = "hobilerHobiAdi")
    @Mapping(target = "kullanici", source = "kullanici", qualifiedByName = "kullaniciAd")
    KullaniciHobisiDTO toDto(KullaniciHobisi s);

    @Named("hobilerHobiAdi")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "hobiAdi", source = "hobiAdi")
    HobilerDTO toDtoHobilerHobiAdi(Hobiler hobiler);

    @Named("kullaniciAd")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ad", source = "ad")
    KullaniciDTO toDtoKullaniciAd(Kullanici kullanici);
}
