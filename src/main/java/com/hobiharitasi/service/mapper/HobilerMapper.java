package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.service.dto.HobilerDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Hobiler} and its DTO {@link HobilerDTO}.
 */
@Mapper(componentModel = "spring")
public interface HobilerMapper extends EntityMapper<HobilerDTO, Hobiler> {}
