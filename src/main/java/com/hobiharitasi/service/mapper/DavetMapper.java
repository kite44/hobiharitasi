package com.hobiharitasi.service.mapper;

import com.hobiharitasi.domain.Davet;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.service.dto.DavetDTO;
import com.hobiharitasi.service.dto.KullaniciDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Davet} and its DTO {@link DavetDTO}.
 */
@Mapper(componentModel = "spring")
public interface DavetMapper extends EntityMapper<DavetDTO, Davet> {
    @Mapping(target = "gonderen", source = "gonderen", qualifiedByName = "kullaniciAd")
    DavetDTO toDto(Davet s);

    @Named("kullaniciAd")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "ad", source = "ad")
    KullaniciDTO toDtoKullaniciAd(Kullanici kullanici);
}
