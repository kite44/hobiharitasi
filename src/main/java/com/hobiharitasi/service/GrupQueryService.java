package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.repository.GrupRepository;
import com.hobiharitasi.service.criteria.GrupCriteria;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.mapper.GrupMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Grup} entities in the database.
 * The main input is a {@link GrupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GrupDTO} or a {@link Page} of {@link GrupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GrupQueryService extends QueryService<Grup> {

    private final Logger log = LoggerFactory.getLogger(GrupQueryService.class);

    private final GrupRepository grupRepository;

    private final GrupMapper grupMapper;

    public GrupQueryService(GrupRepository grupRepository, GrupMapper grupMapper) {
        this.grupRepository = grupRepository;
        this.grupMapper = grupMapper;
    }

    /**
     * Return a {@link List} of {@link GrupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GrupDTO> findByCriteria(GrupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Grup> specification = createSpecification(criteria);
        return grupMapper.toDto(grupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link GrupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GrupDTO> findByCriteria(GrupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Grup> specification = createSpecification(criteria);
        return grupRepository.findAll(specification, page).map(grupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GrupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Grup> specification = createSpecification(criteria);
        return grupRepository.count(specification);
    }

    /**
     * Function to convert {@link GrupCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Grup> createSpecification(GrupCriteria criteria) {
        Specification<Grup> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Grup_.id));
            }
            if (criteria.getGrupAdi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGrupAdi(), Grup_.grupAdi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), Grup_.aciklama));
            }
            if (criteria.getGrupKategorisi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGrupKategorisi(), Grup_.grupKategorisi));
            }
            if (criteria.getKategoriId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKategoriId(), root -> root.join(Grup_.kategori, JoinType.LEFT).get(Kategori_.id))
                    );
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKullaniciId(), root -> root.join(Grup_.kullanicis, JoinType.LEFT).get(Kullanici_.id))
                    );
            }
        }
        return specification;
    }
}
