package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Yorum;
import com.hobiharitasi.repository.YorumRepository;
import com.hobiharitasi.service.criteria.YorumCriteria;
import com.hobiharitasi.service.dto.YorumDTO;
import com.hobiharitasi.service.mapper.YorumMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Yorum} entities in the database.
 * The main input is a {@link YorumCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link YorumDTO} or a {@link Page} of {@link YorumDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class YorumQueryService extends QueryService<Yorum> {

    private final Logger log = LoggerFactory.getLogger(YorumQueryService.class);

    private final YorumRepository yorumRepository;

    private final YorumMapper yorumMapper;

    public YorumQueryService(YorumRepository yorumRepository, YorumMapper yorumMapper) {
        this.yorumRepository = yorumRepository;
        this.yorumMapper = yorumMapper;
    }

    /**
     * Return a {@link List} of {@link YorumDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<YorumDTO> findByCriteria(YorumCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Yorum> specification = createSpecification(criteria);
        return yorumMapper.toDto(yorumRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link YorumDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<YorumDTO> findByCriteria(YorumCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Yorum> specification = createSpecification(criteria);
        return yorumRepository.findAll(specification, page).map(yorumMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(YorumCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Yorum> specification = createSpecification(criteria);
        return yorumRepository.count(specification);
    }

    /**
     * Function to convert {@link YorumCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Yorum> createSpecification(YorumCriteria criteria) {
        Specification<Yorum> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Yorum_.id));
            }
            if (criteria.getBaslik() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBaslik(), Yorum_.baslik));
            }
            if (criteria.getIcerik() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIcerik(), Yorum_.icerik));
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Yorum_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getKullaniciId(), root -> root.join(Yorum_.kullanici, JoinType.LEFT).get(Kullanici_.id))
                    );
            }
            if (criteria.getEtkinlikId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getEtkinlikId(), root -> root.join(Yorum_.etkinlik, JoinType.LEFT).get(Etkinlik_.id))
                    );
            }
            if (criteria.getGrupEtkinligiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getGrupEtkinligiId(),
                            root -> root.join(Yorum_.grupEtkinligi, JoinType.LEFT).get(GrupEtkinligi_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
