package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.repository.HobilerRepository;
import com.hobiharitasi.service.criteria.HobilerCriteria;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.service.mapper.HobilerMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Hobiler} entities in the database.
 * The main input is a {@link HobilerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link HobilerDTO} or a {@link Page} of {@link HobilerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HobilerQueryService extends QueryService<Hobiler> {

    private final Logger log = LoggerFactory.getLogger(HobilerQueryService.class);

    private final HobilerRepository hobilerRepository;

    private final HobilerMapper hobilerMapper;

    public HobilerQueryService(HobilerRepository hobilerRepository, HobilerMapper hobilerMapper) {
        this.hobilerRepository = hobilerRepository;
        this.hobilerMapper = hobilerMapper;
    }

    /**
     * Return a {@link List} of {@link HobilerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<HobilerDTO> findByCriteria(HobilerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Hobiler> specification = createSpecification(criteria);
        return hobilerMapper.toDto(hobilerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link HobilerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<HobilerDTO> findByCriteria(HobilerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Hobiler> specification = createSpecification(criteria);
        return hobilerRepository.findAll(specification, page).map(hobilerMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HobilerCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Hobiler> specification = createSpecification(criteria);
        return hobilerRepository.count(specification);
    }

    /**
     * Function to convert {@link HobilerCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Hobiler> createSpecification(HobilerCriteria criteria) {
        Specification<Hobiler> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Hobiler_.id));
            }
            if (criteria.getHobiAdi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHobiAdi(), Hobiler_.hobiAdi));
            }
            if (criteria.getAciklama() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciklama(), Hobiler_.aciklama));
            }
            if (criteria.getLokasyon() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLokasyon(), Hobiler_.lokasyon));
            }
            if (criteria.getCesidi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCesidi(), Hobiler_.cesidi));
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(Hobiler_.kullanicis, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
