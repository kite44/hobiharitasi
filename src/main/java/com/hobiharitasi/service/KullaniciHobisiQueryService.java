package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.KullaniciHobisi;
import com.hobiharitasi.repository.KullaniciHobisiRepository;
import com.hobiharitasi.service.criteria.KullaniciHobisiCriteria;
import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import com.hobiharitasi.service.mapper.KullaniciHobisiMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link KullaniciHobisi} entities in the database.
 * The main input is a {@link KullaniciHobisiCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KullaniciHobisiDTO} or a {@link Page} of {@link KullaniciHobisiDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KullaniciHobisiQueryService extends QueryService<KullaniciHobisi> {

    private final Logger log = LoggerFactory.getLogger(KullaniciHobisiQueryService.class);

    private final KullaniciHobisiRepository kullaniciHobisiRepository;

    private final KullaniciHobisiMapper kullaniciHobisiMapper;

    public KullaniciHobisiQueryService(KullaniciHobisiRepository kullaniciHobisiRepository, KullaniciHobisiMapper kullaniciHobisiMapper) {
        this.kullaniciHobisiRepository = kullaniciHobisiRepository;
        this.kullaniciHobisiMapper = kullaniciHobisiMapper;
    }

    /**
     * Return a {@link List} of {@link KullaniciHobisiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KullaniciHobisiDTO> findByCriteria(KullaniciHobisiCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KullaniciHobisi> specification = createSpecification(criteria);
        return kullaniciHobisiMapper.toDto(kullaniciHobisiRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KullaniciHobisiDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KullaniciHobisiDTO> findByCriteria(KullaniciHobisiCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KullaniciHobisi> specification = createSpecification(criteria);
        return kullaniciHobisiRepository.findAll(specification, page).map(kullaniciHobisiMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KullaniciHobisiCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KullaniciHobisi> specification = createSpecification(criteria);
        return kullaniciHobisiRepository.count(specification);
    }

    /**
     * Function to convert {@link KullaniciHobisiCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KullaniciHobisi> createSpecification(KullaniciHobisiCriteria criteria) {
        Specification<KullaniciHobisi> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KullaniciHobisi_.id));
            }
            if (criteria.getSeviye() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeviye(), KullaniciHobisi_.seviye));
            }
            if (criteria.getHobiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getHobiId(), root -> root.join(KullaniciHobisi_.hobi, JoinType.LEFT).get(Hobiler_.id))
                    );
            }
            if (criteria.getKullaniciId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getKullaniciId(),
                            root -> root.join(KullaniciHobisi_.kullanici, JoinType.LEFT).get(Kullanici_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
