package com.hobiharitasi.service;

import com.hobiharitasi.service.dto.EtkinlikDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.hobiharitasi.domain.Etkinlik}.
 */
public interface EtkinlikService {
    /**
     * Save a etkinlik.
     *
     * @param etkinlikDTO the entity to save.
     * @return the persisted entity.
     */
    EtkinlikDTO save(EtkinlikDTO etkinlikDTO);

    /**
     * Updates a etkinlik.
     *
     * @param etkinlikDTO the entity to update.
     * @return the persisted entity.
     */
    EtkinlikDTO update(EtkinlikDTO etkinlikDTO);

    /**
     * Partially updates a etkinlik.
     *
     * @param etkinlikDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EtkinlikDTO> partialUpdate(EtkinlikDTO etkinlikDTO);

    /**
     * Get all the etkinliks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EtkinlikDTO> findAll(Pageable pageable);

    /**
     * Get all the etkinliks with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EtkinlikDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" etkinlik.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EtkinlikDTO> findOne(Long id);

    /**
     * Delete the "id" etkinlik.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
