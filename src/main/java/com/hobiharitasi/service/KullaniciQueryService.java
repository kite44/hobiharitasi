package com.hobiharitasi.service;

import com.hobiharitasi.domain.*; // for static metamodels
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.repository.KullaniciRepository;
import com.hobiharitasi.service.criteria.KullaniciCriteria;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.mapper.KullaniciMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Kullanici} entities in the database.
 * The main input is a {@link KullaniciCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KullaniciDTO} or a {@link Page} of {@link KullaniciDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KullaniciQueryService extends QueryService<Kullanici> {

    private final Logger log = LoggerFactory.getLogger(KullaniciQueryService.class);

    private final KullaniciRepository kullaniciRepository;

    private final KullaniciMapper kullaniciMapper;

    public KullaniciQueryService(KullaniciRepository kullaniciRepository, KullaniciMapper kullaniciMapper) {
        this.kullaniciRepository = kullaniciRepository;
        this.kullaniciMapper = kullaniciMapper;
    }

    /**
     * Return a {@link List} of {@link KullaniciDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KullaniciDTO> findByCriteria(KullaniciCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Kullanici> specification = createSpecification(criteria);
        return kullaniciMapper.toDto(kullaniciRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link KullaniciDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KullaniciDTO> findByCriteria(KullaniciCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Kullanici> specification = createSpecification(criteria);
        return kullaniciRepository.findAll(specification, page).map(kullaniciMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KullaniciCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Kullanici> specification = createSpecification(criteria);
        return kullaniciRepository.count(specification);
    }

    /**
     * Function to convert {@link KullaniciCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Kullanici> createSpecification(KullaniciCriteria criteria) {
        Specification<Kullanici> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Kullanici_.id));
            }
            if (criteria.getAd() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAd(), Kullanici_.ad));
            }
            if (criteria.getSoyad() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSoyad(), Kullanici_.soyad));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Kullanici_.email));
            }
            if (criteria.getSifre() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSifre(), Kullanici_.sifre));
            }
            if (criteria.getTelNo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTelNo(), Kullanici_.telNo));
            }
            if (criteria.getBiyografi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiyografi(), Kullanici_.biyografi));
            }
            if (criteria.getAdres() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdres(), Kullanici_.adres));
            }
            if (criteria.getProfilResmi() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProfilResmi(), Kullanici_.profilResmi));
            }
            if (criteria.getAccesCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccesCode(), Kullanici_.accesCode));
            }
            if (criteria.getJwtCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJwtCode(), Kullanici_.jwtCode));
            }
            if (criteria.getTavsiyeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getTavsiyeId(), root -> root.join(Kullanici_.tavsiyes, JoinType.LEFT).get(Tavsiye_.id))
                    );
            }
            if (criteria.getTakvimId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getTakvimId(), root -> root.join(Kullanici_.takvims, JoinType.LEFT).get(Takvim_.id))
                    );
            }
            if (criteria.getUserId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getUserId(), root -> root.join(Kullanici_.user, JoinType.LEFT).get(User_.id))
                    );
            }
            if (criteria.getEtkinlikId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getEtkinlikId(),
                            root -> root.join(Kullanici_.etkinlik, JoinType.LEFT).get(Etkinlik_.id)
                        )
                    );
            }
            if (criteria.getGrupId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getGrupId(), root -> root.join(Kullanici_.grups, JoinType.LEFT).get(Grup_.id))
                    );
            }
            if (criteria.getHobiId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getHobiId(), root -> root.join(Kullanici_.hobis, JoinType.LEFT).get(Hobiler_.id))
                    );
            }
        }
        return specification;
    }
}
