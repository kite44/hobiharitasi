import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './grup-etkinligi.reducer';

export const GrupEtkinligiDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const grupEtkinligiEntity = useAppSelector(state => state.grupEtkinligi.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="grupEtkinligiDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.grupEtkinligi.detail.title">GrupEtkinligi</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{grupEtkinligiEntity.id}</dd>
          <dt>
            <span id="etkinlikAdi">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.etkinlikAdi">Etkinlik Adi</Translate>
            </span>
          </dt>
          <dd>{grupEtkinligiEntity.etkinlikAdi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{grupEtkinligiEntity.aciklama}</dd>
          <dt>
            <span id="tarih">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.tarih">Tarih</Translate>
            </span>
          </dt>
          <dd>
            {grupEtkinligiEntity.tarih ? <TextFormat value={grupEtkinligiEntity.tarih} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="konum">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.konum">Konum</Translate>
            </span>
          </dt>
          <dd>{grupEtkinligiEntity.konum}</dd>
          <dt>
            <span id="groupId">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.groupId">Group Id</Translate>
            </span>
          </dt>
          <dd>{grupEtkinligiEntity.groupId}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.grupEtkinligi.grup">Grup</Translate>
          </dt>
          <dd>{grupEtkinligiEntity.grup ? grupEtkinligiEntity.grup.grupAdi : ''}</dd>
        </dl>
        <Button tag={Link} to="/grup-etkinligi" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/grup-etkinligi/${grupEtkinligiEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default GrupEtkinligiDetail;
