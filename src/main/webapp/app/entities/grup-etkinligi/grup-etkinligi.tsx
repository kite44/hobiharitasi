import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, TextFormat, getSortState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IGrupEtkinligi } from 'app/shared/model/grup-etkinligi.model';
import { getEntities } from './grup-etkinligi.reducer';

export const GrupEtkinligi = () => {
  const dispatch = useAppDispatch();

  const location = useLocation();
  const navigate = useNavigate();

  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE, 'id'), location.search)
  );

  const grupEtkinligiList = useAppSelector(state => state.grupEtkinligi.entities);
  const loading = useAppSelector(state => state.grupEtkinligi.loading);
  const totalItems = useAppSelector(state => state.grupEtkinligi.totalItems);

  const getAllEntities = () => {
    dispatch(
      getEntities({
        page: paginationState.activePage - 1,
        size: paginationState.itemsPerPage,
        sort: `${paginationState.sort},${paginationState.order}`,
      })
    );
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (location.search !== endURL) {
      navigate(`${location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const page = params.get('page');
    const sort = params.get(SORT);
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === ASC ? DESC : ASC,
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  return (
    <div>
      <h2 id="grup-etkinligi-heading" data-cy="GrupEtkinligiHeading">
        <Translate contentKey="hobiHaritasiApp.grupEtkinligi.home.title">Grup Etkinligis</Translate>
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="hobiHaritasiApp.grupEtkinligi.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to="/grup-etkinligi/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="hobiHaritasiApp.grupEtkinligi.home.createLabel">Create new Grup Etkinligi</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {grupEtkinligiList && grupEtkinligiList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('etkinlikAdi')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.etkinlikAdi">Etkinlik Adi</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('aciklama')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.aciklama">Aciklama</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('tarih')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.tarih">Tarih</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('konum')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.konum">Konum</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('groupId')}>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.groupId">Group Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="hobiHaritasiApp.grupEtkinligi.grup">Grup</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {grupEtkinligiList.map((grupEtkinligi, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/grup-etkinligi/${grupEtkinligi.id}`} color="link" size="sm">
                      {grupEtkinligi.id}
                    </Button>
                  </td>
                  <td>{grupEtkinligi.etkinlikAdi}</td>
                  <td>{grupEtkinligi.aciklama}</td>
                  <td>{grupEtkinligi.tarih ? <TextFormat type="date" value={grupEtkinligi.tarih} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{grupEtkinligi.konum}</td>
                  <td>{grupEtkinligi.groupId}</td>
                  <td>{grupEtkinligi.grup ? <Link to={`/grup/${grupEtkinligi.grup.id}`}>{grupEtkinligi.grup.grupAdi}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/grup-etkinligi/${grupEtkinligi.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`/grup-etkinligi/${grupEtkinligi.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`/grup-etkinligi/${grupEtkinligi.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="hobiHaritasiApp.grupEtkinligi.home.notFound">No Grup Etkinligis found</Translate>
            </div>
          )
        )}
      </div>
      {totalItems ? (
        <div className={grupEtkinligiList && grupEtkinligiList.length > 0 ? '' : 'd-none'}>
          <div className="justify-content-center d-flex">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </div>
          <div className="justify-content-center d-flex">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={totalItems}
            />
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default GrupEtkinligi;
