import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IGrup } from 'app/shared/model/grup.model';
import { getEntities as getGrups } from 'app/entities/grup/grup.reducer';
import { IGrupEtkinligi } from 'app/shared/model/grup-etkinligi.model';
import { getEntity, updateEntity, createEntity, reset } from './grup-etkinligi.reducer';

export const GrupEtkinligiUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const grups = useAppSelector(state => state.grup.entities);
  const grupEtkinligiEntity = useAppSelector(state => state.grupEtkinligi.entity);
  const loading = useAppSelector(state => state.grupEtkinligi.loading);
  const updating = useAppSelector(state => state.grupEtkinligi.updating);
  const updateSuccess = useAppSelector(state => state.grupEtkinligi.updateSuccess);

  const handleClose = () => {
    navigate('/grup-etkinligi' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getGrups({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.tarih = convertDateTimeToServer(values.tarih);

    const entity = {
      ...grupEtkinligiEntity,
      ...values,
      grup: grups.find(it => it.id.toString() === values.grup.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          tarih: displayDefaultDateTime(),
        }
      : {
          ...grupEtkinligiEntity,
          tarih: convertDateTimeFromServer(grupEtkinligiEntity.tarih),
          grup: grupEtkinligiEntity?.grup?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.grupEtkinligi.home.createOrEditLabel" data-cy="GrupEtkinligiCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.grupEtkinligi.home.createOrEditLabel">Create or edit a GrupEtkinligi</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="grup-etkinligi-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.grupEtkinligi.etkinlikAdi')}
                id="grup-etkinligi-etkinlikAdi"
                name="etkinlikAdi"
                data-cy="etkinlikAdi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grupEtkinligi.aciklama')}
                id="grup-etkinligi-aciklama"
                name="aciklama"
                data-cy="aciklama"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grupEtkinligi.tarih')}
                id="grup-etkinligi-tarih"
                name="tarih"
                data-cy="tarih"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grupEtkinligi.konum')}
                id="grup-etkinligi-konum"
                name="konum"
                data-cy="konum"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grupEtkinligi.groupId')}
                id="grup-etkinligi-groupId"
                name="groupId"
                data-cy="groupId"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                id="grup-etkinligi-grup"
                name="grup"
                data-cy="grup"
                label={translate('hobiHaritasiApp.grupEtkinligi.grup')}
                type="select"
              >
                <option value="" key="0" />
                {grups
                  ? grups.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.grupAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/grup-etkinligi" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default GrupEtkinligiUpdate;
