import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import GrupEtkinligi from './grup-etkinligi';
import GrupEtkinligiDetail from './grup-etkinligi-detail';
import GrupEtkinligiUpdate from './grup-etkinligi-update';
import GrupEtkinligiDeleteDialog from './grup-etkinligi-delete-dialog';

const GrupEtkinligiRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<GrupEtkinligi />} />
    <Route path="new" element={<GrupEtkinligiUpdate />} />
    <Route path=":id">
      <Route index element={<GrupEtkinligiDetail />} />
      <Route path="edit" element={<GrupEtkinligiUpdate />} />
      <Route path="delete" element={<GrupEtkinligiDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default GrupEtkinligiRoutes;
