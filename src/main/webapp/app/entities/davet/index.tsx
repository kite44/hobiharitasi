import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Davet from './davet';
import DavetDetail from './davet-detail';
import DavetUpdate from './davet-update';
import DavetDeleteDialog from './davet-delete-dialog';

const DavetRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Davet />} />
    <Route path="new" element={<DavetUpdate />} />
    <Route path=":id">
      <Route index element={<DavetDetail />} />
      <Route path="edit" element={<DavetUpdate />} />
      <Route path="delete" element={<DavetDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default DavetRoutes;
