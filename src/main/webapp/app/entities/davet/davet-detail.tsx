import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './davet.reducer';

export const DavetDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const davetEntity = useAppSelector(state => state.davet.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="davetDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.davet.detail.title">Davet</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{davetEntity.id}</dd>
          <dt>
            <span id="davetMesaji">
              <Translate contentKey="hobiHaritasiApp.davet.davetMesaji">Davet Mesaji</Translate>
            </span>
          </dt>
          <dd>{davetEntity.davetMesaji}</dd>
          <dt>
            <span id="durum">
              <Translate contentKey="hobiHaritasiApp.davet.durum">Durum</Translate>
            </span>
          </dt>
          <dd>{davetEntity.durum}</dd>
          <dt>
            <span id="olusturulmaTarihi">
              <Translate contentKey="hobiHaritasiApp.davet.olusturulmaTarihi">Olusturulma Tarihi</Translate>
            </span>
          </dt>
          <dd>
            {davetEntity.olusturulmaTarihi ? (
              <TextFormat value={davetEntity.olusturulmaTarihi} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="davetTarihi">
              <Translate contentKey="hobiHaritasiApp.davet.davetTarihi">Davet Tarihi</Translate>
            </span>
          </dt>
          <dd>{davetEntity.davetTarihi ? <TextFormat value={davetEntity.davetTarihi} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="kabulEdildiMi">
              <Translate contentKey="hobiHaritasiApp.davet.kabulEdildiMi">Kabul Edildi Mi</Translate>
            </span>
          </dt>
          <dd>{davetEntity.kabulEdildiMi ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.davet.gonderen">Gonderen</Translate>
          </dt>
          <dd>{davetEntity.gonderen ? davetEntity.gonderen.ad : ''}</dd>
        </dl>
        <Button tag={Link} to="/davet" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/davet/${davetEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default DavetDetail;
