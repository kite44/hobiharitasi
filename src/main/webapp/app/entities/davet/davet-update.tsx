import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { IDavet } from 'app/shared/model/davet.model';
import { getEntity, updateEntity, createEntity, reset } from './davet.reducer';

export const DavetUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const davetEntity = useAppSelector(state => state.davet.entity);
  const loading = useAppSelector(state => state.davet.loading);
  const updating = useAppSelector(state => state.davet.updating);
  const updateSuccess = useAppSelector(state => state.davet.updateSuccess);

  const handleClose = () => {
    navigate('/davet' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getKullanicis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.olusturulmaTarihi = convertDateTimeToServer(values.olusturulmaTarihi);
    values.davetTarihi = convertDateTimeToServer(values.davetTarihi);

    const entity = {
      ...davetEntity,
      ...values,
      gonderen: kullanicis.find(it => it.id.toString() === values.gonderen.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          olusturulmaTarihi: displayDefaultDateTime(),
          davetTarihi: displayDefaultDateTime(),
        }
      : {
          ...davetEntity,
          olusturulmaTarihi: convertDateTimeFromServer(davetEntity.olusturulmaTarihi),
          davetTarihi: convertDateTimeFromServer(davetEntity.davetTarihi),
          gonderen: davetEntity?.gonderen?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.davet.home.createOrEditLabel" data-cy="DavetCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.davet.home.createOrEditLabel">Create or edit a Davet</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="davet-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.davet.davetMesaji')}
                id="davet-davetMesaji"
                name="davetMesaji"
                data-cy="davetMesaji"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.davet.durum')}
                id="davet-durum"
                name="durum"
                data-cy="durum"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.davet.olusturulmaTarihi')}
                id="davet-olusturulmaTarihi"
                name="olusturulmaTarihi"
                data-cy="olusturulmaTarihi"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.davet.davetTarihi')}
                id="davet-davetTarihi"
                name="davetTarihi"
                data-cy="davetTarihi"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.davet.kabulEdildiMi')}
                id="davet-kabulEdildiMi"
                name="kabulEdildiMi"
                data-cy="kabulEdildiMi"
                check
                type="checkbox"
              />
              <ValidatedField
                id="davet-gonderen"
                name="gonderen"
                data-cy="gonderen"
                label={translate('hobiHaritasiApp.davet.gonderen')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/davet" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default DavetUpdate;
