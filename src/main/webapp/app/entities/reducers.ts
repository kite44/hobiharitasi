import hobiler from 'app/entities/hobiler/hobiler.reducer';
import kullaniciHobisi from 'app/entities/kullanici-hobisi/kullanici-hobisi.reducer';
import tavsiye from 'app/entities/tavsiye/tavsiye.reducer';
import davet from 'app/entities/davet/davet.reducer';
import takvim from 'app/entities/takvim/takvim.reducer';
import kullanici from 'app/entities/kullanici/kullanici.reducer';
import etkinlik from 'app/entities/etkinlik/etkinlik.reducer';
import grup from 'app/entities/grup/grup.reducer';
import grupEtkinligi from 'app/entities/grup-etkinligi/grup-etkinligi.reducer';
import yorum from 'app/entities/yorum/yorum.reducer';
import kategori from 'app/entities/kategori/kategori.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  hobiler,
  kullaniciHobisi,
  tavsiye,
  davet,
  takvim,
  kullanici,
  etkinlik,
  grup,
  grupEtkinligi,
  yorum,
  kategori,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
