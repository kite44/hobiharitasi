import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import KullaniciHobisi from './kullanici-hobisi';
import KullaniciHobisiDetail from './kullanici-hobisi-detail';
import KullaniciHobisiUpdate from './kullanici-hobisi-update';
import KullaniciHobisiDeleteDialog from './kullanici-hobisi-delete-dialog';

const KullaniciHobisiRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<KullaniciHobisi />} />
    <Route path="new" element={<KullaniciHobisiUpdate />} />
    <Route path=":id">
      <Route index element={<KullaniciHobisiDetail />} />
      <Route path="edit" element={<KullaniciHobisiUpdate />} />
      <Route path="delete" element={<KullaniciHobisiDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default KullaniciHobisiRoutes;
