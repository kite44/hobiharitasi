import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './kullanici-hobisi.reducer';

export const KullaniciHobisiDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const kullaniciHobisiEntity = useAppSelector(state => state.kullaniciHobisi.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="kullaniciHobisiDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.kullaniciHobisi.detail.title">KullaniciHobisi</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{kullaniciHobisiEntity.id}</dd>
          <dt>
            <span id="seviye">
              <Translate contentKey="hobiHaritasiApp.kullaniciHobisi.seviye">Seviye</Translate>
            </span>
          </dt>
          <dd>{kullaniciHobisiEntity.seviye}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullaniciHobisi.hobi">Hobi</Translate>
          </dt>
          <dd>{kullaniciHobisiEntity.hobi ? kullaniciHobisiEntity.hobi.hobiAdi : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullaniciHobisi.kullanici">Kullanici</Translate>
          </dt>
          <dd>{kullaniciHobisiEntity.kullanici ? kullaniciHobisiEntity.kullanici.ad : ''}</dd>
        </dl>
        <Button tag={Link} to="/kullanici-hobisi" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/kullanici-hobisi/${kullaniciHobisiEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default KullaniciHobisiDetail;
