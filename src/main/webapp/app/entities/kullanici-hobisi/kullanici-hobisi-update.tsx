import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IHobiler } from 'app/shared/model/hobiler.model';
import { getEntities as getHobilers } from 'app/entities/hobiler/hobiler.reducer';
import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { IKullaniciHobisi } from 'app/shared/model/kullanici-hobisi.model';
import { getEntity, updateEntity, createEntity, reset } from './kullanici-hobisi.reducer';

export const KullaniciHobisiUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const hobilers = useAppSelector(state => state.hobiler.entities);
  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const kullaniciHobisiEntity = useAppSelector(state => state.kullaniciHobisi.entity);
  const loading = useAppSelector(state => state.kullaniciHobisi.loading);
  const updating = useAppSelector(state => state.kullaniciHobisi.updating);
  const updateSuccess = useAppSelector(state => state.kullaniciHobisi.updateSuccess);

  const handleClose = () => {
    navigate('/kullanici-hobisi' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getHobilers({}));
    dispatch(getKullanicis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...kullaniciHobisiEntity,
      ...values,
      hobi: hobilers.find(it => it.id.toString() === values.hobi.toString()),
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...kullaniciHobisiEntity,
          hobi: kullaniciHobisiEntity?.hobi?.id,
          kullanici: kullaniciHobisiEntity?.kullanici?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.kullaniciHobisi.home.createOrEditLabel" data-cy="KullaniciHobisiCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.kullaniciHobisi.home.createOrEditLabel">Create or edit a KullaniciHobisi</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="kullanici-hobisi-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.kullaniciHobisi.seviye')}
                id="kullanici-hobisi-seviye"
                name="seviye"
                data-cy="seviye"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                id="kullanici-hobisi-hobi"
                name="hobi"
                data-cy="hobi"
                label={translate('hobiHaritasiApp.kullaniciHobisi.hobi')}
                type="select"
              >
                <option value="" key="0" />
                {hobilers
                  ? hobilers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.hobiAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="kullanici-hobisi-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.kullaniciHobisi.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/kullanici-hobisi" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default KullaniciHobisiUpdate;
