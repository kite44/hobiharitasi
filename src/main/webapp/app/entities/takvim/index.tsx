import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Takvim from './takvim';
import TakvimDetail from './takvim-detail';
import TakvimUpdate from './takvim-update';
import TakvimDeleteDialog from './takvim-delete-dialog';

const TakvimRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Takvim />} />
    <Route path="new" element={<TakvimUpdate />} />
    <Route path=":id">
      <Route index element={<TakvimDetail />} />
      <Route path="edit" element={<TakvimUpdate />} />
      <Route path="delete" element={<TakvimDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default TakvimRoutes;
