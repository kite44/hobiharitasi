import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './takvim.reducer';

export const TakvimDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const takvimEntity = useAppSelector(state => state.takvim.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="takvimDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.takvim.detail.title">Takvim</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{takvimEntity.id}</dd>
          <dt>
            <span id="etkinlikTarihi">
              <Translate contentKey="hobiHaritasiApp.takvim.etkinlikTarihi">Etkinlik Tarihi</Translate>
            </span>
          </dt>
          <dd>
            {takvimEntity.etkinlikTarihi ? <TextFormat value={takvimEntity.etkinlikTarihi} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="etkinlikBasligi">
              <Translate contentKey="hobiHaritasiApp.takvim.etkinlikBasligi">Etkinlik Basligi</Translate>
            </span>
          </dt>
          <dd>{takvimEntity.etkinlikBasligi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.takvim.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{takvimEntity.aciklama}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.takvim.kullanici">Kullanici</Translate>
          </dt>
          <dd>{takvimEntity.kullanici ? takvimEntity.kullanici.ad : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.takvim.kullanici">Kullanici</Translate>
          </dt>
          <dd>{takvimEntity.kullanici ? takvimEntity.kullanici.ad : ''}</dd>
        </dl>
        <Button tag={Link} to="/takvim" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/takvim/${takvimEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default TakvimDetail;
