import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { ITakvim } from 'app/shared/model/takvim.model';
import { getEntity, updateEntity, createEntity, reset } from './takvim.reducer';

export const TakvimUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const takvimEntity = useAppSelector(state => state.takvim.entity);
  const loading = useAppSelector(state => state.takvim.loading);
  const updating = useAppSelector(state => state.takvim.updating);
  const updateSuccess = useAppSelector(state => state.takvim.updateSuccess);

  const handleClose = () => {
    navigate('/takvim' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getKullanicis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.etkinlikTarihi = convertDateTimeToServer(values.etkinlikTarihi);

    const entity = {
      ...takvimEntity,
      ...values,
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          etkinlikTarihi: displayDefaultDateTime(),
        }
      : {
          ...takvimEntity,
          etkinlikTarihi: convertDateTimeFromServer(takvimEntity.etkinlikTarihi),
          kullanici: takvimEntity?.kullanici?.id,
          kullanici: takvimEntity?.kullanici?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.takvim.home.createOrEditLabel" data-cy="TakvimCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.takvim.home.createOrEditLabel">Create or edit a Takvim</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="takvim-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.takvim.etkinlikTarihi')}
                id="takvim-etkinlikTarihi"
                name="etkinlikTarihi"
                data-cy="etkinlikTarihi"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.takvim.etkinlikBasligi')}
                id="takvim-etkinlikBasligi"
                name="etkinlikBasligi"
                data-cy="etkinlikBasligi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.takvim.aciklama')}
                id="takvim-aciklama"
                name="aciklama"
                data-cy="aciklama"
                type="text"
              />
              <ValidatedField
                id="takvim-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.takvim.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="takvim-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.takvim.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/takvim" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TakvimUpdate;
