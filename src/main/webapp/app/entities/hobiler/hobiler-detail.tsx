import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './hobiler.reducer';

export const HobilerDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const hobilerEntity = useAppSelector(state => state.hobiler.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="hobilerDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.hobiler.detail.title">Hobiler</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{hobilerEntity.id}</dd>
          <dt>
            <span id="hobiAdi">
              <Translate contentKey="hobiHaritasiApp.hobiler.hobiAdi">Hobi Adi</Translate>
            </span>
          </dt>
          <dd>{hobilerEntity.hobiAdi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.hobiler.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{hobilerEntity.aciklama}</dd>
          <dt>
            <span id="lokasyon">
              <Translate contentKey="hobiHaritasiApp.hobiler.lokasyon">Lokasyon</Translate>
            </span>
          </dt>
          <dd>{hobilerEntity.lokasyon}</dd>
          <dt>
            <span id="cesidi">
              <Translate contentKey="hobiHaritasiApp.hobiler.cesidi">Cesidi</Translate>
            </span>
          </dt>
          <dd>{hobilerEntity.cesidi}</dd>
        </dl>
        <Button tag={Link} to="/hobiler" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/hobiler/${hobilerEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default HobilerDetail;
