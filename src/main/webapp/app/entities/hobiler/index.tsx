import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Hobiler from './hobiler';
import HobilerDetail from './hobiler-detail';
import HobilerUpdate from './hobiler-update';
import HobilerDeleteDialog from './hobiler-delete-dialog';

const HobilerRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Hobiler />} />
    <Route path="new" element={<HobilerUpdate />} />
    <Route path=":id">
      <Route index element={<HobilerDetail />} />
      <Route path="edit" element={<HobilerUpdate />} />
      <Route path="delete" element={<HobilerDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default HobilerRoutes;
