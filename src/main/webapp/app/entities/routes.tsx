import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Hobiler from './hobiler';
import KullaniciHobisi from './kullanici-hobisi';
import Tavsiye from './tavsiye';
import Davet from './davet';
import Takvim from './takvim';
import Kullanici from './kullanici';
import Etkinlik from './etkinlik';
import Grup from './grup';
import GrupEtkinligi from './grup-etkinligi';
import Yorum from './yorum';
import Kategori from './kategori';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="hobiler/*" element={<Hobiler />} />
        <Route path="kullanici-hobisi/*" element={<KullaniciHobisi />} />
        <Route path="tavsiye/*" element={<Tavsiye />} />
        <Route path="davet/*" element={<Davet />} />
        <Route path="takvim/*" element={<Takvim />} />
        <Route path="kullanici/*" element={<Kullanici />} />
        <Route path="etkinlik/*" element={<Etkinlik />} />
        <Route path="grup/*" element={<Grup />} />
        <Route path="grup-etkinligi/*" element={<GrupEtkinligi />} />
        <Route path="yorum/*" element={<Yorum />} />
        <Route path="kategori/*" element={<Kategori />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
