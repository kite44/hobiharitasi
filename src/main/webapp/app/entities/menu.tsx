import React from 'react';
import { Translate } from 'react-jhipster';

import MenuItem from 'app/shared/layout/menus/menu-item';

const EntitiesMenu = () => {
  return (
    <>
      {/* prettier-ignore */}
      <MenuItem icon="asterisk" to="/hobiler">
        <Translate contentKey="global.menu.entities.hobiler" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/kullanici-hobisi">
        <Translate contentKey="global.menu.entities.kullaniciHobisi" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/tavsiye">
        <Translate contentKey="global.menu.entities.tavsiye" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/davet">
        <Translate contentKey="global.menu.entities.davet" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/takvim">
        <Translate contentKey="global.menu.entities.takvim" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/kullanici">
        <Translate contentKey="global.menu.entities.kullanici" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/etkinlik">
        <Translate contentKey="global.menu.entities.etkinlik" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/grup">
        <Translate contentKey="global.menu.entities.grup" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/grup-etkinligi">
        <Translate contentKey="global.menu.entities.grupEtkinligi" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/yorum">
        <Translate contentKey="global.menu.entities.yorum" />
      </MenuItem>
      <MenuItem icon="asterisk" to="/kategori">
        <Translate contentKey="global.menu.entities.kategori" />
      </MenuItem>
      {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
    </>
  );
};

export default EntitiesMenu;
