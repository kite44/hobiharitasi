import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Kullanici from './kullanici';
import KullaniciDetail from './kullanici-detail';
import KullaniciUpdate from './kullanici-update';
import KullaniciDeleteDialog from './kullanici-delete-dialog';

const KullaniciRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Kullanici />} />
    <Route path="new" element={<KullaniciUpdate />} />
    <Route path=":id">
      <Route index element={<KullaniciDetail />} />
      <Route path="edit" element={<KullaniciUpdate />} />
      <Route path="delete" element={<KullaniciDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default KullaniciRoutes;
