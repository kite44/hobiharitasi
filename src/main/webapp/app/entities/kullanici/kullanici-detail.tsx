import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './kullanici.reducer';

export const KullaniciDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const kullaniciEntity = useAppSelector(state => state.kullanici.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="kullaniciDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.kullanici.detail.title">Kullanici</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.id}</dd>
          <dt>
            <span id="ad">
              <Translate contentKey="hobiHaritasiApp.kullanici.ad">Ad</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.ad}</dd>
          <dt>
            <span id="soyad">
              <Translate contentKey="hobiHaritasiApp.kullanici.soyad">Soyad</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.soyad}</dd>
          <dt>
            <span id="email">
              <Translate contentKey="hobiHaritasiApp.kullanici.email">Email</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.email}</dd>
          <dt>
            <span id="sifre">
              <Translate contentKey="hobiHaritasiApp.kullanici.sifre">Sifre</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.sifre}</dd>
          <dt>
            <span id="telNo">
              <Translate contentKey="hobiHaritasiApp.kullanici.telNo">Tel No</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.telNo}</dd>
          <dt>
            <span id="biyografi">
              <Translate contentKey="hobiHaritasiApp.kullanici.biyografi">Biyografi</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.biyografi}</dd>
          <dt>
            <span id="adres">
              <Translate contentKey="hobiHaritasiApp.kullanici.adres">Adres</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.adres}</dd>
          <dt>
            <span id="profilResmi">
              <Translate contentKey="hobiHaritasiApp.kullanici.profilResmi">Profil Resmi</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.profilResmi}</dd>
          <dt>
            <span id="accesCode">
              <Translate contentKey="hobiHaritasiApp.kullanici.accesCode">Acces Code</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.accesCode}</dd>
          <dt>
            <span id="jwtCode">
              <Translate contentKey="hobiHaritasiApp.kullanici.jwtCode">Jwt Code</Translate>
            </span>
          </dt>
          <dd>{kullaniciEntity.jwtCode}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullanici.user">User</Translate>
          </dt>
          <dd>{kullaniciEntity.user ? kullaniciEntity.user.login : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullanici.etkinlik">Etkinlik</Translate>
          </dt>
          <dd>{kullaniciEntity.etkinlik ? kullaniciEntity.etkinlik.etkinlikAdi : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullanici.grup">Grup</Translate>
          </dt>
          <dd>
            {kullaniciEntity.grups
              ? kullaniciEntity.grups.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.grupAdi}</a>
                    {kullaniciEntity.grups && i === kullaniciEntity.grups.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.kullanici.hobi">Hobi</Translate>
          </dt>
          <dd>
            {kullaniciEntity.hobis
              ? kullaniciEntity.hobis.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.hobiAdi}</a>
                    {kullaniciEntity.hobis && i === kullaniciEntity.hobis.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/kullanici" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/kullanici/${kullaniciEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default KullaniciDetail;
