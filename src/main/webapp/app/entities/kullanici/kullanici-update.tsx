import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IEtkinlik } from 'app/shared/model/etkinlik.model';
import { getEntities as getEtkinliks } from 'app/entities/etkinlik/etkinlik.reducer';
import { IGrup } from 'app/shared/model/grup.model';
import { getEntities as getGrups } from 'app/entities/grup/grup.reducer';
import { IHobiler } from 'app/shared/model/hobiler.model';
import { getEntities as getHobilers } from 'app/entities/hobiler/hobiler.reducer';
import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntity, updateEntity, createEntity, reset } from './kullanici.reducer';

export const KullaniciUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const users = useAppSelector(state => state.userManagement.users);
  const etkinliks = useAppSelector(state => state.etkinlik.entities);
  const grups = useAppSelector(state => state.grup.entities);
  const hobilers = useAppSelector(state => state.hobiler.entities);
  const kullaniciEntity = useAppSelector(state => state.kullanici.entity);
  const loading = useAppSelector(state => state.kullanici.loading);
  const updating = useAppSelector(state => state.kullanici.updating);
  const updateSuccess = useAppSelector(state => state.kullanici.updateSuccess);

  const handleClose = () => {
    navigate('/kullanici' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getUsers({}));
    dispatch(getEtkinliks({}));
    dispatch(getGrups({}));
    dispatch(getHobilers({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...kullaniciEntity,
      ...values,
      grups: mapIdList(values.grups),
      hobis: mapIdList(values.hobis),
      user: users.find(it => it.id.toString() === values.user.toString()),
      etkinlik: etkinliks.find(it => it.id.toString() === values.etkinlik.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...kullaniciEntity,
          user: kullaniciEntity?.user?.id,
          etkinlik: kullaniciEntity?.etkinlik?.id,
          grups: kullaniciEntity?.grups?.map(e => e.id.toString()),
          hobis: kullaniciEntity?.hobis?.map(e => e.id.toString()),
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.kullanici.home.createOrEditLabel" data-cy="KullaniciCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.kullanici.home.createOrEditLabel">Create or edit a Kullanici</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="kullanici-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.ad')}
                id="kullanici-ad"
                name="ad"
                data-cy="ad"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.soyad')}
                id="kullanici-soyad"
                name="soyad"
                data-cy="soyad"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.email')}
                id="kullanici-email"
                name="email"
                data-cy="email"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.sifre')}
                id="kullanici-sifre"
                name="sifre"
                data-cy="sifre"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.telNo')}
                id="kullanici-telNo"
                name="telNo"
                data-cy="telNo"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.biyografi')}
                id="kullanici-biyografi"
                name="biyografi"
                data-cy="biyografi"
                type="text"
                validate={{
                  maxLength: { value: 500, message: translate('entity.validation.maxlength', { max: 500 }) },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.adres')}
                id="kullanici-adres"
                name="adres"
                data-cy="adres"
                type="text"
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.profilResmi')}
                id="kullanici-profilResmi"
                name="profilResmi"
                data-cy="profilResmi"
                type="text"
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.accesCode')}
                id="kullanici-accesCode"
                name="accesCode"
                data-cy="accesCode"
                type="text"
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.jwtCode')}
                id="kullanici-jwtCode"
                name="jwtCode"
                data-cy="jwtCode"
                type="text"
              />
              <ValidatedField
                id="kullanici-user"
                name="user"
                data-cy="user"
                label={translate('hobiHaritasiApp.kullanici.user')}
                type="select"
              >
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.login}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="kullanici-etkinlik"
                name="etkinlik"
                data-cy="etkinlik"
                label={translate('hobiHaritasiApp.kullanici.etkinlik')}
                type="select"
              >
                <option value="" key="0" />
                {etkinliks
                  ? etkinliks.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.etkinlikAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.grup')}
                id="kullanici-grup"
                data-cy="grup"
                type="select"
                multiple
                name="grups"
              >
                <option value="" key="0" />
                {grups
                  ? grups.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.grupAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                label={translate('hobiHaritasiApp.kullanici.hobi')}
                id="kullanici-hobi"
                data-cy="hobi"
                type="select"
                multiple
                name="hobis"
              >
                <option value="" key="0" />
                {hobilers
                  ? hobilers.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.hobiAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/kullanici" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default KullaniciUpdate;
