import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './grup.reducer';

export const GrupDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const grupEntity = useAppSelector(state => state.grup.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="grupDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.grup.detail.title">Grup</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{grupEntity.id}</dd>
          <dt>
            <span id="grupAdi">
              <Translate contentKey="hobiHaritasiApp.grup.grupAdi">Grup Adi</Translate>
            </span>
          </dt>
          <dd>{grupEntity.grupAdi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.grup.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{grupEntity.aciklama}</dd>
          <dt>
            <span id="grupKategorisi">
              <Translate contentKey="hobiHaritasiApp.grup.grupKategorisi">Grup Kategorisi</Translate>
            </span>
          </dt>
          <dd>{grupEntity.grupKategorisi}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.grup.kategori">Kategori</Translate>
          </dt>
          <dd>{grupEntity.kategori ? grupEntity.kategori.kategoriAdi : ''}</dd>
        </dl>
        <Button tag={Link} to="/grup" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/grup/${grupEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default GrupDetail;
