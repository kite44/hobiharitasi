import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IKategori } from 'app/shared/model/kategori.model';
import { getEntities as getKategoris } from 'app/entities/kategori/kategori.reducer';
import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { IGrup } from 'app/shared/model/grup.model';
import { getEntity, updateEntity, createEntity, reset } from './grup.reducer';

export const GrupUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const kategoris = useAppSelector(state => state.kategori.entities);
  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const grupEntity = useAppSelector(state => state.grup.entity);
  const loading = useAppSelector(state => state.grup.loading);
  const updating = useAppSelector(state => state.grup.updating);
  const updateSuccess = useAppSelector(state => state.grup.updateSuccess);

  const handleClose = () => {
    navigate('/grup' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getKategoris({}));
    dispatch(getKullanicis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...grupEntity,
      ...values,
      kategori: kategoris.find(it => it.id.toString() === values.kategori.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...grupEntity,
          kategori: grupEntity?.kategori?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.grup.home.createOrEditLabel" data-cy="GrupCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.grup.home.createOrEditLabel">Create or edit a Grup</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="grup-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.grup.grupAdi')}
                id="grup-grupAdi"
                name="grupAdi"
                data-cy="grupAdi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grup.aciklama')}
                id="grup-aciklama"
                name="aciklama"
                data-cy="aciklama"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.grup.grupKategorisi')}
                id="grup-grupKategorisi"
                name="grupKategorisi"
                data-cy="grupKategorisi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                id="grup-kategori"
                name="kategori"
                data-cy="kategori"
                label={translate('hobiHaritasiApp.grup.kategori')}
                type="select"
              >
                <option value="" key="0" />
                {kategoris
                  ? kategoris.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.kategoriAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/grup" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default GrupUpdate;
