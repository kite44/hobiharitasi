import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Grup from './grup';
import GrupDetail from './grup-detail';
import GrupUpdate from './grup-update';
import GrupDeleteDialog from './grup-delete-dialog';

const GrupRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Grup />} />
    <Route path="new" element={<GrupUpdate />} />
    <Route path=":id">
      <Route index element={<GrupDetail />} />
      <Route path="edit" element={<GrupUpdate />} />
      <Route path="delete" element={<GrupDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default GrupRoutes;
