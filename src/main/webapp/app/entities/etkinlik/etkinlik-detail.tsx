import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './etkinlik.reducer';

export const EtkinlikDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const etkinlikEntity = useAppSelector(state => state.etkinlik.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="etkinlikDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.etkinlik.detail.title">Etkinlik</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.id}</dd>
          <dt>
            <span id="etkinlikAdi">
              <Translate contentKey="hobiHaritasiApp.etkinlik.etkinlikAdi">Etkinlik Adi</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.etkinlikAdi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.etkinlik.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.aciklama}</dd>
          <dt>
            <span id="tarih">
              <Translate contentKey="hobiHaritasiApp.etkinlik.tarih">Tarih</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.tarih ? <TextFormat value={etkinlikEntity.tarih} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="konum">
              <Translate contentKey="hobiHaritasiApp.etkinlik.konum">Konum</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.konum}</dd>
          <dt>
            <span id="maksimumKatilimci">
              <Translate contentKey="hobiHaritasiApp.etkinlik.maksimumKatilimci">Maksimum Katilimci</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.maksimumKatilimci}</dd>
          <dt>
            <span id="minimumKatilimci">
              <Translate contentKey="hobiHaritasiApp.etkinlik.minimumKatilimci">Minimum Katilimci</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.minimumKatilimci}</dd>
          <dt>
            <span id="etkinlikKategorisi">
              <Translate contentKey="hobiHaritasiApp.etkinlik.etkinlikKategorisi">Etkinlik Kategorisi</Translate>
            </span>
          </dt>
          <dd>{etkinlikEntity.etkinlikKategorisi}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.etkinlik.kategori">Kategori</Translate>
          </dt>
          <dd>
            {etkinlikEntity.kategoris
              ? etkinlikEntity.kategoris.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.kategoriAdi}</a>
                    {etkinlikEntity.kategoris && i === etkinlikEntity.kategoris.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/etkinlik" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/etkinlik/${etkinlikEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default EtkinlikDetail;
