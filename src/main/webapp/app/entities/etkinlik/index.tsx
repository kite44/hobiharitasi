import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Etkinlik from './etkinlik';
import EtkinlikDetail from './etkinlik-detail';
import EtkinlikUpdate from './etkinlik-update';
import EtkinlikDeleteDialog from './etkinlik-delete-dialog';

const EtkinlikRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Etkinlik />} />
    <Route path="new" element={<EtkinlikUpdate />} />
    <Route path=":id">
      <Route index element={<EtkinlikDetail />} />
      <Route path="edit" element={<EtkinlikUpdate />} />
      <Route path="delete" element={<EtkinlikDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default EtkinlikRoutes;
