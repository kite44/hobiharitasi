import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IKategori } from 'app/shared/model/kategori.model';
import { getEntities as getKategoris } from 'app/entities/kategori/kategori.reducer';
import { IEtkinlik } from 'app/shared/model/etkinlik.model';
import { getEntity, updateEntity, createEntity, reset } from './etkinlik.reducer';

export const EtkinlikUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const kategoris = useAppSelector(state => state.kategori.entities);
  const etkinlikEntity = useAppSelector(state => state.etkinlik.entity);
  const loading = useAppSelector(state => state.etkinlik.loading);
  const updating = useAppSelector(state => state.etkinlik.updating);
  const updateSuccess = useAppSelector(state => state.etkinlik.updateSuccess);

  const handleClose = () => {
    navigate('/etkinlik' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getKategoris({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.tarih = convertDateTimeToServer(values.tarih);

    const entity = {
      ...etkinlikEntity,
      ...values,
      kategoris: mapIdList(values.kategoris),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          tarih: displayDefaultDateTime(),
        }
      : {
          ...etkinlikEntity,
          tarih: convertDateTimeFromServer(etkinlikEntity.tarih),
          kategoris: etkinlikEntity?.kategoris?.map(e => e.id.toString()),
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.etkinlik.home.createOrEditLabel" data-cy="EtkinlikCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.etkinlik.home.createOrEditLabel">Create or edit a Etkinlik</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="etkinlik-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.etkinlikAdi')}
                id="etkinlik-etkinlikAdi"
                name="etkinlikAdi"
                data-cy="etkinlikAdi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.aciklama')}
                id="etkinlik-aciklama"
                name="aciklama"
                data-cy="aciklama"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.tarih')}
                id="etkinlik-tarih"
                name="tarih"
                data-cy="tarih"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.konum')}
                id="etkinlik-konum"
                name="konum"
                data-cy="konum"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.maksimumKatilimci')}
                id="etkinlik-maksimumKatilimci"
                name="maksimumKatilimci"
                data-cy="maksimumKatilimci"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.minimumKatilimci')}
                id="etkinlik-minimumKatilimci"
                name="minimumKatilimci"
                data-cy="minimumKatilimci"
                type="text"
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.etkinlikKategorisi')}
                id="etkinlik-etkinlikKategorisi"
                name="etkinlikKategorisi"
                data-cy="etkinlikKategorisi"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.etkinlik.kategori')}
                id="etkinlik-kategori"
                data-cy="kategori"
                type="select"
                multiple
                name="kategoris"
              >
                <option value="" key="0" />
                {kategoris
                  ? kategoris.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.kategoriAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/etkinlik" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default EtkinlikUpdate;
