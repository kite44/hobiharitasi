import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { ITavsiye } from 'app/shared/model/tavsiye.model';
import { getEntity, updateEntity, createEntity, reset } from './tavsiye.reducer';

export const TavsiyeUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const tavsiyeEntity = useAppSelector(state => state.tavsiye.entity);
  const loading = useAppSelector(state => state.tavsiye.loading);
  const updating = useAppSelector(state => state.tavsiye.updating);
  const updateSuccess = useAppSelector(state => state.tavsiye.updateSuccess);

  const handleClose = () => {
    navigate('/tavsiye' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getKullanicis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.olusturulmaTarihi = convertDateTimeToServer(values.olusturulmaTarihi);

    const entity = {
      ...tavsiyeEntity,
      ...values,
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          olusturulmaTarihi: displayDefaultDateTime(),
        }
      : {
          ...tavsiyeEntity,
          olusturulmaTarihi: convertDateTimeFromServer(tavsiyeEntity.olusturulmaTarihi),
          kullanici: tavsiyeEntity?.kullanici?.id,
          kullanici: tavsiyeEntity?.kullanici?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.tavsiye.home.createOrEditLabel" data-cy="TavsiyeCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.tavsiye.home.createOrEditLabel">Create or edit a Tavsiye</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="tavsiye-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.tavsiye.baslik')}
                id="tavsiye-baslik"
                name="baslik"
                data-cy="baslik"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.tavsiye.icerik')}
                id="tavsiye-icerik"
                name="icerik"
                data-cy="icerik"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.tavsiye.olusturulmaTarihi')}
                id="tavsiye-olusturulmaTarihi"
                name="olusturulmaTarihi"
                data-cy="olusturulmaTarihi"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                id="tavsiye-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.tavsiye.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="tavsiye-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.tavsiye.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/tavsiye" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TavsiyeUpdate;
