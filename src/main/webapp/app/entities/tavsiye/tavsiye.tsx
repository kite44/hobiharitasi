import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, TextFormat, getSortState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { ITavsiye } from 'app/shared/model/tavsiye.model';
import { getEntities } from './tavsiye.reducer';

export const Tavsiye = () => {
  const dispatch = useAppDispatch();

  const location = useLocation();
  const navigate = useNavigate();

  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE, 'id'), location.search)
  );

  const tavsiyeList = useAppSelector(state => state.tavsiye.entities);
  const loading = useAppSelector(state => state.tavsiye.loading);
  const totalItems = useAppSelector(state => state.tavsiye.totalItems);

  const getAllEntities = () => {
    dispatch(
      getEntities({
        page: paginationState.activePage - 1,
        size: paginationState.itemsPerPage,
        sort: `${paginationState.sort},${paginationState.order}`,
      })
    );
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (location.search !== endURL) {
      navigate(`${location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const page = params.get('page');
    const sort = params.get(SORT);
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === ASC ? DESC : ASC,
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  return (
    <div>
      <h2 id="tavsiye-heading" data-cy="TavsiyeHeading">
        <Translate contentKey="hobiHaritasiApp.tavsiye.home.title">Tavsiyes</Translate>
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="hobiHaritasiApp.tavsiye.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to="/tavsiye/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="hobiHaritasiApp.tavsiye.home.createLabel">Create new Tavsiye</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {tavsiyeList && tavsiyeList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('baslik')}>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.baslik">Baslik</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('icerik')}>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.icerik">Icerik</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={sort('olusturulmaTarihi')}>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.olusturulmaTarihi">Olusturulma Tarihi</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.kullanici">Kullanici</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="hobiHaritasiApp.tavsiye.kullanici">Kullanici</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {tavsiyeList.map((tavsiye, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/tavsiye/${tavsiye.id}`} color="link" size="sm">
                      {tavsiye.id}
                    </Button>
                  </td>
                  <td>{tavsiye.baslik}</td>
                  <td>{tavsiye.icerik}</td>
                  <td>
                    {tavsiye.olusturulmaTarihi ? (
                      <TextFormat type="date" value={tavsiye.olusturulmaTarihi} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{tavsiye.kullanici ? <Link to={`/kullanici/${tavsiye.kullanici.id}`}>{tavsiye.kullanici.ad}</Link> : ''}</td>
                  <td>{tavsiye.kullanici ? <Link to={`/kullanici/${tavsiye.kullanici.id}`}>{tavsiye.kullanici.ad}</Link> : ''}</td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/tavsiye/${tavsiye.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`/tavsiye/${tavsiye.id}/edit?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`/tavsiye/${tavsiye.id}/delete?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="hobiHaritasiApp.tavsiye.home.notFound">No Tavsiyes found</Translate>
            </div>
          )
        )}
      </div>
      {totalItems ? (
        <div className={tavsiyeList && tavsiyeList.length > 0 ? '' : 'd-none'}>
          <div className="justify-content-center d-flex">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} i18nEnabled />
          </div>
          <div className="justify-content-center d-flex">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={totalItems}
            />
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default Tavsiye;
