import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './tavsiye.reducer';

export const TavsiyeDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const tavsiyeEntity = useAppSelector(state => state.tavsiye.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="tavsiyeDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.tavsiye.detail.title">Tavsiye</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{tavsiyeEntity.id}</dd>
          <dt>
            <span id="baslik">
              <Translate contentKey="hobiHaritasiApp.tavsiye.baslik">Baslik</Translate>
            </span>
          </dt>
          <dd>{tavsiyeEntity.baslik}</dd>
          <dt>
            <span id="icerik">
              <Translate contentKey="hobiHaritasiApp.tavsiye.icerik">Icerik</Translate>
            </span>
          </dt>
          <dd>{tavsiyeEntity.icerik}</dd>
          <dt>
            <span id="olusturulmaTarihi">
              <Translate contentKey="hobiHaritasiApp.tavsiye.olusturulmaTarihi">Olusturulma Tarihi</Translate>
            </span>
          </dt>
          <dd>
            {tavsiyeEntity.olusturulmaTarihi ? (
              <TextFormat value={tavsiyeEntity.olusturulmaTarihi} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.tavsiye.kullanici">Kullanici</Translate>
          </dt>
          <dd>{tavsiyeEntity.kullanici ? tavsiyeEntity.kullanici.ad : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.tavsiye.kullanici">Kullanici</Translate>
          </dt>
          <dd>{tavsiyeEntity.kullanici ? tavsiyeEntity.kullanici.ad : ''}</dd>
        </dl>
        <Button tag={Link} to="/tavsiye" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/tavsiye/${tavsiyeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default TavsiyeDetail;
