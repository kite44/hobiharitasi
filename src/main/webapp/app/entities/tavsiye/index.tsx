import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Tavsiye from './tavsiye';
import TavsiyeDetail from './tavsiye-detail';
import TavsiyeUpdate from './tavsiye-update';
import TavsiyeDeleteDialog from './tavsiye-delete-dialog';

const TavsiyeRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Tavsiye />} />
    <Route path="new" element={<TavsiyeUpdate />} />
    <Route path=":id">
      <Route index element={<TavsiyeDetail />} />
      <Route path="edit" element={<TavsiyeUpdate />} />
      <Route path="delete" element={<TavsiyeDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default TavsiyeRoutes;
