import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IKullanici } from 'app/shared/model/kullanici.model';
import { getEntities as getKullanicis } from 'app/entities/kullanici/kullanici.reducer';
import { IEtkinlik } from 'app/shared/model/etkinlik.model';
import { getEntities as getEtkinliks } from 'app/entities/etkinlik/etkinlik.reducer';
import { IGrupEtkinligi } from 'app/shared/model/grup-etkinligi.model';
import { getEntities as getGrupEtkinligis } from 'app/entities/grup-etkinligi/grup-etkinligi.reducer';
import { IYorum } from 'app/shared/model/yorum.model';
import { getEntity, updateEntity, createEntity, reset } from './yorum.reducer';

export const YorumUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const users = useAppSelector(state => state.userManagement.users);
  const kullanicis = useAppSelector(state => state.kullanici.entities);
  const etkinliks = useAppSelector(state => state.etkinlik.entities);
  const grupEtkinligis = useAppSelector(state => state.grupEtkinligi.entities);
  const yorumEntity = useAppSelector(state => state.yorum.entity);
  const loading = useAppSelector(state => state.yorum.loading);
  const updating = useAppSelector(state => state.yorum.updating);
  const updateSuccess = useAppSelector(state => state.yorum.updateSuccess);

  const handleClose = () => {
    navigate('/yorum' + location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getUsers({}));
    dispatch(getKullanicis({}));
    dispatch(getEtkinliks({}));
    dispatch(getGrupEtkinligis({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...yorumEntity,
      ...values,
      user: users.find(it => it.id.toString() === values.user.toString()),
      kullanici: kullanicis.find(it => it.id.toString() === values.kullanici.toString()),
      etkinlik: etkinliks.find(it => it.id.toString() === values.etkinlik.toString()),
      grupEtkinligi: grupEtkinligis.find(it => it.id.toString() === values.grupEtkinligi.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...yorumEntity,
          user: yorumEntity?.user?.id,
          kullanici: yorumEntity?.kullanici?.id,
          etkinlik: yorumEntity?.etkinlik?.id,
          grupEtkinligi: yorumEntity?.grupEtkinligi?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="hobiHaritasiApp.yorum.home.createOrEditLabel" data-cy="YorumCreateUpdateHeading">
            <Translate contentKey="hobiHaritasiApp.yorum.home.createOrEditLabel">Create or edit a Yorum</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="yorum-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('hobiHaritasiApp.yorum.baslik')}
                id="yorum-baslik"
                name="baslik"
                data-cy="baslik"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('hobiHaritasiApp.yorum.icerik')}
                id="yorum-icerik"
                name="icerik"
                data-cy="icerik"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField id="yorum-user" name="user" data-cy="user" label={translate('hobiHaritasiApp.yorum.user')} type="select">
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.login}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="yorum-kullanici"
                name="kullanici"
                data-cy="kullanici"
                label={translate('hobiHaritasiApp.yorum.kullanici')}
                type="select"
              >
                <option value="" key="0" />
                {kullanicis
                  ? kullanicis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.ad}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="yorum-etkinlik"
                name="etkinlik"
                data-cy="etkinlik"
                label={translate('hobiHaritasiApp.yorum.etkinlik')}
                type="select"
              >
                <option value="" key="0" />
                {etkinliks
                  ? etkinliks.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.etkinlikAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="yorum-grupEtkinligi"
                name="grupEtkinligi"
                data-cy="grupEtkinligi"
                label={translate('hobiHaritasiApp.yorum.grupEtkinligi')}
                type="select"
              >
                <option value="" key="0" />
                {grupEtkinligis
                  ? grupEtkinligis.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.etkinlikAdi}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/yorum" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default YorumUpdate;
