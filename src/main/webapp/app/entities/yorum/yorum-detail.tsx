import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './yorum.reducer';

export const YorumDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const yorumEntity = useAppSelector(state => state.yorum.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="yorumDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.yorum.detail.title">Yorum</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{yorumEntity.id}</dd>
          <dt>
            <span id="baslik">
              <Translate contentKey="hobiHaritasiApp.yorum.baslik">Baslik</Translate>
            </span>
          </dt>
          <dd>{yorumEntity.baslik}</dd>
          <dt>
            <span id="icerik">
              <Translate contentKey="hobiHaritasiApp.yorum.icerik">Icerik</Translate>
            </span>
          </dt>
          <dd>{yorumEntity.icerik}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.yorum.user">User</Translate>
          </dt>
          <dd>{yorumEntity.user ? yorumEntity.user.login : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.yorum.kullanici">Kullanici</Translate>
          </dt>
          <dd>{yorumEntity.kullanici ? yorumEntity.kullanici.ad : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.yorum.etkinlik">Etkinlik</Translate>
          </dt>
          <dd>{yorumEntity.etkinlik ? yorumEntity.etkinlik.etkinlikAdi : ''}</dd>
          <dt>
            <Translate contentKey="hobiHaritasiApp.yorum.grupEtkinligi">Grup Etkinligi</Translate>
          </dt>
          <dd>{yorumEntity.grupEtkinligi ? yorumEntity.grupEtkinligi.etkinlikAdi : ''}</dd>
        </dl>
        <Button tag={Link} to="/yorum" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/yorum/${yorumEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default YorumDetail;
