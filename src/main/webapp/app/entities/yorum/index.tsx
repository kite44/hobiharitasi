import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Yorum from './yorum';
import YorumDetail from './yorum-detail';
import YorumUpdate from './yorum-update';
import YorumDeleteDialog from './yorum-delete-dialog';

const YorumRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Yorum />} />
    <Route path="new" element={<YorumUpdate />} />
    <Route path=":id">
      <Route index element={<YorumDetail />} />
      <Route path="edit" element={<YorumUpdate />} />
      <Route path="delete" element={<YorumDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default YorumRoutes;
