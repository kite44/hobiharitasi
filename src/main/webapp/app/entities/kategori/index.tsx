import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Kategori from './kategori';
import KategoriDetail from './kategori-detail';
import KategoriUpdate from './kategori-update';
import KategoriDeleteDialog from './kategori-delete-dialog';

const KategoriRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Kategori />} />
    <Route path="new" element={<KategoriUpdate />} />
    <Route path=":id">
      <Route index element={<KategoriDetail />} />
      <Route path="edit" element={<KategoriUpdate />} />
      <Route path="delete" element={<KategoriDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default KategoriRoutes;
