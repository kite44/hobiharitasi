import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './kategori.reducer';

export const KategoriDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const kategoriEntity = useAppSelector(state => state.kategori.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="kategoriDetailsHeading">
          <Translate contentKey="hobiHaritasiApp.kategori.detail.title">Kategori</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{kategoriEntity.id}</dd>
          <dt>
            <span id="kategoriAdi">
              <Translate contentKey="hobiHaritasiApp.kategori.kategoriAdi">Kategori Adi</Translate>
            </span>
          </dt>
          <dd>{kategoriEntity.kategoriAdi}</dd>
          <dt>
            <span id="aciklama">
              <Translate contentKey="hobiHaritasiApp.kategori.aciklama">Aciklama</Translate>
            </span>
          </dt>
          <dd>{kategoriEntity.aciklama}</dd>
        </dl>
        <Button tag={Link} to="/kategori" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/kategori/${kategoriEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default KategoriDetail;
