import dayjs from 'dayjs';
import { IKullanici } from 'app/shared/model/kullanici.model';

export interface IDavet {
  id?: number;
  davetMesaji?: string;
  durum?: string;
  olusturulmaTarihi?: string;
  davetTarihi?: string;
  kabulEdildiMi?: boolean | null;
  gonderen?: IKullanici | null;
}

export const defaultValue: Readonly<IDavet> = {
  kabulEdildiMi: false,
};
