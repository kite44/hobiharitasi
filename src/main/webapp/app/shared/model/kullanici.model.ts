import { ITavsiye } from 'app/shared/model/tavsiye.model';
import { ITakvim } from 'app/shared/model/takvim.model';
import { IUser } from 'app/shared/model/user.model';
import { IEtkinlik } from 'app/shared/model/etkinlik.model';
import { IGrup } from 'app/shared/model/grup.model';
import { IHobiler } from 'app/shared/model/hobiler.model';

export interface IKullanici {
  id?: number;
  ad?: string;
  soyad?: string;
  email?: string;
  sifre?: string;
  telNo?: number;
  biyografi?: string | null;
  adres?: string | null;
  profilResmi?: string | null;
  accesCode?: string | null;
  jwtCode?: string | null;
  tavsiyes?: ITavsiye[] | null;
  takvims?: ITakvim[] | null;
  user?: IUser | null;
  etkinlik?: IEtkinlik | null;
  grups?: IGrup[] | null;
  hobis?: IHobiler[] | null;
}

export const defaultValue: Readonly<IKullanici> = {};
