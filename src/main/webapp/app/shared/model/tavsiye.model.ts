import dayjs from 'dayjs';
import { IKullanici } from 'app/shared/model/kullanici.model';

export interface ITavsiye {
  id?: number;
  baslik?: string;
  icerik?: string;
  olusturulmaTarihi?: string;
  kullanici?: IKullanici | null;
  kullanici?: IKullanici | null;
}

export const defaultValue: Readonly<ITavsiye> = {};
