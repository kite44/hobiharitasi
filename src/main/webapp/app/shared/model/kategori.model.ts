import { IEtkinlik } from 'app/shared/model/etkinlik.model';

export interface IKategori {
  id?: number;
  kategoriAdi?: string;
  aciklama?: string;
  etkinliks?: IEtkinlik[] | null;
}

export const defaultValue: Readonly<IKategori> = {};
