import { IKategori } from 'app/shared/model/kategori.model';
import { IKullanici } from 'app/shared/model/kullanici.model';

export interface IGrup {
  id?: number;
  grupAdi?: string;
  aciklama?: string;
  grupKategorisi?: string;
  kategori?: IKategori | null;
  kullanicis?: IKullanici[] | null;
}

export const defaultValue: Readonly<IGrup> = {};
