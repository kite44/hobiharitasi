import dayjs from 'dayjs';
import { IKullanici } from 'app/shared/model/kullanici.model';

export interface ITakvim {
  id?: number;
  etkinlikTarihi?: string;
  etkinlikBasligi?: string;
  aciklama?: string | null;
  kullanici?: IKullanici | null;
  kullanici?: IKullanici | null;
}

export const defaultValue: Readonly<ITakvim> = {};
