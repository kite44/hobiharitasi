import dayjs from 'dayjs';
import { IKategori } from 'app/shared/model/kategori.model';

export interface IEtkinlik {
  id?: number;
  etkinlikAdi?: string;
  aciklama?: string;
  tarih?: string;
  konum?: string;
  maksimumKatilimci?: number;
  minimumKatilimci?: number | null;
  etkinlikKategorisi?: string;
  kategoris?: IKategori[] | null;
}

export const defaultValue: Readonly<IEtkinlik> = {};
