import { IUser } from 'app/shared/model/user.model';
import { IKullanici } from 'app/shared/model/kullanici.model';
import { IEtkinlik } from 'app/shared/model/etkinlik.model';
import { IGrupEtkinligi } from 'app/shared/model/grup-etkinligi.model';

export interface IYorum {
  id?: number;
  baslik?: string;
  icerik?: string;
  user?: IUser | null;
  kullanici?: IKullanici | null;
  etkinlik?: IEtkinlik | null;
  grupEtkinligi?: IGrupEtkinligi | null;
}

export const defaultValue: Readonly<IYorum> = {};
