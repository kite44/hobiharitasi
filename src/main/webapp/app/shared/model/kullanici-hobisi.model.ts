import { IHobiler } from 'app/shared/model/hobiler.model';
import { IKullanici } from 'app/shared/model/kullanici.model';

export interface IKullaniciHobisi {
  id?: number;
  seviye?: number;
  hobi?: IHobiler | null;
  kullanici?: IKullanici | null;
}

export const defaultValue: Readonly<IKullaniciHobisi> = {};
