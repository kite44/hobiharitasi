import { IKullanici } from 'app/shared/model/kullanici.model';

export interface IHobiler {
  id?: number;
  hobiAdi?: string;
  aciklama?: string | null;
  lokasyon?: string | null;
  cesidi?: string | null;
  kullanicis?: IKullanici[] | null;
}

export const defaultValue: Readonly<IHobiler> = {};
