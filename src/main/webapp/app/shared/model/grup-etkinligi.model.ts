import dayjs from 'dayjs';
import { IGrup } from 'app/shared/model/grup.model';

export interface IGrupEtkinligi {
  id?: number;
  etkinlikAdi?: string;
  aciklama?: string;
  tarih?: string;
  konum?: string;
  groupId?: number;
  grup?: IGrup | null;
}

export const defaultValue: Readonly<IGrupEtkinligi> = {};
