package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GrupEtkinligiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GrupEtkinligi.class);
        GrupEtkinligi grupEtkinligi1 = new GrupEtkinligi();
        grupEtkinligi1.setId(1L);
        GrupEtkinligi grupEtkinligi2 = new GrupEtkinligi();
        grupEtkinligi2.setId(grupEtkinligi1.getId());
        assertThat(grupEtkinligi1).isEqualTo(grupEtkinligi2);
        grupEtkinligi2.setId(2L);
        assertThat(grupEtkinligi1).isNotEqualTo(grupEtkinligi2);
        grupEtkinligi1.setId(null);
        assertThat(grupEtkinligi1).isNotEqualTo(grupEtkinligi2);
    }
}
