package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KullaniciTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kullanici.class);
        Kullanici kullanici1 = new Kullanici();
        kullanici1.setId(1L);
        Kullanici kullanici2 = new Kullanici();
        kullanici2.setId(kullanici1.getId());
        assertThat(kullanici1).isEqualTo(kullanici2);
        kullanici2.setId(2L);
        assertThat(kullanici1).isNotEqualTo(kullanici2);
        kullanici1.setId(null);
        assertThat(kullanici1).isNotEqualTo(kullanici2);
    }
}
