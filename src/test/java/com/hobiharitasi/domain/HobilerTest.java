package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HobilerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hobiler.class);
        Hobiler hobiler1 = new Hobiler();
        hobiler1.setId(1L);
        Hobiler hobiler2 = new Hobiler();
        hobiler2.setId(hobiler1.getId());
        assertThat(hobiler1).isEqualTo(hobiler2);
        hobiler2.setId(2L);
        assertThat(hobiler1).isNotEqualTo(hobiler2);
        hobiler1.setId(null);
        assertThat(hobiler1).isNotEqualTo(hobiler2);
    }
}
