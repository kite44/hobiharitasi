package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EtkinlikTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Etkinlik.class);
        Etkinlik etkinlik1 = new Etkinlik();
        etkinlik1.setId(1L);
        Etkinlik etkinlik2 = new Etkinlik();
        etkinlik2.setId(etkinlik1.getId());
        assertThat(etkinlik1).isEqualTo(etkinlik2);
        etkinlik2.setId(2L);
        assertThat(etkinlik1).isNotEqualTo(etkinlik2);
        etkinlik1.setId(null);
        assertThat(etkinlik1).isNotEqualTo(etkinlik2);
    }
}
