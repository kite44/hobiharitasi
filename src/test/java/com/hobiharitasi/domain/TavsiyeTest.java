package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TavsiyeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tavsiye.class);
        Tavsiye tavsiye1 = new Tavsiye();
        tavsiye1.setId(1L);
        Tavsiye tavsiye2 = new Tavsiye();
        tavsiye2.setId(tavsiye1.getId());
        assertThat(tavsiye1).isEqualTo(tavsiye2);
        tavsiye2.setId(2L);
        assertThat(tavsiye1).isNotEqualTo(tavsiye2);
        tavsiye1.setId(null);
        assertThat(tavsiye1).isNotEqualTo(tavsiye2);
    }
}
