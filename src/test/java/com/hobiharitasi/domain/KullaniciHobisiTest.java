package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KullaniciHobisiTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KullaniciHobisi.class);
        KullaniciHobisi kullaniciHobisi1 = new KullaniciHobisi();
        kullaniciHobisi1.setId(1L);
        KullaniciHobisi kullaniciHobisi2 = new KullaniciHobisi();
        kullaniciHobisi2.setId(kullaniciHobisi1.getId());
        assertThat(kullaniciHobisi1).isEqualTo(kullaniciHobisi2);
        kullaniciHobisi2.setId(2L);
        assertThat(kullaniciHobisi1).isNotEqualTo(kullaniciHobisi2);
        kullaniciHobisi1.setId(null);
        assertThat(kullaniciHobisi1).isNotEqualTo(kullaniciHobisi2);
    }
}
