package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DavetTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Davet.class);
        Davet davet1 = new Davet();
        davet1.setId(1L);
        Davet davet2 = new Davet();
        davet2.setId(davet1.getId());
        assertThat(davet1).isEqualTo(davet2);
        davet2.setId(2L);
        assertThat(davet1).isNotEqualTo(davet2);
        davet1.setId(null);
        assertThat(davet1).isNotEqualTo(davet2);
    }
}
