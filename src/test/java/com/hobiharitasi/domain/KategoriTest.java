package com.hobiharitasi.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KategoriTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kategori.class);
        Kategori kategori1 = new Kategori();
        kategori1.setId(1L);
        Kategori kategori2 = new Kategori();
        kategori2.setId(kategori1.getId());
        assertThat(kategori1).isEqualTo(kategori2);
        kategori2.setId(2L);
        assertThat(kategori1).isNotEqualTo(kategori2);
        kategori1.setId(null);
        assertThat(kategori1).isNotEqualTo(kategori2);
    }
}
