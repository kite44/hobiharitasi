package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.User;
import com.hobiharitasi.domain.Yorum;
import com.hobiharitasi.repository.YorumRepository;
import com.hobiharitasi.service.YorumService;
import com.hobiharitasi.service.criteria.YorumCriteria;
import com.hobiharitasi.service.dto.YorumDTO;
import com.hobiharitasi.service.mapper.YorumMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link YorumResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class YorumResourceIT {

    private static final String DEFAULT_BASLIK = "AAAAAAAAAA";
    private static final String UPDATED_BASLIK = "BBBBBBBBBB";

    private static final String DEFAULT_ICERIK = "AAAAAAAAAA";
    private static final String UPDATED_ICERIK = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/yorums";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private YorumRepository yorumRepository;

    @Mock
    private YorumRepository yorumRepositoryMock;

    @Autowired
    private YorumMapper yorumMapper;

    @Mock
    private YorumService yorumServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restYorumMockMvc;

    private Yorum yorum;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Yorum createEntity(EntityManager em) {
        Yorum yorum = new Yorum().baslik(DEFAULT_BASLIK).icerik(DEFAULT_ICERIK);
        return yorum;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Yorum createUpdatedEntity(EntityManager em) {
        Yorum yorum = new Yorum().baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK);
        return yorum;
    }

    @BeforeEach
    public void initTest() {
        yorum = createEntity(em);
    }

    @Test
    @Transactional
    void createYorum() throws Exception {
        int databaseSizeBeforeCreate = yorumRepository.findAll().size();
        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);
        restYorumMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isCreated());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeCreate + 1);
        Yorum testYorum = yorumList.get(yorumList.size() - 1);
        assertThat(testYorum.getBaslik()).isEqualTo(DEFAULT_BASLIK);
        assertThat(testYorum.getIcerik()).isEqualTo(DEFAULT_ICERIK);
    }

    @Test
    @Transactional
    void createYorumWithExistingId() throws Exception {
        // Create the Yorum with an existing ID
        yorum.setId(1L);
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        int databaseSizeBeforeCreate = yorumRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restYorumMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBaslikIsRequired() throws Exception {
        int databaseSizeBeforeTest = yorumRepository.findAll().size();
        // set the field null
        yorum.setBaslik(null);

        // Create the Yorum, which fails.
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        restYorumMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isBadRequest());

        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIcerikIsRequired() throws Exception {
        int databaseSizeBeforeTest = yorumRepository.findAll().size();
        // set the field null
        yorum.setIcerik(null);

        // Create the Yorum, which fails.
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        restYorumMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isBadRequest());

        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllYorums() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList
        restYorumMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(yorum.getId().intValue())))
            .andExpect(jsonPath("$.[*].baslik").value(hasItem(DEFAULT_BASLIK)))
            .andExpect(jsonPath("$.[*].icerik").value(hasItem(DEFAULT_ICERIK)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllYorumsWithEagerRelationshipsIsEnabled() throws Exception {
        when(yorumServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restYorumMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(yorumServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllYorumsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(yorumServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restYorumMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(yorumRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getYorum() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get the yorum
        restYorumMockMvc
            .perform(get(ENTITY_API_URL_ID, yorum.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(yorum.getId().intValue()))
            .andExpect(jsonPath("$.baslik").value(DEFAULT_BASLIK))
            .andExpect(jsonPath("$.icerik").value(DEFAULT_ICERIK));
    }

    @Test
    @Transactional
    void getYorumsByIdFiltering() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        Long id = yorum.getId();

        defaultYorumShouldBeFound("id.equals=" + id);
        defaultYorumShouldNotBeFound("id.notEquals=" + id);

        defaultYorumShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultYorumShouldNotBeFound("id.greaterThan=" + id);

        defaultYorumShouldBeFound("id.lessThanOrEqual=" + id);
        defaultYorumShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllYorumsByBaslikIsEqualToSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where baslik equals to DEFAULT_BASLIK
        defaultYorumShouldBeFound("baslik.equals=" + DEFAULT_BASLIK);

        // Get all the yorumList where baslik equals to UPDATED_BASLIK
        defaultYorumShouldNotBeFound("baslik.equals=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllYorumsByBaslikIsInShouldWork() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where baslik in DEFAULT_BASLIK or UPDATED_BASLIK
        defaultYorumShouldBeFound("baslik.in=" + DEFAULT_BASLIK + "," + UPDATED_BASLIK);

        // Get all the yorumList where baslik equals to UPDATED_BASLIK
        defaultYorumShouldNotBeFound("baslik.in=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllYorumsByBaslikIsNullOrNotNull() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where baslik is not null
        defaultYorumShouldBeFound("baslik.specified=true");

        // Get all the yorumList where baslik is null
        defaultYorumShouldNotBeFound("baslik.specified=false");
    }

    @Test
    @Transactional
    void getAllYorumsByBaslikContainsSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where baslik contains DEFAULT_BASLIK
        defaultYorumShouldBeFound("baslik.contains=" + DEFAULT_BASLIK);

        // Get all the yorumList where baslik contains UPDATED_BASLIK
        defaultYorumShouldNotBeFound("baslik.contains=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllYorumsByBaslikNotContainsSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where baslik does not contain DEFAULT_BASLIK
        defaultYorumShouldNotBeFound("baslik.doesNotContain=" + DEFAULT_BASLIK);

        // Get all the yorumList where baslik does not contain UPDATED_BASLIK
        defaultYorumShouldBeFound("baslik.doesNotContain=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllYorumsByIcerikIsEqualToSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where icerik equals to DEFAULT_ICERIK
        defaultYorumShouldBeFound("icerik.equals=" + DEFAULT_ICERIK);

        // Get all the yorumList where icerik equals to UPDATED_ICERIK
        defaultYorumShouldNotBeFound("icerik.equals=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllYorumsByIcerikIsInShouldWork() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where icerik in DEFAULT_ICERIK or UPDATED_ICERIK
        defaultYorumShouldBeFound("icerik.in=" + DEFAULT_ICERIK + "," + UPDATED_ICERIK);

        // Get all the yorumList where icerik equals to UPDATED_ICERIK
        defaultYorumShouldNotBeFound("icerik.in=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllYorumsByIcerikIsNullOrNotNull() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where icerik is not null
        defaultYorumShouldBeFound("icerik.specified=true");

        // Get all the yorumList where icerik is null
        defaultYorumShouldNotBeFound("icerik.specified=false");
    }

    @Test
    @Transactional
    void getAllYorumsByIcerikContainsSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where icerik contains DEFAULT_ICERIK
        defaultYorumShouldBeFound("icerik.contains=" + DEFAULT_ICERIK);

        // Get all the yorumList where icerik contains UPDATED_ICERIK
        defaultYorumShouldNotBeFound("icerik.contains=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllYorumsByIcerikNotContainsSomething() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        // Get all the yorumList where icerik does not contain DEFAULT_ICERIK
        defaultYorumShouldNotBeFound("icerik.doesNotContain=" + DEFAULT_ICERIK);

        // Get all the yorumList where icerik does not contain UPDATED_ICERIK
        defaultYorumShouldBeFound("icerik.doesNotContain=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllYorumsByUserIsEqualToSomething() throws Exception {
        User user;
        if (TestUtil.findAll(em, User.class).isEmpty()) {
            yorumRepository.saveAndFlush(yorum);
            user = UserResourceIT.createEntity(em);
        } else {
            user = TestUtil.findAll(em, User.class).get(0);
        }
        em.persist(user);
        em.flush();
        yorum.setUser(user);
        yorumRepository.saveAndFlush(yorum);
        Long userId = user.getId();

        // Get all the yorumList where user equals to userId
        defaultYorumShouldBeFound("userId.equals=" + userId);

        // Get all the yorumList where user equals to (userId + 1)
        defaultYorumShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    @Test
    @Transactional
    void getAllYorumsByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            yorumRepository.saveAndFlush(yorum);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        yorum.setKullanici(kullanici);
        yorumRepository.saveAndFlush(yorum);
        Long kullaniciId = kullanici.getId();

        // Get all the yorumList where kullanici equals to kullaniciId
        defaultYorumShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the yorumList where kullanici equals to (kullaniciId + 1)
        defaultYorumShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    @Test
    @Transactional
    void getAllYorumsByEtkinlikIsEqualToSomething() throws Exception {
        Etkinlik etkinlik;
        if (TestUtil.findAll(em, Etkinlik.class).isEmpty()) {
            yorumRepository.saveAndFlush(yorum);
            etkinlik = EtkinlikResourceIT.createEntity(em);
        } else {
            etkinlik = TestUtil.findAll(em, Etkinlik.class).get(0);
        }
        em.persist(etkinlik);
        em.flush();
        yorum.setEtkinlik(etkinlik);
        yorumRepository.saveAndFlush(yorum);
        Long etkinlikId = etkinlik.getId();

        // Get all the yorumList where etkinlik equals to etkinlikId
        defaultYorumShouldBeFound("etkinlikId.equals=" + etkinlikId);

        // Get all the yorumList where etkinlik equals to (etkinlikId + 1)
        defaultYorumShouldNotBeFound("etkinlikId.equals=" + (etkinlikId + 1));
    }

    @Test
    @Transactional
    void getAllYorumsByGrupEtkinligiIsEqualToSomething() throws Exception {
        GrupEtkinligi grupEtkinligi;
        if (TestUtil.findAll(em, GrupEtkinligi.class).isEmpty()) {
            yorumRepository.saveAndFlush(yorum);
            grupEtkinligi = GrupEtkinligiResourceIT.createEntity(em);
        } else {
            grupEtkinligi = TestUtil.findAll(em, GrupEtkinligi.class).get(0);
        }
        em.persist(grupEtkinligi);
        em.flush();
        yorum.setGrupEtkinligi(grupEtkinligi);
        yorumRepository.saveAndFlush(yorum);
        Long grupEtkinligiId = grupEtkinligi.getId();

        // Get all the yorumList where grupEtkinligi equals to grupEtkinligiId
        defaultYorumShouldBeFound("grupEtkinligiId.equals=" + grupEtkinligiId);

        // Get all the yorumList where grupEtkinligi equals to (grupEtkinligiId + 1)
        defaultYorumShouldNotBeFound("grupEtkinligiId.equals=" + (grupEtkinligiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultYorumShouldBeFound(String filter) throws Exception {
        restYorumMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(yorum.getId().intValue())))
            .andExpect(jsonPath("$.[*].baslik").value(hasItem(DEFAULT_BASLIK)))
            .andExpect(jsonPath("$.[*].icerik").value(hasItem(DEFAULT_ICERIK)));

        // Check, that the count call also returns 1
        restYorumMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultYorumShouldNotBeFound(String filter) throws Exception {
        restYorumMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restYorumMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingYorum() throws Exception {
        // Get the yorum
        restYorumMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingYorum() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();

        // Update the yorum
        Yorum updatedYorum = yorumRepository.findById(yorum.getId()).get();
        // Disconnect from session so that the updates on updatedYorum are not directly saved in db
        em.detach(updatedYorum);
        updatedYorum.baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK);
        YorumDTO yorumDTO = yorumMapper.toDto(updatedYorum);

        restYorumMockMvc
            .perform(
                put(ENTITY_API_URL_ID, yorumDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(yorumDTO))
            )
            .andExpect(status().isOk());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
        Yorum testYorum = yorumList.get(yorumList.size() - 1);
        assertThat(testYorum.getBaslik()).isEqualTo(UPDATED_BASLIK);
        assertThat(testYorum.getIcerik()).isEqualTo(UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void putNonExistingYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(
                put(ENTITY_API_URL_ID, yorumDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(yorumDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(yorumDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateYorumWithPatch() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();

        // Update the yorum using partial update
        Yorum partialUpdatedYorum = new Yorum();
        partialUpdatedYorum.setId(yorum.getId());

        partialUpdatedYorum.icerik(UPDATED_ICERIK);

        restYorumMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedYorum.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedYorum))
            )
            .andExpect(status().isOk());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
        Yorum testYorum = yorumList.get(yorumList.size() - 1);
        assertThat(testYorum.getBaslik()).isEqualTo(DEFAULT_BASLIK);
        assertThat(testYorum.getIcerik()).isEqualTo(UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void fullUpdateYorumWithPatch() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();

        // Update the yorum using partial update
        Yorum partialUpdatedYorum = new Yorum();
        partialUpdatedYorum.setId(yorum.getId());

        partialUpdatedYorum.baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK);

        restYorumMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedYorum.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedYorum))
            )
            .andExpect(status().isOk());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
        Yorum testYorum = yorumList.get(yorumList.size() - 1);
        assertThat(testYorum.getBaslik()).isEqualTo(UPDATED_BASLIK);
        assertThat(testYorum.getIcerik()).isEqualTo(UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void patchNonExistingYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, yorumDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(yorumDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(yorumDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamYorum() throws Exception {
        int databaseSizeBeforeUpdate = yorumRepository.findAll().size();
        yorum.setId(count.incrementAndGet());

        // Create the Yorum
        YorumDTO yorumDTO = yorumMapper.toDto(yorum);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restYorumMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(yorumDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Yorum in the database
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteYorum() throws Exception {
        // Initialize the database
        yorumRepository.saveAndFlush(yorum);

        int databaseSizeBeforeDelete = yorumRepository.findAll().size();

        // Delete the yorum
        restYorumMockMvc
            .perform(delete(ENTITY_API_URL_ID, yorum.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Yorum> yorumList = yorumRepository.findAll();
        assertThat(yorumList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
