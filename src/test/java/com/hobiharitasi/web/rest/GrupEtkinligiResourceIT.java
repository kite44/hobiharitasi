package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.GrupEtkinligi;
import com.hobiharitasi.repository.GrupEtkinligiRepository;
import com.hobiharitasi.service.GrupEtkinligiService;
import com.hobiharitasi.service.criteria.GrupEtkinligiCriteria;
import com.hobiharitasi.service.dto.GrupEtkinligiDTO;
import com.hobiharitasi.service.mapper.GrupEtkinligiMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GrupEtkinligiResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class GrupEtkinligiResourceIT {

    private static final String DEFAULT_ETKINLIK_ADI = "AAAAAAAAAA";
    private static final String UPDATED_ETKINLIK_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final Instant DEFAULT_TARIH = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TARIH = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_KONUM = "AAAAAAAAAA";
    private static final String UPDATED_KONUM = "BBBBBBBBBB";

    private static final Long DEFAULT_GROUP_ID = 1L;
    private static final Long UPDATED_GROUP_ID = 2L;
    private static final Long SMALLER_GROUP_ID = 1L - 1L;

    private static final String ENTITY_API_URL = "/api/grup-etkinligis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GrupEtkinligiRepository grupEtkinligiRepository;

    @Mock
    private GrupEtkinligiRepository grupEtkinligiRepositoryMock;

    @Autowired
    private GrupEtkinligiMapper grupEtkinligiMapper;

    @Mock
    private GrupEtkinligiService grupEtkinligiServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGrupEtkinligiMockMvc;

    private GrupEtkinligi grupEtkinligi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GrupEtkinligi createEntity(EntityManager em) {
        GrupEtkinligi grupEtkinligi = new GrupEtkinligi()
            .etkinlikAdi(DEFAULT_ETKINLIK_ADI)
            .aciklama(DEFAULT_ACIKLAMA)
            .tarih(DEFAULT_TARIH)
            .konum(DEFAULT_KONUM)
            .groupId(DEFAULT_GROUP_ID);
        return grupEtkinligi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GrupEtkinligi createUpdatedEntity(EntityManager em) {
        GrupEtkinligi grupEtkinligi = new GrupEtkinligi()
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .groupId(UPDATED_GROUP_ID);
        return grupEtkinligi;
    }

    @BeforeEach
    public void initTest() {
        grupEtkinligi = createEntity(em);
    }

    @Test
    @Transactional
    void createGrupEtkinligi() throws Exception {
        int databaseSizeBeforeCreate = grupEtkinligiRepository.findAll().size();
        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);
        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isCreated());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeCreate + 1);
        GrupEtkinligi testGrupEtkinligi = grupEtkinligiList.get(grupEtkinligiList.size() - 1);
        assertThat(testGrupEtkinligi.getEtkinlikAdi()).isEqualTo(DEFAULT_ETKINLIK_ADI);
        assertThat(testGrupEtkinligi.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testGrupEtkinligi.getTarih()).isEqualTo(DEFAULT_TARIH);
        assertThat(testGrupEtkinligi.getKonum()).isEqualTo(DEFAULT_KONUM);
        assertThat(testGrupEtkinligi.getGroupId()).isEqualTo(DEFAULT_GROUP_ID);
    }

    @Test
    @Transactional
    void createGrupEtkinligiWithExistingId() throws Exception {
        // Create the GrupEtkinligi with an existing ID
        grupEtkinligi.setId(1L);
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        int databaseSizeBeforeCreate = grupEtkinligiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkEtkinlikAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupEtkinligiRepository.findAll().size();
        // set the field null
        grupEtkinligi.setEtkinlikAdi(null);

        // Create the GrupEtkinligi, which fails.
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAciklamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupEtkinligiRepository.findAll().size();
        // set the field null
        grupEtkinligi.setAciklama(null);

        // Create the GrupEtkinligi, which fails.
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTarihIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupEtkinligiRepository.findAll().size();
        // set the field null
        grupEtkinligi.setTarih(null);

        // Create the GrupEtkinligi, which fails.
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkKonumIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupEtkinligiRepository.findAll().size();
        // set the field null
        grupEtkinligi.setKonum(null);

        // Create the GrupEtkinligi, which fails.
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGroupIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupEtkinligiRepository.findAll().size();
        // set the field null
        grupEtkinligi.setGroupId(null);

        // Create the GrupEtkinligi, which fails.
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligis() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grupEtkinligi.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikAdi").value(hasItem(DEFAULT_ETKINLIK_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].tarih").value(hasItem(DEFAULT_TARIH.toString())))
            .andExpect(jsonPath("$.[*].konum").value(hasItem(DEFAULT_KONUM)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID.intValue())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllGrupEtkinligisWithEagerRelationshipsIsEnabled() throws Exception {
        when(grupEtkinligiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restGrupEtkinligiMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(grupEtkinligiServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllGrupEtkinligisWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(grupEtkinligiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restGrupEtkinligiMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(grupEtkinligiRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getGrupEtkinligi() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get the grupEtkinligi
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL_ID, grupEtkinligi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(grupEtkinligi.getId().intValue()))
            .andExpect(jsonPath("$.etkinlikAdi").value(DEFAULT_ETKINLIK_ADI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA))
            .andExpect(jsonPath("$.tarih").value(DEFAULT_TARIH.toString()))
            .andExpect(jsonPath("$.konum").value(DEFAULT_KONUM))
            .andExpect(jsonPath("$.groupId").value(DEFAULT_GROUP_ID.intValue()));
    }

    @Test
    @Transactional
    void getGrupEtkinligisByIdFiltering() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        Long id = grupEtkinligi.getId();

        defaultGrupEtkinligiShouldBeFound("id.equals=" + id);
        defaultGrupEtkinligiShouldNotBeFound("id.notEquals=" + id);

        defaultGrupEtkinligiShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultGrupEtkinligiShouldNotBeFound("id.greaterThan=" + id);

        defaultGrupEtkinligiShouldBeFound("id.lessThanOrEqual=" + id);
        defaultGrupEtkinligiShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByEtkinlikAdiIsEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where etkinlikAdi equals to DEFAULT_ETKINLIK_ADI
        defaultGrupEtkinligiShouldBeFound("etkinlikAdi.equals=" + DEFAULT_ETKINLIK_ADI);

        // Get all the grupEtkinligiList where etkinlikAdi equals to UPDATED_ETKINLIK_ADI
        defaultGrupEtkinligiShouldNotBeFound("etkinlikAdi.equals=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByEtkinlikAdiIsInShouldWork() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where etkinlikAdi in DEFAULT_ETKINLIK_ADI or UPDATED_ETKINLIK_ADI
        defaultGrupEtkinligiShouldBeFound("etkinlikAdi.in=" + DEFAULT_ETKINLIK_ADI + "," + UPDATED_ETKINLIK_ADI);

        // Get all the grupEtkinligiList where etkinlikAdi equals to UPDATED_ETKINLIK_ADI
        defaultGrupEtkinligiShouldNotBeFound("etkinlikAdi.in=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByEtkinlikAdiIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where etkinlikAdi is not null
        defaultGrupEtkinligiShouldBeFound("etkinlikAdi.specified=true");

        // Get all the grupEtkinligiList where etkinlikAdi is null
        defaultGrupEtkinligiShouldNotBeFound("etkinlikAdi.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByEtkinlikAdiContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where etkinlikAdi contains DEFAULT_ETKINLIK_ADI
        defaultGrupEtkinligiShouldBeFound("etkinlikAdi.contains=" + DEFAULT_ETKINLIK_ADI);

        // Get all the grupEtkinligiList where etkinlikAdi contains UPDATED_ETKINLIK_ADI
        defaultGrupEtkinligiShouldNotBeFound("etkinlikAdi.contains=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByEtkinlikAdiNotContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where etkinlikAdi does not contain DEFAULT_ETKINLIK_ADI
        defaultGrupEtkinligiShouldNotBeFound("etkinlikAdi.doesNotContain=" + DEFAULT_ETKINLIK_ADI);

        // Get all the grupEtkinligiList where etkinlikAdi does not contain UPDATED_ETKINLIK_ADI
        defaultGrupEtkinligiShouldBeFound("etkinlikAdi.doesNotContain=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where aciklama equals to DEFAULT_ACIKLAMA
        defaultGrupEtkinligiShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the grupEtkinligiList where aciklama equals to UPDATED_ACIKLAMA
        defaultGrupEtkinligiShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultGrupEtkinligiShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the grupEtkinligiList where aciklama equals to UPDATED_ACIKLAMA
        defaultGrupEtkinligiShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where aciklama is not null
        defaultGrupEtkinligiShouldBeFound("aciklama.specified=true");

        // Get all the grupEtkinligiList where aciklama is null
        defaultGrupEtkinligiShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where aciklama contains DEFAULT_ACIKLAMA
        defaultGrupEtkinligiShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the grupEtkinligiList where aciklama contains UPDATED_ACIKLAMA
        defaultGrupEtkinligiShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultGrupEtkinligiShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the grupEtkinligiList where aciklama does not contain UPDATED_ACIKLAMA
        defaultGrupEtkinligiShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByTarihIsEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where tarih equals to DEFAULT_TARIH
        defaultGrupEtkinligiShouldBeFound("tarih.equals=" + DEFAULT_TARIH);

        // Get all the grupEtkinligiList where tarih equals to UPDATED_TARIH
        defaultGrupEtkinligiShouldNotBeFound("tarih.equals=" + UPDATED_TARIH);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByTarihIsInShouldWork() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where tarih in DEFAULT_TARIH or UPDATED_TARIH
        defaultGrupEtkinligiShouldBeFound("tarih.in=" + DEFAULT_TARIH + "," + UPDATED_TARIH);

        // Get all the grupEtkinligiList where tarih equals to UPDATED_TARIH
        defaultGrupEtkinligiShouldNotBeFound("tarih.in=" + UPDATED_TARIH);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByTarihIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where tarih is not null
        defaultGrupEtkinligiShouldBeFound("tarih.specified=true");

        // Get all the grupEtkinligiList where tarih is null
        defaultGrupEtkinligiShouldNotBeFound("tarih.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByKonumIsEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where konum equals to DEFAULT_KONUM
        defaultGrupEtkinligiShouldBeFound("konum.equals=" + DEFAULT_KONUM);

        // Get all the grupEtkinligiList where konum equals to UPDATED_KONUM
        defaultGrupEtkinligiShouldNotBeFound("konum.equals=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByKonumIsInShouldWork() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where konum in DEFAULT_KONUM or UPDATED_KONUM
        defaultGrupEtkinligiShouldBeFound("konum.in=" + DEFAULT_KONUM + "," + UPDATED_KONUM);

        // Get all the grupEtkinligiList where konum equals to UPDATED_KONUM
        defaultGrupEtkinligiShouldNotBeFound("konum.in=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByKonumIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where konum is not null
        defaultGrupEtkinligiShouldBeFound("konum.specified=true");

        // Get all the grupEtkinligiList where konum is null
        defaultGrupEtkinligiShouldNotBeFound("konum.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByKonumContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where konum contains DEFAULT_KONUM
        defaultGrupEtkinligiShouldBeFound("konum.contains=" + DEFAULT_KONUM);

        // Get all the grupEtkinligiList where konum contains UPDATED_KONUM
        defaultGrupEtkinligiShouldNotBeFound("konum.contains=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByKonumNotContainsSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where konum does not contain DEFAULT_KONUM
        defaultGrupEtkinligiShouldNotBeFound("konum.doesNotContain=" + DEFAULT_KONUM);

        // Get all the grupEtkinligiList where konum does not contain UPDATED_KONUM
        defaultGrupEtkinligiShouldBeFound("konum.doesNotContain=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId equals to DEFAULT_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.equals=" + DEFAULT_GROUP_ID);

        // Get all the grupEtkinligiList where groupId equals to UPDATED_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.equals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId in DEFAULT_GROUP_ID or UPDATED_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.in=" + DEFAULT_GROUP_ID + "," + UPDATED_GROUP_ID);

        // Get all the grupEtkinligiList where groupId equals to UPDATED_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.in=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId is not null
        defaultGrupEtkinligiShouldBeFound("groupId.specified=true");

        // Get all the grupEtkinligiList where groupId is null
        defaultGrupEtkinligiShouldNotBeFound("groupId.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId is greater than or equal to DEFAULT_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.greaterThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the grupEtkinligiList where groupId is greater than or equal to UPDATED_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.greaterThanOrEqual=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId is less than or equal to DEFAULT_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.lessThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the grupEtkinligiList where groupId is less than or equal to SMALLER_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.lessThanOrEqual=" + SMALLER_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsLessThanSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId is less than DEFAULT_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.lessThan=" + DEFAULT_GROUP_ID);

        // Get all the grupEtkinligiList where groupId is less than UPDATED_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.lessThan=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGroupIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        // Get all the grupEtkinligiList where groupId is greater than DEFAULT_GROUP_ID
        defaultGrupEtkinligiShouldNotBeFound("groupId.greaterThan=" + DEFAULT_GROUP_ID);

        // Get all the grupEtkinligiList where groupId is greater than SMALLER_GROUP_ID
        defaultGrupEtkinligiShouldBeFound("groupId.greaterThan=" + SMALLER_GROUP_ID);
    }

    @Test
    @Transactional
    void getAllGrupEtkinligisByGrupIsEqualToSomething() throws Exception {
        Grup grup;
        if (TestUtil.findAll(em, Grup.class).isEmpty()) {
            grupEtkinligiRepository.saveAndFlush(grupEtkinligi);
            grup = GrupResourceIT.createEntity(em);
        } else {
            grup = TestUtil.findAll(em, Grup.class).get(0);
        }
        em.persist(grup);
        em.flush();
        grupEtkinligi.setGrup(grup);
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);
        Long grupId = grup.getId();

        // Get all the grupEtkinligiList where grup equals to grupId
        defaultGrupEtkinligiShouldBeFound("grupId.equals=" + grupId);

        // Get all the grupEtkinligiList where grup equals to (grupId + 1)
        defaultGrupEtkinligiShouldNotBeFound("grupId.equals=" + (grupId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultGrupEtkinligiShouldBeFound(String filter) throws Exception {
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grupEtkinligi.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikAdi").value(hasItem(DEFAULT_ETKINLIK_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].tarih").value(hasItem(DEFAULT_TARIH.toString())))
            .andExpect(jsonPath("$.[*].konum").value(hasItem(DEFAULT_KONUM)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID.intValue())));

        // Check, that the count call also returns 1
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultGrupEtkinligiShouldNotBeFound(String filter) throws Exception {
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restGrupEtkinligiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingGrupEtkinligi() throws Exception {
        // Get the grupEtkinligi
        restGrupEtkinligiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingGrupEtkinligi() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();

        // Update the grupEtkinligi
        GrupEtkinligi updatedGrupEtkinligi = grupEtkinligiRepository.findById(grupEtkinligi.getId()).get();
        // Disconnect from session so that the updates on updatedGrupEtkinligi are not directly saved in db
        em.detach(updatedGrupEtkinligi);
        updatedGrupEtkinligi
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .groupId(UPDATED_GROUP_ID);
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(updatedGrupEtkinligi);

        restGrupEtkinligiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, grupEtkinligiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isOk());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
        GrupEtkinligi testGrupEtkinligi = grupEtkinligiList.get(grupEtkinligiList.size() - 1);
        assertThat(testGrupEtkinligi.getEtkinlikAdi()).isEqualTo(UPDATED_ETKINLIK_ADI);
        assertThat(testGrupEtkinligi.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testGrupEtkinligi.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testGrupEtkinligi.getKonum()).isEqualTo(UPDATED_KONUM);
        assertThat(testGrupEtkinligi.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void putNonExistingGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, grupEtkinligiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGrupEtkinligiWithPatch() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();

        // Update the grupEtkinligi using partial update
        GrupEtkinligi partialUpdatedGrupEtkinligi = new GrupEtkinligi();
        partialUpdatedGrupEtkinligi.setId(grupEtkinligi.getId());

        partialUpdatedGrupEtkinligi.tarih(UPDATED_TARIH).groupId(UPDATED_GROUP_ID);

        restGrupEtkinligiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGrupEtkinligi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGrupEtkinligi))
            )
            .andExpect(status().isOk());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
        GrupEtkinligi testGrupEtkinligi = grupEtkinligiList.get(grupEtkinligiList.size() - 1);
        assertThat(testGrupEtkinligi.getEtkinlikAdi()).isEqualTo(DEFAULT_ETKINLIK_ADI);
        assertThat(testGrupEtkinligi.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testGrupEtkinligi.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testGrupEtkinligi.getKonum()).isEqualTo(DEFAULT_KONUM);
        assertThat(testGrupEtkinligi.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void fullUpdateGrupEtkinligiWithPatch() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();

        // Update the grupEtkinligi using partial update
        GrupEtkinligi partialUpdatedGrupEtkinligi = new GrupEtkinligi();
        partialUpdatedGrupEtkinligi.setId(grupEtkinligi.getId());

        partialUpdatedGrupEtkinligi
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .groupId(UPDATED_GROUP_ID);

        restGrupEtkinligiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGrupEtkinligi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGrupEtkinligi))
            )
            .andExpect(status().isOk());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
        GrupEtkinligi testGrupEtkinligi = grupEtkinligiList.get(grupEtkinligiList.size() - 1);
        assertThat(testGrupEtkinligi.getEtkinlikAdi()).isEqualTo(UPDATED_ETKINLIK_ADI);
        assertThat(testGrupEtkinligi.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testGrupEtkinligi.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testGrupEtkinligi.getKonum()).isEqualTo(UPDATED_KONUM);
        assertThat(testGrupEtkinligi.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    void patchNonExistingGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, grupEtkinligiDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGrupEtkinligi() throws Exception {
        int databaseSizeBeforeUpdate = grupEtkinligiRepository.findAll().size();
        grupEtkinligi.setId(count.incrementAndGet());

        // Create the GrupEtkinligi
        GrupEtkinligiDTO grupEtkinligiDTO = grupEtkinligiMapper.toDto(grupEtkinligi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupEtkinligiMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(grupEtkinligiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the GrupEtkinligi in the database
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGrupEtkinligi() throws Exception {
        // Initialize the database
        grupEtkinligiRepository.saveAndFlush(grupEtkinligi);

        int databaseSizeBeforeDelete = grupEtkinligiRepository.findAll().size();

        // Delete the grupEtkinligi
        restGrupEtkinligiMockMvc
            .perform(delete(ENTITY_API_URL_ID, grupEtkinligi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GrupEtkinligi> grupEtkinligiList = grupEtkinligiRepository.findAll();
        assertThat(grupEtkinligiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
