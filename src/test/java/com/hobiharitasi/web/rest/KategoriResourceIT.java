package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.repository.KategoriRepository;
import com.hobiharitasi.service.criteria.KategoriCriteria;
import com.hobiharitasi.service.dto.KategoriDTO;
import com.hobiharitasi.service.mapper.KategoriMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KategoriResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class KategoriResourceIT {

    private static final String DEFAULT_KATEGORI_ADI = "AAAAAAAAAA";
    private static final String UPDATED_KATEGORI_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/kategoris";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KategoriRepository kategoriRepository;

    @Autowired
    private KategoriMapper kategoriMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKategoriMockMvc;

    private Kategori kategori;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kategori createEntity(EntityManager em) {
        Kategori kategori = new Kategori().kategoriAdi(DEFAULT_KATEGORI_ADI).aciklama(DEFAULT_ACIKLAMA);
        return kategori;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kategori createUpdatedEntity(EntityManager em) {
        Kategori kategori = new Kategori().kategoriAdi(UPDATED_KATEGORI_ADI).aciklama(UPDATED_ACIKLAMA);
        return kategori;
    }

    @BeforeEach
    public void initTest() {
        kategori = createEntity(em);
    }

    @Test
    @Transactional
    void createKategori() throws Exception {
        int databaseSizeBeforeCreate = kategoriRepository.findAll().size();
        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);
        restKategoriMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kategoriDTO)))
            .andExpect(status().isCreated());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeCreate + 1);
        Kategori testKategori = kategoriList.get(kategoriList.size() - 1);
        assertThat(testKategori.getKategoriAdi()).isEqualTo(DEFAULT_KATEGORI_ADI);
        assertThat(testKategori.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
    }

    @Test
    @Transactional
    void createKategoriWithExistingId() throws Exception {
        // Create the Kategori with an existing ID
        kategori.setId(1L);
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        int databaseSizeBeforeCreate = kategoriRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKategoriMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kategoriDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkKategoriAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = kategoriRepository.findAll().size();
        // set the field null
        kategori.setKategoriAdi(null);

        // Create the Kategori, which fails.
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        restKategoriMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kategoriDTO)))
            .andExpect(status().isBadRequest());

        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAciklamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = kategoriRepository.findAll().size();
        // set the field null
        kategori.setAciklama(null);

        // Create the Kategori, which fails.
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        restKategoriMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kategoriDTO)))
            .andExpect(status().isBadRequest());

        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKategoris() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kategori.getId().intValue())))
            .andExpect(jsonPath("$.[*].kategoriAdi").value(hasItem(DEFAULT_KATEGORI_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)));
    }

    @Test
    @Transactional
    void getKategori() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get the kategori
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL_ID, kategori.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kategori.getId().intValue()))
            .andExpect(jsonPath("$.kategoriAdi").value(DEFAULT_KATEGORI_ADI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA));
    }

    @Test
    @Transactional
    void getKategorisByIdFiltering() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        Long id = kategori.getId();

        defaultKategoriShouldBeFound("id.equals=" + id);
        defaultKategoriShouldNotBeFound("id.notEquals=" + id);

        defaultKategoriShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKategoriShouldNotBeFound("id.greaterThan=" + id);

        defaultKategoriShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKategoriShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKategorisByKategoriAdiIsEqualToSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where kategoriAdi equals to DEFAULT_KATEGORI_ADI
        defaultKategoriShouldBeFound("kategoriAdi.equals=" + DEFAULT_KATEGORI_ADI);

        // Get all the kategoriList where kategoriAdi equals to UPDATED_KATEGORI_ADI
        defaultKategoriShouldNotBeFound("kategoriAdi.equals=" + UPDATED_KATEGORI_ADI);
    }

    @Test
    @Transactional
    void getAllKategorisByKategoriAdiIsInShouldWork() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where kategoriAdi in DEFAULT_KATEGORI_ADI or UPDATED_KATEGORI_ADI
        defaultKategoriShouldBeFound("kategoriAdi.in=" + DEFAULT_KATEGORI_ADI + "," + UPDATED_KATEGORI_ADI);

        // Get all the kategoriList where kategoriAdi equals to UPDATED_KATEGORI_ADI
        defaultKategoriShouldNotBeFound("kategoriAdi.in=" + UPDATED_KATEGORI_ADI);
    }

    @Test
    @Transactional
    void getAllKategorisByKategoriAdiIsNullOrNotNull() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where kategoriAdi is not null
        defaultKategoriShouldBeFound("kategoriAdi.specified=true");

        // Get all the kategoriList where kategoriAdi is null
        defaultKategoriShouldNotBeFound("kategoriAdi.specified=false");
    }

    @Test
    @Transactional
    void getAllKategorisByKategoriAdiContainsSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where kategoriAdi contains DEFAULT_KATEGORI_ADI
        defaultKategoriShouldBeFound("kategoriAdi.contains=" + DEFAULT_KATEGORI_ADI);

        // Get all the kategoriList where kategoriAdi contains UPDATED_KATEGORI_ADI
        defaultKategoriShouldNotBeFound("kategoriAdi.contains=" + UPDATED_KATEGORI_ADI);
    }

    @Test
    @Transactional
    void getAllKategorisByKategoriAdiNotContainsSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where kategoriAdi does not contain DEFAULT_KATEGORI_ADI
        defaultKategoriShouldNotBeFound("kategoriAdi.doesNotContain=" + DEFAULT_KATEGORI_ADI);

        // Get all the kategoriList where kategoriAdi does not contain UPDATED_KATEGORI_ADI
        defaultKategoriShouldBeFound("kategoriAdi.doesNotContain=" + UPDATED_KATEGORI_ADI);
    }

    @Test
    @Transactional
    void getAllKategorisByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where aciklama equals to DEFAULT_ACIKLAMA
        defaultKategoriShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the kategoriList where aciklama equals to UPDATED_ACIKLAMA
        defaultKategoriShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllKategorisByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultKategoriShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the kategoriList where aciklama equals to UPDATED_ACIKLAMA
        defaultKategoriShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllKategorisByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where aciklama is not null
        defaultKategoriShouldBeFound("aciklama.specified=true");

        // Get all the kategoriList where aciklama is null
        defaultKategoriShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllKategorisByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where aciklama contains DEFAULT_ACIKLAMA
        defaultKategoriShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the kategoriList where aciklama contains UPDATED_ACIKLAMA
        defaultKategoriShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllKategorisByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        // Get all the kategoriList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultKategoriShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the kategoriList where aciklama does not contain UPDATED_ACIKLAMA
        defaultKategoriShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllKategorisByEtkinlikIsEqualToSomething() throws Exception {
        Etkinlik etkinlik;
        if (TestUtil.findAll(em, Etkinlik.class).isEmpty()) {
            kategoriRepository.saveAndFlush(kategori);
            etkinlik = EtkinlikResourceIT.createEntity(em);
        } else {
            etkinlik = TestUtil.findAll(em, Etkinlik.class).get(0);
        }
        em.persist(etkinlik);
        em.flush();
        kategori.addEtkinlik(etkinlik);
        kategoriRepository.saveAndFlush(kategori);
        Long etkinlikId = etkinlik.getId();

        // Get all the kategoriList where etkinlik equals to etkinlikId
        defaultKategoriShouldBeFound("etkinlikId.equals=" + etkinlikId);

        // Get all the kategoriList where etkinlik equals to (etkinlikId + 1)
        defaultKategoriShouldNotBeFound("etkinlikId.equals=" + (etkinlikId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKategoriShouldBeFound(String filter) throws Exception {
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kategori.getId().intValue())))
            .andExpect(jsonPath("$.[*].kategoriAdi").value(hasItem(DEFAULT_KATEGORI_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)));

        // Check, that the count call also returns 1
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKategoriShouldNotBeFound(String filter) throws Exception {
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKategoriMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKategori() throws Exception {
        // Get the kategori
        restKategoriMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingKategori() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();

        // Update the kategori
        Kategori updatedKategori = kategoriRepository.findById(kategori.getId()).get();
        // Disconnect from session so that the updates on updatedKategori are not directly saved in db
        em.detach(updatedKategori);
        updatedKategori.kategoriAdi(UPDATED_KATEGORI_ADI).aciklama(UPDATED_ACIKLAMA);
        KategoriDTO kategoriDTO = kategoriMapper.toDto(updatedKategori);

        restKategoriMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kategoriDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isOk());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
        Kategori testKategori = kategoriList.get(kategoriList.size() - 1);
        assertThat(testKategori.getKategoriAdi()).isEqualTo(UPDATED_KATEGORI_ADI);
        assertThat(testKategori.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void putNonExistingKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kategoriDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kategoriDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKategoriWithPatch() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();

        // Update the kategori using partial update
        Kategori partialUpdatedKategori = new Kategori();
        partialUpdatedKategori.setId(kategori.getId());

        partialUpdatedKategori.kategoriAdi(UPDATED_KATEGORI_ADI).aciklama(UPDATED_ACIKLAMA);

        restKategoriMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKategori.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKategori))
            )
            .andExpect(status().isOk());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
        Kategori testKategori = kategoriList.get(kategoriList.size() - 1);
        assertThat(testKategori.getKategoriAdi()).isEqualTo(UPDATED_KATEGORI_ADI);
        assertThat(testKategori.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void fullUpdateKategoriWithPatch() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();

        // Update the kategori using partial update
        Kategori partialUpdatedKategori = new Kategori();
        partialUpdatedKategori.setId(kategori.getId());

        partialUpdatedKategori.kategoriAdi(UPDATED_KATEGORI_ADI).aciklama(UPDATED_ACIKLAMA);

        restKategoriMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKategori.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKategori))
            )
            .andExpect(status().isOk());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
        Kategori testKategori = kategoriList.get(kategoriList.size() - 1);
        assertThat(testKategori.getKategoriAdi()).isEqualTo(UPDATED_KATEGORI_ADI);
        assertThat(testKategori.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void patchNonExistingKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, kategoriDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKategori() throws Exception {
        int databaseSizeBeforeUpdate = kategoriRepository.findAll().size();
        kategori.setId(count.incrementAndGet());

        // Create the Kategori
        KategoriDTO kategoriDTO = kategoriMapper.toDto(kategori);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKategoriMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(kategoriDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kategori in the database
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKategori() throws Exception {
        // Initialize the database
        kategoriRepository.saveAndFlush(kategori);

        int databaseSizeBeforeDelete = kategoriRepository.findAll().size();

        // Delete the kategori
        restKategoriMockMvc
            .perform(delete(ENTITY_API_URL_ID, kategori.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kategori> kategoriList = kategoriRepository.findAll();
        assertThat(kategoriList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
