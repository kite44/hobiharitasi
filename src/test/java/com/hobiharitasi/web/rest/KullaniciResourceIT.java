package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.Takvim;
import com.hobiharitasi.domain.Tavsiye;
import com.hobiharitasi.domain.User;
import com.hobiharitasi.repository.KullaniciRepository;
import com.hobiharitasi.service.KullaniciService;
import com.hobiharitasi.service.criteria.KullaniciCriteria;
import com.hobiharitasi.service.dto.KullaniciDTO;
import com.hobiharitasi.service.mapper.KullaniciMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KullaniciResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KullaniciResourceIT {

    private static final String DEFAULT_AD = "AAAAAAAAAA";
    private static final String UPDATED_AD = "BBBBBBBBBB";

    private static final String DEFAULT_SOYAD = "AAAAAAAAAA";
    private static final String UPDATED_SOYAD = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_SIFRE = "AAAAAAAAAA";
    private static final String UPDATED_SIFRE = "BBBBBBBBBB";

    private static final Integer DEFAULT_TEL_NO = 1;
    private static final Integer UPDATED_TEL_NO = 2;
    private static final Integer SMALLER_TEL_NO = 1 - 1;

    private static final String DEFAULT_BIYOGRAFI = "AAAAAAAAAA";
    private static final String UPDATED_BIYOGRAFI = "BBBBBBBBBB";

    private static final String DEFAULT_ADRES = "AAAAAAAAAA";
    private static final String UPDATED_ADRES = "BBBBBBBBBB";

    private static final String DEFAULT_PROFIL_RESMI = "AAAAAAAAAA";
    private static final String UPDATED_PROFIL_RESMI = "BBBBBBBBBB";

    private static final String DEFAULT_ACCES_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ACCES_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_JWT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_JWT_CODE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/kullanicis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KullaniciRepository kullaniciRepository;

    @Mock
    private KullaniciRepository kullaniciRepositoryMock;

    @Autowired
    private KullaniciMapper kullaniciMapper;

    @Mock
    private KullaniciService kullaniciServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKullaniciMockMvc;

    private Kullanici kullanici;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kullanici createEntity(EntityManager em) {
        Kullanici kullanici = new Kullanici()
            .ad(DEFAULT_AD)
            .soyad(DEFAULT_SOYAD)
            .email(DEFAULT_EMAIL)
            .sifre(DEFAULT_SIFRE)
            .telNo(DEFAULT_TEL_NO)
            .biyografi(DEFAULT_BIYOGRAFI)
            .adres(DEFAULT_ADRES)
            .profilResmi(DEFAULT_PROFIL_RESMI)
            .accesCode(DEFAULT_ACCES_CODE)
            .jwtCode(DEFAULT_JWT_CODE);
        return kullanici;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Kullanici createUpdatedEntity(EntityManager em) {
        Kullanici kullanici = new Kullanici()
            .ad(UPDATED_AD)
            .soyad(UPDATED_SOYAD)
            .email(UPDATED_EMAIL)
            .sifre(UPDATED_SIFRE)
            .telNo(UPDATED_TEL_NO)
            .biyografi(UPDATED_BIYOGRAFI)
            .adres(UPDATED_ADRES)
            .profilResmi(UPDATED_PROFIL_RESMI)
            .accesCode(UPDATED_ACCES_CODE)
            .jwtCode(UPDATED_JWT_CODE);
        return kullanici;
    }

    @BeforeEach
    public void initTest() {
        kullanici = createEntity(em);
    }

    @Test
    @Transactional
    void createKullanici() throws Exception {
        int databaseSizeBeforeCreate = kullaniciRepository.findAll().size();
        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);
        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isCreated());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeCreate + 1);
        Kullanici testKullanici = kullaniciList.get(kullaniciList.size() - 1);
        assertThat(testKullanici.getAd()).isEqualTo(DEFAULT_AD);
        assertThat(testKullanici.getSoyad()).isEqualTo(DEFAULT_SOYAD);
        assertThat(testKullanici.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testKullanici.getSifre()).isEqualTo(DEFAULT_SIFRE);
        assertThat(testKullanici.getTelNo()).isEqualTo(DEFAULT_TEL_NO);
        assertThat(testKullanici.getBiyografi()).isEqualTo(DEFAULT_BIYOGRAFI);
        assertThat(testKullanici.getAdres()).isEqualTo(DEFAULT_ADRES);
        assertThat(testKullanici.getProfilResmi()).isEqualTo(DEFAULT_PROFIL_RESMI);
        assertThat(testKullanici.getAccesCode()).isEqualTo(DEFAULT_ACCES_CODE);
        assertThat(testKullanici.getJwtCode()).isEqualTo(DEFAULT_JWT_CODE);
    }

    @Test
    @Transactional
    void createKullaniciWithExistingId() throws Exception {
        // Create the Kullanici with an existing ID
        kullanici.setId(1L);
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        int databaseSizeBeforeCreate = kullaniciRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAdIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciRepository.findAll().size();
        // set the field null
        kullanici.setAd(null);

        // Create the Kullanici, which fails.
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSoyadIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciRepository.findAll().size();
        // set the field null
        kullanici.setSoyad(null);

        // Create the Kullanici, which fails.
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciRepository.findAll().size();
        // set the field null
        kullanici.setEmail(null);

        // Create the Kullanici, which fails.
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSifreIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciRepository.findAll().size();
        // set the field null
        kullanici.setSifre(null);

        // Create the Kullanici, which fails.
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTelNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciRepository.findAll().size();
        // set the field null
        kullanici.setTelNo(null);

        // Create the Kullanici, which fails.
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        restKullaniciMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isBadRequest());

        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKullanicis() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kullanici.getId().intValue())))
            .andExpect(jsonPath("$.[*].ad").value(hasItem(DEFAULT_AD)))
            .andExpect(jsonPath("$.[*].soyad").value(hasItem(DEFAULT_SOYAD)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].sifre").value(hasItem(DEFAULT_SIFRE)))
            .andExpect(jsonPath("$.[*].telNo").value(hasItem(DEFAULT_TEL_NO)))
            .andExpect(jsonPath("$.[*].biyografi").value(hasItem(DEFAULT_BIYOGRAFI)))
            .andExpect(jsonPath("$.[*].adres").value(hasItem(DEFAULT_ADRES)))
            .andExpect(jsonPath("$.[*].profilResmi").value(hasItem(DEFAULT_PROFIL_RESMI)))
            .andExpect(jsonPath("$.[*].accesCode").value(hasItem(DEFAULT_ACCES_CODE)))
            .andExpect(jsonPath("$.[*].jwtCode").value(hasItem(DEFAULT_JWT_CODE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllKullanicisWithEagerRelationshipsIsEnabled() throws Exception {
        when(kullaniciServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restKullaniciMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(kullaniciServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllKullanicisWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(kullaniciServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restKullaniciMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(kullaniciRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getKullanici() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get the kullanici
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL_ID, kullanici.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kullanici.getId().intValue()))
            .andExpect(jsonPath("$.ad").value(DEFAULT_AD))
            .andExpect(jsonPath("$.soyad").value(DEFAULT_SOYAD))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.sifre").value(DEFAULT_SIFRE))
            .andExpect(jsonPath("$.telNo").value(DEFAULT_TEL_NO))
            .andExpect(jsonPath("$.biyografi").value(DEFAULT_BIYOGRAFI))
            .andExpect(jsonPath("$.adres").value(DEFAULT_ADRES))
            .andExpect(jsonPath("$.profilResmi").value(DEFAULT_PROFIL_RESMI))
            .andExpect(jsonPath("$.accesCode").value(DEFAULT_ACCES_CODE))
            .andExpect(jsonPath("$.jwtCode").value(DEFAULT_JWT_CODE));
    }

    @Test
    @Transactional
    void getKullanicisByIdFiltering() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        Long id = kullanici.getId();

        defaultKullaniciShouldBeFound("id.equals=" + id);
        defaultKullaniciShouldNotBeFound("id.notEquals=" + id);

        defaultKullaniciShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKullaniciShouldNotBeFound("id.greaterThan=" + id);

        defaultKullaniciShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKullaniciShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where ad equals to DEFAULT_AD
        defaultKullaniciShouldBeFound("ad.equals=" + DEFAULT_AD);

        // Get all the kullaniciList where ad equals to UPDATED_AD
        defaultKullaniciShouldNotBeFound("ad.equals=" + UPDATED_AD);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where ad in DEFAULT_AD or UPDATED_AD
        defaultKullaniciShouldBeFound("ad.in=" + DEFAULT_AD + "," + UPDATED_AD);

        // Get all the kullaniciList where ad equals to UPDATED_AD
        defaultKullaniciShouldNotBeFound("ad.in=" + UPDATED_AD);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where ad is not null
        defaultKullaniciShouldBeFound("ad.specified=true");

        // Get all the kullaniciList where ad is null
        defaultKullaniciShouldNotBeFound("ad.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByAdContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where ad contains DEFAULT_AD
        defaultKullaniciShouldBeFound("ad.contains=" + DEFAULT_AD);

        // Get all the kullaniciList where ad contains UPDATED_AD
        defaultKullaniciShouldNotBeFound("ad.contains=" + UPDATED_AD);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where ad does not contain DEFAULT_AD
        defaultKullaniciShouldNotBeFound("ad.doesNotContain=" + DEFAULT_AD);

        // Get all the kullaniciList where ad does not contain UPDATED_AD
        defaultKullaniciShouldBeFound("ad.doesNotContain=" + UPDATED_AD);
    }

    @Test
    @Transactional
    void getAllKullanicisBySoyadIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where soyad equals to DEFAULT_SOYAD
        defaultKullaniciShouldBeFound("soyad.equals=" + DEFAULT_SOYAD);

        // Get all the kullaniciList where soyad equals to UPDATED_SOYAD
        defaultKullaniciShouldNotBeFound("soyad.equals=" + UPDATED_SOYAD);
    }

    @Test
    @Transactional
    void getAllKullanicisBySoyadIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where soyad in DEFAULT_SOYAD or UPDATED_SOYAD
        defaultKullaniciShouldBeFound("soyad.in=" + DEFAULT_SOYAD + "," + UPDATED_SOYAD);

        // Get all the kullaniciList where soyad equals to UPDATED_SOYAD
        defaultKullaniciShouldNotBeFound("soyad.in=" + UPDATED_SOYAD);
    }

    @Test
    @Transactional
    void getAllKullanicisBySoyadIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where soyad is not null
        defaultKullaniciShouldBeFound("soyad.specified=true");

        // Get all the kullaniciList where soyad is null
        defaultKullaniciShouldNotBeFound("soyad.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisBySoyadContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where soyad contains DEFAULT_SOYAD
        defaultKullaniciShouldBeFound("soyad.contains=" + DEFAULT_SOYAD);

        // Get all the kullaniciList where soyad contains UPDATED_SOYAD
        defaultKullaniciShouldNotBeFound("soyad.contains=" + UPDATED_SOYAD);
    }

    @Test
    @Transactional
    void getAllKullanicisBySoyadNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where soyad does not contain DEFAULT_SOYAD
        defaultKullaniciShouldNotBeFound("soyad.doesNotContain=" + DEFAULT_SOYAD);

        // Get all the kullaniciList where soyad does not contain UPDATED_SOYAD
        defaultKullaniciShouldBeFound("soyad.doesNotContain=" + UPDATED_SOYAD);
    }

    @Test
    @Transactional
    void getAllKullanicisByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where email equals to DEFAULT_EMAIL
        defaultKullaniciShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the kullaniciList where email equals to UPDATED_EMAIL
        defaultKullaniciShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKullanicisByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultKullaniciShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the kullaniciList where email equals to UPDATED_EMAIL
        defaultKullaniciShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKullanicisByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where email is not null
        defaultKullaniciShouldBeFound("email.specified=true");

        // Get all the kullaniciList where email is null
        defaultKullaniciShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByEmailContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where email contains DEFAULT_EMAIL
        defaultKullaniciShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the kullaniciList where email contains UPDATED_EMAIL
        defaultKullaniciShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKullanicisByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where email does not contain DEFAULT_EMAIL
        defaultKullaniciShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the kullaniciList where email does not contain UPDATED_EMAIL
        defaultKullaniciShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void getAllKullanicisBySifreIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where sifre equals to DEFAULT_SIFRE
        defaultKullaniciShouldBeFound("sifre.equals=" + DEFAULT_SIFRE);

        // Get all the kullaniciList where sifre equals to UPDATED_SIFRE
        defaultKullaniciShouldNotBeFound("sifre.equals=" + UPDATED_SIFRE);
    }

    @Test
    @Transactional
    void getAllKullanicisBySifreIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where sifre in DEFAULT_SIFRE or UPDATED_SIFRE
        defaultKullaniciShouldBeFound("sifre.in=" + DEFAULT_SIFRE + "," + UPDATED_SIFRE);

        // Get all the kullaniciList where sifre equals to UPDATED_SIFRE
        defaultKullaniciShouldNotBeFound("sifre.in=" + UPDATED_SIFRE);
    }

    @Test
    @Transactional
    void getAllKullanicisBySifreIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where sifre is not null
        defaultKullaniciShouldBeFound("sifre.specified=true");

        // Get all the kullaniciList where sifre is null
        defaultKullaniciShouldNotBeFound("sifre.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisBySifreContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where sifre contains DEFAULT_SIFRE
        defaultKullaniciShouldBeFound("sifre.contains=" + DEFAULT_SIFRE);

        // Get all the kullaniciList where sifre contains UPDATED_SIFRE
        defaultKullaniciShouldNotBeFound("sifre.contains=" + UPDATED_SIFRE);
    }

    @Test
    @Transactional
    void getAllKullanicisBySifreNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where sifre does not contain DEFAULT_SIFRE
        defaultKullaniciShouldNotBeFound("sifre.doesNotContain=" + DEFAULT_SIFRE);

        // Get all the kullaniciList where sifre does not contain UPDATED_SIFRE
        defaultKullaniciShouldBeFound("sifre.doesNotContain=" + UPDATED_SIFRE);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo equals to DEFAULT_TEL_NO
        defaultKullaniciShouldBeFound("telNo.equals=" + DEFAULT_TEL_NO);

        // Get all the kullaniciList where telNo equals to UPDATED_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.equals=" + UPDATED_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo in DEFAULT_TEL_NO or UPDATED_TEL_NO
        defaultKullaniciShouldBeFound("telNo.in=" + DEFAULT_TEL_NO + "," + UPDATED_TEL_NO);

        // Get all the kullaniciList where telNo equals to UPDATED_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.in=" + UPDATED_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo is not null
        defaultKullaniciShouldBeFound("telNo.specified=true");

        // Get all the kullaniciList where telNo is null
        defaultKullaniciShouldNotBeFound("telNo.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo is greater than or equal to DEFAULT_TEL_NO
        defaultKullaniciShouldBeFound("telNo.greaterThanOrEqual=" + DEFAULT_TEL_NO);

        // Get all the kullaniciList where telNo is greater than or equal to UPDATED_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.greaterThanOrEqual=" + UPDATED_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo is less than or equal to DEFAULT_TEL_NO
        defaultKullaniciShouldBeFound("telNo.lessThanOrEqual=" + DEFAULT_TEL_NO);

        // Get all the kullaniciList where telNo is less than or equal to SMALLER_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.lessThanOrEqual=" + SMALLER_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsLessThanSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo is less than DEFAULT_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.lessThan=" + DEFAULT_TEL_NO);

        // Get all the kullaniciList where telNo is less than UPDATED_TEL_NO
        defaultKullaniciShouldBeFound("telNo.lessThan=" + UPDATED_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByTelNoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where telNo is greater than DEFAULT_TEL_NO
        defaultKullaniciShouldNotBeFound("telNo.greaterThan=" + DEFAULT_TEL_NO);

        // Get all the kullaniciList where telNo is greater than SMALLER_TEL_NO
        defaultKullaniciShouldBeFound("telNo.greaterThan=" + SMALLER_TEL_NO);
    }

    @Test
    @Transactional
    void getAllKullanicisByBiyografiIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where biyografi equals to DEFAULT_BIYOGRAFI
        defaultKullaniciShouldBeFound("biyografi.equals=" + DEFAULT_BIYOGRAFI);

        // Get all the kullaniciList where biyografi equals to UPDATED_BIYOGRAFI
        defaultKullaniciShouldNotBeFound("biyografi.equals=" + UPDATED_BIYOGRAFI);
    }

    @Test
    @Transactional
    void getAllKullanicisByBiyografiIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where biyografi in DEFAULT_BIYOGRAFI or UPDATED_BIYOGRAFI
        defaultKullaniciShouldBeFound("biyografi.in=" + DEFAULT_BIYOGRAFI + "," + UPDATED_BIYOGRAFI);

        // Get all the kullaniciList where biyografi equals to UPDATED_BIYOGRAFI
        defaultKullaniciShouldNotBeFound("biyografi.in=" + UPDATED_BIYOGRAFI);
    }

    @Test
    @Transactional
    void getAllKullanicisByBiyografiIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where biyografi is not null
        defaultKullaniciShouldBeFound("biyografi.specified=true");

        // Get all the kullaniciList where biyografi is null
        defaultKullaniciShouldNotBeFound("biyografi.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByBiyografiContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where biyografi contains DEFAULT_BIYOGRAFI
        defaultKullaniciShouldBeFound("biyografi.contains=" + DEFAULT_BIYOGRAFI);

        // Get all the kullaniciList where biyografi contains UPDATED_BIYOGRAFI
        defaultKullaniciShouldNotBeFound("biyografi.contains=" + UPDATED_BIYOGRAFI);
    }

    @Test
    @Transactional
    void getAllKullanicisByBiyografiNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where biyografi does not contain DEFAULT_BIYOGRAFI
        defaultKullaniciShouldNotBeFound("biyografi.doesNotContain=" + DEFAULT_BIYOGRAFI);

        // Get all the kullaniciList where biyografi does not contain UPDATED_BIYOGRAFI
        defaultKullaniciShouldBeFound("biyografi.doesNotContain=" + UPDATED_BIYOGRAFI);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdresIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where adres equals to DEFAULT_ADRES
        defaultKullaniciShouldBeFound("adres.equals=" + DEFAULT_ADRES);

        // Get all the kullaniciList where adres equals to UPDATED_ADRES
        defaultKullaniciShouldNotBeFound("adres.equals=" + UPDATED_ADRES);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdresIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where adres in DEFAULT_ADRES or UPDATED_ADRES
        defaultKullaniciShouldBeFound("adres.in=" + DEFAULT_ADRES + "," + UPDATED_ADRES);

        // Get all the kullaniciList where adres equals to UPDATED_ADRES
        defaultKullaniciShouldNotBeFound("adres.in=" + UPDATED_ADRES);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdresIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where adres is not null
        defaultKullaniciShouldBeFound("adres.specified=true");

        // Get all the kullaniciList where adres is null
        defaultKullaniciShouldNotBeFound("adres.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByAdresContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where adres contains DEFAULT_ADRES
        defaultKullaniciShouldBeFound("adres.contains=" + DEFAULT_ADRES);

        // Get all the kullaniciList where adres contains UPDATED_ADRES
        defaultKullaniciShouldNotBeFound("adres.contains=" + UPDATED_ADRES);
    }

    @Test
    @Transactional
    void getAllKullanicisByAdresNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where adres does not contain DEFAULT_ADRES
        defaultKullaniciShouldNotBeFound("adres.doesNotContain=" + DEFAULT_ADRES);

        // Get all the kullaniciList where adres does not contain UPDATED_ADRES
        defaultKullaniciShouldBeFound("adres.doesNotContain=" + UPDATED_ADRES);
    }

    @Test
    @Transactional
    void getAllKullanicisByProfilResmiIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where profilResmi equals to DEFAULT_PROFIL_RESMI
        defaultKullaniciShouldBeFound("profilResmi.equals=" + DEFAULT_PROFIL_RESMI);

        // Get all the kullaniciList where profilResmi equals to UPDATED_PROFIL_RESMI
        defaultKullaniciShouldNotBeFound("profilResmi.equals=" + UPDATED_PROFIL_RESMI);
    }

    @Test
    @Transactional
    void getAllKullanicisByProfilResmiIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where profilResmi in DEFAULT_PROFIL_RESMI or UPDATED_PROFIL_RESMI
        defaultKullaniciShouldBeFound("profilResmi.in=" + DEFAULT_PROFIL_RESMI + "," + UPDATED_PROFIL_RESMI);

        // Get all the kullaniciList where profilResmi equals to UPDATED_PROFIL_RESMI
        defaultKullaniciShouldNotBeFound("profilResmi.in=" + UPDATED_PROFIL_RESMI);
    }

    @Test
    @Transactional
    void getAllKullanicisByProfilResmiIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where profilResmi is not null
        defaultKullaniciShouldBeFound("profilResmi.specified=true");

        // Get all the kullaniciList where profilResmi is null
        defaultKullaniciShouldNotBeFound("profilResmi.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByProfilResmiContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where profilResmi contains DEFAULT_PROFIL_RESMI
        defaultKullaniciShouldBeFound("profilResmi.contains=" + DEFAULT_PROFIL_RESMI);

        // Get all the kullaniciList where profilResmi contains UPDATED_PROFIL_RESMI
        defaultKullaniciShouldNotBeFound("profilResmi.contains=" + UPDATED_PROFIL_RESMI);
    }

    @Test
    @Transactional
    void getAllKullanicisByProfilResmiNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where profilResmi does not contain DEFAULT_PROFIL_RESMI
        defaultKullaniciShouldNotBeFound("profilResmi.doesNotContain=" + DEFAULT_PROFIL_RESMI);

        // Get all the kullaniciList where profilResmi does not contain UPDATED_PROFIL_RESMI
        defaultKullaniciShouldBeFound("profilResmi.doesNotContain=" + UPDATED_PROFIL_RESMI);
    }

    @Test
    @Transactional
    void getAllKullanicisByAccesCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where accesCode equals to DEFAULT_ACCES_CODE
        defaultKullaniciShouldBeFound("accesCode.equals=" + DEFAULT_ACCES_CODE);

        // Get all the kullaniciList where accesCode equals to UPDATED_ACCES_CODE
        defaultKullaniciShouldNotBeFound("accesCode.equals=" + UPDATED_ACCES_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByAccesCodeIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where accesCode in DEFAULT_ACCES_CODE or UPDATED_ACCES_CODE
        defaultKullaniciShouldBeFound("accesCode.in=" + DEFAULT_ACCES_CODE + "," + UPDATED_ACCES_CODE);

        // Get all the kullaniciList where accesCode equals to UPDATED_ACCES_CODE
        defaultKullaniciShouldNotBeFound("accesCode.in=" + UPDATED_ACCES_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByAccesCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where accesCode is not null
        defaultKullaniciShouldBeFound("accesCode.specified=true");

        // Get all the kullaniciList where accesCode is null
        defaultKullaniciShouldNotBeFound("accesCode.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByAccesCodeContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where accesCode contains DEFAULT_ACCES_CODE
        defaultKullaniciShouldBeFound("accesCode.contains=" + DEFAULT_ACCES_CODE);

        // Get all the kullaniciList where accesCode contains UPDATED_ACCES_CODE
        defaultKullaniciShouldNotBeFound("accesCode.contains=" + UPDATED_ACCES_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByAccesCodeNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where accesCode does not contain DEFAULT_ACCES_CODE
        defaultKullaniciShouldNotBeFound("accesCode.doesNotContain=" + DEFAULT_ACCES_CODE);

        // Get all the kullaniciList where accesCode does not contain UPDATED_ACCES_CODE
        defaultKullaniciShouldBeFound("accesCode.doesNotContain=" + UPDATED_ACCES_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByJwtCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where jwtCode equals to DEFAULT_JWT_CODE
        defaultKullaniciShouldBeFound("jwtCode.equals=" + DEFAULT_JWT_CODE);

        // Get all the kullaniciList where jwtCode equals to UPDATED_JWT_CODE
        defaultKullaniciShouldNotBeFound("jwtCode.equals=" + UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByJwtCodeIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where jwtCode in DEFAULT_JWT_CODE or UPDATED_JWT_CODE
        defaultKullaniciShouldBeFound("jwtCode.in=" + DEFAULT_JWT_CODE + "," + UPDATED_JWT_CODE);

        // Get all the kullaniciList where jwtCode equals to UPDATED_JWT_CODE
        defaultKullaniciShouldNotBeFound("jwtCode.in=" + UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByJwtCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where jwtCode is not null
        defaultKullaniciShouldBeFound("jwtCode.specified=true");

        // Get all the kullaniciList where jwtCode is null
        defaultKullaniciShouldNotBeFound("jwtCode.specified=false");
    }

    @Test
    @Transactional
    void getAllKullanicisByJwtCodeContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where jwtCode contains DEFAULT_JWT_CODE
        defaultKullaniciShouldBeFound("jwtCode.contains=" + DEFAULT_JWT_CODE);

        // Get all the kullaniciList where jwtCode contains UPDATED_JWT_CODE
        defaultKullaniciShouldNotBeFound("jwtCode.contains=" + UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByJwtCodeNotContainsSomething() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        // Get all the kullaniciList where jwtCode does not contain DEFAULT_JWT_CODE
        defaultKullaniciShouldNotBeFound("jwtCode.doesNotContain=" + DEFAULT_JWT_CODE);

        // Get all the kullaniciList where jwtCode does not contain UPDATED_JWT_CODE
        defaultKullaniciShouldBeFound("jwtCode.doesNotContain=" + UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void getAllKullanicisByTavsiyeIsEqualToSomething() throws Exception {
        Tavsiye tavsiye;
        if (TestUtil.findAll(em, Tavsiye.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            tavsiye = TavsiyeResourceIT.createEntity(em);
        } else {
            tavsiye = TestUtil.findAll(em, Tavsiye.class).get(0);
        }
        em.persist(tavsiye);
        em.flush();
        kullanici.addTavsiye(tavsiye);
        kullaniciRepository.saveAndFlush(kullanici);
        Long tavsiyeId = tavsiye.getId();

        // Get all the kullaniciList where tavsiye equals to tavsiyeId
        defaultKullaniciShouldBeFound("tavsiyeId.equals=" + tavsiyeId);

        // Get all the kullaniciList where tavsiye equals to (tavsiyeId + 1)
        defaultKullaniciShouldNotBeFound("tavsiyeId.equals=" + (tavsiyeId + 1));
    }

    @Test
    @Transactional
    void getAllKullanicisByTakvimIsEqualToSomething() throws Exception {
        Takvim takvim;
        if (TestUtil.findAll(em, Takvim.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            takvim = TakvimResourceIT.createEntity(em);
        } else {
            takvim = TestUtil.findAll(em, Takvim.class).get(0);
        }
        em.persist(takvim);
        em.flush();
        kullanici.addTakvim(takvim);
        kullaniciRepository.saveAndFlush(kullanici);
        Long takvimId = takvim.getId();

        // Get all the kullaniciList where takvim equals to takvimId
        defaultKullaniciShouldBeFound("takvimId.equals=" + takvimId);

        // Get all the kullaniciList where takvim equals to (takvimId + 1)
        defaultKullaniciShouldNotBeFound("takvimId.equals=" + (takvimId + 1));
    }

    @Test
    @Transactional
    void getAllKullanicisByUserIsEqualToSomething() throws Exception {
        User user;
        if (TestUtil.findAll(em, User.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            user = UserResourceIT.createEntity(em);
        } else {
            user = TestUtil.findAll(em, User.class).get(0);
        }
        em.persist(user);
        em.flush();
        kullanici.setUser(user);
        kullaniciRepository.saveAndFlush(kullanici);
        Long userId = user.getId();

        // Get all the kullaniciList where user equals to userId
        defaultKullaniciShouldBeFound("userId.equals=" + userId);

        // Get all the kullaniciList where user equals to (userId + 1)
        defaultKullaniciShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    @Test
    @Transactional
    void getAllKullanicisByEtkinlikIsEqualToSomething() throws Exception {
        Etkinlik etkinlik;
        if (TestUtil.findAll(em, Etkinlik.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            etkinlik = EtkinlikResourceIT.createEntity(em);
        } else {
            etkinlik = TestUtil.findAll(em, Etkinlik.class).get(0);
        }
        em.persist(etkinlik);
        em.flush();
        kullanici.setEtkinlik(etkinlik);
        kullaniciRepository.saveAndFlush(kullanici);
        Long etkinlikId = etkinlik.getId();

        // Get all the kullaniciList where etkinlik equals to etkinlikId
        defaultKullaniciShouldBeFound("etkinlikId.equals=" + etkinlikId);

        // Get all the kullaniciList where etkinlik equals to (etkinlikId + 1)
        defaultKullaniciShouldNotBeFound("etkinlikId.equals=" + (etkinlikId + 1));
    }

    @Test
    @Transactional
    void getAllKullanicisByGrupIsEqualToSomething() throws Exception {
        Grup grup;
        if (TestUtil.findAll(em, Grup.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            grup = GrupResourceIT.createEntity(em);
        } else {
            grup = TestUtil.findAll(em, Grup.class).get(0);
        }
        em.persist(grup);
        em.flush();
        kullanici.addGrup(grup);
        kullaniciRepository.saveAndFlush(kullanici);
        Long grupId = grup.getId();

        // Get all the kullaniciList where grup equals to grupId
        defaultKullaniciShouldBeFound("grupId.equals=" + grupId);

        // Get all the kullaniciList where grup equals to (grupId + 1)
        defaultKullaniciShouldNotBeFound("grupId.equals=" + (grupId + 1));
    }

    @Test
    @Transactional
    void getAllKullanicisByHobiIsEqualToSomething() throws Exception {
        Hobiler hobi;
        if (TestUtil.findAll(em, Hobiler.class).isEmpty()) {
            kullaniciRepository.saveAndFlush(kullanici);
            hobi = HobilerResourceIT.createEntity(em);
        } else {
            hobi = TestUtil.findAll(em, Hobiler.class).get(0);
        }
        em.persist(hobi);
        em.flush();
        kullanici.addHobi(hobi);
        kullaniciRepository.saveAndFlush(kullanici);
        Long hobiId = hobi.getId();

        // Get all the kullaniciList where hobi equals to hobiId
        defaultKullaniciShouldBeFound("hobiId.equals=" + hobiId);

        // Get all the kullaniciList where hobi equals to (hobiId + 1)
        defaultKullaniciShouldNotBeFound("hobiId.equals=" + (hobiId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKullaniciShouldBeFound(String filter) throws Exception {
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kullanici.getId().intValue())))
            .andExpect(jsonPath("$.[*].ad").value(hasItem(DEFAULT_AD)))
            .andExpect(jsonPath("$.[*].soyad").value(hasItem(DEFAULT_SOYAD)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].sifre").value(hasItem(DEFAULT_SIFRE)))
            .andExpect(jsonPath("$.[*].telNo").value(hasItem(DEFAULT_TEL_NO)))
            .andExpect(jsonPath("$.[*].biyografi").value(hasItem(DEFAULT_BIYOGRAFI)))
            .andExpect(jsonPath("$.[*].adres").value(hasItem(DEFAULT_ADRES)))
            .andExpect(jsonPath("$.[*].profilResmi").value(hasItem(DEFAULT_PROFIL_RESMI)))
            .andExpect(jsonPath("$.[*].accesCode").value(hasItem(DEFAULT_ACCES_CODE)))
            .andExpect(jsonPath("$.[*].jwtCode").value(hasItem(DEFAULT_JWT_CODE)));

        // Check, that the count call also returns 1
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKullaniciShouldNotBeFound(String filter) throws Exception {
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKullaniciMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKullanici() throws Exception {
        // Get the kullanici
        restKullaniciMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingKullanici() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();

        // Update the kullanici
        Kullanici updatedKullanici = kullaniciRepository.findById(kullanici.getId()).get();
        // Disconnect from session so that the updates on updatedKullanici are not directly saved in db
        em.detach(updatedKullanici);
        updatedKullanici
            .ad(UPDATED_AD)
            .soyad(UPDATED_SOYAD)
            .email(UPDATED_EMAIL)
            .sifre(UPDATED_SIFRE)
            .telNo(UPDATED_TEL_NO)
            .biyografi(UPDATED_BIYOGRAFI)
            .adres(UPDATED_ADRES)
            .profilResmi(UPDATED_PROFIL_RESMI)
            .accesCode(UPDATED_ACCES_CODE)
            .jwtCode(UPDATED_JWT_CODE);
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(updatedKullanici);

        restKullaniciMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kullaniciDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isOk());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
        Kullanici testKullanici = kullaniciList.get(kullaniciList.size() - 1);
        assertThat(testKullanici.getAd()).isEqualTo(UPDATED_AD);
        assertThat(testKullanici.getSoyad()).isEqualTo(UPDATED_SOYAD);
        assertThat(testKullanici.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKullanici.getSifre()).isEqualTo(UPDATED_SIFRE);
        assertThat(testKullanici.getTelNo()).isEqualTo(UPDATED_TEL_NO);
        assertThat(testKullanici.getBiyografi()).isEqualTo(UPDATED_BIYOGRAFI);
        assertThat(testKullanici.getAdres()).isEqualTo(UPDATED_ADRES);
        assertThat(testKullanici.getProfilResmi()).isEqualTo(UPDATED_PROFIL_RESMI);
        assertThat(testKullanici.getAccesCode()).isEqualTo(UPDATED_ACCES_CODE);
        assertThat(testKullanici.getJwtCode()).isEqualTo(UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void putNonExistingKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kullaniciDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKullaniciWithPatch() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();

        // Update the kullanici using partial update
        Kullanici partialUpdatedKullanici = new Kullanici();
        partialUpdatedKullanici.setId(kullanici.getId());

        partialUpdatedKullanici
            .ad(UPDATED_AD)
            .email(UPDATED_EMAIL)
            .biyografi(UPDATED_BIYOGRAFI)
            .adres(UPDATED_ADRES)
            .profilResmi(UPDATED_PROFIL_RESMI)
            .accesCode(UPDATED_ACCES_CODE)
            .jwtCode(UPDATED_JWT_CODE);

        restKullaniciMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKullanici.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKullanici))
            )
            .andExpect(status().isOk());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
        Kullanici testKullanici = kullaniciList.get(kullaniciList.size() - 1);
        assertThat(testKullanici.getAd()).isEqualTo(UPDATED_AD);
        assertThat(testKullanici.getSoyad()).isEqualTo(DEFAULT_SOYAD);
        assertThat(testKullanici.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKullanici.getSifre()).isEqualTo(DEFAULT_SIFRE);
        assertThat(testKullanici.getTelNo()).isEqualTo(DEFAULT_TEL_NO);
        assertThat(testKullanici.getBiyografi()).isEqualTo(UPDATED_BIYOGRAFI);
        assertThat(testKullanici.getAdres()).isEqualTo(UPDATED_ADRES);
        assertThat(testKullanici.getProfilResmi()).isEqualTo(UPDATED_PROFIL_RESMI);
        assertThat(testKullanici.getAccesCode()).isEqualTo(UPDATED_ACCES_CODE);
        assertThat(testKullanici.getJwtCode()).isEqualTo(UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void fullUpdateKullaniciWithPatch() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();

        // Update the kullanici using partial update
        Kullanici partialUpdatedKullanici = new Kullanici();
        partialUpdatedKullanici.setId(kullanici.getId());

        partialUpdatedKullanici
            .ad(UPDATED_AD)
            .soyad(UPDATED_SOYAD)
            .email(UPDATED_EMAIL)
            .sifre(UPDATED_SIFRE)
            .telNo(UPDATED_TEL_NO)
            .biyografi(UPDATED_BIYOGRAFI)
            .adres(UPDATED_ADRES)
            .profilResmi(UPDATED_PROFIL_RESMI)
            .accesCode(UPDATED_ACCES_CODE)
            .jwtCode(UPDATED_JWT_CODE);

        restKullaniciMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKullanici.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKullanici))
            )
            .andExpect(status().isOk());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
        Kullanici testKullanici = kullaniciList.get(kullaniciList.size() - 1);
        assertThat(testKullanici.getAd()).isEqualTo(UPDATED_AD);
        assertThat(testKullanici.getSoyad()).isEqualTo(UPDATED_SOYAD);
        assertThat(testKullanici.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKullanici.getSifre()).isEqualTo(UPDATED_SIFRE);
        assertThat(testKullanici.getTelNo()).isEqualTo(UPDATED_TEL_NO);
        assertThat(testKullanici.getBiyografi()).isEqualTo(UPDATED_BIYOGRAFI);
        assertThat(testKullanici.getAdres()).isEqualTo(UPDATED_ADRES);
        assertThat(testKullanici.getProfilResmi()).isEqualTo(UPDATED_PROFIL_RESMI);
        assertThat(testKullanici.getAccesCode()).isEqualTo(UPDATED_ACCES_CODE);
        assertThat(testKullanici.getJwtCode()).isEqualTo(UPDATED_JWT_CODE);
    }

    @Test
    @Transactional
    void patchNonExistingKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, kullaniciDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKullanici() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciRepository.findAll().size();
        kullanici.setId(count.incrementAndGet());

        // Create the Kullanici
        KullaniciDTO kullaniciDTO = kullaniciMapper.toDto(kullanici);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(kullaniciDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Kullanici in the database
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKullanici() throws Exception {
        // Initialize the database
        kullaniciRepository.saveAndFlush(kullanici);

        int databaseSizeBeforeDelete = kullaniciRepository.findAll().size();

        // Delete the kullanici
        restKullaniciMockMvc
            .perform(delete(ENTITY_API_URL_ID, kullanici.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Kullanici> kullaniciList = kullaniciRepository.findAll();
        assertThat(kullaniciList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
