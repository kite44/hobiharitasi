package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.Tavsiye;
import com.hobiharitasi.repository.TavsiyeRepository;
import com.hobiharitasi.service.TavsiyeService;
import com.hobiharitasi.service.criteria.TavsiyeCriteria;
import com.hobiharitasi.service.dto.TavsiyeDTO;
import com.hobiharitasi.service.mapper.TavsiyeMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TavsiyeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class TavsiyeResourceIT {

    private static final String DEFAULT_BASLIK = "AAAAAAAAAA";
    private static final String UPDATED_BASLIK = "BBBBBBBBBB";

    private static final String DEFAULT_ICERIK = "AAAAAAAAAA";
    private static final String UPDATED_ICERIK = "BBBBBBBBBB";

    private static final Instant DEFAULT_OLUSTURULMA_TARIHI = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OLUSTURULMA_TARIHI = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/tavsiyes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TavsiyeRepository tavsiyeRepository;

    @Mock
    private TavsiyeRepository tavsiyeRepositoryMock;

    @Autowired
    private TavsiyeMapper tavsiyeMapper;

    @Mock
    private TavsiyeService tavsiyeServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTavsiyeMockMvc;

    private Tavsiye tavsiye;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tavsiye createEntity(EntityManager em) {
        Tavsiye tavsiye = new Tavsiye().baslik(DEFAULT_BASLIK).icerik(DEFAULT_ICERIK).olusturulmaTarihi(DEFAULT_OLUSTURULMA_TARIHI);
        return tavsiye;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tavsiye createUpdatedEntity(EntityManager em) {
        Tavsiye tavsiye = new Tavsiye().baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK).olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI);
        return tavsiye;
    }

    @BeforeEach
    public void initTest() {
        tavsiye = createEntity(em);
    }

    @Test
    @Transactional
    void createTavsiye() throws Exception {
        int databaseSizeBeforeCreate = tavsiyeRepository.findAll().size();
        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);
        restTavsiyeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isCreated());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeCreate + 1);
        Tavsiye testTavsiye = tavsiyeList.get(tavsiyeList.size() - 1);
        assertThat(testTavsiye.getBaslik()).isEqualTo(DEFAULT_BASLIK);
        assertThat(testTavsiye.getIcerik()).isEqualTo(DEFAULT_ICERIK);
        assertThat(testTavsiye.getOlusturulmaTarihi()).isEqualTo(DEFAULT_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void createTavsiyeWithExistingId() throws Exception {
        // Create the Tavsiye with an existing ID
        tavsiye.setId(1L);
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        int databaseSizeBeforeCreate = tavsiyeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTavsiyeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBaslikIsRequired() throws Exception {
        int databaseSizeBeforeTest = tavsiyeRepository.findAll().size();
        // set the field null
        tavsiye.setBaslik(null);

        // Create the Tavsiye, which fails.
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        restTavsiyeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isBadRequest());

        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIcerikIsRequired() throws Exception {
        int databaseSizeBeforeTest = tavsiyeRepository.findAll().size();
        // set the field null
        tavsiye.setIcerik(null);

        // Create the Tavsiye, which fails.
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        restTavsiyeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isBadRequest());

        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkOlusturulmaTarihiIsRequired() throws Exception {
        int databaseSizeBeforeTest = tavsiyeRepository.findAll().size();
        // set the field null
        tavsiye.setOlusturulmaTarihi(null);

        // Create the Tavsiye, which fails.
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        restTavsiyeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isBadRequest());

        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTavsiyes() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tavsiye.getId().intValue())))
            .andExpect(jsonPath("$.[*].baslik").value(hasItem(DEFAULT_BASLIK)))
            .andExpect(jsonPath("$.[*].icerik").value(hasItem(DEFAULT_ICERIK)))
            .andExpect(jsonPath("$.[*].olusturulmaTarihi").value(hasItem(DEFAULT_OLUSTURULMA_TARIHI.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTavsiyesWithEagerRelationshipsIsEnabled() throws Exception {
        when(tavsiyeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTavsiyeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(tavsiyeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTavsiyesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(tavsiyeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTavsiyeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(tavsiyeRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getTavsiye() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get the tavsiye
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL_ID, tavsiye.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tavsiye.getId().intValue()))
            .andExpect(jsonPath("$.baslik").value(DEFAULT_BASLIK))
            .andExpect(jsonPath("$.icerik").value(DEFAULT_ICERIK))
            .andExpect(jsonPath("$.olusturulmaTarihi").value(DEFAULT_OLUSTURULMA_TARIHI.toString()));
    }

    @Test
    @Transactional
    void getTavsiyesByIdFiltering() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        Long id = tavsiye.getId();

        defaultTavsiyeShouldBeFound("id.equals=" + id);
        defaultTavsiyeShouldNotBeFound("id.notEquals=" + id);

        defaultTavsiyeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTavsiyeShouldNotBeFound("id.greaterThan=" + id);

        defaultTavsiyeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTavsiyeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTavsiyesByBaslikIsEqualToSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where baslik equals to DEFAULT_BASLIK
        defaultTavsiyeShouldBeFound("baslik.equals=" + DEFAULT_BASLIK);

        // Get all the tavsiyeList where baslik equals to UPDATED_BASLIK
        defaultTavsiyeShouldNotBeFound("baslik.equals=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByBaslikIsInShouldWork() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where baslik in DEFAULT_BASLIK or UPDATED_BASLIK
        defaultTavsiyeShouldBeFound("baslik.in=" + DEFAULT_BASLIK + "," + UPDATED_BASLIK);

        // Get all the tavsiyeList where baslik equals to UPDATED_BASLIK
        defaultTavsiyeShouldNotBeFound("baslik.in=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByBaslikIsNullOrNotNull() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where baslik is not null
        defaultTavsiyeShouldBeFound("baslik.specified=true");

        // Get all the tavsiyeList where baslik is null
        defaultTavsiyeShouldNotBeFound("baslik.specified=false");
    }

    @Test
    @Transactional
    void getAllTavsiyesByBaslikContainsSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where baslik contains DEFAULT_BASLIK
        defaultTavsiyeShouldBeFound("baslik.contains=" + DEFAULT_BASLIK);

        // Get all the tavsiyeList where baslik contains UPDATED_BASLIK
        defaultTavsiyeShouldNotBeFound("baslik.contains=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByBaslikNotContainsSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where baslik does not contain DEFAULT_BASLIK
        defaultTavsiyeShouldNotBeFound("baslik.doesNotContain=" + DEFAULT_BASLIK);

        // Get all the tavsiyeList where baslik does not contain UPDATED_BASLIK
        defaultTavsiyeShouldBeFound("baslik.doesNotContain=" + UPDATED_BASLIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByIcerikIsEqualToSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where icerik equals to DEFAULT_ICERIK
        defaultTavsiyeShouldBeFound("icerik.equals=" + DEFAULT_ICERIK);

        // Get all the tavsiyeList where icerik equals to UPDATED_ICERIK
        defaultTavsiyeShouldNotBeFound("icerik.equals=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByIcerikIsInShouldWork() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where icerik in DEFAULT_ICERIK or UPDATED_ICERIK
        defaultTavsiyeShouldBeFound("icerik.in=" + DEFAULT_ICERIK + "," + UPDATED_ICERIK);

        // Get all the tavsiyeList where icerik equals to UPDATED_ICERIK
        defaultTavsiyeShouldNotBeFound("icerik.in=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByIcerikIsNullOrNotNull() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where icerik is not null
        defaultTavsiyeShouldBeFound("icerik.specified=true");

        // Get all the tavsiyeList where icerik is null
        defaultTavsiyeShouldNotBeFound("icerik.specified=false");
    }

    @Test
    @Transactional
    void getAllTavsiyesByIcerikContainsSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where icerik contains DEFAULT_ICERIK
        defaultTavsiyeShouldBeFound("icerik.contains=" + DEFAULT_ICERIK);

        // Get all the tavsiyeList where icerik contains UPDATED_ICERIK
        defaultTavsiyeShouldNotBeFound("icerik.contains=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByIcerikNotContainsSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where icerik does not contain DEFAULT_ICERIK
        defaultTavsiyeShouldNotBeFound("icerik.doesNotContain=" + DEFAULT_ICERIK);

        // Get all the tavsiyeList where icerik does not contain UPDATED_ICERIK
        defaultTavsiyeShouldBeFound("icerik.doesNotContain=" + UPDATED_ICERIK);
    }

    @Test
    @Transactional
    void getAllTavsiyesByOlusturulmaTarihiIsEqualToSomething() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where olusturulmaTarihi equals to DEFAULT_OLUSTURULMA_TARIHI
        defaultTavsiyeShouldBeFound("olusturulmaTarihi.equals=" + DEFAULT_OLUSTURULMA_TARIHI);

        // Get all the tavsiyeList where olusturulmaTarihi equals to UPDATED_OLUSTURULMA_TARIHI
        defaultTavsiyeShouldNotBeFound("olusturulmaTarihi.equals=" + UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void getAllTavsiyesByOlusturulmaTarihiIsInShouldWork() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where olusturulmaTarihi in DEFAULT_OLUSTURULMA_TARIHI or UPDATED_OLUSTURULMA_TARIHI
        defaultTavsiyeShouldBeFound("olusturulmaTarihi.in=" + DEFAULT_OLUSTURULMA_TARIHI + "," + UPDATED_OLUSTURULMA_TARIHI);

        // Get all the tavsiyeList where olusturulmaTarihi equals to UPDATED_OLUSTURULMA_TARIHI
        defaultTavsiyeShouldNotBeFound("olusturulmaTarihi.in=" + UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void getAllTavsiyesByOlusturulmaTarihiIsNullOrNotNull() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        // Get all the tavsiyeList where olusturulmaTarihi is not null
        defaultTavsiyeShouldBeFound("olusturulmaTarihi.specified=true");

        // Get all the tavsiyeList where olusturulmaTarihi is null
        defaultTavsiyeShouldNotBeFound("olusturulmaTarihi.specified=false");
    }

    @Test
    @Transactional
    void getAllTavsiyesByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            tavsiyeRepository.saveAndFlush(tavsiye);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        tavsiye.setKullanici(kullanici);
        tavsiyeRepository.saveAndFlush(tavsiye);
        Long kullaniciId = kullanici.getId();

        // Get all the tavsiyeList where kullanici equals to kullaniciId
        defaultTavsiyeShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the tavsiyeList where kullanici equals to (kullaniciId + 1)
        defaultTavsiyeShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    @Test
    @Transactional
    void getAllTavsiyesByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            tavsiyeRepository.saveAndFlush(tavsiye);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        tavsiye.setKullanici(kullanici);
        tavsiyeRepository.saveAndFlush(tavsiye);
        Long kullaniciId = kullanici.getId();

        // Get all the tavsiyeList where kullanici equals to kullaniciId
        defaultTavsiyeShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the tavsiyeList where kullanici equals to (kullaniciId + 1)
        defaultTavsiyeShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTavsiyeShouldBeFound(String filter) throws Exception {
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tavsiye.getId().intValue())))
            .andExpect(jsonPath("$.[*].baslik").value(hasItem(DEFAULT_BASLIK)))
            .andExpect(jsonPath("$.[*].icerik").value(hasItem(DEFAULT_ICERIK)))
            .andExpect(jsonPath("$.[*].olusturulmaTarihi").value(hasItem(DEFAULT_OLUSTURULMA_TARIHI.toString())));

        // Check, that the count call also returns 1
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTavsiyeShouldNotBeFound(String filter) throws Exception {
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTavsiyeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTavsiye() throws Exception {
        // Get the tavsiye
        restTavsiyeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTavsiye() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();

        // Update the tavsiye
        Tavsiye updatedTavsiye = tavsiyeRepository.findById(tavsiye.getId()).get();
        // Disconnect from session so that the updates on updatedTavsiye are not directly saved in db
        em.detach(updatedTavsiye);
        updatedTavsiye.baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK).olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI);
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(updatedTavsiye);

        restTavsiyeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tavsiyeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
        Tavsiye testTavsiye = tavsiyeList.get(tavsiyeList.size() - 1);
        assertThat(testTavsiye.getBaslik()).isEqualTo(UPDATED_BASLIK);
        assertThat(testTavsiye.getIcerik()).isEqualTo(UPDATED_ICERIK);
        assertThat(testTavsiye.getOlusturulmaTarihi()).isEqualTo(UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void putNonExistingTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tavsiyeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTavsiyeWithPatch() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();

        // Update the tavsiye using partial update
        Tavsiye partialUpdatedTavsiye = new Tavsiye();
        partialUpdatedTavsiye.setId(tavsiye.getId());

        restTavsiyeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTavsiye.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTavsiye))
            )
            .andExpect(status().isOk());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
        Tavsiye testTavsiye = tavsiyeList.get(tavsiyeList.size() - 1);
        assertThat(testTavsiye.getBaslik()).isEqualTo(DEFAULT_BASLIK);
        assertThat(testTavsiye.getIcerik()).isEqualTo(DEFAULT_ICERIK);
        assertThat(testTavsiye.getOlusturulmaTarihi()).isEqualTo(DEFAULT_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void fullUpdateTavsiyeWithPatch() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();

        // Update the tavsiye using partial update
        Tavsiye partialUpdatedTavsiye = new Tavsiye();
        partialUpdatedTavsiye.setId(tavsiye.getId());

        partialUpdatedTavsiye.baslik(UPDATED_BASLIK).icerik(UPDATED_ICERIK).olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI);

        restTavsiyeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTavsiye.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTavsiye))
            )
            .andExpect(status().isOk());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
        Tavsiye testTavsiye = tavsiyeList.get(tavsiyeList.size() - 1);
        assertThat(testTavsiye.getBaslik()).isEqualTo(UPDATED_BASLIK);
        assertThat(testTavsiye.getIcerik()).isEqualTo(UPDATED_ICERIK);
        assertThat(testTavsiye.getOlusturulmaTarihi()).isEqualTo(UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void patchNonExistingTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, tavsiyeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTavsiye() throws Exception {
        int databaseSizeBeforeUpdate = tavsiyeRepository.findAll().size();
        tavsiye.setId(count.incrementAndGet());

        // Create the Tavsiye
        TavsiyeDTO tavsiyeDTO = tavsiyeMapper.toDto(tavsiye);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTavsiyeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(tavsiyeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Tavsiye in the database
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTavsiye() throws Exception {
        // Initialize the database
        tavsiyeRepository.saveAndFlush(tavsiye);

        int databaseSizeBeforeDelete = tavsiyeRepository.findAll().size();

        // Delete the tavsiye
        restTavsiyeMockMvc
            .perform(delete(ENTITY_API_URL_ID, tavsiye.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tavsiye> tavsiyeList = tavsiyeRepository.findAll();
        assertThat(tavsiyeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
