package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Davet;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.repository.DavetRepository;
import com.hobiharitasi.service.DavetService;
import com.hobiharitasi.service.criteria.DavetCriteria;
import com.hobiharitasi.service.dto.DavetDTO;
import com.hobiharitasi.service.mapper.DavetMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DavetResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DavetResourceIT {

    private static final String DEFAULT_DAVET_MESAJI = "AAAAAAAAAA";
    private static final String UPDATED_DAVET_MESAJI = "BBBBBBBBBB";

    private static final String DEFAULT_DURUM = "AAAAAAAAAA";
    private static final String UPDATED_DURUM = "BBBBBBBBBB";

    private static final Instant DEFAULT_OLUSTURULMA_TARIHI = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OLUSTURULMA_TARIHI = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DAVET_TARIHI = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DAVET_TARIHI = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_KABUL_EDILDI_MI = false;
    private static final Boolean UPDATED_KABUL_EDILDI_MI = true;

    private static final String ENTITY_API_URL = "/api/davets";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DavetRepository davetRepository;

    @Mock
    private DavetRepository davetRepositoryMock;

    @Autowired
    private DavetMapper davetMapper;

    @Mock
    private DavetService davetServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDavetMockMvc;

    private Davet davet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Davet createEntity(EntityManager em) {
        Davet davet = new Davet()
            .davetMesaji(DEFAULT_DAVET_MESAJI)
            .durum(DEFAULT_DURUM)
            .olusturulmaTarihi(DEFAULT_OLUSTURULMA_TARIHI)
            .davetTarihi(DEFAULT_DAVET_TARIHI)
            .kabulEdildiMi(DEFAULT_KABUL_EDILDI_MI);
        return davet;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Davet createUpdatedEntity(EntityManager em) {
        Davet davet = new Davet()
            .davetMesaji(UPDATED_DAVET_MESAJI)
            .durum(UPDATED_DURUM)
            .olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI)
            .davetTarihi(UPDATED_DAVET_TARIHI)
            .kabulEdildiMi(UPDATED_KABUL_EDILDI_MI);
        return davet;
    }

    @BeforeEach
    public void initTest() {
        davet = createEntity(em);
    }

    @Test
    @Transactional
    void createDavet() throws Exception {
        int databaseSizeBeforeCreate = davetRepository.findAll().size();
        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);
        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isCreated());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeCreate + 1);
        Davet testDavet = davetList.get(davetList.size() - 1);
        assertThat(testDavet.getDavetMesaji()).isEqualTo(DEFAULT_DAVET_MESAJI);
        assertThat(testDavet.getDurum()).isEqualTo(DEFAULT_DURUM);
        assertThat(testDavet.getOlusturulmaTarihi()).isEqualTo(DEFAULT_OLUSTURULMA_TARIHI);
        assertThat(testDavet.getDavetTarihi()).isEqualTo(DEFAULT_DAVET_TARIHI);
        assertThat(testDavet.getKabulEdildiMi()).isEqualTo(DEFAULT_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void createDavetWithExistingId() throws Exception {
        // Create the Davet with an existing ID
        davet.setId(1L);
        DavetDTO davetDTO = davetMapper.toDto(davet);

        int databaseSizeBeforeCreate = davetRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDavetMesajiIsRequired() throws Exception {
        int databaseSizeBeforeTest = davetRepository.findAll().size();
        // set the field null
        davet.setDavetMesaji(null);

        // Create the Davet, which fails.
        DavetDTO davetDTO = davetMapper.toDto(davet);

        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isBadRequest());

        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDurumIsRequired() throws Exception {
        int databaseSizeBeforeTest = davetRepository.findAll().size();
        // set the field null
        davet.setDurum(null);

        // Create the Davet, which fails.
        DavetDTO davetDTO = davetMapper.toDto(davet);

        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isBadRequest());

        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkOlusturulmaTarihiIsRequired() throws Exception {
        int databaseSizeBeforeTest = davetRepository.findAll().size();
        // set the field null
        davet.setOlusturulmaTarihi(null);

        // Create the Davet, which fails.
        DavetDTO davetDTO = davetMapper.toDto(davet);

        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isBadRequest());

        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDavetTarihiIsRequired() throws Exception {
        int databaseSizeBeforeTest = davetRepository.findAll().size();
        // set the field null
        davet.setDavetTarihi(null);

        // Create the Davet, which fails.
        DavetDTO davetDTO = davetMapper.toDto(davet);

        restDavetMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isBadRequest());

        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDavets() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList
        restDavetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(davet.getId().intValue())))
            .andExpect(jsonPath("$.[*].davetMesaji").value(hasItem(DEFAULT_DAVET_MESAJI)))
            .andExpect(jsonPath("$.[*].durum").value(hasItem(DEFAULT_DURUM)))
            .andExpect(jsonPath("$.[*].olusturulmaTarihi").value(hasItem(DEFAULT_OLUSTURULMA_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].davetTarihi").value(hasItem(DEFAULT_DAVET_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].kabulEdildiMi").value(hasItem(DEFAULT_KABUL_EDILDI_MI.booleanValue())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDavetsWithEagerRelationshipsIsEnabled() throws Exception {
        when(davetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDavetMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(davetServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDavetsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(davetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDavetMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(davetRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getDavet() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get the davet
        restDavetMockMvc
            .perform(get(ENTITY_API_URL_ID, davet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(davet.getId().intValue()))
            .andExpect(jsonPath("$.davetMesaji").value(DEFAULT_DAVET_MESAJI))
            .andExpect(jsonPath("$.durum").value(DEFAULT_DURUM))
            .andExpect(jsonPath("$.olusturulmaTarihi").value(DEFAULT_OLUSTURULMA_TARIHI.toString()))
            .andExpect(jsonPath("$.davetTarihi").value(DEFAULT_DAVET_TARIHI.toString()))
            .andExpect(jsonPath("$.kabulEdildiMi").value(DEFAULT_KABUL_EDILDI_MI.booleanValue()));
    }

    @Test
    @Transactional
    void getDavetsByIdFiltering() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        Long id = davet.getId();

        defaultDavetShouldBeFound("id.equals=" + id);
        defaultDavetShouldNotBeFound("id.notEquals=" + id);

        defaultDavetShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDavetShouldNotBeFound("id.greaterThan=" + id);

        defaultDavetShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDavetShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetMesajiIsEqualToSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetMesaji equals to DEFAULT_DAVET_MESAJI
        defaultDavetShouldBeFound("davetMesaji.equals=" + DEFAULT_DAVET_MESAJI);

        // Get all the davetList where davetMesaji equals to UPDATED_DAVET_MESAJI
        defaultDavetShouldNotBeFound("davetMesaji.equals=" + UPDATED_DAVET_MESAJI);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetMesajiIsInShouldWork() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetMesaji in DEFAULT_DAVET_MESAJI or UPDATED_DAVET_MESAJI
        defaultDavetShouldBeFound("davetMesaji.in=" + DEFAULT_DAVET_MESAJI + "," + UPDATED_DAVET_MESAJI);

        // Get all the davetList where davetMesaji equals to UPDATED_DAVET_MESAJI
        defaultDavetShouldNotBeFound("davetMesaji.in=" + UPDATED_DAVET_MESAJI);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetMesajiIsNullOrNotNull() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetMesaji is not null
        defaultDavetShouldBeFound("davetMesaji.specified=true");

        // Get all the davetList where davetMesaji is null
        defaultDavetShouldNotBeFound("davetMesaji.specified=false");
    }

    @Test
    @Transactional
    void getAllDavetsByDavetMesajiContainsSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetMesaji contains DEFAULT_DAVET_MESAJI
        defaultDavetShouldBeFound("davetMesaji.contains=" + DEFAULT_DAVET_MESAJI);

        // Get all the davetList where davetMesaji contains UPDATED_DAVET_MESAJI
        defaultDavetShouldNotBeFound("davetMesaji.contains=" + UPDATED_DAVET_MESAJI);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetMesajiNotContainsSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetMesaji does not contain DEFAULT_DAVET_MESAJI
        defaultDavetShouldNotBeFound("davetMesaji.doesNotContain=" + DEFAULT_DAVET_MESAJI);

        // Get all the davetList where davetMesaji does not contain UPDATED_DAVET_MESAJI
        defaultDavetShouldBeFound("davetMesaji.doesNotContain=" + UPDATED_DAVET_MESAJI);
    }

    @Test
    @Transactional
    void getAllDavetsByDurumIsEqualToSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where durum equals to DEFAULT_DURUM
        defaultDavetShouldBeFound("durum.equals=" + DEFAULT_DURUM);

        // Get all the davetList where durum equals to UPDATED_DURUM
        defaultDavetShouldNotBeFound("durum.equals=" + UPDATED_DURUM);
    }

    @Test
    @Transactional
    void getAllDavetsByDurumIsInShouldWork() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where durum in DEFAULT_DURUM or UPDATED_DURUM
        defaultDavetShouldBeFound("durum.in=" + DEFAULT_DURUM + "," + UPDATED_DURUM);

        // Get all the davetList where durum equals to UPDATED_DURUM
        defaultDavetShouldNotBeFound("durum.in=" + UPDATED_DURUM);
    }

    @Test
    @Transactional
    void getAllDavetsByDurumIsNullOrNotNull() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where durum is not null
        defaultDavetShouldBeFound("durum.specified=true");

        // Get all the davetList where durum is null
        defaultDavetShouldNotBeFound("durum.specified=false");
    }

    @Test
    @Transactional
    void getAllDavetsByDurumContainsSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where durum contains DEFAULT_DURUM
        defaultDavetShouldBeFound("durum.contains=" + DEFAULT_DURUM);

        // Get all the davetList where durum contains UPDATED_DURUM
        defaultDavetShouldNotBeFound("durum.contains=" + UPDATED_DURUM);
    }

    @Test
    @Transactional
    void getAllDavetsByDurumNotContainsSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where durum does not contain DEFAULT_DURUM
        defaultDavetShouldNotBeFound("durum.doesNotContain=" + DEFAULT_DURUM);

        // Get all the davetList where durum does not contain UPDATED_DURUM
        defaultDavetShouldBeFound("durum.doesNotContain=" + UPDATED_DURUM);
    }

    @Test
    @Transactional
    void getAllDavetsByOlusturulmaTarihiIsEqualToSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where olusturulmaTarihi equals to DEFAULT_OLUSTURULMA_TARIHI
        defaultDavetShouldBeFound("olusturulmaTarihi.equals=" + DEFAULT_OLUSTURULMA_TARIHI);

        // Get all the davetList where olusturulmaTarihi equals to UPDATED_OLUSTURULMA_TARIHI
        defaultDavetShouldNotBeFound("olusturulmaTarihi.equals=" + UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void getAllDavetsByOlusturulmaTarihiIsInShouldWork() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where olusturulmaTarihi in DEFAULT_OLUSTURULMA_TARIHI or UPDATED_OLUSTURULMA_TARIHI
        defaultDavetShouldBeFound("olusturulmaTarihi.in=" + DEFAULT_OLUSTURULMA_TARIHI + "," + UPDATED_OLUSTURULMA_TARIHI);

        // Get all the davetList where olusturulmaTarihi equals to UPDATED_OLUSTURULMA_TARIHI
        defaultDavetShouldNotBeFound("olusturulmaTarihi.in=" + UPDATED_OLUSTURULMA_TARIHI);
    }

    @Test
    @Transactional
    void getAllDavetsByOlusturulmaTarihiIsNullOrNotNull() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where olusturulmaTarihi is not null
        defaultDavetShouldBeFound("olusturulmaTarihi.specified=true");

        // Get all the davetList where olusturulmaTarihi is null
        defaultDavetShouldNotBeFound("olusturulmaTarihi.specified=false");
    }

    @Test
    @Transactional
    void getAllDavetsByDavetTarihiIsEqualToSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetTarihi equals to DEFAULT_DAVET_TARIHI
        defaultDavetShouldBeFound("davetTarihi.equals=" + DEFAULT_DAVET_TARIHI);

        // Get all the davetList where davetTarihi equals to UPDATED_DAVET_TARIHI
        defaultDavetShouldNotBeFound("davetTarihi.equals=" + UPDATED_DAVET_TARIHI);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetTarihiIsInShouldWork() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetTarihi in DEFAULT_DAVET_TARIHI or UPDATED_DAVET_TARIHI
        defaultDavetShouldBeFound("davetTarihi.in=" + DEFAULT_DAVET_TARIHI + "," + UPDATED_DAVET_TARIHI);

        // Get all the davetList where davetTarihi equals to UPDATED_DAVET_TARIHI
        defaultDavetShouldNotBeFound("davetTarihi.in=" + UPDATED_DAVET_TARIHI);
    }

    @Test
    @Transactional
    void getAllDavetsByDavetTarihiIsNullOrNotNull() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where davetTarihi is not null
        defaultDavetShouldBeFound("davetTarihi.specified=true");

        // Get all the davetList where davetTarihi is null
        defaultDavetShouldNotBeFound("davetTarihi.specified=false");
    }

    @Test
    @Transactional
    void getAllDavetsByKabulEdildiMiIsEqualToSomething() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where kabulEdildiMi equals to DEFAULT_KABUL_EDILDI_MI
        defaultDavetShouldBeFound("kabulEdildiMi.equals=" + DEFAULT_KABUL_EDILDI_MI);

        // Get all the davetList where kabulEdildiMi equals to UPDATED_KABUL_EDILDI_MI
        defaultDavetShouldNotBeFound("kabulEdildiMi.equals=" + UPDATED_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void getAllDavetsByKabulEdildiMiIsInShouldWork() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where kabulEdildiMi in DEFAULT_KABUL_EDILDI_MI or UPDATED_KABUL_EDILDI_MI
        defaultDavetShouldBeFound("kabulEdildiMi.in=" + DEFAULT_KABUL_EDILDI_MI + "," + UPDATED_KABUL_EDILDI_MI);

        // Get all the davetList where kabulEdildiMi equals to UPDATED_KABUL_EDILDI_MI
        defaultDavetShouldNotBeFound("kabulEdildiMi.in=" + UPDATED_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void getAllDavetsByKabulEdildiMiIsNullOrNotNull() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        // Get all the davetList where kabulEdildiMi is not null
        defaultDavetShouldBeFound("kabulEdildiMi.specified=true");

        // Get all the davetList where kabulEdildiMi is null
        defaultDavetShouldNotBeFound("kabulEdildiMi.specified=false");
    }

    @Test
    @Transactional
    void getAllDavetsByGonderenIsEqualToSomething() throws Exception {
        Kullanici gonderen;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            davetRepository.saveAndFlush(davet);
            gonderen = KullaniciResourceIT.createEntity(em);
        } else {
            gonderen = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(gonderen);
        em.flush();
        davet.setGonderen(gonderen);
        davetRepository.saveAndFlush(davet);
        Long gonderenId = gonderen.getId();

        // Get all the davetList where gonderen equals to gonderenId
        defaultDavetShouldBeFound("gonderenId.equals=" + gonderenId);

        // Get all the davetList where gonderen equals to (gonderenId + 1)
        defaultDavetShouldNotBeFound("gonderenId.equals=" + (gonderenId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDavetShouldBeFound(String filter) throws Exception {
        restDavetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(davet.getId().intValue())))
            .andExpect(jsonPath("$.[*].davetMesaji").value(hasItem(DEFAULT_DAVET_MESAJI)))
            .andExpect(jsonPath("$.[*].durum").value(hasItem(DEFAULT_DURUM)))
            .andExpect(jsonPath("$.[*].olusturulmaTarihi").value(hasItem(DEFAULT_OLUSTURULMA_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].davetTarihi").value(hasItem(DEFAULT_DAVET_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].kabulEdildiMi").value(hasItem(DEFAULT_KABUL_EDILDI_MI.booleanValue())));

        // Check, that the count call also returns 1
        restDavetMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDavetShouldNotBeFound(String filter) throws Exception {
        restDavetMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDavetMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDavet() throws Exception {
        // Get the davet
        restDavetMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingDavet() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        int databaseSizeBeforeUpdate = davetRepository.findAll().size();

        // Update the davet
        Davet updatedDavet = davetRepository.findById(davet.getId()).get();
        // Disconnect from session so that the updates on updatedDavet are not directly saved in db
        em.detach(updatedDavet);
        updatedDavet
            .davetMesaji(UPDATED_DAVET_MESAJI)
            .durum(UPDATED_DURUM)
            .olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI)
            .davetTarihi(UPDATED_DAVET_TARIHI)
            .kabulEdildiMi(UPDATED_KABUL_EDILDI_MI);
        DavetDTO davetDTO = davetMapper.toDto(updatedDavet);

        restDavetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, davetDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(davetDTO))
            )
            .andExpect(status().isOk());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
        Davet testDavet = davetList.get(davetList.size() - 1);
        assertThat(testDavet.getDavetMesaji()).isEqualTo(UPDATED_DAVET_MESAJI);
        assertThat(testDavet.getDurum()).isEqualTo(UPDATED_DURUM);
        assertThat(testDavet.getOlusturulmaTarihi()).isEqualTo(UPDATED_OLUSTURULMA_TARIHI);
        assertThat(testDavet.getDavetTarihi()).isEqualTo(UPDATED_DAVET_TARIHI);
        assertThat(testDavet.getKabulEdildiMi()).isEqualTo(UPDATED_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void putNonExistingDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, davetDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(davetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(davetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDavetWithPatch() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        int databaseSizeBeforeUpdate = davetRepository.findAll().size();

        // Update the davet using partial update
        Davet partialUpdatedDavet = new Davet();
        partialUpdatedDavet.setId(davet.getId());

        partialUpdatedDavet
            .olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI)
            .davetTarihi(UPDATED_DAVET_TARIHI)
            .kabulEdildiMi(UPDATED_KABUL_EDILDI_MI);

        restDavetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDavet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDavet))
            )
            .andExpect(status().isOk());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
        Davet testDavet = davetList.get(davetList.size() - 1);
        assertThat(testDavet.getDavetMesaji()).isEqualTo(DEFAULT_DAVET_MESAJI);
        assertThat(testDavet.getDurum()).isEqualTo(DEFAULT_DURUM);
        assertThat(testDavet.getOlusturulmaTarihi()).isEqualTo(UPDATED_OLUSTURULMA_TARIHI);
        assertThat(testDavet.getDavetTarihi()).isEqualTo(UPDATED_DAVET_TARIHI);
        assertThat(testDavet.getKabulEdildiMi()).isEqualTo(UPDATED_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void fullUpdateDavetWithPatch() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        int databaseSizeBeforeUpdate = davetRepository.findAll().size();

        // Update the davet using partial update
        Davet partialUpdatedDavet = new Davet();
        partialUpdatedDavet.setId(davet.getId());

        partialUpdatedDavet
            .davetMesaji(UPDATED_DAVET_MESAJI)
            .durum(UPDATED_DURUM)
            .olusturulmaTarihi(UPDATED_OLUSTURULMA_TARIHI)
            .davetTarihi(UPDATED_DAVET_TARIHI)
            .kabulEdildiMi(UPDATED_KABUL_EDILDI_MI);

        restDavetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDavet.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDavet))
            )
            .andExpect(status().isOk());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
        Davet testDavet = davetList.get(davetList.size() - 1);
        assertThat(testDavet.getDavetMesaji()).isEqualTo(UPDATED_DAVET_MESAJI);
        assertThat(testDavet.getDurum()).isEqualTo(UPDATED_DURUM);
        assertThat(testDavet.getOlusturulmaTarihi()).isEqualTo(UPDATED_OLUSTURULMA_TARIHI);
        assertThat(testDavet.getDavetTarihi()).isEqualTo(UPDATED_DAVET_TARIHI);
        assertThat(testDavet.getKabulEdildiMi()).isEqualTo(UPDATED_KABUL_EDILDI_MI);
    }

    @Test
    @Transactional
    void patchNonExistingDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, davetDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(davetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(davetDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDavet() throws Exception {
        int databaseSizeBeforeUpdate = davetRepository.findAll().size();
        davet.setId(count.incrementAndGet());

        // Create the Davet
        DavetDTO davetDTO = davetMapper.toDto(davet);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDavetMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(davetDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Davet in the database
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDavet() throws Exception {
        // Initialize the database
        davetRepository.saveAndFlush(davet);

        int databaseSizeBeforeDelete = davetRepository.findAll().size();

        // Delete the davet
        restDavetMockMvc
            .perform(delete(ENTITY_API_URL_ID, davet.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Davet> davetList = davetRepository.findAll();
        assertThat(davetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
