package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.repository.HobilerRepository;
import com.hobiharitasi.service.criteria.HobilerCriteria;
import com.hobiharitasi.service.dto.HobilerDTO;
import com.hobiharitasi.service.mapper.HobilerMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HobilerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class HobilerResourceIT {

    private static final String DEFAULT_HOBI_ADI = "AAAAAAAAAA";
    private static final String UPDATED_HOBI_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final String DEFAULT_LOKASYON = "AAAAAAAAAA";
    private static final String UPDATED_LOKASYON = "BBBBBBBBBB";

    private static final String DEFAULT_CESIDI = "AAAAAAAAAA";
    private static final String UPDATED_CESIDI = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/hobilers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HobilerRepository hobilerRepository;

    @Autowired
    private HobilerMapper hobilerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHobilerMockMvc;

    private Hobiler hobiler;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hobiler createEntity(EntityManager em) {
        Hobiler hobiler = new Hobiler()
            .hobiAdi(DEFAULT_HOBI_ADI)
            .aciklama(DEFAULT_ACIKLAMA)
            .lokasyon(DEFAULT_LOKASYON)
            .cesidi(DEFAULT_CESIDI);
        return hobiler;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hobiler createUpdatedEntity(EntityManager em) {
        Hobiler hobiler = new Hobiler()
            .hobiAdi(UPDATED_HOBI_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .lokasyon(UPDATED_LOKASYON)
            .cesidi(UPDATED_CESIDI);
        return hobiler;
    }

    @BeforeEach
    public void initTest() {
        hobiler = createEntity(em);
    }

    @Test
    @Transactional
    void createHobiler() throws Exception {
        int databaseSizeBeforeCreate = hobilerRepository.findAll().size();
        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);
        restHobilerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hobilerDTO)))
            .andExpect(status().isCreated());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeCreate + 1);
        Hobiler testHobiler = hobilerList.get(hobilerList.size() - 1);
        assertThat(testHobiler.getHobiAdi()).isEqualTo(DEFAULT_HOBI_ADI);
        assertThat(testHobiler.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testHobiler.getLokasyon()).isEqualTo(DEFAULT_LOKASYON);
        assertThat(testHobiler.getCesidi()).isEqualTo(DEFAULT_CESIDI);
    }

    @Test
    @Transactional
    void createHobilerWithExistingId() throws Exception {
        // Create the Hobiler with an existing ID
        hobiler.setId(1L);
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        int databaseSizeBeforeCreate = hobilerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHobilerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hobilerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkHobiAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = hobilerRepository.findAll().size();
        // set the field null
        hobiler.setHobiAdi(null);

        // Create the Hobiler, which fails.
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        restHobilerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hobilerDTO)))
            .andExpect(status().isBadRequest());

        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllHobilers() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hobiler.getId().intValue())))
            .andExpect(jsonPath("$.[*].hobiAdi").value(hasItem(DEFAULT_HOBI_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].lokasyon").value(hasItem(DEFAULT_LOKASYON)))
            .andExpect(jsonPath("$.[*].cesidi").value(hasItem(DEFAULT_CESIDI)));
    }

    @Test
    @Transactional
    void getHobiler() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get the hobiler
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL_ID, hobiler.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hobiler.getId().intValue()))
            .andExpect(jsonPath("$.hobiAdi").value(DEFAULT_HOBI_ADI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA))
            .andExpect(jsonPath("$.lokasyon").value(DEFAULT_LOKASYON))
            .andExpect(jsonPath("$.cesidi").value(DEFAULT_CESIDI));
    }

    @Test
    @Transactional
    void getHobilersByIdFiltering() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        Long id = hobiler.getId();

        defaultHobilerShouldBeFound("id.equals=" + id);
        defaultHobilerShouldNotBeFound("id.notEquals=" + id);

        defaultHobilerShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultHobilerShouldNotBeFound("id.greaterThan=" + id);

        defaultHobilerShouldBeFound("id.lessThanOrEqual=" + id);
        defaultHobilerShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllHobilersByHobiAdiIsEqualToSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where hobiAdi equals to DEFAULT_HOBI_ADI
        defaultHobilerShouldBeFound("hobiAdi.equals=" + DEFAULT_HOBI_ADI);

        // Get all the hobilerList where hobiAdi equals to UPDATED_HOBI_ADI
        defaultHobilerShouldNotBeFound("hobiAdi.equals=" + UPDATED_HOBI_ADI);
    }

    @Test
    @Transactional
    void getAllHobilersByHobiAdiIsInShouldWork() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where hobiAdi in DEFAULT_HOBI_ADI or UPDATED_HOBI_ADI
        defaultHobilerShouldBeFound("hobiAdi.in=" + DEFAULT_HOBI_ADI + "," + UPDATED_HOBI_ADI);

        // Get all the hobilerList where hobiAdi equals to UPDATED_HOBI_ADI
        defaultHobilerShouldNotBeFound("hobiAdi.in=" + UPDATED_HOBI_ADI);
    }

    @Test
    @Transactional
    void getAllHobilersByHobiAdiIsNullOrNotNull() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where hobiAdi is not null
        defaultHobilerShouldBeFound("hobiAdi.specified=true");

        // Get all the hobilerList where hobiAdi is null
        defaultHobilerShouldNotBeFound("hobiAdi.specified=false");
    }

    @Test
    @Transactional
    void getAllHobilersByHobiAdiContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where hobiAdi contains DEFAULT_HOBI_ADI
        defaultHobilerShouldBeFound("hobiAdi.contains=" + DEFAULT_HOBI_ADI);

        // Get all the hobilerList where hobiAdi contains UPDATED_HOBI_ADI
        defaultHobilerShouldNotBeFound("hobiAdi.contains=" + UPDATED_HOBI_ADI);
    }

    @Test
    @Transactional
    void getAllHobilersByHobiAdiNotContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where hobiAdi does not contain DEFAULT_HOBI_ADI
        defaultHobilerShouldNotBeFound("hobiAdi.doesNotContain=" + DEFAULT_HOBI_ADI);

        // Get all the hobilerList where hobiAdi does not contain UPDATED_HOBI_ADI
        defaultHobilerShouldBeFound("hobiAdi.doesNotContain=" + UPDATED_HOBI_ADI);
    }

    @Test
    @Transactional
    void getAllHobilersByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where aciklama equals to DEFAULT_ACIKLAMA
        defaultHobilerShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the hobilerList where aciklama equals to UPDATED_ACIKLAMA
        defaultHobilerShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllHobilersByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultHobilerShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the hobilerList where aciklama equals to UPDATED_ACIKLAMA
        defaultHobilerShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllHobilersByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where aciklama is not null
        defaultHobilerShouldBeFound("aciklama.specified=true");

        // Get all the hobilerList where aciklama is null
        defaultHobilerShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllHobilersByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where aciklama contains DEFAULT_ACIKLAMA
        defaultHobilerShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the hobilerList where aciklama contains UPDATED_ACIKLAMA
        defaultHobilerShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllHobilersByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultHobilerShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the hobilerList where aciklama does not contain UPDATED_ACIKLAMA
        defaultHobilerShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllHobilersByLokasyonIsEqualToSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where lokasyon equals to DEFAULT_LOKASYON
        defaultHobilerShouldBeFound("lokasyon.equals=" + DEFAULT_LOKASYON);

        // Get all the hobilerList where lokasyon equals to UPDATED_LOKASYON
        defaultHobilerShouldNotBeFound("lokasyon.equals=" + UPDATED_LOKASYON);
    }

    @Test
    @Transactional
    void getAllHobilersByLokasyonIsInShouldWork() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where lokasyon in DEFAULT_LOKASYON or UPDATED_LOKASYON
        defaultHobilerShouldBeFound("lokasyon.in=" + DEFAULT_LOKASYON + "," + UPDATED_LOKASYON);

        // Get all the hobilerList where lokasyon equals to UPDATED_LOKASYON
        defaultHobilerShouldNotBeFound("lokasyon.in=" + UPDATED_LOKASYON);
    }

    @Test
    @Transactional
    void getAllHobilersByLokasyonIsNullOrNotNull() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where lokasyon is not null
        defaultHobilerShouldBeFound("lokasyon.specified=true");

        // Get all the hobilerList where lokasyon is null
        defaultHobilerShouldNotBeFound("lokasyon.specified=false");
    }

    @Test
    @Transactional
    void getAllHobilersByLokasyonContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where lokasyon contains DEFAULT_LOKASYON
        defaultHobilerShouldBeFound("lokasyon.contains=" + DEFAULT_LOKASYON);

        // Get all the hobilerList where lokasyon contains UPDATED_LOKASYON
        defaultHobilerShouldNotBeFound("lokasyon.contains=" + UPDATED_LOKASYON);
    }

    @Test
    @Transactional
    void getAllHobilersByLokasyonNotContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where lokasyon does not contain DEFAULT_LOKASYON
        defaultHobilerShouldNotBeFound("lokasyon.doesNotContain=" + DEFAULT_LOKASYON);

        // Get all the hobilerList where lokasyon does not contain UPDATED_LOKASYON
        defaultHobilerShouldBeFound("lokasyon.doesNotContain=" + UPDATED_LOKASYON);
    }

    @Test
    @Transactional
    void getAllHobilersByCesidiIsEqualToSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where cesidi equals to DEFAULT_CESIDI
        defaultHobilerShouldBeFound("cesidi.equals=" + DEFAULT_CESIDI);

        // Get all the hobilerList where cesidi equals to UPDATED_CESIDI
        defaultHobilerShouldNotBeFound("cesidi.equals=" + UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void getAllHobilersByCesidiIsInShouldWork() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where cesidi in DEFAULT_CESIDI or UPDATED_CESIDI
        defaultHobilerShouldBeFound("cesidi.in=" + DEFAULT_CESIDI + "," + UPDATED_CESIDI);

        // Get all the hobilerList where cesidi equals to UPDATED_CESIDI
        defaultHobilerShouldNotBeFound("cesidi.in=" + UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void getAllHobilersByCesidiIsNullOrNotNull() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where cesidi is not null
        defaultHobilerShouldBeFound("cesidi.specified=true");

        // Get all the hobilerList where cesidi is null
        defaultHobilerShouldNotBeFound("cesidi.specified=false");
    }

    @Test
    @Transactional
    void getAllHobilersByCesidiContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where cesidi contains DEFAULT_CESIDI
        defaultHobilerShouldBeFound("cesidi.contains=" + DEFAULT_CESIDI);

        // Get all the hobilerList where cesidi contains UPDATED_CESIDI
        defaultHobilerShouldNotBeFound("cesidi.contains=" + UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void getAllHobilersByCesidiNotContainsSomething() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        // Get all the hobilerList where cesidi does not contain DEFAULT_CESIDI
        defaultHobilerShouldNotBeFound("cesidi.doesNotContain=" + DEFAULT_CESIDI);

        // Get all the hobilerList where cesidi does not contain UPDATED_CESIDI
        defaultHobilerShouldBeFound("cesidi.doesNotContain=" + UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void getAllHobilersByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            hobilerRepository.saveAndFlush(hobiler);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        hobiler.addKullanici(kullanici);
        hobilerRepository.saveAndFlush(hobiler);
        Long kullaniciId = kullanici.getId();

        // Get all the hobilerList where kullanici equals to kullaniciId
        defaultHobilerShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the hobilerList where kullanici equals to (kullaniciId + 1)
        defaultHobilerShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultHobilerShouldBeFound(String filter) throws Exception {
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hobiler.getId().intValue())))
            .andExpect(jsonPath("$.[*].hobiAdi").value(hasItem(DEFAULT_HOBI_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].lokasyon").value(hasItem(DEFAULT_LOKASYON)))
            .andExpect(jsonPath("$.[*].cesidi").value(hasItem(DEFAULT_CESIDI)));

        // Check, that the count call also returns 1
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultHobilerShouldNotBeFound(String filter) throws Exception {
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restHobilerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingHobiler() throws Exception {
        // Get the hobiler
        restHobilerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingHobiler() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();

        // Update the hobiler
        Hobiler updatedHobiler = hobilerRepository.findById(hobiler.getId()).get();
        // Disconnect from session so that the updates on updatedHobiler are not directly saved in db
        em.detach(updatedHobiler);
        updatedHobiler.hobiAdi(UPDATED_HOBI_ADI).aciklama(UPDATED_ACIKLAMA).lokasyon(UPDATED_LOKASYON).cesidi(UPDATED_CESIDI);
        HobilerDTO hobilerDTO = hobilerMapper.toDto(updatedHobiler);

        restHobilerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hobilerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
        Hobiler testHobiler = hobilerList.get(hobilerList.size() - 1);
        assertThat(testHobiler.getHobiAdi()).isEqualTo(UPDATED_HOBI_ADI);
        assertThat(testHobiler.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testHobiler.getLokasyon()).isEqualTo(UPDATED_LOKASYON);
        assertThat(testHobiler.getCesidi()).isEqualTo(UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void putNonExistingHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hobilerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hobilerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHobilerWithPatch() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();

        // Update the hobiler using partial update
        Hobiler partialUpdatedHobiler = new Hobiler();
        partialUpdatedHobiler.setId(hobiler.getId());

        partialUpdatedHobiler.hobiAdi(UPDATED_HOBI_ADI).aciklama(UPDATED_ACIKLAMA);

        restHobilerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHobiler.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHobiler))
            )
            .andExpect(status().isOk());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
        Hobiler testHobiler = hobilerList.get(hobilerList.size() - 1);
        assertThat(testHobiler.getHobiAdi()).isEqualTo(UPDATED_HOBI_ADI);
        assertThat(testHobiler.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testHobiler.getLokasyon()).isEqualTo(DEFAULT_LOKASYON);
        assertThat(testHobiler.getCesidi()).isEqualTo(DEFAULT_CESIDI);
    }

    @Test
    @Transactional
    void fullUpdateHobilerWithPatch() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();

        // Update the hobiler using partial update
        Hobiler partialUpdatedHobiler = new Hobiler();
        partialUpdatedHobiler.setId(hobiler.getId());

        partialUpdatedHobiler.hobiAdi(UPDATED_HOBI_ADI).aciklama(UPDATED_ACIKLAMA).lokasyon(UPDATED_LOKASYON).cesidi(UPDATED_CESIDI);

        restHobilerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHobiler.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHobiler))
            )
            .andExpect(status().isOk());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
        Hobiler testHobiler = hobilerList.get(hobilerList.size() - 1);
        assertThat(testHobiler.getHobiAdi()).isEqualTo(UPDATED_HOBI_ADI);
        assertThat(testHobiler.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testHobiler.getLokasyon()).isEqualTo(UPDATED_LOKASYON);
        assertThat(testHobiler.getCesidi()).isEqualTo(UPDATED_CESIDI);
    }

    @Test
    @Transactional
    void patchNonExistingHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hobilerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHobiler() throws Exception {
        int databaseSizeBeforeUpdate = hobilerRepository.findAll().size();
        hobiler.setId(count.incrementAndGet());

        // Create the Hobiler
        HobilerDTO hobilerDTO = hobilerMapper.toDto(hobiler);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHobilerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(hobilerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hobiler in the database
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHobiler() throws Exception {
        // Initialize the database
        hobilerRepository.saveAndFlush(hobiler);

        int databaseSizeBeforeDelete = hobilerRepository.findAll().size();

        // Delete the hobiler
        restHobilerMockMvc
            .perform(delete(ENTITY_API_URL_ID, hobiler.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Hobiler> hobilerList = hobilerRepository.findAll();
        assertThat(hobilerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
