package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Etkinlik;
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.repository.EtkinlikRepository;
import com.hobiharitasi.service.EtkinlikService;
import com.hobiharitasi.service.criteria.EtkinlikCriteria;
import com.hobiharitasi.service.dto.EtkinlikDTO;
import com.hobiharitasi.service.mapper.EtkinlikMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EtkinlikResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class EtkinlikResourceIT {

    private static final String DEFAULT_ETKINLIK_ADI = "AAAAAAAAAA";
    private static final String UPDATED_ETKINLIK_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final Instant DEFAULT_TARIH = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TARIH = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_KONUM = "AAAAAAAAAA";
    private static final String UPDATED_KONUM = "BBBBBBBBBB";

    private static final Integer DEFAULT_MAKSIMUM_KATILIMCI = 1;
    private static final Integer UPDATED_MAKSIMUM_KATILIMCI = 2;
    private static final Integer SMALLER_MAKSIMUM_KATILIMCI = 1 - 1;

    private static final Integer DEFAULT_MINIMUM_KATILIMCI = 1;
    private static final Integer UPDATED_MINIMUM_KATILIMCI = 2;
    private static final Integer SMALLER_MINIMUM_KATILIMCI = 1 - 1;

    private static final String DEFAULT_ETKINLIK_KATEGORISI = "AAAAAAAAAA";
    private static final String UPDATED_ETKINLIK_KATEGORISI = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/etkinliks";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EtkinlikRepository etkinlikRepository;

    @Mock
    private EtkinlikRepository etkinlikRepositoryMock;

    @Autowired
    private EtkinlikMapper etkinlikMapper;

    @Mock
    private EtkinlikService etkinlikServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtkinlikMockMvc;

    private Etkinlik etkinlik;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etkinlik createEntity(EntityManager em) {
        Etkinlik etkinlik = new Etkinlik()
            .etkinlikAdi(DEFAULT_ETKINLIK_ADI)
            .aciklama(DEFAULT_ACIKLAMA)
            .tarih(DEFAULT_TARIH)
            .konum(DEFAULT_KONUM)
            .maksimumKatilimci(DEFAULT_MAKSIMUM_KATILIMCI)
            .minimumKatilimci(DEFAULT_MINIMUM_KATILIMCI)
            .etkinlikKategorisi(DEFAULT_ETKINLIK_KATEGORISI);
        return etkinlik;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etkinlik createUpdatedEntity(EntityManager em) {
        Etkinlik etkinlik = new Etkinlik()
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .maksimumKatilimci(UPDATED_MAKSIMUM_KATILIMCI)
            .minimumKatilimci(UPDATED_MINIMUM_KATILIMCI)
            .etkinlikKategorisi(UPDATED_ETKINLIK_KATEGORISI);
        return etkinlik;
    }

    @BeforeEach
    public void initTest() {
        etkinlik = createEntity(em);
    }

    @Test
    @Transactional
    void createEtkinlik() throws Exception {
        int databaseSizeBeforeCreate = etkinlikRepository.findAll().size();
        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);
        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isCreated());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeCreate + 1);
        Etkinlik testEtkinlik = etkinlikList.get(etkinlikList.size() - 1);
        assertThat(testEtkinlik.getEtkinlikAdi()).isEqualTo(DEFAULT_ETKINLIK_ADI);
        assertThat(testEtkinlik.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testEtkinlik.getTarih()).isEqualTo(DEFAULT_TARIH);
        assertThat(testEtkinlik.getKonum()).isEqualTo(DEFAULT_KONUM);
        assertThat(testEtkinlik.getMaksimumKatilimci()).isEqualTo(DEFAULT_MAKSIMUM_KATILIMCI);
        assertThat(testEtkinlik.getMinimumKatilimci()).isEqualTo(DEFAULT_MINIMUM_KATILIMCI);
        assertThat(testEtkinlik.getEtkinlikKategorisi()).isEqualTo(DEFAULT_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void createEtkinlikWithExistingId() throws Exception {
        // Create the Etkinlik with an existing ID
        etkinlik.setId(1L);
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        int databaseSizeBeforeCreate = etkinlikRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkEtkinlikAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setEtkinlikAdi(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAciklamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setAciklama(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTarihIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setTarih(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkKonumIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setKonum(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMaksimumKatilimciIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setMaksimumKatilimci(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEtkinlikKategorisiIsRequired() throws Exception {
        int databaseSizeBeforeTest = etkinlikRepository.findAll().size();
        // set the field null
        etkinlik.setEtkinlikKategorisi(null);

        // Create the Etkinlik, which fails.
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        restEtkinlikMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isBadRequest());

        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEtkinliks() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etkinlik.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikAdi").value(hasItem(DEFAULT_ETKINLIK_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].tarih").value(hasItem(DEFAULT_TARIH.toString())))
            .andExpect(jsonPath("$.[*].konum").value(hasItem(DEFAULT_KONUM)))
            .andExpect(jsonPath("$.[*].maksimumKatilimci").value(hasItem(DEFAULT_MAKSIMUM_KATILIMCI)))
            .andExpect(jsonPath("$.[*].minimumKatilimci").value(hasItem(DEFAULT_MINIMUM_KATILIMCI)))
            .andExpect(jsonPath("$.[*].etkinlikKategorisi").value(hasItem(DEFAULT_ETKINLIK_KATEGORISI)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllEtkinliksWithEagerRelationshipsIsEnabled() throws Exception {
        when(etkinlikServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restEtkinlikMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(etkinlikServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllEtkinliksWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(etkinlikServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restEtkinlikMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(etkinlikRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getEtkinlik() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get the etkinlik
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL_ID, etkinlik.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etkinlik.getId().intValue()))
            .andExpect(jsonPath("$.etkinlikAdi").value(DEFAULT_ETKINLIK_ADI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA))
            .andExpect(jsonPath("$.tarih").value(DEFAULT_TARIH.toString()))
            .andExpect(jsonPath("$.konum").value(DEFAULT_KONUM))
            .andExpect(jsonPath("$.maksimumKatilimci").value(DEFAULT_MAKSIMUM_KATILIMCI))
            .andExpect(jsonPath("$.minimumKatilimci").value(DEFAULT_MINIMUM_KATILIMCI))
            .andExpect(jsonPath("$.etkinlikKategorisi").value(DEFAULT_ETKINLIK_KATEGORISI));
    }

    @Test
    @Transactional
    void getEtkinliksByIdFiltering() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        Long id = etkinlik.getId();

        defaultEtkinlikShouldBeFound("id.equals=" + id);
        defaultEtkinlikShouldNotBeFound("id.notEquals=" + id);

        defaultEtkinlikShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEtkinlikShouldNotBeFound("id.greaterThan=" + id);

        defaultEtkinlikShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEtkinlikShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikAdiIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikAdi equals to DEFAULT_ETKINLIK_ADI
        defaultEtkinlikShouldBeFound("etkinlikAdi.equals=" + DEFAULT_ETKINLIK_ADI);

        // Get all the etkinlikList where etkinlikAdi equals to UPDATED_ETKINLIK_ADI
        defaultEtkinlikShouldNotBeFound("etkinlikAdi.equals=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikAdiIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikAdi in DEFAULT_ETKINLIK_ADI or UPDATED_ETKINLIK_ADI
        defaultEtkinlikShouldBeFound("etkinlikAdi.in=" + DEFAULT_ETKINLIK_ADI + "," + UPDATED_ETKINLIK_ADI);

        // Get all the etkinlikList where etkinlikAdi equals to UPDATED_ETKINLIK_ADI
        defaultEtkinlikShouldNotBeFound("etkinlikAdi.in=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikAdiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikAdi is not null
        defaultEtkinlikShouldBeFound("etkinlikAdi.specified=true");

        // Get all the etkinlikList where etkinlikAdi is null
        defaultEtkinlikShouldNotBeFound("etkinlikAdi.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikAdiContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikAdi contains DEFAULT_ETKINLIK_ADI
        defaultEtkinlikShouldBeFound("etkinlikAdi.contains=" + DEFAULT_ETKINLIK_ADI);

        // Get all the etkinlikList where etkinlikAdi contains UPDATED_ETKINLIK_ADI
        defaultEtkinlikShouldNotBeFound("etkinlikAdi.contains=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikAdiNotContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikAdi does not contain DEFAULT_ETKINLIK_ADI
        defaultEtkinlikShouldNotBeFound("etkinlikAdi.doesNotContain=" + DEFAULT_ETKINLIK_ADI);

        // Get all the etkinlikList where etkinlikAdi does not contain UPDATED_ETKINLIK_ADI
        defaultEtkinlikShouldBeFound("etkinlikAdi.doesNotContain=" + UPDATED_ETKINLIK_ADI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where aciklama equals to DEFAULT_ACIKLAMA
        defaultEtkinlikShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the etkinlikList where aciklama equals to UPDATED_ACIKLAMA
        defaultEtkinlikShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllEtkinliksByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultEtkinlikShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the etkinlikList where aciklama equals to UPDATED_ACIKLAMA
        defaultEtkinlikShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllEtkinliksByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where aciklama is not null
        defaultEtkinlikShouldBeFound("aciklama.specified=true");

        // Get all the etkinlikList where aciklama is null
        defaultEtkinlikShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where aciklama contains DEFAULT_ACIKLAMA
        defaultEtkinlikShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the etkinlikList where aciklama contains UPDATED_ACIKLAMA
        defaultEtkinlikShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllEtkinliksByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultEtkinlikShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the etkinlikList where aciklama does not contain UPDATED_ACIKLAMA
        defaultEtkinlikShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllEtkinliksByTarihIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where tarih equals to DEFAULT_TARIH
        defaultEtkinlikShouldBeFound("tarih.equals=" + DEFAULT_TARIH);

        // Get all the etkinlikList where tarih equals to UPDATED_TARIH
        defaultEtkinlikShouldNotBeFound("tarih.equals=" + UPDATED_TARIH);
    }

    @Test
    @Transactional
    void getAllEtkinliksByTarihIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where tarih in DEFAULT_TARIH or UPDATED_TARIH
        defaultEtkinlikShouldBeFound("tarih.in=" + DEFAULT_TARIH + "," + UPDATED_TARIH);

        // Get all the etkinlikList where tarih equals to UPDATED_TARIH
        defaultEtkinlikShouldNotBeFound("tarih.in=" + UPDATED_TARIH);
    }

    @Test
    @Transactional
    void getAllEtkinliksByTarihIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where tarih is not null
        defaultEtkinlikShouldBeFound("tarih.specified=true");

        // Get all the etkinlikList where tarih is null
        defaultEtkinlikShouldNotBeFound("tarih.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByKonumIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where konum equals to DEFAULT_KONUM
        defaultEtkinlikShouldBeFound("konum.equals=" + DEFAULT_KONUM);

        // Get all the etkinlikList where konum equals to UPDATED_KONUM
        defaultEtkinlikShouldNotBeFound("konum.equals=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllEtkinliksByKonumIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where konum in DEFAULT_KONUM or UPDATED_KONUM
        defaultEtkinlikShouldBeFound("konum.in=" + DEFAULT_KONUM + "," + UPDATED_KONUM);

        // Get all the etkinlikList where konum equals to UPDATED_KONUM
        defaultEtkinlikShouldNotBeFound("konum.in=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllEtkinliksByKonumIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where konum is not null
        defaultEtkinlikShouldBeFound("konum.specified=true");

        // Get all the etkinlikList where konum is null
        defaultEtkinlikShouldNotBeFound("konum.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByKonumContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where konum contains DEFAULT_KONUM
        defaultEtkinlikShouldBeFound("konum.contains=" + DEFAULT_KONUM);

        // Get all the etkinlikList where konum contains UPDATED_KONUM
        defaultEtkinlikShouldNotBeFound("konum.contains=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllEtkinliksByKonumNotContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where konum does not contain DEFAULT_KONUM
        defaultEtkinlikShouldNotBeFound("konum.doesNotContain=" + DEFAULT_KONUM);

        // Get all the etkinlikList where konum does not contain UPDATED_KONUM
        defaultEtkinlikShouldBeFound("konum.doesNotContain=" + UPDATED_KONUM);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci equals to DEFAULT_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.equals=" + DEFAULT_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci equals to UPDATED_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.equals=" + UPDATED_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci in DEFAULT_MAKSIMUM_KATILIMCI or UPDATED_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.in=" + DEFAULT_MAKSIMUM_KATILIMCI + "," + UPDATED_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci equals to UPDATED_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.in=" + UPDATED_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci is not null
        defaultEtkinlikShouldBeFound("maksimumKatilimci.specified=true");

        // Get all the etkinlikList where maksimumKatilimci is null
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci is greater than or equal to DEFAULT_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.greaterThanOrEqual=" + DEFAULT_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci is greater than or equal to UPDATED_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.greaterThanOrEqual=" + UPDATED_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci is less than or equal to DEFAULT_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.lessThanOrEqual=" + DEFAULT_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci is less than or equal to SMALLER_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.lessThanOrEqual=" + SMALLER_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsLessThanSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci is less than DEFAULT_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.lessThan=" + DEFAULT_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci is less than UPDATED_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.lessThan=" + UPDATED_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMaksimumKatilimciIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where maksimumKatilimci is greater than DEFAULT_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("maksimumKatilimci.greaterThan=" + DEFAULT_MAKSIMUM_KATILIMCI);

        // Get all the etkinlikList where maksimumKatilimci is greater than SMALLER_MAKSIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("maksimumKatilimci.greaterThan=" + SMALLER_MAKSIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci equals to DEFAULT_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.equals=" + DEFAULT_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci equals to UPDATED_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.equals=" + UPDATED_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci in DEFAULT_MINIMUM_KATILIMCI or UPDATED_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.in=" + DEFAULT_MINIMUM_KATILIMCI + "," + UPDATED_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci equals to UPDATED_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.in=" + UPDATED_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci is not null
        defaultEtkinlikShouldBeFound("minimumKatilimci.specified=true");

        // Get all the etkinlikList where minimumKatilimci is null
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci is greater than or equal to DEFAULT_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.greaterThanOrEqual=" + DEFAULT_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci is greater than or equal to UPDATED_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.greaterThanOrEqual=" + UPDATED_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci is less than or equal to DEFAULT_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.lessThanOrEqual=" + DEFAULT_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci is less than or equal to SMALLER_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.lessThanOrEqual=" + SMALLER_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsLessThanSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci is less than DEFAULT_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.lessThan=" + DEFAULT_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci is less than UPDATED_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.lessThan=" + UPDATED_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByMinimumKatilimciIsGreaterThanSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where minimumKatilimci is greater than DEFAULT_MINIMUM_KATILIMCI
        defaultEtkinlikShouldNotBeFound("minimumKatilimci.greaterThan=" + DEFAULT_MINIMUM_KATILIMCI);

        // Get all the etkinlikList where minimumKatilimci is greater than SMALLER_MINIMUM_KATILIMCI
        defaultEtkinlikShouldBeFound("minimumKatilimci.greaterThan=" + SMALLER_MINIMUM_KATILIMCI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikKategorisiIsEqualToSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikKategorisi equals to DEFAULT_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldBeFound("etkinlikKategorisi.equals=" + DEFAULT_ETKINLIK_KATEGORISI);

        // Get all the etkinlikList where etkinlikKategorisi equals to UPDATED_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldNotBeFound("etkinlikKategorisi.equals=" + UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikKategorisiIsInShouldWork() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikKategorisi in DEFAULT_ETKINLIK_KATEGORISI or UPDATED_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldBeFound("etkinlikKategorisi.in=" + DEFAULT_ETKINLIK_KATEGORISI + "," + UPDATED_ETKINLIK_KATEGORISI);

        // Get all the etkinlikList where etkinlikKategorisi equals to UPDATED_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldNotBeFound("etkinlikKategorisi.in=" + UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikKategorisiIsNullOrNotNull() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikKategorisi is not null
        defaultEtkinlikShouldBeFound("etkinlikKategorisi.specified=true");

        // Get all the etkinlikList where etkinlikKategorisi is null
        defaultEtkinlikShouldNotBeFound("etkinlikKategorisi.specified=false");
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikKategorisiContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikKategorisi contains DEFAULT_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldBeFound("etkinlikKategorisi.contains=" + DEFAULT_ETKINLIK_KATEGORISI);

        // Get all the etkinlikList where etkinlikKategorisi contains UPDATED_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldNotBeFound("etkinlikKategorisi.contains=" + UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByEtkinlikKategorisiNotContainsSomething() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        // Get all the etkinlikList where etkinlikKategorisi does not contain DEFAULT_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldNotBeFound("etkinlikKategorisi.doesNotContain=" + DEFAULT_ETKINLIK_KATEGORISI);

        // Get all the etkinlikList where etkinlikKategorisi does not contain UPDATED_ETKINLIK_KATEGORISI
        defaultEtkinlikShouldBeFound("etkinlikKategorisi.doesNotContain=" + UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllEtkinliksByKategoriIsEqualToSomething() throws Exception {
        Kategori kategori;
        if (TestUtil.findAll(em, Kategori.class).isEmpty()) {
            etkinlikRepository.saveAndFlush(etkinlik);
            kategori = KategoriResourceIT.createEntity(em);
        } else {
            kategori = TestUtil.findAll(em, Kategori.class).get(0);
        }
        em.persist(kategori);
        em.flush();
        etkinlik.addKategori(kategori);
        etkinlikRepository.saveAndFlush(etkinlik);
        Long kategoriId = kategori.getId();

        // Get all the etkinlikList where kategori equals to kategoriId
        defaultEtkinlikShouldBeFound("kategoriId.equals=" + kategoriId);

        // Get all the etkinlikList where kategori equals to (kategoriId + 1)
        defaultEtkinlikShouldNotBeFound("kategoriId.equals=" + (kategoriId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEtkinlikShouldBeFound(String filter) throws Exception {
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etkinlik.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikAdi").value(hasItem(DEFAULT_ETKINLIK_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].tarih").value(hasItem(DEFAULT_TARIH.toString())))
            .andExpect(jsonPath("$.[*].konum").value(hasItem(DEFAULT_KONUM)))
            .andExpect(jsonPath("$.[*].maksimumKatilimci").value(hasItem(DEFAULT_MAKSIMUM_KATILIMCI)))
            .andExpect(jsonPath("$.[*].minimumKatilimci").value(hasItem(DEFAULT_MINIMUM_KATILIMCI)))
            .andExpect(jsonPath("$.[*].etkinlikKategorisi").value(hasItem(DEFAULT_ETKINLIK_KATEGORISI)));

        // Check, that the count call also returns 1
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEtkinlikShouldNotBeFound(String filter) throws Exception {
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEtkinlikMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingEtkinlik() throws Exception {
        // Get the etkinlik
        restEtkinlikMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingEtkinlik() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();

        // Update the etkinlik
        Etkinlik updatedEtkinlik = etkinlikRepository.findById(etkinlik.getId()).get();
        // Disconnect from session so that the updates on updatedEtkinlik are not directly saved in db
        em.detach(updatedEtkinlik);
        updatedEtkinlik
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .maksimumKatilimci(UPDATED_MAKSIMUM_KATILIMCI)
            .minimumKatilimci(UPDATED_MINIMUM_KATILIMCI)
            .etkinlikKategorisi(UPDATED_ETKINLIK_KATEGORISI);
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(updatedEtkinlik);

        restEtkinlikMockMvc
            .perform(
                put(ENTITY_API_URL_ID, etkinlikDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isOk());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
        Etkinlik testEtkinlik = etkinlikList.get(etkinlikList.size() - 1);
        assertThat(testEtkinlik.getEtkinlikAdi()).isEqualTo(UPDATED_ETKINLIK_ADI);
        assertThat(testEtkinlik.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testEtkinlik.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testEtkinlik.getKonum()).isEqualTo(UPDATED_KONUM);
        assertThat(testEtkinlik.getMaksimumKatilimci()).isEqualTo(UPDATED_MAKSIMUM_KATILIMCI);
        assertThat(testEtkinlik.getMinimumKatilimci()).isEqualTo(UPDATED_MINIMUM_KATILIMCI);
        assertThat(testEtkinlik.getEtkinlikKategorisi()).isEqualTo(UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void putNonExistingEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(
                put(ENTITY_API_URL_ID, etkinlikDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(etkinlikDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEtkinlikWithPatch() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();

        // Update the etkinlik using partial update
        Etkinlik partialUpdatedEtkinlik = new Etkinlik();
        partialUpdatedEtkinlik.setId(etkinlik.getId());

        partialUpdatedEtkinlik
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .maksimumKatilimci(UPDATED_MAKSIMUM_KATILIMCI);

        restEtkinlikMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEtkinlik.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEtkinlik))
            )
            .andExpect(status().isOk());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
        Etkinlik testEtkinlik = etkinlikList.get(etkinlikList.size() - 1);
        assertThat(testEtkinlik.getEtkinlikAdi()).isEqualTo(UPDATED_ETKINLIK_ADI);
        assertThat(testEtkinlik.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testEtkinlik.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testEtkinlik.getKonum()).isEqualTo(UPDATED_KONUM);
        assertThat(testEtkinlik.getMaksimumKatilimci()).isEqualTo(UPDATED_MAKSIMUM_KATILIMCI);
        assertThat(testEtkinlik.getMinimumKatilimci()).isEqualTo(DEFAULT_MINIMUM_KATILIMCI);
        assertThat(testEtkinlik.getEtkinlikKategorisi()).isEqualTo(DEFAULT_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void fullUpdateEtkinlikWithPatch() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();

        // Update the etkinlik using partial update
        Etkinlik partialUpdatedEtkinlik = new Etkinlik();
        partialUpdatedEtkinlik.setId(etkinlik.getId());

        partialUpdatedEtkinlik
            .etkinlikAdi(UPDATED_ETKINLIK_ADI)
            .aciklama(UPDATED_ACIKLAMA)
            .tarih(UPDATED_TARIH)
            .konum(UPDATED_KONUM)
            .maksimumKatilimci(UPDATED_MAKSIMUM_KATILIMCI)
            .minimumKatilimci(UPDATED_MINIMUM_KATILIMCI)
            .etkinlikKategorisi(UPDATED_ETKINLIK_KATEGORISI);

        restEtkinlikMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEtkinlik.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEtkinlik))
            )
            .andExpect(status().isOk());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
        Etkinlik testEtkinlik = etkinlikList.get(etkinlikList.size() - 1);
        assertThat(testEtkinlik.getEtkinlikAdi()).isEqualTo(UPDATED_ETKINLIK_ADI);
        assertThat(testEtkinlik.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testEtkinlik.getTarih()).isEqualTo(UPDATED_TARIH);
        assertThat(testEtkinlik.getKonum()).isEqualTo(UPDATED_KONUM);
        assertThat(testEtkinlik.getMaksimumKatilimci()).isEqualTo(UPDATED_MAKSIMUM_KATILIMCI);
        assertThat(testEtkinlik.getMinimumKatilimci()).isEqualTo(UPDATED_MINIMUM_KATILIMCI);
        assertThat(testEtkinlik.getEtkinlikKategorisi()).isEqualTo(UPDATED_ETKINLIK_KATEGORISI);
    }

    @Test
    @Transactional
    void patchNonExistingEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, etkinlikDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEtkinlik() throws Exception {
        int databaseSizeBeforeUpdate = etkinlikRepository.findAll().size();
        etkinlik.setId(count.incrementAndGet());

        // Create the Etkinlik
        EtkinlikDTO etkinlikDTO = etkinlikMapper.toDto(etkinlik);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEtkinlikMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(etkinlikDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Etkinlik in the database
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEtkinlik() throws Exception {
        // Initialize the database
        etkinlikRepository.saveAndFlush(etkinlik);

        int databaseSizeBeforeDelete = etkinlikRepository.findAll().size();

        // Delete the etkinlik
        restEtkinlikMockMvc
            .perform(delete(ENTITY_API_URL_ID, etkinlik.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Etkinlik> etkinlikList = etkinlikRepository.findAll();
        assertThat(etkinlikList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
