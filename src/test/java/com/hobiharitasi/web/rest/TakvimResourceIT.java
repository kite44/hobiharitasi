package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.Takvim;
import com.hobiharitasi.repository.TakvimRepository;
import com.hobiharitasi.service.TakvimService;
import com.hobiharitasi.service.criteria.TakvimCriteria;
import com.hobiharitasi.service.dto.TakvimDTO;
import com.hobiharitasi.service.mapper.TakvimMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TakvimResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class TakvimResourceIT {

    private static final Instant DEFAULT_ETKINLIK_TARIHI = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ETKINLIK_TARIHI = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ETKINLIK_BASLIGI = "AAAAAAAAAA";
    private static final String UPDATED_ETKINLIK_BASLIGI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/takvims";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TakvimRepository takvimRepository;

    @Mock
    private TakvimRepository takvimRepositoryMock;

    @Autowired
    private TakvimMapper takvimMapper;

    @Mock
    private TakvimService takvimServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTakvimMockMvc;

    private Takvim takvim;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Takvim createEntity(EntityManager em) {
        Takvim takvim = new Takvim()
            .etkinlikTarihi(DEFAULT_ETKINLIK_TARIHI)
            .etkinlikBasligi(DEFAULT_ETKINLIK_BASLIGI)
            .aciklama(DEFAULT_ACIKLAMA);
        return takvim;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Takvim createUpdatedEntity(EntityManager em) {
        Takvim takvim = new Takvim()
            .etkinlikTarihi(UPDATED_ETKINLIK_TARIHI)
            .etkinlikBasligi(UPDATED_ETKINLIK_BASLIGI)
            .aciklama(UPDATED_ACIKLAMA);
        return takvim;
    }

    @BeforeEach
    public void initTest() {
        takvim = createEntity(em);
    }

    @Test
    @Transactional
    void createTakvim() throws Exception {
        int databaseSizeBeforeCreate = takvimRepository.findAll().size();
        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);
        restTakvimMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(takvimDTO)))
            .andExpect(status().isCreated());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeCreate + 1);
        Takvim testTakvim = takvimList.get(takvimList.size() - 1);
        assertThat(testTakvim.getEtkinlikTarihi()).isEqualTo(DEFAULT_ETKINLIK_TARIHI);
        assertThat(testTakvim.getEtkinlikBasligi()).isEqualTo(DEFAULT_ETKINLIK_BASLIGI);
        assertThat(testTakvim.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
    }

    @Test
    @Transactional
    void createTakvimWithExistingId() throws Exception {
        // Create the Takvim with an existing ID
        takvim.setId(1L);
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        int databaseSizeBeforeCreate = takvimRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTakvimMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(takvimDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkEtkinlikTarihiIsRequired() throws Exception {
        int databaseSizeBeforeTest = takvimRepository.findAll().size();
        // set the field null
        takvim.setEtkinlikTarihi(null);

        // Create the Takvim, which fails.
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        restTakvimMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(takvimDTO)))
            .andExpect(status().isBadRequest());

        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEtkinlikBasligiIsRequired() throws Exception {
        int databaseSizeBeforeTest = takvimRepository.findAll().size();
        // set the field null
        takvim.setEtkinlikBasligi(null);

        // Create the Takvim, which fails.
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        restTakvimMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(takvimDTO)))
            .andExpect(status().isBadRequest());

        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTakvims() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(takvim.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikTarihi").value(hasItem(DEFAULT_ETKINLIK_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].etkinlikBasligi").value(hasItem(DEFAULT_ETKINLIK_BASLIGI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTakvimsWithEagerRelationshipsIsEnabled() throws Exception {
        when(takvimServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTakvimMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(takvimServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTakvimsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(takvimServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restTakvimMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(takvimRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getTakvim() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get the takvim
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL_ID, takvim.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(takvim.getId().intValue()))
            .andExpect(jsonPath("$.etkinlikTarihi").value(DEFAULT_ETKINLIK_TARIHI.toString()))
            .andExpect(jsonPath("$.etkinlikBasligi").value(DEFAULT_ETKINLIK_BASLIGI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA));
    }

    @Test
    @Transactional
    void getTakvimsByIdFiltering() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        Long id = takvim.getId();

        defaultTakvimShouldBeFound("id.equals=" + id);
        defaultTakvimShouldNotBeFound("id.notEquals=" + id);

        defaultTakvimShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTakvimShouldNotBeFound("id.greaterThan=" + id);

        defaultTakvimShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTakvimShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikTarihiIsEqualToSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikTarihi equals to DEFAULT_ETKINLIK_TARIHI
        defaultTakvimShouldBeFound("etkinlikTarihi.equals=" + DEFAULT_ETKINLIK_TARIHI);

        // Get all the takvimList where etkinlikTarihi equals to UPDATED_ETKINLIK_TARIHI
        defaultTakvimShouldNotBeFound("etkinlikTarihi.equals=" + UPDATED_ETKINLIK_TARIHI);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikTarihiIsInShouldWork() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikTarihi in DEFAULT_ETKINLIK_TARIHI or UPDATED_ETKINLIK_TARIHI
        defaultTakvimShouldBeFound("etkinlikTarihi.in=" + DEFAULT_ETKINLIK_TARIHI + "," + UPDATED_ETKINLIK_TARIHI);

        // Get all the takvimList where etkinlikTarihi equals to UPDATED_ETKINLIK_TARIHI
        defaultTakvimShouldNotBeFound("etkinlikTarihi.in=" + UPDATED_ETKINLIK_TARIHI);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikTarihiIsNullOrNotNull() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikTarihi is not null
        defaultTakvimShouldBeFound("etkinlikTarihi.specified=true");

        // Get all the takvimList where etkinlikTarihi is null
        defaultTakvimShouldNotBeFound("etkinlikTarihi.specified=false");
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikBasligiIsEqualToSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikBasligi equals to DEFAULT_ETKINLIK_BASLIGI
        defaultTakvimShouldBeFound("etkinlikBasligi.equals=" + DEFAULT_ETKINLIK_BASLIGI);

        // Get all the takvimList where etkinlikBasligi equals to UPDATED_ETKINLIK_BASLIGI
        defaultTakvimShouldNotBeFound("etkinlikBasligi.equals=" + UPDATED_ETKINLIK_BASLIGI);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikBasligiIsInShouldWork() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikBasligi in DEFAULT_ETKINLIK_BASLIGI or UPDATED_ETKINLIK_BASLIGI
        defaultTakvimShouldBeFound("etkinlikBasligi.in=" + DEFAULT_ETKINLIK_BASLIGI + "," + UPDATED_ETKINLIK_BASLIGI);

        // Get all the takvimList where etkinlikBasligi equals to UPDATED_ETKINLIK_BASLIGI
        defaultTakvimShouldNotBeFound("etkinlikBasligi.in=" + UPDATED_ETKINLIK_BASLIGI);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikBasligiIsNullOrNotNull() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikBasligi is not null
        defaultTakvimShouldBeFound("etkinlikBasligi.specified=true");

        // Get all the takvimList where etkinlikBasligi is null
        defaultTakvimShouldNotBeFound("etkinlikBasligi.specified=false");
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikBasligiContainsSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikBasligi contains DEFAULT_ETKINLIK_BASLIGI
        defaultTakvimShouldBeFound("etkinlikBasligi.contains=" + DEFAULT_ETKINLIK_BASLIGI);

        // Get all the takvimList where etkinlikBasligi contains UPDATED_ETKINLIK_BASLIGI
        defaultTakvimShouldNotBeFound("etkinlikBasligi.contains=" + UPDATED_ETKINLIK_BASLIGI);
    }

    @Test
    @Transactional
    void getAllTakvimsByEtkinlikBasligiNotContainsSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where etkinlikBasligi does not contain DEFAULT_ETKINLIK_BASLIGI
        defaultTakvimShouldNotBeFound("etkinlikBasligi.doesNotContain=" + DEFAULT_ETKINLIK_BASLIGI);

        // Get all the takvimList where etkinlikBasligi does not contain UPDATED_ETKINLIK_BASLIGI
        defaultTakvimShouldBeFound("etkinlikBasligi.doesNotContain=" + UPDATED_ETKINLIK_BASLIGI);
    }

    @Test
    @Transactional
    void getAllTakvimsByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where aciklama equals to DEFAULT_ACIKLAMA
        defaultTakvimShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the takvimList where aciklama equals to UPDATED_ACIKLAMA
        defaultTakvimShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllTakvimsByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultTakvimShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the takvimList where aciklama equals to UPDATED_ACIKLAMA
        defaultTakvimShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllTakvimsByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where aciklama is not null
        defaultTakvimShouldBeFound("aciklama.specified=true");

        // Get all the takvimList where aciklama is null
        defaultTakvimShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllTakvimsByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where aciklama contains DEFAULT_ACIKLAMA
        defaultTakvimShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the takvimList where aciklama contains UPDATED_ACIKLAMA
        defaultTakvimShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllTakvimsByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        // Get all the takvimList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultTakvimShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the takvimList where aciklama does not contain UPDATED_ACIKLAMA
        defaultTakvimShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllTakvimsByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            takvimRepository.saveAndFlush(takvim);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        takvim.setKullanici(kullanici);
        takvimRepository.saveAndFlush(takvim);
        Long kullaniciId = kullanici.getId();

        // Get all the takvimList where kullanici equals to kullaniciId
        defaultTakvimShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the takvimList where kullanici equals to (kullaniciId + 1)
        defaultTakvimShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    @Test
    @Transactional
    void getAllTakvimsByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            takvimRepository.saveAndFlush(takvim);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        takvim.setKullanici(kullanici);
        takvimRepository.saveAndFlush(takvim);
        Long kullaniciId = kullanici.getId();

        // Get all the takvimList where kullanici equals to kullaniciId
        defaultTakvimShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the takvimList where kullanici equals to (kullaniciId + 1)
        defaultTakvimShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTakvimShouldBeFound(String filter) throws Exception {
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(takvim.getId().intValue())))
            .andExpect(jsonPath("$.[*].etkinlikTarihi").value(hasItem(DEFAULT_ETKINLIK_TARIHI.toString())))
            .andExpect(jsonPath("$.[*].etkinlikBasligi").value(hasItem(DEFAULT_ETKINLIK_BASLIGI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)));

        // Check, that the count call also returns 1
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTakvimShouldNotBeFound(String filter) throws Exception {
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTakvimMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTakvim() throws Exception {
        // Get the takvim
        restTakvimMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTakvim() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();

        // Update the takvim
        Takvim updatedTakvim = takvimRepository.findById(takvim.getId()).get();
        // Disconnect from session so that the updates on updatedTakvim are not directly saved in db
        em.detach(updatedTakvim);
        updatedTakvim.etkinlikTarihi(UPDATED_ETKINLIK_TARIHI).etkinlikBasligi(UPDATED_ETKINLIK_BASLIGI).aciklama(UPDATED_ACIKLAMA);
        TakvimDTO takvimDTO = takvimMapper.toDto(updatedTakvim);

        restTakvimMockMvc
            .perform(
                put(ENTITY_API_URL_ID, takvimDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isOk());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
        Takvim testTakvim = takvimList.get(takvimList.size() - 1);
        assertThat(testTakvim.getEtkinlikTarihi()).isEqualTo(UPDATED_ETKINLIK_TARIHI);
        assertThat(testTakvim.getEtkinlikBasligi()).isEqualTo(UPDATED_ETKINLIK_BASLIGI);
        assertThat(testTakvim.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void putNonExistingTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(
                put(ENTITY_API_URL_ID, takvimDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(takvimDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTakvimWithPatch() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();

        // Update the takvim using partial update
        Takvim partialUpdatedTakvim = new Takvim();
        partialUpdatedTakvim.setId(takvim.getId());

        partialUpdatedTakvim.etkinlikBasligi(UPDATED_ETKINLIK_BASLIGI).aciklama(UPDATED_ACIKLAMA);

        restTakvimMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTakvim.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTakvim))
            )
            .andExpect(status().isOk());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
        Takvim testTakvim = takvimList.get(takvimList.size() - 1);
        assertThat(testTakvim.getEtkinlikTarihi()).isEqualTo(DEFAULT_ETKINLIK_TARIHI);
        assertThat(testTakvim.getEtkinlikBasligi()).isEqualTo(UPDATED_ETKINLIK_BASLIGI);
        assertThat(testTakvim.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void fullUpdateTakvimWithPatch() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();

        // Update the takvim using partial update
        Takvim partialUpdatedTakvim = new Takvim();
        partialUpdatedTakvim.setId(takvim.getId());

        partialUpdatedTakvim.etkinlikTarihi(UPDATED_ETKINLIK_TARIHI).etkinlikBasligi(UPDATED_ETKINLIK_BASLIGI).aciklama(UPDATED_ACIKLAMA);

        restTakvimMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTakvim.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTakvim))
            )
            .andExpect(status().isOk());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
        Takvim testTakvim = takvimList.get(takvimList.size() - 1);
        assertThat(testTakvim.getEtkinlikTarihi()).isEqualTo(UPDATED_ETKINLIK_TARIHI);
        assertThat(testTakvim.getEtkinlikBasligi()).isEqualTo(UPDATED_ETKINLIK_BASLIGI);
        assertThat(testTakvim.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void patchNonExistingTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, takvimDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTakvim() throws Exception {
        int databaseSizeBeforeUpdate = takvimRepository.findAll().size();
        takvim.setId(count.incrementAndGet());

        // Create the Takvim
        TakvimDTO takvimDTO = takvimMapper.toDto(takvim);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTakvimMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(takvimDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Takvim in the database
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTakvim() throws Exception {
        // Initialize the database
        takvimRepository.saveAndFlush(takvim);

        int databaseSizeBeforeDelete = takvimRepository.findAll().size();

        // Delete the takvim
        restTakvimMockMvc
            .perform(delete(ENTITY_API_URL_ID, takvim.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Takvim> takvimList = takvimRepository.findAll();
        assertThat(takvimList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
