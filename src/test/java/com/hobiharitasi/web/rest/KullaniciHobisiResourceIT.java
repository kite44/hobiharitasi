package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Hobiler;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.domain.KullaniciHobisi;
import com.hobiharitasi.repository.KullaniciHobisiRepository;
import com.hobiharitasi.service.KullaniciHobisiService;
import com.hobiharitasi.service.criteria.KullaniciHobisiCriteria;
import com.hobiharitasi.service.dto.KullaniciHobisiDTO;
import com.hobiharitasi.service.mapper.KullaniciHobisiMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link KullaniciHobisiResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class KullaniciHobisiResourceIT {

    private static final Integer DEFAULT_SEVIYE = 1;
    private static final Integer UPDATED_SEVIYE = 2;
    private static final Integer SMALLER_SEVIYE = 1 - 1;

    private static final String ENTITY_API_URL = "/api/kullanici-hobisis";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private KullaniciHobisiRepository kullaniciHobisiRepository;

    @Mock
    private KullaniciHobisiRepository kullaniciHobisiRepositoryMock;

    @Autowired
    private KullaniciHobisiMapper kullaniciHobisiMapper;

    @Mock
    private KullaniciHobisiService kullaniciHobisiServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKullaniciHobisiMockMvc;

    private KullaniciHobisi kullaniciHobisi;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KullaniciHobisi createEntity(EntityManager em) {
        KullaniciHobisi kullaniciHobisi = new KullaniciHobisi().seviye(DEFAULT_SEVIYE);
        return kullaniciHobisi;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KullaniciHobisi createUpdatedEntity(EntityManager em) {
        KullaniciHobisi kullaniciHobisi = new KullaniciHobisi().seviye(UPDATED_SEVIYE);
        return kullaniciHobisi;
    }

    @BeforeEach
    public void initTest() {
        kullaniciHobisi = createEntity(em);
    }

    @Test
    @Transactional
    void createKullaniciHobisi() throws Exception {
        int databaseSizeBeforeCreate = kullaniciHobisiRepository.findAll().size();
        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);
        restKullaniciHobisiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isCreated());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeCreate + 1);
        KullaniciHobisi testKullaniciHobisi = kullaniciHobisiList.get(kullaniciHobisiList.size() - 1);
        assertThat(testKullaniciHobisi.getSeviye()).isEqualTo(DEFAULT_SEVIYE);
    }

    @Test
    @Transactional
    void createKullaniciHobisiWithExistingId() throws Exception {
        // Create the KullaniciHobisi with an existing ID
        kullaniciHobisi.setId(1L);
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        int databaseSizeBeforeCreate = kullaniciHobisiRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restKullaniciHobisiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSeviyeIsRequired() throws Exception {
        int databaseSizeBeforeTest = kullaniciHobisiRepository.findAll().size();
        // set the field null
        kullaniciHobisi.setSeviye(null);

        // Create the KullaniciHobisi, which fails.
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        restKullaniciHobisiMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisis() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kullaniciHobisi.getId().intValue())))
            .andExpect(jsonPath("$.[*].seviye").value(hasItem(DEFAULT_SEVIYE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllKullaniciHobisisWithEagerRelationshipsIsEnabled() throws Exception {
        when(kullaniciHobisiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restKullaniciHobisiMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(kullaniciHobisiServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllKullaniciHobisisWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(kullaniciHobisiServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restKullaniciHobisiMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(kullaniciHobisiRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getKullaniciHobisi() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get the kullaniciHobisi
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL_ID, kullaniciHobisi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(kullaniciHobisi.getId().intValue()))
            .andExpect(jsonPath("$.seviye").value(DEFAULT_SEVIYE));
    }

    @Test
    @Transactional
    void getKullaniciHobisisByIdFiltering() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        Long id = kullaniciHobisi.getId();

        defaultKullaniciHobisiShouldBeFound("id.equals=" + id);
        defaultKullaniciHobisiShouldNotBeFound("id.notEquals=" + id);

        defaultKullaniciHobisiShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKullaniciHobisiShouldNotBeFound("id.greaterThan=" + id);

        defaultKullaniciHobisiShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKullaniciHobisiShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye equals to DEFAULT_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.equals=" + DEFAULT_SEVIYE);

        // Get all the kullaniciHobisiList where seviye equals to UPDATED_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.equals=" + UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsInShouldWork() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye in DEFAULT_SEVIYE or UPDATED_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.in=" + DEFAULT_SEVIYE + "," + UPDATED_SEVIYE);

        // Get all the kullaniciHobisiList where seviye equals to UPDATED_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.in=" + UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsNullOrNotNull() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye is not null
        defaultKullaniciHobisiShouldBeFound("seviye.specified=true");

        // Get all the kullaniciHobisiList where seviye is null
        defaultKullaniciHobisiShouldNotBeFound("seviye.specified=false");
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye is greater than or equal to DEFAULT_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.greaterThanOrEqual=" + DEFAULT_SEVIYE);

        // Get all the kullaniciHobisiList where seviye is greater than or equal to UPDATED_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.greaterThanOrEqual=" + UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye is less than or equal to DEFAULT_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.lessThanOrEqual=" + DEFAULT_SEVIYE);

        // Get all the kullaniciHobisiList where seviye is less than or equal to SMALLER_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.lessThanOrEqual=" + SMALLER_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsLessThanSomething() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye is less than DEFAULT_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.lessThan=" + DEFAULT_SEVIYE);

        // Get all the kullaniciHobisiList where seviye is less than UPDATED_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.lessThan=" + UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisBySeviyeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        // Get all the kullaniciHobisiList where seviye is greater than DEFAULT_SEVIYE
        defaultKullaniciHobisiShouldNotBeFound("seviye.greaterThan=" + DEFAULT_SEVIYE);

        // Get all the kullaniciHobisiList where seviye is greater than SMALLER_SEVIYE
        defaultKullaniciHobisiShouldBeFound("seviye.greaterThan=" + SMALLER_SEVIYE);
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisByHobiIsEqualToSomething() throws Exception {
        Hobiler hobi;
        if (TestUtil.findAll(em, Hobiler.class).isEmpty()) {
            kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);
            hobi = HobilerResourceIT.createEntity(em);
        } else {
            hobi = TestUtil.findAll(em, Hobiler.class).get(0);
        }
        em.persist(hobi);
        em.flush();
        kullaniciHobisi.setHobi(hobi);
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);
        Long hobiId = hobi.getId();

        // Get all the kullaniciHobisiList where hobi equals to hobiId
        defaultKullaniciHobisiShouldBeFound("hobiId.equals=" + hobiId);

        // Get all the kullaniciHobisiList where hobi equals to (hobiId + 1)
        defaultKullaniciHobisiShouldNotBeFound("hobiId.equals=" + (hobiId + 1));
    }

    @Test
    @Transactional
    void getAllKullaniciHobisisByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        kullaniciHobisi.setKullanici(kullanici);
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);
        Long kullaniciId = kullanici.getId();

        // Get all the kullaniciHobisiList where kullanici equals to kullaniciId
        defaultKullaniciHobisiShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the kullaniciHobisiList where kullanici equals to (kullaniciId + 1)
        defaultKullaniciHobisiShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKullaniciHobisiShouldBeFound(String filter) throws Exception {
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(kullaniciHobisi.getId().intValue())))
            .andExpect(jsonPath("$.[*].seviye").value(hasItem(DEFAULT_SEVIYE)));

        // Check, that the count call also returns 1
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKullaniciHobisiShouldNotBeFound(String filter) throws Exception {
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKullaniciHobisiMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingKullaniciHobisi() throws Exception {
        // Get the kullaniciHobisi
        restKullaniciHobisiMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingKullaniciHobisi() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();

        // Update the kullaniciHobisi
        KullaniciHobisi updatedKullaniciHobisi = kullaniciHobisiRepository.findById(kullaniciHobisi.getId()).get();
        // Disconnect from session so that the updates on updatedKullaniciHobisi are not directly saved in db
        em.detach(updatedKullaniciHobisi);
        updatedKullaniciHobisi.seviye(UPDATED_SEVIYE);
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(updatedKullaniciHobisi);

        restKullaniciHobisiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kullaniciHobisiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isOk());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
        KullaniciHobisi testKullaniciHobisi = kullaniciHobisiList.get(kullaniciHobisiList.size() - 1);
        assertThat(testKullaniciHobisi.getSeviye()).isEqualTo(UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void putNonExistingKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, kullaniciHobisiDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateKullaniciHobisiWithPatch() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();

        // Update the kullaniciHobisi using partial update
        KullaniciHobisi partialUpdatedKullaniciHobisi = new KullaniciHobisi();
        partialUpdatedKullaniciHobisi.setId(kullaniciHobisi.getId());

        restKullaniciHobisiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKullaniciHobisi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKullaniciHobisi))
            )
            .andExpect(status().isOk());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
        KullaniciHobisi testKullaniciHobisi = kullaniciHobisiList.get(kullaniciHobisiList.size() - 1);
        assertThat(testKullaniciHobisi.getSeviye()).isEqualTo(DEFAULT_SEVIYE);
    }

    @Test
    @Transactional
    void fullUpdateKullaniciHobisiWithPatch() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();

        // Update the kullaniciHobisi using partial update
        KullaniciHobisi partialUpdatedKullaniciHobisi = new KullaniciHobisi();
        partialUpdatedKullaniciHobisi.setId(kullaniciHobisi.getId());

        partialUpdatedKullaniciHobisi.seviye(UPDATED_SEVIYE);

        restKullaniciHobisiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedKullaniciHobisi.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedKullaniciHobisi))
            )
            .andExpect(status().isOk());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
        KullaniciHobisi testKullaniciHobisi = kullaniciHobisiList.get(kullaniciHobisiList.size() - 1);
        assertThat(testKullaniciHobisi.getSeviye()).isEqualTo(UPDATED_SEVIYE);
    }

    @Test
    @Transactional
    void patchNonExistingKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, kullaniciHobisiDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamKullaniciHobisi() throws Exception {
        int databaseSizeBeforeUpdate = kullaniciHobisiRepository.findAll().size();
        kullaniciHobisi.setId(count.incrementAndGet());

        // Create the KullaniciHobisi
        KullaniciHobisiDTO kullaniciHobisiDTO = kullaniciHobisiMapper.toDto(kullaniciHobisi);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restKullaniciHobisiMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(kullaniciHobisiDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the KullaniciHobisi in the database
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteKullaniciHobisi() throws Exception {
        // Initialize the database
        kullaniciHobisiRepository.saveAndFlush(kullaniciHobisi);

        int databaseSizeBeforeDelete = kullaniciHobisiRepository.findAll().size();

        // Delete the kullaniciHobisi
        restKullaniciHobisiMockMvc
            .perform(delete(ENTITY_API_URL_ID, kullaniciHobisi.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KullaniciHobisi> kullaniciHobisiList = kullaniciHobisiRepository.findAll();
        assertThat(kullaniciHobisiList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
