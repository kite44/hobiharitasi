package com.hobiharitasi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hobiharitasi.IntegrationTest;
import com.hobiharitasi.domain.Grup;
import com.hobiharitasi.domain.Kategori;
import com.hobiharitasi.domain.Kullanici;
import com.hobiharitasi.repository.GrupRepository;
import com.hobiharitasi.service.GrupService;
import com.hobiharitasi.service.criteria.GrupCriteria;
import com.hobiharitasi.service.dto.GrupDTO;
import com.hobiharitasi.service.mapper.GrupMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link GrupResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class GrupResourceIT {

    private static final String DEFAULT_GRUP_ADI = "AAAAAAAAAA";
    private static final String UPDATED_GRUP_ADI = "BBBBBBBBBB";

    private static final String DEFAULT_ACIKLAMA = "AAAAAAAAAA";
    private static final String UPDATED_ACIKLAMA = "BBBBBBBBBB";

    private static final String DEFAULT_GRUP_KATEGORISI = "AAAAAAAAAA";
    private static final String UPDATED_GRUP_KATEGORISI = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/grups";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private GrupRepository grupRepository;

    @Mock
    private GrupRepository grupRepositoryMock;

    @Autowired
    private GrupMapper grupMapper;

    @Mock
    private GrupService grupServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGrupMockMvc;

    private Grup grup;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Grup createEntity(EntityManager em) {
        Grup grup = new Grup().grupAdi(DEFAULT_GRUP_ADI).aciklama(DEFAULT_ACIKLAMA).grupKategorisi(DEFAULT_GRUP_KATEGORISI);
        return grup;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Grup createUpdatedEntity(EntityManager em) {
        Grup grup = new Grup().grupAdi(UPDATED_GRUP_ADI).aciklama(UPDATED_ACIKLAMA).grupKategorisi(UPDATED_GRUP_KATEGORISI);
        return grup;
    }

    @BeforeEach
    public void initTest() {
        grup = createEntity(em);
    }

    @Test
    @Transactional
    void createGrup() throws Exception {
        int databaseSizeBeforeCreate = grupRepository.findAll().size();
        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);
        restGrupMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isCreated());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeCreate + 1);
        Grup testGrup = grupList.get(grupList.size() - 1);
        assertThat(testGrup.getGrupAdi()).isEqualTo(DEFAULT_GRUP_ADI);
        assertThat(testGrup.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testGrup.getGrupKategorisi()).isEqualTo(DEFAULT_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void createGrupWithExistingId() throws Exception {
        // Create the Grup with an existing ID
        grup.setId(1L);
        GrupDTO grupDTO = grupMapper.toDto(grup);

        int databaseSizeBeforeCreate = grupRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restGrupMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkGrupAdiIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupRepository.findAll().size();
        // set the field null
        grup.setGrupAdi(null);

        // Create the Grup, which fails.
        GrupDTO grupDTO = grupMapper.toDto(grup);

        restGrupMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isBadRequest());

        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAciklamaIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupRepository.findAll().size();
        // set the field null
        grup.setAciklama(null);

        // Create the Grup, which fails.
        GrupDTO grupDTO = grupMapper.toDto(grup);

        restGrupMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isBadRequest());

        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkGrupKategorisiIsRequired() throws Exception {
        int databaseSizeBeforeTest = grupRepository.findAll().size();
        // set the field null
        grup.setGrupKategorisi(null);

        // Create the Grup, which fails.
        GrupDTO grupDTO = grupMapper.toDto(grup);

        restGrupMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isBadRequest());

        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGrups() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList
        restGrupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grup.getId().intValue())))
            .andExpect(jsonPath("$.[*].grupAdi").value(hasItem(DEFAULT_GRUP_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].grupKategorisi").value(hasItem(DEFAULT_GRUP_KATEGORISI)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllGrupsWithEagerRelationshipsIsEnabled() throws Exception {
        when(grupServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restGrupMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(grupServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllGrupsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(grupServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restGrupMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(grupRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getGrup() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get the grup
        restGrupMockMvc
            .perform(get(ENTITY_API_URL_ID, grup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(grup.getId().intValue()))
            .andExpect(jsonPath("$.grupAdi").value(DEFAULT_GRUP_ADI))
            .andExpect(jsonPath("$.aciklama").value(DEFAULT_ACIKLAMA))
            .andExpect(jsonPath("$.grupKategorisi").value(DEFAULT_GRUP_KATEGORISI));
    }

    @Test
    @Transactional
    void getGrupsByIdFiltering() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        Long id = grup.getId();

        defaultGrupShouldBeFound("id.equals=" + id);
        defaultGrupShouldNotBeFound("id.notEquals=" + id);

        defaultGrupShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultGrupShouldNotBeFound("id.greaterThan=" + id);

        defaultGrupShouldBeFound("id.lessThanOrEqual=" + id);
        defaultGrupShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupAdiIsEqualToSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupAdi equals to DEFAULT_GRUP_ADI
        defaultGrupShouldBeFound("grupAdi.equals=" + DEFAULT_GRUP_ADI);

        // Get all the grupList where grupAdi equals to UPDATED_GRUP_ADI
        defaultGrupShouldNotBeFound("grupAdi.equals=" + UPDATED_GRUP_ADI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupAdiIsInShouldWork() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupAdi in DEFAULT_GRUP_ADI or UPDATED_GRUP_ADI
        defaultGrupShouldBeFound("grupAdi.in=" + DEFAULT_GRUP_ADI + "," + UPDATED_GRUP_ADI);

        // Get all the grupList where grupAdi equals to UPDATED_GRUP_ADI
        defaultGrupShouldNotBeFound("grupAdi.in=" + UPDATED_GRUP_ADI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupAdiIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupAdi is not null
        defaultGrupShouldBeFound("grupAdi.specified=true");

        // Get all the grupList where grupAdi is null
        defaultGrupShouldNotBeFound("grupAdi.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupsByGrupAdiContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupAdi contains DEFAULT_GRUP_ADI
        defaultGrupShouldBeFound("grupAdi.contains=" + DEFAULT_GRUP_ADI);

        // Get all the grupList where grupAdi contains UPDATED_GRUP_ADI
        defaultGrupShouldNotBeFound("grupAdi.contains=" + UPDATED_GRUP_ADI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupAdiNotContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupAdi does not contain DEFAULT_GRUP_ADI
        defaultGrupShouldNotBeFound("grupAdi.doesNotContain=" + DEFAULT_GRUP_ADI);

        // Get all the grupList where grupAdi does not contain UPDATED_GRUP_ADI
        defaultGrupShouldBeFound("grupAdi.doesNotContain=" + UPDATED_GRUP_ADI);
    }

    @Test
    @Transactional
    void getAllGrupsByAciklamaIsEqualToSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where aciklama equals to DEFAULT_ACIKLAMA
        defaultGrupShouldBeFound("aciklama.equals=" + DEFAULT_ACIKLAMA);

        // Get all the grupList where aciklama equals to UPDATED_ACIKLAMA
        defaultGrupShouldNotBeFound("aciklama.equals=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupsByAciklamaIsInShouldWork() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where aciklama in DEFAULT_ACIKLAMA or UPDATED_ACIKLAMA
        defaultGrupShouldBeFound("aciklama.in=" + DEFAULT_ACIKLAMA + "," + UPDATED_ACIKLAMA);

        // Get all the grupList where aciklama equals to UPDATED_ACIKLAMA
        defaultGrupShouldNotBeFound("aciklama.in=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupsByAciklamaIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where aciklama is not null
        defaultGrupShouldBeFound("aciklama.specified=true");

        // Get all the grupList where aciklama is null
        defaultGrupShouldNotBeFound("aciklama.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupsByAciklamaContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where aciklama contains DEFAULT_ACIKLAMA
        defaultGrupShouldBeFound("aciklama.contains=" + DEFAULT_ACIKLAMA);

        // Get all the grupList where aciklama contains UPDATED_ACIKLAMA
        defaultGrupShouldNotBeFound("aciklama.contains=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupsByAciklamaNotContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where aciklama does not contain DEFAULT_ACIKLAMA
        defaultGrupShouldNotBeFound("aciklama.doesNotContain=" + DEFAULT_ACIKLAMA);

        // Get all the grupList where aciklama does not contain UPDATED_ACIKLAMA
        defaultGrupShouldBeFound("aciklama.doesNotContain=" + UPDATED_ACIKLAMA);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupKategorisiIsEqualToSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupKategorisi equals to DEFAULT_GRUP_KATEGORISI
        defaultGrupShouldBeFound("grupKategorisi.equals=" + DEFAULT_GRUP_KATEGORISI);

        // Get all the grupList where grupKategorisi equals to UPDATED_GRUP_KATEGORISI
        defaultGrupShouldNotBeFound("grupKategorisi.equals=" + UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupKategorisiIsInShouldWork() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupKategorisi in DEFAULT_GRUP_KATEGORISI or UPDATED_GRUP_KATEGORISI
        defaultGrupShouldBeFound("grupKategorisi.in=" + DEFAULT_GRUP_KATEGORISI + "," + UPDATED_GRUP_KATEGORISI);

        // Get all the grupList where grupKategorisi equals to UPDATED_GRUP_KATEGORISI
        defaultGrupShouldNotBeFound("grupKategorisi.in=" + UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupKategorisiIsNullOrNotNull() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupKategorisi is not null
        defaultGrupShouldBeFound("grupKategorisi.specified=true");

        // Get all the grupList where grupKategorisi is null
        defaultGrupShouldNotBeFound("grupKategorisi.specified=false");
    }

    @Test
    @Transactional
    void getAllGrupsByGrupKategorisiContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupKategorisi contains DEFAULT_GRUP_KATEGORISI
        defaultGrupShouldBeFound("grupKategorisi.contains=" + DEFAULT_GRUP_KATEGORISI);

        // Get all the grupList where grupKategorisi contains UPDATED_GRUP_KATEGORISI
        defaultGrupShouldNotBeFound("grupKategorisi.contains=" + UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllGrupsByGrupKategorisiNotContainsSomething() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        // Get all the grupList where grupKategorisi does not contain DEFAULT_GRUP_KATEGORISI
        defaultGrupShouldNotBeFound("grupKategorisi.doesNotContain=" + DEFAULT_GRUP_KATEGORISI);

        // Get all the grupList where grupKategorisi does not contain UPDATED_GRUP_KATEGORISI
        defaultGrupShouldBeFound("grupKategorisi.doesNotContain=" + UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void getAllGrupsByKategoriIsEqualToSomething() throws Exception {
        Kategori kategori;
        if (TestUtil.findAll(em, Kategori.class).isEmpty()) {
            grupRepository.saveAndFlush(grup);
            kategori = KategoriResourceIT.createEntity(em);
        } else {
            kategori = TestUtil.findAll(em, Kategori.class).get(0);
        }
        em.persist(kategori);
        em.flush();
        grup.setKategori(kategori);
        grupRepository.saveAndFlush(grup);
        Long kategoriId = kategori.getId();

        // Get all the grupList where kategori equals to kategoriId
        defaultGrupShouldBeFound("kategoriId.equals=" + kategoriId);

        // Get all the grupList where kategori equals to (kategoriId + 1)
        defaultGrupShouldNotBeFound("kategoriId.equals=" + (kategoriId + 1));
    }

    @Test
    @Transactional
    void getAllGrupsByKullaniciIsEqualToSomething() throws Exception {
        Kullanici kullanici;
        if (TestUtil.findAll(em, Kullanici.class).isEmpty()) {
            grupRepository.saveAndFlush(grup);
            kullanici = KullaniciResourceIT.createEntity(em);
        } else {
            kullanici = TestUtil.findAll(em, Kullanici.class).get(0);
        }
        em.persist(kullanici);
        em.flush();
        grup.addKullanici(kullanici);
        grupRepository.saveAndFlush(grup);
        Long kullaniciId = kullanici.getId();

        // Get all the grupList where kullanici equals to kullaniciId
        defaultGrupShouldBeFound("kullaniciId.equals=" + kullaniciId);

        // Get all the grupList where kullanici equals to (kullaniciId + 1)
        defaultGrupShouldNotBeFound("kullaniciId.equals=" + (kullaniciId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultGrupShouldBeFound(String filter) throws Exception {
        restGrupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(grup.getId().intValue())))
            .andExpect(jsonPath("$.[*].grupAdi").value(hasItem(DEFAULT_GRUP_ADI)))
            .andExpect(jsonPath("$.[*].aciklama").value(hasItem(DEFAULT_ACIKLAMA)))
            .andExpect(jsonPath("$.[*].grupKategorisi").value(hasItem(DEFAULT_GRUP_KATEGORISI)));

        // Check, that the count call also returns 1
        restGrupMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultGrupShouldNotBeFound(String filter) throws Exception {
        restGrupMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restGrupMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingGrup() throws Exception {
        // Get the grup
        restGrupMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingGrup() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        int databaseSizeBeforeUpdate = grupRepository.findAll().size();

        // Update the grup
        Grup updatedGrup = grupRepository.findById(grup.getId()).get();
        // Disconnect from session so that the updates on updatedGrup are not directly saved in db
        em.detach(updatedGrup);
        updatedGrup.grupAdi(UPDATED_GRUP_ADI).aciklama(UPDATED_ACIKLAMA).grupKategorisi(UPDATED_GRUP_KATEGORISI);
        GrupDTO grupDTO = grupMapper.toDto(updatedGrup);

        restGrupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, grupDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupDTO))
            )
            .andExpect(status().isOk());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
        Grup testGrup = grupList.get(grupList.size() - 1);
        assertThat(testGrup.getGrupAdi()).isEqualTo(UPDATED_GRUP_ADI);
        assertThat(testGrup.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testGrup.getGrupKategorisi()).isEqualTo(UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void putNonExistingGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, grupDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(grupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateGrupWithPatch() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        int databaseSizeBeforeUpdate = grupRepository.findAll().size();

        // Update the grup using partial update
        Grup partialUpdatedGrup = new Grup();
        partialUpdatedGrup.setId(grup.getId());

        restGrupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGrup.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGrup))
            )
            .andExpect(status().isOk());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
        Grup testGrup = grupList.get(grupList.size() - 1);
        assertThat(testGrup.getGrupAdi()).isEqualTo(DEFAULT_GRUP_ADI);
        assertThat(testGrup.getAciklama()).isEqualTo(DEFAULT_ACIKLAMA);
        assertThat(testGrup.getGrupKategorisi()).isEqualTo(DEFAULT_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void fullUpdateGrupWithPatch() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        int databaseSizeBeforeUpdate = grupRepository.findAll().size();

        // Update the grup using partial update
        Grup partialUpdatedGrup = new Grup();
        partialUpdatedGrup.setId(grup.getId());

        partialUpdatedGrup.grupAdi(UPDATED_GRUP_ADI).aciklama(UPDATED_ACIKLAMA).grupKategorisi(UPDATED_GRUP_KATEGORISI);

        restGrupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedGrup.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedGrup))
            )
            .andExpect(status().isOk());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
        Grup testGrup = grupList.get(grupList.size() - 1);
        assertThat(testGrup.getGrupAdi()).isEqualTo(UPDATED_GRUP_ADI);
        assertThat(testGrup.getAciklama()).isEqualTo(UPDATED_ACIKLAMA);
        assertThat(testGrup.getGrupKategorisi()).isEqualTo(UPDATED_GRUP_KATEGORISI);
    }

    @Test
    @Transactional
    void patchNonExistingGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, grupDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(grupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(grupDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamGrup() throws Exception {
        int databaseSizeBeforeUpdate = grupRepository.findAll().size();
        grup.setId(count.incrementAndGet());

        // Create the Grup
        GrupDTO grupDTO = grupMapper.toDto(grup);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restGrupMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(grupDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Grup in the database
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGrup() throws Exception {
        // Initialize the database
        grupRepository.saveAndFlush(grup);

        int databaseSizeBeforeDelete = grupRepository.findAll().size();

        // Delete the grup
        restGrupMockMvc
            .perform(delete(ENTITY_API_URL_ID, grup.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Grup> grupList = grupRepository.findAll();
        assertThat(grupList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
