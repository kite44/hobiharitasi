package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GrupEtkinligiDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GrupEtkinligiDTO.class);
        GrupEtkinligiDTO grupEtkinligiDTO1 = new GrupEtkinligiDTO();
        grupEtkinligiDTO1.setId(1L);
        GrupEtkinligiDTO grupEtkinligiDTO2 = new GrupEtkinligiDTO();
        assertThat(grupEtkinligiDTO1).isNotEqualTo(grupEtkinligiDTO2);
        grupEtkinligiDTO2.setId(grupEtkinligiDTO1.getId());
        assertThat(grupEtkinligiDTO1).isEqualTo(grupEtkinligiDTO2);
        grupEtkinligiDTO2.setId(2L);
        assertThat(grupEtkinligiDTO1).isNotEqualTo(grupEtkinligiDTO2);
        grupEtkinligiDTO1.setId(null);
        assertThat(grupEtkinligiDTO1).isNotEqualTo(grupEtkinligiDTO2);
    }
}
