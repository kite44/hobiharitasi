package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EtkinlikDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EtkinlikDTO.class);
        EtkinlikDTO etkinlikDTO1 = new EtkinlikDTO();
        etkinlikDTO1.setId(1L);
        EtkinlikDTO etkinlikDTO2 = new EtkinlikDTO();
        assertThat(etkinlikDTO1).isNotEqualTo(etkinlikDTO2);
        etkinlikDTO2.setId(etkinlikDTO1.getId());
        assertThat(etkinlikDTO1).isEqualTo(etkinlikDTO2);
        etkinlikDTO2.setId(2L);
        assertThat(etkinlikDTO1).isNotEqualTo(etkinlikDTO2);
        etkinlikDTO1.setId(null);
        assertThat(etkinlikDTO1).isNotEqualTo(etkinlikDTO2);
    }
}
