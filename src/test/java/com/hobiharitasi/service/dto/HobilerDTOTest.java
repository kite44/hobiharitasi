package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HobilerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HobilerDTO.class);
        HobilerDTO hobilerDTO1 = new HobilerDTO();
        hobilerDTO1.setId(1L);
        HobilerDTO hobilerDTO2 = new HobilerDTO();
        assertThat(hobilerDTO1).isNotEqualTo(hobilerDTO2);
        hobilerDTO2.setId(hobilerDTO1.getId());
        assertThat(hobilerDTO1).isEqualTo(hobilerDTO2);
        hobilerDTO2.setId(2L);
        assertThat(hobilerDTO1).isNotEqualTo(hobilerDTO2);
        hobilerDTO1.setId(null);
        assertThat(hobilerDTO1).isNotEqualTo(hobilerDTO2);
    }
}
