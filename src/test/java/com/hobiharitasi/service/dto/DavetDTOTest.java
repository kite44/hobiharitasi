package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DavetDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DavetDTO.class);
        DavetDTO davetDTO1 = new DavetDTO();
        davetDTO1.setId(1L);
        DavetDTO davetDTO2 = new DavetDTO();
        assertThat(davetDTO1).isNotEqualTo(davetDTO2);
        davetDTO2.setId(davetDTO1.getId());
        assertThat(davetDTO1).isEqualTo(davetDTO2);
        davetDTO2.setId(2L);
        assertThat(davetDTO1).isNotEqualTo(davetDTO2);
        davetDTO1.setId(null);
        assertThat(davetDTO1).isNotEqualTo(davetDTO2);
    }
}
