package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KullaniciHobisiDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KullaniciHobisiDTO.class);
        KullaniciHobisiDTO kullaniciHobisiDTO1 = new KullaniciHobisiDTO();
        kullaniciHobisiDTO1.setId(1L);
        KullaniciHobisiDTO kullaniciHobisiDTO2 = new KullaniciHobisiDTO();
        assertThat(kullaniciHobisiDTO1).isNotEqualTo(kullaniciHobisiDTO2);
        kullaniciHobisiDTO2.setId(kullaniciHobisiDTO1.getId());
        assertThat(kullaniciHobisiDTO1).isEqualTo(kullaniciHobisiDTO2);
        kullaniciHobisiDTO2.setId(2L);
        assertThat(kullaniciHobisiDTO1).isNotEqualTo(kullaniciHobisiDTO2);
        kullaniciHobisiDTO1.setId(null);
        assertThat(kullaniciHobisiDTO1).isNotEqualTo(kullaniciHobisiDTO2);
    }
}
