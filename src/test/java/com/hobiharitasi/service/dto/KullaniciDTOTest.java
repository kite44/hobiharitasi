package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KullaniciDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KullaniciDTO.class);
        KullaniciDTO kullaniciDTO1 = new KullaniciDTO();
        kullaniciDTO1.setId(1L);
        KullaniciDTO kullaniciDTO2 = new KullaniciDTO();
        assertThat(kullaniciDTO1).isNotEqualTo(kullaniciDTO2);
        kullaniciDTO2.setId(kullaniciDTO1.getId());
        assertThat(kullaniciDTO1).isEqualTo(kullaniciDTO2);
        kullaniciDTO2.setId(2L);
        assertThat(kullaniciDTO1).isNotEqualTo(kullaniciDTO2);
        kullaniciDTO1.setId(null);
        assertThat(kullaniciDTO1).isNotEqualTo(kullaniciDTO2);
    }
}
