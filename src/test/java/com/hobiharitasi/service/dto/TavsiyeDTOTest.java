package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TavsiyeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TavsiyeDTO.class);
        TavsiyeDTO tavsiyeDTO1 = new TavsiyeDTO();
        tavsiyeDTO1.setId(1L);
        TavsiyeDTO tavsiyeDTO2 = new TavsiyeDTO();
        assertThat(tavsiyeDTO1).isNotEqualTo(tavsiyeDTO2);
        tavsiyeDTO2.setId(tavsiyeDTO1.getId());
        assertThat(tavsiyeDTO1).isEqualTo(tavsiyeDTO2);
        tavsiyeDTO2.setId(2L);
        assertThat(tavsiyeDTO1).isNotEqualTo(tavsiyeDTO2);
        tavsiyeDTO1.setId(null);
        assertThat(tavsiyeDTO1).isNotEqualTo(tavsiyeDTO2);
    }
}
