package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class GrupDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GrupDTO.class);
        GrupDTO grupDTO1 = new GrupDTO();
        grupDTO1.setId(1L);
        GrupDTO grupDTO2 = new GrupDTO();
        assertThat(grupDTO1).isNotEqualTo(grupDTO2);
        grupDTO2.setId(grupDTO1.getId());
        assertThat(grupDTO1).isEqualTo(grupDTO2);
        grupDTO2.setId(2L);
        assertThat(grupDTO1).isNotEqualTo(grupDTO2);
        grupDTO1.setId(null);
        assertThat(grupDTO1).isNotEqualTo(grupDTO2);
    }
}
