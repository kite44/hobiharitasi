package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class YorumDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(YorumDTO.class);
        YorumDTO yorumDTO1 = new YorumDTO();
        yorumDTO1.setId(1L);
        YorumDTO yorumDTO2 = new YorumDTO();
        assertThat(yorumDTO1).isNotEqualTo(yorumDTO2);
        yorumDTO2.setId(yorumDTO1.getId());
        assertThat(yorumDTO1).isEqualTo(yorumDTO2);
        yorumDTO2.setId(2L);
        assertThat(yorumDTO1).isNotEqualTo(yorumDTO2);
        yorumDTO1.setId(null);
        assertThat(yorumDTO1).isNotEqualTo(yorumDTO2);
    }
}
