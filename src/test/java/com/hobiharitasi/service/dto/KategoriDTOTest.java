package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class KategoriDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KategoriDTO.class);
        KategoriDTO kategoriDTO1 = new KategoriDTO();
        kategoriDTO1.setId(1L);
        KategoriDTO kategoriDTO2 = new KategoriDTO();
        assertThat(kategoriDTO1).isNotEqualTo(kategoriDTO2);
        kategoriDTO2.setId(kategoriDTO1.getId());
        assertThat(kategoriDTO1).isEqualTo(kategoriDTO2);
        kategoriDTO2.setId(2L);
        assertThat(kategoriDTO1).isNotEqualTo(kategoriDTO2);
        kategoriDTO1.setId(null);
        assertThat(kategoriDTO1).isNotEqualTo(kategoriDTO2);
    }
}
