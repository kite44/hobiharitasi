package com.hobiharitasi.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.hobiharitasi.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TakvimDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TakvimDTO.class);
        TakvimDTO takvimDTO1 = new TakvimDTO();
        takvimDTO1.setId(1L);
        TakvimDTO takvimDTO2 = new TakvimDTO();
        assertThat(takvimDTO1).isNotEqualTo(takvimDTO2);
        takvimDTO2.setId(takvimDTO1.getId());
        assertThat(takvimDTO1).isEqualTo(takvimDTO2);
        takvimDTO2.setId(2L);
        assertThat(takvimDTO1).isNotEqualTo(takvimDTO2);
        takvimDTO1.setId(null);
        assertThat(takvimDTO1).isNotEqualTo(takvimDTO2);
    }
}
